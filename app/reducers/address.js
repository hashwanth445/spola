import {
    ADDRESS_GET_CHECK_SUCCESS,
    ADDRESS_POST_CHECK_SUCCESS,
    ADDRESS_UPDATE_CHECK_SUCCESS,
    ADDRESS_DELETE_CHECK_SUCCESS,
    ADDRESS_SET_DATA,
} from '../actions/actionTypes';

const initialState = {
    addressGetCheckSuccess: false,
    addressData: [],
    addressPostCheckSuccess: false,
    addressUpdateCheckSuccess: false,
    addressDeleteCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADDRESS_GET_CHECK_SUCCESS:
            return {
                ...state,
                addressGetCheckSuccess: action.payload,
            };
        case ADDRESS_POST_CHECK_SUCCESS:
            return {
                ...state,
                addressPostCheckSuccess: action.payload,
            };
        case ADDRESS_UPDATE_CHECK_SUCCESS:
            return {
                ...state,
                addressUpdateCheckSuccess: action.payload,
            };
        case ADDRESS_DELETE_CHECK_SUCCESS:
            return {
                ...state,
                addressDeleteCheckSuccess: action.payload,
            };

        case ADDRESS_SET_DATA:
            return {
                ...state,
                addressData: action.payload,
            };
        default:
            return state;
    }
};

export default reducer;
