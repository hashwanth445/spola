import {
    RAZORPAYPAYMENTS_GET_CHECK_SUCCESS,
    RAZORPAYPAYMENTS_POST_CHECK_SUCCESS,
    RAZORPAYPAYMENTS_UPDATE_CHECK_SUCCESS,
    RAZORPAYPAYMENTS_DELETE_CHECK_SUCCESS,
    RAZORPAYPAYMENTS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   razorpaypaymentsGetCheckSuccess: false,
   razorpaypaymentsData: [],
   razorpaypaymentsPostCheckSuccess: false,
   razorpaypaymentsUpdateCheckSuccess: false,
   razorpaypaymentsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case RAZORPAYPAYMENTS_GET_CHECK_SUCCESS:
        return {
          ...state,
         razorpaypaymentsGetCheckSuccess: action.payload,
        };
        case RAZORPAYPAYMENTS_POST_CHECK_SUCCESS:
        return {
          ...state,
         razorpaypaymentsPostCheckSuccess: action.payload,
        };
        case RAZORPAYPAYMENTS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         razorpaypaymentsUpdateCheckSuccess: action.payload,
        };
        case RAZORPAYPAYMENTS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             razorpaypaymentsDeleteCheckSuccess: action.payload,
            };
        
      case RAZORPAYPAYMENTS_SET_DATA:
        return {
          ...state,
         razorpaypaymentsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  