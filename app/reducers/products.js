import {
  PRODUCTS_GET_CHECK_SUCCESS,
  PRODUCTS_POST_CHECK_SUCCESS,
  PRODUCTS_UPDATE_CHECK_SUCCESS,
  PRODUCTS_DELETE_CHECK_SUCCESS,
  PRODUCTS_SET_DATA,
} from '../actions/actionTypes';

const initialState = {
 productsGetCheckSuccess: false,
 productsData: [],
 productsPostCheckSuccess: false,
 productsUpdateCheckSuccess: false,
 productsDeleteCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCTS_GET_CHECK_SUCCESS:
      return {
        ...state,
       productsGetCheckSuccess: action.payload,
      };
      case PRODUCTS_POST_CHECK_SUCCESS:
      return {
        ...state,
       productsPostCheckSuccess: action.payload,
      };
      case PRODUCTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
       productsUpdateCheckSuccess: action.payload,
      };
      case PRODUCTS_DELETE_CHECK_SUCCESS:
          return {
            ...state,
           productsDeleteCheckSuccess: action.payload,
          };
      
    case PRODUCTS_SET_DATA:
      return {
        ...state,
       productsData: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
