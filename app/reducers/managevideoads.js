import {
    MANAGEVIDEOADS_GET_CHECK_SUCCESS,
    MANAGEVIDEOADS_POST_CHECK_SUCCESS,
    MANAGEVIDEOADS_UPDATE_CHECK_SUCCESS,
    MANAGEVIDEOADS_DELETE_CHECK_SUCCESS,
    MANAGEVIDEOADS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   managevideoadsGetCheckSuccess: false,
   managevideoadsData: [],
   managevideoadsPostCheckSuccess: false,
   managevideoadsUpdateCheckSuccess: false,
   managevideoadsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case MANAGEVIDEOADS_GET_CHECK_SUCCESS:
        return {
          ...state,
         managevideoadsGetCheckSuccess: action.payload,
        };
        case MANAGEVIDEOADS_POST_CHECK_SUCCESS:
        return {
          ...state,
         managevideoadsPostCheckSuccess: action.payload,
        };
        case MANAGEVIDEOADS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         managevideoadsUpdateCheckSuccess: action.payload,
        };
        case MANAGEVIDEOADS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             managevideoadsDeleteCheckSuccess: action.payload,
            };
        
      case MANAGEVIDEOADS_SET_DATA:
        return {
          ...state,
         managevideoadsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  