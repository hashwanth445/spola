import {
    USERSETTINGS_GET_CHECK_SUCCESS,
    USERSETTINGS_POST_CHECK_SUCCESS,
    USERSETTINGS_UPDATE_CHECK_SUCCESS,
    USERSETTINGS_DELETE_CHECK_SUCCESS,
    USERSETTINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   usersettingsGetCheckSuccess: false,
   usersettingsData: [],
   usersettingsPostCheckSuccess: false,
   usersettingsUpdateCheckSuccess: false,
   usersettingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case USERSETTINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
         usersettingsGetCheckSuccess: action.payload,
        };
        case USERSETTINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
         usersettingsPostCheckSuccess: action.payload,
        };
        case USERSETTINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         usersettingsUpdateCheckSuccess: action.payload,
        };
        case USERSETTINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             usersettingsDeleteCheckSuccess: action.payload,
            };
        
      case USERSETTINGS_SET_DATA:
        return {
          ...state,
         usersettingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  