import {
    FEEDS_GET_CHECK_SUCCESS,
    FEEDS_POST_CHECK_SUCCESS,
    FEEDS_UPDATE_CHECK_SUCCESS,
    FEEDS_DELETE_CHECK_SUCCESS,
    FEEDS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   feedsGetCheckSuccess: false,
   feedsData: [],
   feedsPostCheckSuccess: false,
   feedsUpdateCheckSuccess: false,
   feedsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case FEEDS_GET_CHECK_SUCCESS:
        return {
          ...state,
         feedsGetCheckSuccess: action.payload,
        };
        case FEEDS_POST_CHECK_SUCCESS:
        return {
          ...state,
         feedsPostCheckSuccess: action.payload,
        };
        case FEEDS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         feedsUpdateCheckSuccess: action.payload,
        };
        case FEEDS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             feedsDeleteCheckSuccess: action.payload,
            };
        
      case FEEDS_SET_DATA:
        return {
          ...state,
         feedsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  