import {
    MAINSETTINGS_GET_CHECK_SUCCESS,
    MAINSETTINGS_POST_CHECK_SUCCESS,
    MAINSETTINGS_UPDATE_CHECK_SUCCESS,
    MAINSETTINGS_DELETE_CHECK_SUCCESS,
    MAINSETTINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
    mainsettingsGetCheckSuccess: false,
    mainsettingsData: [],
    mainsettingsPostCheckSuccess: false,
    mainsettingsUpdateCheckSuccess: false,
    mainsettingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case MAINSETTINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
          mainsettingsGetCheckSuccess: action.payload,
        };
        case MAINSETTINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
          mainsettingsPostCheckSuccess: action.payload,
        };
        case MAINSETTINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
          mainsettingsUpdateCheckSuccess: action.payload,
        };
        case MAINSETTINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
              mainsettingsDeleteCheckSuccess: action.payload,
            };
        
      case MAINSETTINGS_SET_DATA:
        return {
          ...state,
          mainsettingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  