import {
    MANAGEMOVIES_GET_CHECK_SUCCESS,
    MANAGEMOVIES_POST_CHECK_SUCCESS,
    MANAGEMOVIES_UPDATE_CHECK_SUCCESS,
    MANAGEMOVIES_DELETE_CHECK_SUCCESS,
    MANAGEMOVIES_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   managemoviesGetCheckSuccess: false,
   managemoviesData: [],
   managemoviesPostCheckSuccess: false,
   managemoviesUpdateCheckSuccess: false,
   managemoviesDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case MANAGEMOVIES_GET_CHECK_SUCCESS:
        return {
          ...state,
         managemoviesGetCheckSuccess: action.payload,
        };
        case MANAGEMOVIES_POST_CHECK_SUCCESS:
        return {
          ...state,
         managemoviesPostCheckSuccess: action.payload,
        };
        case MANAGEMOVIES_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         managemoviesUpdateCheckSuccess: action.payload,
        };
        case MANAGEMOVIES_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             managemoviesDeleteCheckSuccess: action.payload,
            };
        
      case MANAGEMOVIES_SET_DATA:
        return {
          ...state,
         managemoviesData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  