import {
    UPLOADSETTINGS_GET_CHECK_SUCCESS,
    UPLOADSETTINGS_POST_CHECK_SUCCESS,
    UPLOADSETTINGS_UPDATE_CHECK_SUCCESS,
    UPLOADSETTINGS_DELETE_CHECK_SUCCESS,
    UPLOADSETTINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   uploadsettingsGetCheckSuccess: false,
   uploadsettingsData: [],
   uploadsettingsPostCheckSuccess: false,
   uploadsettingsUpdateCheckSuccess: false,
   uploadsettingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case UPLOADSETTINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
         uploadsettingsGetCheckSuccess: action.payload,
        };
        case UPLOADSETTINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
         uploadsettingsPostCheckSuccess: action.payload,
        };
        case UPLOADSETTINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         uploadsettingsUpdateCheckSuccess: action.payload,
        };
        case UPLOADSETTINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             uploadsettingsDeleteCheckSuccess: action.payload,
            };
        
      case UPLOADSETTINGS_SET_DATA:
        return {
          ...state,
         uploadsettingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  