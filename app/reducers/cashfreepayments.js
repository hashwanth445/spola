import {
    CASHFREEPAYMENTS_GET_CHECK_SUCCESS,
    CASHFREEPAYMENTS_POST_CHECK_SUCCESS,
    CASHFREEPAYMENTS_UPDATE_CHECK_SUCCESS,
    CASHFREEPAYMENTS_DELETE_CHECK_SUCCESS,
    CASHFREEPAYMENTS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   cashfreepaymentsGetCheckSuccess: false,
   cashfreepaymentsData: [],
   cashfreepaymentsPostCheckSuccess: false,
   cashfreepaymentsUpdateCheckSuccess: false,
   cashfreepaymentsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case CASHFREEPAYMENTS_GET_CHECK_SUCCESS:
        return {
          ...state,
         cashfreepaymentsGetCheckSuccess: action.payload,
        };
        case CASHFREEPAYMENTS_POST_CHECK_SUCCESS:
        return {
          ...state,
         cashfreepaymentsPostCheckSuccess: action.payload,
        };
        case CASHFREEPAYMENTS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         cashfreepaymentsUpdateCheckSuccess: action.payload,
        };
        case CASHFREEPAYMENTS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             cashfreepaymentsDeleteCheckSuccess: action.payload,
            };
        
      case CASHFREEPAYMENTS_SET_DATA:
        return {
          ...state,
         cashfreepaymentsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  