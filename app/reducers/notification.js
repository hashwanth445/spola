import {
    NOTIFICATION_GET_CHECK_SUCCESS,
    NOTIFICATION_POST_CHECK_SUCCESS,
    NOTIFICATION_UPDATE_CHECK_SUCCESS,
    NOTIFICATION_DELETE_CHECK_SUCCESS,
    NOTIFICATION_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   notificationGetCheckSuccess: false,
   notificationData: [],
   notificationPostCheckSuccess: false,
   notificationUpdateCheckSuccess: false,
   notificationDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case NOTIFICATION_GET_CHECK_SUCCESS:
        return {
          ...state,
         notificationGetCheckSuccess: action.payload,
        };
        case NOTIFICATION_POST_CHECK_SUCCESS:
        return {
          ...state,
         notificationPostCheckSuccess: action.payload,
        };
        case NOTIFICATION_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         notificationUpdateCheckSuccess: action.payload,
        };
        case NOTIFICATION_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             notificationDeleteCheckSuccess: action.payload,
            };
        
      case NOTIFICATION_SET_DATA:
        return {
          ...state,
         notificationData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  