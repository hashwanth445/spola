import {
    SHORTS_GET_CHECK_SUCCESS,
    SHORTS_POST_CHECK_SUCCESS,
    SHORTS_UPDATE_CHECK_SUCCESS,
    SHORTS_DELETE_CHECK_SUCCESS,
    SHORTS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   shortsGetCheckSuccess: false,
   shortsData: [],
   shortsPostCheckSuccess: false,
   shortsUpdateCheckSuccess: false,
   shortsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case SHORTS_GET_CHECK_SUCCESS:
        return {
          ...state,
         shortsGetCheckSuccess: action.payload,
        };
        case SHORTS_POST_CHECK_SUCCESS:
        return {
          ...state,
         shortsPostCheckSuccess: action.payload,
        };
        case SHORTS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         shortsUpdateCheckSuccess: action.payload,
        };
        case SHORTS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             shortsDeleteCheckSuccess: action.payload,
            };
        
      case SHORTS_SET_DATA:
        return {
          ...state,
         shortsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  