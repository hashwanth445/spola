import {LOGOUT} from '../actions/actionTypes';
let initialState = {
  accessToken: null,
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGOUT:
      return {
        accessToken:null
      };
    default:
      return state;
  }
};

export default reducer;
