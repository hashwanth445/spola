import {
  DISLIKE_GET_CHECK_SUCCESS,
  DISLIKE_POST_CHECK_SUCCESS,
  DISLIKE_UPDATE_CHECK_SUCCESS,
  DISLIKE_DELETE_CHECK_SUCCESS,
  DISLIKE_SET_DATA,
  DISLIKE_SET_DATA_BY_USER,
  DISLIKE_GET_CHECK_SUCCESS_BY_USER


} from '../actions/actionTypes';

const initialState = {
  dislikeGetCheckSuccess: false,
  dislikeData: [],
  dislikePostCheckSuccess: false,
  dislikeUpdateCheckSuccess: false,
  dislikeDeleteCheckSuccess: false,
  dislikeGetCheckSuccessByUser: false,
  dislikeDataByUser: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case DISLIKE_GET_CHECK_SUCCESS:
      return {
        ...state,
        dislikeGetCheckSuccess: action.payload,
      };
    case DISLIKE_GET_CHECK_SUCCESS_BY_USER:
      return {
        ...state,
        dislikeGetCheckSuccessByUser: action.payload,
      };
    case DISLIKE_POST_CHECK_SUCCESS:
      return {
        ...state,
        dislikePostCheckSuccess: action.payload,
      };
    case DISLIKE_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        dislikeUpdateCheckSuccess: action.payload,
      };
    case DISLIKE_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        dislikeDeleteCheckSuccess: action.payload,
      };

    case DISLIKE_SET_DATA:
      return {
        ...state,
        dislikeData: action.payload,
      };
    case DISLIKE_SET_DATA_BY_USER:
      return {
        ...state,
        dislikeDataByUser: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
