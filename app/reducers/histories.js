import {
    HISTORIES_GET_CHECK_SUCCESS,
    HISTORIES_POST_CHECK_SUCCESS,
    HISTORIES_UPDATE_CHECK_SUCCESS,
    HISTORIES_DELETE_CHECK_SUCCESS,
    HISTORIES_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   historiesGetCheckSuccess: false,
   historiesData: [],
   historiesPostCheckSuccess: false,
   historiesUpdateCheckSuccess: false,
   historiesDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case HISTORIES_GET_CHECK_SUCCESS:
        return {
          ...state,
         historiesGetCheckSuccess: action.payload,
        };
        case HISTORIES_POST_CHECK_SUCCESS:
        return {
          ...state,
         historiesPostCheckSuccess: action.payload,
        };
        case HISTORIES_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         historiesUpdateCheckSuccess: action.payload,
        };
        case HISTORIES_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             historiesDeleteCheckSuccess: action.payload,
            };
        
      case HISTORIES_SET_DATA:
        return {
          ...state,
         historiesData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  