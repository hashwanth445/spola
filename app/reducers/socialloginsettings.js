import {
    SOCIALLOGINSETTINGS_GET_CHECK_SUCCESS,
    SOCIALLOGINSETTINGS_POST_CHECK_SUCCESS,
    SOCIALLOGINSETTINGS_UPDATE_CHECK_SUCCESS,
    SOCIALLOGINSETTINGS_DELETE_CHECK_SUCCESS,
    SOCIALLOGINSETTINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   socialloginsettingsGetCheckSuccess: false,
   socialloginsettingsData: [],
   socialloginsettingsPostCheckSuccess: false,
   socialloginsettingsUpdateCheckSuccess: false,
   socialloginsettingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case SOCIALLOGINSETTINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
         socialloginsettingsGetCheckSuccess: action.payload,
        };
        case SOCIALLOGINSETTINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
         socialloginsettingsPostCheckSuccess: action.payload,
        };
        case SOCIALLOGINSETTINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         socialloginsettingsUpdateCheckSuccess: action.payload,
        };
        case SOCIALLOGINSETTINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             socialloginsettingsDeleteCheckSuccess: action.payload,
            };
        
      case SOCIALLOGINSETTINGS_SET_DATA:
        return {
          ...state,
         socialloginsettingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  