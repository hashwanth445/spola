import {
  CHANNEL_GET_CHECK_SUCCESS,
  CHANNEL_GET_CHECK_SUCCESS_FOR_USER,
  CHANNEL_POST_CHECK_SUCCESS,
  CHANNEL_UPDATE_CHECK_SUCCESS,
  CHANNEL_DELETE_CHECK_SUCCESS,
  CHANNEL_SET_DATA,
  CHANNEL_SET_DATA_FOR_USER,
} from '../actions/actionTypes';

const initialState = {
  channelGetCheckSuccess: false,
  channelGetCheckSuccessForUser: false,
  channelData: [],
  channelPostCheckSuccess: false,
  channelUpdateCheckSuccess: false,
  channelDeleteCheckSuccess: false,
  channelDataForUser: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANNEL_GET_CHECK_SUCCESS:
      return {
        ...state,
        channelGetCheckSuccess: action.payload,
      };
    case CHANNEL_GET_CHECK_SUCCESS_FOR_USER:
      return {
        ...state,
        channelGetCheckSuccessForUser: action.payload,
      };
    case CHANNEL_POST_CHECK_SUCCESS:
      return {
        ...state,
        channelPostCheckSuccess: action.payload,
      };
    case CHANNEL_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        channelUpdateCheckSuccess: action.payload,
      };
    case CHANNEL_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        channelDeleteCheckSuccess: action.payload,
      };

    case CHANNEL_SET_DATA:
      return {
        ...state,
        channelData: action.payload,
      };
    case CHANNEL_SET_DATA_FOR_USER:
      return {
        ...state,
        channelDataForUser: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
