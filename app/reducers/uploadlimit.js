import {
    UPLOADLIMIT_GET_CHECK_SUCCESS,
    UPLOADLIMIT_POST_CHECK_SUCCESS,
    UPLOADLIMIT_UPDATE_CHECK_SUCCESS,
    UPLOADLIMIT_DELETE_CHECK_SUCCESS,
    UPLOADLIMIT_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   uploadlimitGetCheckSuccess: false,
   uploadlimitData: [],
   uploadlimitPostCheckSuccess: false,
   uploadlimitUpdateCheckSuccess: false,
   uploadlimitDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case UPLOADLIMIT_GET_CHECK_SUCCESS:
        return {
          ...state,
         uploadlimitGetCheckSuccess: action.payload,
        };
        case UPLOADLIMIT_POST_CHECK_SUCCESS:
        return {
          ...state,
         uploadlimitPostCheckSuccess: action.payload,
        };
        case UPLOADLIMIT_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         uploadlimitUpdateCheckSuccess: action.payload,
        };
        case UPLOADLIMIT_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             uploadlimitDeleteCheckSuccess: action.payload,
            };
        
      case UPLOADLIMIT_SET_DATA:
        return {
          ...state,
         uploadlimitData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  