import {
    CHATS_GET_CHECK_SUCCESS,
    CHATS_POST_CHECK_SUCCESS,
    CHATS_UPDATE_CHECK_SUCCESS,
    CHATS_DELETE_CHECK_SUCCESS,
    CHATS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   chatsGetCheckSuccess: false,
   chatsData: [],
   chatsPostCheckSuccess: false,
   chatsUpdateCheckSuccess: false,
   chatsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case CHATS_GET_CHECK_SUCCESS:
        return {
          ...state,
         chatsGetCheckSuccess: action.payload,
        };
        case CHATS_POST_CHECK_SUCCESS:
        return {
          ...state,
         chatsPostCheckSuccess: action.payload,
        };
        case CHATS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         chatsUpdateCheckSuccess: action.payload,
        };
        case CHATS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             chatsDeleteCheckSuccess: action.payload,
            };
        
      case CHATS_SET_DATA:
        return {
          ...state,
         chatsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  