import {
  COMMENTS_GET_CHECK_SUCCESS,
  COMMENTS_POST_CHECK_SUCCESS,
  COMMENTS_UPDATE_CHECK_SUCCESS,
  COMMENTS_DELETE_CHECK_SUCCESS,
  COMMENTS_SET_DATA,
  COMMENTS_GET_CHECK_SUCCESS_FOR_FEED,
  COMMENTS_SET_DATA_FOR_FEED
} from '../actions/actionTypes';

const initialState = {
  commentsGetCheckSuccess: false,
  commentsData: [],
  commentsPostCheckSuccess: false,
  commentsUpdateCheckSuccess: false,
  commentsDeleteCheckSuccess: false,
  commentsGetCheckSuccessForFeed: false,
  commentsDataForFeed: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case COMMENTS_GET_CHECK_SUCCESS:
      return {
        ...state,
        commentsGetCheckSuccess: action.payload,
      };
    case COMMENTS_GET_CHECK_SUCCESS_FOR_FEED:
      return {
        ...state,
        commentsGetCheckSuccessForFeed: action.payload,
      };
    case COMMENTS_POST_CHECK_SUCCESS:
      return {
        ...state,
        commentsPostCheckSuccess: action.payload,
      };
    case COMMENTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        commentsUpdateCheckSuccess: action.payload,
      };
    case COMMENTS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        commentsDeleteCheckSuccess: action.payload,
      };

    case COMMENTS_SET_DATA:
      return {
        ...state,
        commentsData: action.payload,
      };
    case COMMENTS_SET_DATA_FOR_FEED:
      return {
        ...state,
        commentsDataForFeed: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
