import {
    FRIENDS_GET_CHECK_SUCCESS,
    FRIENDS_POST_CHECK_SUCCESS,
    FRIENDS_UPDATE_CHECK_SUCCESS,
    FRIENDS_DELETE_CHECK_SUCCESS,
    FRIENDS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   friendsGetCheckSuccess: false,
   friendsData: [],
   friendsPostCheckSuccess: false,
   friendsUpdateCheckSuccess: false,
   friendsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case FRIENDS_GET_CHECK_SUCCESS:
        return {
          ...state,
         friendsGetCheckSuccess: action.payload,
        };
        case FRIENDS_POST_CHECK_SUCCESS:
        return {
          ...state,
         friendsPostCheckSuccess: action.payload,
        };
        case FRIENDS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         friendsUpdateCheckSuccess: action.payload,
        };
        case FRIENDS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             friendsDeleteCheckSuccess: action.payload,
            };
        
      case FRIENDS_SET_DATA:
        return {
          ...state,
         friendsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  