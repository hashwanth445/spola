import {
  CHANNEL_VIDEO_GET_CHECK_SUCCESS,
  CHANNEL_VIDEO_GET_CHECK_SUCCESS_FOR_USER,
  CHANNEL_VIDEO_POST_CHECK_SUCCESS,
  CHANNEL_VIDEO_POST_CHECK_SUCCESS_FOR_REWARD,
  CHANNEL_VIDEO_UPDATE_CHECK_SUCCESS,
  CHANNEL_VIDEO_DELETE_CHECK_SUCCESS,
  CHANNEL_VIDEO_SET_DATA,
  CHANNEL_VIDEO_SET_DATA_FOR_REWARD,
  CHANNEL_VIDEO_SET_DATA_FOR_USER,
} from '../actions/actionTypes';

const initialState = {
  channelVideoGetCheckSuccess: false,
  channelVideoGetCheckSuccessForUser: false,
  channelVideoData: [],
  channelVideoDataForReward: [],
  channelVideoPostCheckSuccess: false,
  channelVideoPostCheckSuccessForReward: false,
  channelVideoUpdateCheckSuccess: false,
  channelVideoDeleteCheckSuccess: false,
  channelVideoDataForUser: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANNEL_VIDEO_GET_CHECK_SUCCESS:
      return {
        ...state,
        channelVideoGetCheckSuccess: action.payload,
      };
    case CHANNEL_VIDEO_GET_CHECK_SUCCESS_FOR_USER:
      return {
        ...state,
        channelVideoGetCheckSuccessForUser: action.payload,
      };
    case CHANNEL_VIDEO_POST_CHECK_SUCCESS:
      return {
        ...state,
        channelVideoPostCheckSuccess: action.payload,
      };
    case CHANNEL_VIDEO_POST_CHECK_SUCCESS_FOR_REWARD:
      return {
        ...state,
        channelVideoPostCheckSuccessForReward: action.payload,
      };
    case CHANNEL_VIDEO_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        channelVideoUpdateCheckSuccess: action.payload,
      };
    case CHANNEL_VIDEO_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        channelVideoDeleteCheckSuccess: action.payload,
      };

    case CHANNEL_VIDEO_SET_DATA:
      return {
        ...state,
        channelVideoData: action.payload,
      };
    case CHANNEL_VIDEO_SET_DATA_FOR_REWARD:
      return {
        ...state,
        channelVideoDataForReward: action.payload,
      };
    case CHANNEL_VIDEO_SET_DATA_FOR_USER:
      return {
        ...state,
        channelVideoDataForUser: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
