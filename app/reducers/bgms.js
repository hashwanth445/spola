import {
    AUDIOROOM_GET_CHECK_SUCCESS,
    AUDIOROOM_POST_CHECK_SUCCESS,
    AUDIOROOM_UPDATE_CHECK_SUCCESS,
    AUDIOROOM_DELETE_CHECK_SUCCESS,
    AUDIOROOM_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   audioroomGetCheckSuccess: false,
   audioroomData: [],
   audioroomPostCheckSuccess: false,
   audioroomUpdateCheckSuccess: false,
   audioroomDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case AUDIOROOM_GET_CHECK_SUCCESS:
        return {
          ...state,
         audioroomGetCheckSuccess: action.payload,
        };
        case AUDIOROOM_POST_CHECK_SUCCESS:
        return {
          ...state,
         audioroomPostCheckSuccess: action.payload,
        };
        case AUDIOROOM_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         audioroomUpdateCheckSuccess: action.payload,
        };
        case AUDIOROOM_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             audioroomDeleteCheckSuccess: action.payload,
            };
        
      case AUDIOROOM_SET_DATA:
        return {
          ...state,
         audioroomData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  