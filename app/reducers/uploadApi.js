import {
    UPLOADFILES_GET_CHECK_SUCCESS,
    UPLOADFILES_POST_CHECK_SUCCESS,
    UPLOADFILES_UPDATE_CHECK_SUCCESS,
    UPLOADFILES_DELETE_CHECK_SUCCESS,
    UPLOADFILES_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   uploadFilesGetCheckSuccess: false,
   uploadFilesData: [],
   uploadFilesPostCheckSuccess: false,
   uploadFilesUpdateCheckSuccess: false,
   uploadFilesDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case UPLOADFILES_GET_CHECK_SUCCESS:
        return {
          ...state,
         uploadFilesGetCheckSuccess: action.payload,
        };
        case UPLOADFILES_POST_CHECK_SUCCESS:
        return {
          ...state,
         uploadFilesPostCheckSuccess: action.payload,
        };
        case UPLOADFILES_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         uploadFilesUpdateCheckSuccess: action.payload,
        };
        case UPLOADFILES_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             uploadFilesDeleteCheckSuccess: action.payload,
            };
        
      case UPLOADFILES_SET_DATA:
        return {
          ...state,
         uploadFilesData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  