import {
    POINTLEVELSETTINGS_GET_CHECK_SUCCESS,
    POINTLEVELSETTINGS_POST_CHECK_SUCCESS,
    POINTLEVELSETTINGS_UPDATE_CHECK_SUCCESS,
    POINTLEVELSETTINGS_DELETE_CHECK_SUCCESS,
    POINTLEVELSETTINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   pointlevelsettingsGetCheckSuccess: false,
   pointlevelsettingsData: [],
   pointlevelsettingsPostCheckSuccess: false,
   pointlevelsettingsUpdateCheckSuccess: false,
   pointlevelsettingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case POINTLEVELSETTINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
         pointlevelsettingsGetCheckSuccess: action.payload,
        };
        case POINTLEVELSETTINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
         pointlevelsettingsPostCheckSuccess: action.payload,
        };
        case POINTLEVELSETTINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         pointlevelsettingsUpdateCheckSuccess: action.payload,
        };
        case POINTLEVELSETTINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             pointlevelsettingsDeleteCheckSuccess: action.payload,
            };
        
      case POINTLEVELSETTINGS_SET_DATA:
        return {
          ...state,
         pointlevelsettingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  