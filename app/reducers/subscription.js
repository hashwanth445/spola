import {
    SUBSCRIPTION_GET_CHECK_SUCCESS,
    SUBSCRIPTION_POST_CHECK_SUCCESS,
    SUBSCRIPTION_UPDATE_CHECK_SUCCESS,
    SUBSCRIPTION_DELETE_CHECK_SUCCESS,
    SUBSCRIPTION_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   subscriptionGetCheckSuccess: false,
   subscriptionData: [],
   subscriptionPostCheckSuccess: false,
   subscriptionUpdateCheckSuccess: false,
   subscriptionDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case SUBSCRIPTION_GET_CHECK_SUCCESS:
        return {
          ...state,
         subscriptionGetCheckSuccess: action.payload,
        };
        case SUBSCRIPTION_POST_CHECK_SUCCESS:
        return {
          ...state,
         subscriptionPostCheckSuccess: action.payload,
        };
        case SUBSCRIPTION_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         subscriptionUpdateCheckSuccess: action.payload,
        };
        case SUBSCRIPTION_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             subscriptionDeleteCheckSuccess: action.payload,
            };
        
      case SUBSCRIPTION_SET_DATA:
        return {
          ...state,
         subscriptionData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  