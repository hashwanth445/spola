import {
  VIDEOS_GET_CHECK_SUCCESS,
  VIDEOS_POST_CHECK_SUCCESS,
  VIDEOS_UPDATE_CHECK_SUCCESS,
  VIDEOS_DELETE_CHECK_SUCCESS,
  VIDEOS_SET_DATA,
  VIDEOS_SEARCH_SET_DATA,
} from '../actions/actionTypes';

const initialState = {
  videosGetCheckSuccess: false,
  videosData: [],
  videosSearchData: [],
  videosPostCheckSuccess: false,
  videosUpdateCheckSuccess: false,
  videosDeleteCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case VIDEOS_GET_CHECK_SUCCESS:
      return {
        ...state,
        videosGetCheckSuccess: action.payload,
      };
    case VIDEOS_POST_CHECK_SUCCESS:
      return {
        ...state,
        videosPostCheckSuccess: action.payload,
      };
    case VIDEOS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        videosUpdateCheckSuccess: action.payload,
      };
    case VIDEOS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        videosDeleteCheckSuccess: action.payload,
      };

    case VIDEOS_SET_DATA:
      return {
        ...state,
        videosData: action.payload,
      };

    case VIDEOS_SEARCH_SET_DATA:
      return {
        ...state,
        videosSearchData: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
