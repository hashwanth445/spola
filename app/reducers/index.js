import {combineReducers} from 'redux';
import AuthReducer from './auth';
import {LOGOUT} from "../actions/actionTypes";
import LogoutReducer from "./logout";
import ApplicationReducer from './application';
import FurinishReducer from "./sample";
import MainsettingsReducer from "./mainsettings";
import signupReducer from "./signup";
import PlayersettingsReducer from "./playersettings";
import UploadsettingsReducer from "./uploadsettings";
import UsersettingsReducer from "./usersettings";
import CheckoutpaymentsReducer from "./checkoutpayments";
import AudiencedetailsReducer from "./audiencedetails";
import CashfreepaymentsReducer from "./cashfreepayments";
import CommentsettingsReducer from "./commentsettings";
import EmailsettingsReducer from "./emailsettings";
import LimitperpageReducer from "./limitperpage";
import LivesettingsReducer from "./livesettings";
import ManagemoviesReducer from "./managemovies";
import ManageuseradsReducer from "./manageuserads";
import LoginReducer from "./login";
import AddressReducer from './address';
import deviceReducer from './device'

import ChatsReducer from "./chats";

import ManagevideoadsReducer from "./managevideoads";
import ManagevideoreportsReducer from "./managevideoreports";
import OthersettingsReducer from "./othersettings";
import PointlevelsettingsReducer from "./pointlevelsettings";
import RazorpaypaymentsReducer from "./razorpaypayments";
import SocialloginsettingsReducer from "./socialloginsettings";
import UploadlimitReducer from "./uploadlimit";
import UploadFilesReducer from "./uploadApi";
import CategoriesReducer from "./categories";
import ChannelReducer from "./channel";
import CommentsReducer from "./comments";
import FilesReducer from "./files";
import FeedsReducer from "./feeds";
import LikesReducer from "./likes";
import FriendsReducer from "./friends";
import NotificationReducer from "./notification";
import PlaylistsReducer from "./playlists";
import MoviesReducer from "./movies";
import SubscriptionReducer from "./subscription";
import WalletReducer from "./wallet";
import VideosReducer from "./videos";
import ShortsReducer from "./shorts";
import BgmsReducer from "./bgms";
import TrendingsReducer from "./trendings";
import HistoriesReducer from "./histories";
import AudioroomReducer from "./audioroom";
import RewardedvideosReducer from "./rewardedvideos";
import ProductsReducer from "./products";
import BookmarksReducer from "./bookmarks";
import DislikeReducer from "./dislike";
import AccessTokenReducer from "../reducers/accessToken";
import OrdersReducer from "./orders";
import cartsReducer from "./carts";
import channelVideoReducer from './channelVideos';
import usersReducer from './users'

const appReducer= combineReducers({
  auth: AuthReducer,
  application: ApplicationReducer,
  deviceReducer:deviceReducer,
  usersReducer:usersReducer,
  channelVideoReducer:channelVideoReducer,
  AddressReducer:AddressReducer,
  furinishReducer : FurinishReducer,
  signupReducer:signupReducer,
  accessTokenReducer: AccessTokenReducer,
  MainsettingsReducer : MainsettingsReducer,
  PlayersettingsReducer : PlayersettingsReducer,
  UploadsettingsReducer : UploadsettingsReducer,
  UsersettingsReducer : UsersettingsReducer,
  CheckoutpaymentsReducer : CheckoutpaymentsReducer,
  AudiencedetailsReducer : AudiencedetailsReducer,
  CashfreepaymentsReducer : CashfreepaymentsReducer,
  CommentsettingsReducer : CommentsettingsReducer,
  EmailsettingsReducer : EmailsettingsReducer,
  LimitperpageReducer : LimitperpageReducer,
  LivesettingsReducer : LivesettingsReducer,
  ManagemoviesReducer : ManagemoviesReducer,
  ManageuseradsReducer : ManageuseradsReducer,
  ManagevideoadsReducer : ManagevideoadsReducer,
  ManagevideoreportsReducer : ManagevideoreportsReducer,
  OthersettingsReducer : OthersettingsReducer,
  PointlevelsettingsReducer : PointlevelsettingsReducer,
  RazorpaypaymentsReducer : RazorpaypaymentsReducer,
  SocialloginsettingsReducer : SocialloginsettingsReducer,
  FriendsReducer : FriendsReducer,
  CategoriesReducer : CategoriesReducer,
  ChannelReducer : ChannelReducer,
  CommentsReducer : CommentsReducer,
  FilesReducer : FilesReducer,
  FeedsReducer : FeedsReducer,
  LikesReducer : LikesReducer,
  UploadlimitReducer : UploadlimitReducer,
  UploadFilesReducer : UploadFilesReducer,
  FriendsReducer : FriendsReducer,
  NotificationReducer : NotificationReducer,
  PlaylistsReducer : PlaylistsReducer,
  MoviesReducer : MoviesReducer,
  SubscriptionReducer : SubscriptionReducer,
  WalletReducer : WalletReducer,
  VideosReducer : VideosReducer,
  ChatsReducer : ChatsReducer,
  ShortsReducer : ShortsReducer,
  BgmsReducer : BgmsReducer,
  TrendingsReducer : TrendingsReducer,
  HistoriesReducer : HistoriesReducer,
  AudioroomReducer : AudioroomReducer,
  logoutReducer: LogoutReducer,
  loginReducer: LoginReducer,
  ProductsReducer: ProductsReducer,
  OrdersReducer:OrdersReducer,
  cartsReducer:cartsReducer,
  BookmarksReducer: BookmarksReducer,
  DislikeReducer: DislikeReducer,
  RewardedvideosReducer: RewardedvideosReducer,
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
      state = undefined
  }
  return appReducer(state, action)
};

export default rootReducer;