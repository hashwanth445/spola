import {
    TRENDINGS_GET_CHECK_SUCCESS,
    TRENDINGS_POST_CHECK_SUCCESS,
    TRENDINGS_UPDATE_CHECK_SUCCESS,
    TRENDINGS_DELETE_CHECK_SUCCESS,
    TRENDINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   trendingsGetCheckSuccess: false,
   trendingsData: [],
   trendingsPostCheckSuccess: false,
   trendingsUpdateCheckSuccess: false,
   trendingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case TRENDINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
         trendingsGetCheckSuccess: action.payload,
        };
        case TRENDINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
         trendingsPostCheckSuccess: action.payload,
        };
        case TRENDINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         trendingsUpdateCheckSuccess: action.payload,
        };
        case TRENDINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             trendingsDeleteCheckSuccess: action.payload,
            };
        
      case TRENDINGS_SET_DATA:
        return {
          ...state,
         trendingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  