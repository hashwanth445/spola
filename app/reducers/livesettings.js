import {
  LIVESETTINGS_GET_CHECK_SUCCESS,
  LIVESETTINGS_POST_CHECK_SUCCESS,
  LIVESETTINGS_UPDATE_CHECK_SUCCESS,
  LIVESETTINGS_DELETE_CHECK_SUCCESS,
  LIVESETTINGS_SET_DATA,
} from '../actions/actionTypes';

const initialState = {
 livesettingsGetCheckSuccess: false,
 livesettingsData: [],
 livesettingsPostCheckSuccess: false,
 livesettingsUpdateCheckSuccess: false,
 livesettingsDeleteCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LIVESETTINGS_GET_CHECK_SUCCESS:
      return {
        ...state,
       livesettingsGetCheckSuccess: action.payload,
      };
      case LIVESETTINGS_POST_CHECK_SUCCESS:
      return {
        ...state,
       livesettingsPostCheckSuccess: action.payload,
      };
      case LIVESETTINGS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
       livesettingsUpdateCheckSuccess: action.payload,
      };
      case LIVESETTINGS_DELETE_CHECK_SUCCESS:
          return {
            ...state,
           livesettingsDeleteCheckSuccess: action.payload,
          };
      
    case LIVESETTINGS_SET_DATA:
      return {
        ...state,
       livesettingsData: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
