import {
    MANAGEUSERADS_GET_CHECK_SUCCESS,
    MANAGEUSERADS_POST_CHECK_SUCCESS,
    MANAGEUSERADS_UPDATE_CHECK_SUCCESS,
    MANAGEUSERADS_DELETE_CHECK_SUCCESS,
    MANAGEUSERADS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   manageuseradsGetCheckSuccess: false,
   manageuseradsData: [],
   manageuseradsPostCheckSuccess: false,
   manageuseradsUpdateCheckSuccess: false,
   manageuseradsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case MANAGEUSERADS_GET_CHECK_SUCCESS:
        return {
          ...state,
         manageuseradsGetCheckSuccess: action.payload,
        };
        case MANAGEUSERADS_POST_CHECK_SUCCESS:
        return {
          ...state,
         manageuseradsPostCheckSuccess: action.payload,
        };
        case MANAGEUSERADS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         manageuseradsUpdateCheckSuccess: action.payload,
        };
        case MANAGEUSERADS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             manageuseradsDeleteCheckSuccess: action.payload,
            };
        
      case MANAGEUSERADS_SET_DATA:
        return {
          ...state,
         manageuseradsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  