import {
  ORDERS_GET_CHECK_SUCCESS,
  ORDERS_GET_CHECK_SUCCESS_FOR_USER,
  ORDERS_POST_CHECK_SUCCESS,
  ORDERS_UPDATE_CHECK_SUCCESS,
  ORDERS_DELETE_CHECK_SUCCESS,
  ORDERS_SET_DATA,
  ORDERS_SET_DATA_FOR_USER,
} from '../actions/actionTypes';

const initialState = {
 ordersGetCheckSuccess: false,
 ordersGetCheckSuccessForUser: false,
 ordersData: [],
 ordersDataForUser: [],
 ordersPostCheckSuccess: false,
 ordersUpdateCheckSuccess: false,
 ordersDeleteCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ORDERS_GET_CHECK_SUCCESS:
      return {
        ...state,
        ordersGetCheckSuccess: action.payload,
      };
    case ORDERS_GET_CHECK_SUCCESS_FOR_USER:
      return {
        ...state,
        ordersGetCheckSuccessForUser: action.payload,
      };
      case ORDERS_POST_CHECK_SUCCESS:
      return {
        ...state,
        ordersPostCheckSuccess: action.payload,
      };
      case ORDERS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        ordersUpdateCheckSuccess: action.payload,
      };
      case ORDERS_DELETE_CHECK_SUCCESS:
          return {
            ...state,
            ordersDeleteCheckSuccess: action.payload,
          };
      
    case ORDERS_SET_DATA:
      return {
        ...state,
       ordersData: action.payload,
      };
    case ORDERS_SET_DATA_FOR_USER:
      return {
        ...state,
       ordersDataForUser: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
