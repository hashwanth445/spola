import {
    PLAYERSETTINGS_GET_CHECK_SUCCESS,
    PLAYERSETTINGS_POST_CHECK_SUCCESS,
    PLAYERSETTINGS_UPDATE_CHECK_SUCCESS,
    PLAYERSETTINGS_DELETE_CHECK_SUCCESS,
    PLAYERSETTINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   playersettingsGetCheckSuccess: false,
   playersettingsData: [],
   playersettingsPostCheckSuccess: false,
   playersettingsUpdateCheckSuccess: false,
   playersettingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case PLAYERSETTINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
         playersettingsGetCheckSuccess: action.payload,
        };
        case PLAYERSETTINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
         playersettingsPostCheckSuccess: action.payload,
        };
        case PLAYERSETTINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         playersettingsUpdateCheckSuccess: action.payload,
        };
        case PLAYERSETTINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             playersettingsDeleteCheckSuccess: action.payload,
            };
        
      case PLAYERSETTINGS_SET_DATA:
        return {
          ...state,
         playersettingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  