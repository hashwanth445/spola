import {
  CATEGORIES_GET_CHECK_SUCCESS,
  CATEGORIES_POST_CHECK_SUCCESS,
  CATEGORIES_UPDATE_CHECK_SUCCESS,
  CATEGORIES_DELETE_CHECK_SUCCESS,
  CATEGORIES_SET_DATA,
  CREATORS_SET_DATA,
} from '../actions/actionTypes';

const initialState = {
 categoriesGetCheckSuccess: false,
 categoriesData: [],
 categoriesPostCheckSuccess: false,
 categoriesUpdateCheckSuccess: false,
 categoriesDeleteCheckSuccess: false,
 creatorsData:[],
};

const reducer = (state = initialState, action) => {
  console.log("action from recuder",action)
  switch (action.type) {
    case CATEGORIES_GET_CHECK_SUCCESS:
      return {
        ...state,
       categoriesGetCheckSuccess: action.payload,
      };
      case CATEGORIES_POST_CHECK_SUCCESS:
      return {
        ...state,
       categoriesPostCheckSuccess: action.payload,
      };
      case CATEGORIES_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
       categoriesUpdateCheckSuccess: action.payload,
      };
      case CATEGORIES_DELETE_CHECK_SUCCESS:
          return {
            ...state,
           categoriesDeleteCheckSuccess: action.payload,
          };
      
    case CATEGORIES_SET_DATA:
      return {
        ...state,
       categoriesData: action.payload,
      };
      case CREATORS_SET_DATA:
      return {
        ...state,
       creatorsData: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
