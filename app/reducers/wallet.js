import {
    WALLET_GET_CHECK_SUCCESS,
    WALLET_POST_CHECK_SUCCESS,
    WALLET_UPDATE_CHECK_SUCCESS,
    WALLET_DELETE_CHECK_SUCCESS,
    WALLET_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   walletGetCheckSuccess: false,
   walletData: [],
   walletPostCheckSuccess: false,
   walletUpdateCheckSuccess: false,
   walletDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case WALLET_GET_CHECK_SUCCESS:
        return {
          ...state,
         walletGetCheckSuccess: action.payload,
        };
        case WALLET_POST_CHECK_SUCCESS:
        return {
          ...state,
         walletPostCheckSuccess: action.payload,
        };
        case WALLET_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         walletUpdateCheckSuccess: action.payload,
        };
        case WALLET_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             walletDeleteCheckSuccess: action.payload,
            };
        
      case WALLET_SET_DATA:
        return {
          ...state,
         walletData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  