import {
    COMMENTSETTINGS_GET_CHECK_SUCCESS,
    COMMENTSETTINGS_POST_CHECK_SUCCESS,
    COMMENTSETTINGS_UPDATE_CHECK_SUCCESS,
    COMMENTSETTINGS_DELETE_CHECK_SUCCESS,
    COMMENTSETTINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   commentsettingsGetCheckSuccess: false,
   commentsettingsData: [],
   commentsettingsPostCheckSuccess: false,
   commentsettingsUpdateCheckSuccess: false,
   commentsettingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case COMMENTSETTINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
         commentsettingsGetCheckSuccess: action.payload,
        };
        case COMMENTSETTINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
         commentsettingsPostCheckSuccess: action.payload,
        };
        case COMMENTSETTINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         commentsettingsUpdateCheckSuccess: action.payload,
        };
        case COMMENTSETTINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             commentsettingsDeleteCheckSuccess: action.payload,
            };
        
      case COMMENTSETTINGS_SET_DATA:
        return {
          ...state,
         commentsettingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  