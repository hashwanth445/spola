import {
    LIMITPERPAGE_GET_CHECK_SUCCESS,
    LIMITPERPAGE_POST_CHECK_SUCCESS,
    LIMITPERPAGE_UPDATE_CHECK_SUCCESS,
    LIMITPERPAGE_DELETE_CHECK_SUCCESS,
    LIMITPERPAGE_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   limitperpageGetCheckSuccess: false,
   limitperpageData: [],
   limitperpagePostCheckSuccess: false,
   limitperpageUpdateCheckSuccess: false,
   limitperpageDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case LIMITPERPAGE_GET_CHECK_SUCCESS:
        return {
          ...state,
         limitperpageGetCheckSuccess: action.payload,
        };
        case LIMITPERPAGE_POST_CHECK_SUCCESS:
        return {
          ...state,
         limitperpagePostCheckSuccess: action.payload,
        };
        case LIMITPERPAGE_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         limitperpageUpdateCheckSuccess: action.payload,
        };
        case LIMITPERPAGE_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             limitperpageDeleteCheckSuccess: action.payload,
            };
        
      case LIMITPERPAGE_SET_DATA:
        return {
          ...state,
         limitperpageData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  