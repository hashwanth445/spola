import {
    OTHERSETTINGS_GET_CHECK_SUCCESS,
    OTHERSETTINGS_POST_CHECK_SUCCESS,
    OTHERSETTINGS_UPDATE_CHECK_SUCCESS,
    OTHERSETTINGS_DELETE_CHECK_SUCCESS,
    OTHERSETTINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   othersettingsGetCheckSuccess: false,
   othersettingsData: [],
   othersettingsPostCheckSuccess: false,
   othersettingsUpdateCheckSuccess: false,
   othersettingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case OTHERSETTINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
         othersettingsGetCheckSuccess: action.payload,
        };
        case OTHERSETTINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
         othersettingsPostCheckSuccess: action.payload,
        };
        case OTHERSETTINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         othersettingsUpdateCheckSuccess: action.payload,
        };
        case OTHERSETTINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             othersettingsDeleteCheckSuccess: action.payload,
            };
        
      case OTHERSETTINGS_SET_DATA:
        return {
          ...state,
         othersettingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  