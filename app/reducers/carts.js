import {
  CARTS_GET_CHECK_SUCCESS,
  CARTS_GET_CHECK_SUCCESS_FOR_USER,
  CARTS_POST_CHECK_SUCCESS,
  CARTS_UPDATE_CHECK_SUCCESS,
  CARTS_DELETE_CHECK_SUCCESS,
  CARTS_SET_DATA,
  CARTS_SET_DATA_FOR_USER,
} from '../actions/actionTypes';

const initialState = {
 cartsGetCheckSuccess: false,
 cartsGetCheckSuccessForUser: false,
 cartsData: [],
 cartsDataForUser: [],
 cartsPostCheckSuccess: false,
 cartsUpdateCheckSuccess: false,
 cartsDeleteCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CARTS_GET_CHECK_SUCCESS:
      return {
        ...state,
        cartsGetCheckSuccess: action.payload,
      };
    case CARTS_GET_CHECK_SUCCESS_FOR_USER:
      return {
        ...state,
        cartsGetCheckSuccess: action.payload,
      };
      case CARTS_POST_CHECK_SUCCESS:
      return {
        ...state,
        cartsPostCheckSuccess: action.payload,
      };
      case CARTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        cartsUpdateCheckSuccess: action.payload,
      };
      case CARTS_DELETE_CHECK_SUCCESS:
          return {
            ...state,
            cartsDeleteCheckSuccess: action.payload,
          };
      
    case CARTS_SET_DATA:
      return {
        ...state,
       cartsData: action.payload,
      };
    case CARTS_SET_DATA_FOR_USER:
      return {
        ...state,
       cartsDataForUser: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
