import {
    SIGNUP_LOADING,
    SIGNUP_SUCCESS,
    SIGNUP_FAILURE,
    VERIFYOTP_SUCCESS,
    VERIFYOTP_FAILURE,
    SIGNUP_DATA,
    SIGNUP_PUT_CHANNEL_DATA_SUCCESS,
    SIGNUP_PUT_SUCCESS,
    SIGNUP_PUT_LOADING,
    SIGNUP_PUT_FAILURE,
    
  } from '../actions/actionTypes';
  
  let initialState = {
    signupLoading: false,
    signupSuccess: false,
    signupFailure: false,
    signupPutLoading: false,
    signupPutSuccess: false,
    signupPutChannelDataSuccess:false,
    signupPutFailure: false,
    verifyOtpSuccess: false,
    verifyOtpFailure: true,
    signupMessageData : null
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case SIGNUP_DATA:
        return {
          ...state,
          signupMessageData: action.payload,
        };
      case SIGNUP_LOADING:
        return {
          ...state,
          signupLoading: action.payload,
        };
      case SIGNUP_SUCCESS:  
        return {
          ...state,
          signupSuccess: action.payload,
        };
        case VERIFYOTP_SUCCESS:  
        return {
          ...state,
          verifyOtpSuccess: action.payload,
        };
        case VERIFYOTP_FAILURE:
          return {
            ...state,
            verifyOtpFailure : action.payload,
          };
      case SIGNUP_FAILURE:
        return {
          ...state,
          signupFailure: action.payload,
        };
        case SIGNUP_PUT_FAILURE:
        return {
          ...state,
          signupPutFailure: action.payload,
        };
        case SIGNUP_PUT_LOADING:
        return {
          ...state,
          signupPutLoading: action.payload,
        };
      case SIGNUP_PUT_SUCCESS:  
        return {
          ...state,
          signupPutSuccess: action.payload,
        };
        case SIGNUP_PUT_CHANNEL_DATA_SUCCESS:  
        return {
          ...state,
          signupPutChannelDataSuccess: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default reducer;
  