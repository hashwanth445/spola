import {
  LIKES_GET_CHECK_SUCCESS,
  LIKES_POST_CHECK_SUCCESS,
  LIKES_UPDATE_CHECK_SUCCESS,
  LIKES_DELETE_CHECK_SUCCESS,
  LIKES_SET_DATA,
  LIKES_GET_CHECK_SUCCESS_BY_USER,
  LIKES_SET_DATA_BY_USER,
  LIKES_SET_DATA_FOR_FEED,
  LIKES_GET_CHECK_SUCCESS_FOR_FEED,
  LIKES_SET_DATA_FOR_SHORT,
  LIKES_GET_CHECK_SUCCESS_FOR_SHORT
} from '../actions/actionTypes';

const initialState = {
  likesGetCheckSuccess: false,
  likesData: [],
  likesPostCheckSuccess: false,
  likesUpdateCheckSuccess: false,
  likesDeleteCheckSuccess: false,
  likesGetCheckSuccessByUser: false,
  likesGetCheckSuccessForFeed: false,
  likesGetCheckSuccessForShort: false,
  likesDataByUser: [],
  likesDataForFeed: [],
  likesDataForShort: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LIKES_GET_CHECK_SUCCESS:
      return {
        ...state,
        likesGetCheckSuccess: action.payload,
      };
    case LIKES_GET_CHECK_SUCCESS_BY_USER:
      return {
        ...state,
        likesGetCheckSuccessByUser: action.payload,
      };
    case LIKES_GET_CHECK_SUCCESS_FOR_FEED:
      return {
        ...state,
        likesGetCheckSuccessForFeed: action.payload,
      };
    case LIKES_GET_CHECK_SUCCESS_FOR_SHORT:
      return {
        ...state,
        likesGetCheckSuccessForShort: action.payload,
      };
    case LIKES_POST_CHECK_SUCCESS:
      return {
        ...state,
        likesPostCheckSuccess: action.payload,
      };
    case LIKES_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        likesUpdateCheckSuccess: action.payload,
      };
    case LIKES_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        likesDeleteCheckSuccess: action.payload,
      };

    case LIKES_SET_DATA:
      return {
        ...state,
        likesData: action.payload,
      };
    case LIKES_SET_DATA_BY_USER:
      return {
        ...state,
        likesDataByUser: action.payload,
      };
    case LIKES_SET_DATA_FOR_FEED:
      return {
        ...state,
        likesDataForFeed: action.payload,
      };
    case LIKES_SET_DATA_FOR_SHORT:
      return {
        ...state,
        likesDataForShort: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
