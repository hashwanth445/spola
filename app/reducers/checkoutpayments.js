import {
    CHECKOUTPAYMENTS_GET_CHECK_SUCCESS,
    CHECKOUTPAYMENTS_POST_CHECK_SUCCESS,
    CHECKOUTPAYMENTS_UPDATE_CHECK_SUCCESS,
    CHECKOUTPAYMENTS_DELETE_CHECK_SUCCESS,
    CHECKOUTPAYMENTS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   checkoutpaymentsGetCheckSuccess: false,
   checkoutpaymentsData: [],
   checkoutpaymentsPostCheckSuccess: false,
   checkoutpaymentsUpdateCheckSuccess: false,
   checkoutpaymentsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case CHECKOUTPAYMENTS_GET_CHECK_SUCCESS:
        return {
          ...state,
         checkoutpaymentsGetCheckSuccess: action.payload,
        };
        case CHECKOUTPAYMENTS_POST_CHECK_SUCCESS:
        return {
          ...state,
         checkoutpaymentsPostCheckSuccess: action.payload,
        };
        case CHECKOUTPAYMENTS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         checkoutpaymentsUpdateCheckSuccess: action.payload,
        };
        case CHECKOUTPAYMENTS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             checkoutpaymentsDeleteCheckSuccess: action.payload,
            };
        
      case CHECKOUTPAYMENTS_SET_DATA:
        return {
          ...state,
         checkoutpaymentsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  