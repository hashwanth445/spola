import {
    EMAILSETTINGS_GET_CHECK_SUCCESS,
    EMAILSETTINGS_POST_CHECK_SUCCESS,
    EMAILSETTINGS_UPDATE_CHECK_SUCCESS,
    EMAILSETTINGS_DELETE_CHECK_SUCCESS,
    EMAILSETTINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   emailsettingsGetCheckSuccess: false,
   emailsettingsData: [],
   emailsettingsPostCheckSuccess: false,
   emailsettingsUpdateCheckSuccess: false,
   emailsettingsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case EMAILSETTINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
         emailsettingsGetCheckSuccess: action.payload,
        };
        case EMAILSETTINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
         emailsettingsPostCheckSuccess: action.payload,
        };
        case EMAILSETTINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         emailsettingsUpdateCheckSuccess: action.payload,
        };
        case EMAILSETTINGS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             emailsettingsDeleteCheckSuccess: action.payload,
            };
        
      case EMAILSETTINGS_SET_DATA:
        return {
          ...state,
         emailsettingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  