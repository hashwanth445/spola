import {
    FURNISHINGS_GET_CHECK_SUCCESS,
    FURNISHINGS_POST_CHECK_SUCCESS,
    FURNISHINGS_UPDATE_CHECK_SUCCESS,
    FURNISHINGS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
    furnishingsGetCheckSuccess: false,
    furnishingsData: [],
    furnishingsPostCheckSuccess: false,
    furnishingsUpdateCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case FURNISHINGS_GET_CHECK_SUCCESS:
        return {
          ...state,
          furnishingsGetCheckSuccess: action.payload,
        };
        case FURNISHINGS_POST_CHECK_SUCCESS:
        return {
          ...state,
          furnishingsPostCheckSuccess: action.payload,
        };
        case FURNISHINGS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
          furnishingsUpdateCheckSuccess: action.payload,
        };
        
      case FURNISHINGS_SET_DATA:
        return {
          ...state,
          furnishingsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  