import {SET_ACCESS_TOKEN,SET_USER_DATA} from '../actions/actionTypes';

const initialState = {
  accessToken: null,
  userData:{}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ACCESS_TOKEN:
      return {
        ...state,
        accessToken: action.payload,
      };
      case SET_USER_DATA:
      return {
        ...state,
        userData: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
