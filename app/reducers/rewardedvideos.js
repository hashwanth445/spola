import {
  REWARDEDVIDEOS_GET_CHECK_SUCCESS,
  REWARDEDVIDEOS_POST_CHECK_SUCCESS,
  REWARDEDVIDEOS_UPDATE_CHECK_SUCCESS,
  REWARDEDVIDEOS_DELETE_CHECK_SUCCESS,
  REWARDEDVIDEOS_SET_DATA,
} from '../actions/actionTypes';

const initialState = {
 rewardedvideosGetCheckSuccess: false,
 rewardedvideosData: [],
 rewardedvideosPostCheckSuccess: false,
 rewardedvideosUpdateCheckSuccess: false,
 rewardedvideosDeleteCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REWARDEDVIDEOS_GET_CHECK_SUCCESS:
      return {
        ...state,
       rewardedvideosGetCheckSuccess: action.payload,
      };
      case REWARDEDVIDEOS_POST_CHECK_SUCCESS:
      return {
        ...state,
       rewardedvideosPostCheckSuccess: action.payload,
      };
      case REWARDEDVIDEOS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
       rewardedvideosUpdateCheckSuccess: action.payload,
      };
      case REWARDEDVIDEOS_DELETE_CHECK_SUCCESS:
          return {
            ...state,
           rewardedvideosDeleteCheckSuccess: action.payload,
          };
      
    case REWARDEDVIDEOS_SET_DATA:
      return {
        ...state,
       rewardedvideosData: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
