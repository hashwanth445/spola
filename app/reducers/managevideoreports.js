import {
    MANAGEVIDEOREPORTS_GET_CHECK_SUCCESS,
    MANAGEVIDEOREPORTS_POST_CHECK_SUCCESS,
    MANAGEVIDEOREPORTS_UPDATE_CHECK_SUCCESS,
    MANAGEVIDEOREPORTS_DELETE_CHECK_SUCCESS,
    MANAGEVIDEOREPORTS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   managevideoreportsGetCheckSuccess: false,
   managevideoreportsData: [],
   managevideoreportsPostCheckSuccess: false,
   managevideoreportsUpdateCheckSuccess: false,
   managevideoreportsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case MANAGEVIDEOREPORTS_GET_CHECK_SUCCESS:
        return {
          ...state,
         managevideoreportsGetCheckSuccess: action.payload,
        };
        case MANAGEVIDEOREPORTS_POST_CHECK_SUCCESS:
        return {
          ...state,
         managevideoreportsPostCheckSuccess: action.payload,
        };
        case MANAGEVIDEOREPORTS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         managevideoreportsUpdateCheckSuccess: action.payload,
        };
        case MANAGEVIDEOREPORTS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             managevideoreportsDeleteCheckSuccess: action.payload,
            };
        
      case MANAGEVIDEOREPORTS_SET_DATA:
        return {
          ...state,
         managevideoreportsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  