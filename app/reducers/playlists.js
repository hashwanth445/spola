import {
    PLAYLISTS_GET_CHECK_SUCCESS,
    PLAYLISTS_POST_CHECK_SUCCESS,
    PLAYLISTS_UPDATE_CHECK_SUCCESS,
    PLAYLISTS_DELETE_CHECK_SUCCESS,
    PLAYLISTS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   playlistsGetCheckSuccess: false,
   playlistsData: [],
   playlistsPostCheckSuccess: false,
   playlistsUpdateCheckSuccess: false,
   playlistsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case PLAYLISTS_GET_CHECK_SUCCESS:
        return {
          ...state,
         playlistsGetCheckSuccess: action.payload,
        };
        case PLAYLISTS_POST_CHECK_SUCCESS:
        return {
          ...state,
         playlistsPostCheckSuccess: action.payload,
        };
        case PLAYLISTS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         playlistsUpdateCheckSuccess: action.payload,
        };
        case PLAYLISTS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             playlistsDeleteCheckSuccess: action.payload,
            };
        
      case PLAYLISTS_SET_DATA:
        return {
          ...state,
         playlistsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  