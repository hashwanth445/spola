import {
    VERIFYOTP_LOADING,
    VERIFYOTP_SUCCESS,
    VERIFYOTP_FAILURE,
    VERIFYOTP_DATA,
    
  } from '../actions/actionTypes';
  
  let initialState = {
    verifyOtpLoading: false,
    verifyOtpSuccess: false,
    verifyOtpFailure: false,
    verifyOtpMessageData : null
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case VERIFYOTP_DATA:
        return {
          ...state,
          verifyOtpMessageData: action.payload,
        };
      case VERIFYOTP_LOADING:
        return {
          ...state,
          verifyOtpLoading: action.payload,
        };
      case VERIFYOTP_SUCCESS:  
        return {
          ...state,
          verifyOtpSuccess: action.payload,
        };
      case VERIFYOTP_FAILURE:
        return {
          ...state,
          verifyOtpFailure: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default reducer;
  