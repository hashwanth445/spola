import {
    FILES_GET_CHECK_SUCCESS,
    FILES_POST_CHECK_SUCCESS,
    FILES_UPDATE_CHECK_SUCCESS,
    FILES_DELETE_CHECK_SUCCESS,
    FILES_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   filesGetCheckSuccess: false,
   filesData: [],
   filesPostCheckSuccess: false,
   filesUpdateCheckSuccess: false,
   filesDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case FILES_GET_CHECK_SUCCESS:
        return {
          ...state,
         filesGetCheckSuccess: action.payload,
        };
        case FILES_POST_CHECK_SUCCESS:
        return {
          ...state,
         filesPostCheckSuccess: action.payload,
        };
        case FILES_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         filesUpdateCheckSuccess: action.payload,
        };
        case FILES_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             filesDeleteCheckSuccess: action.payload,
            };
        
      case FILES_SET_DATA:
        return {
          ...state,
         filesData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  