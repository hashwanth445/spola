import {ADD_QUEUE_QUEUE, SET_CURRENT_STATE, REMOVE_FROM_QUEUE, SET_VIDEO_LIST } from './actionTypes';
 
const appReducer = (state = reducerInitialState, action) => {
    switch (action.type) {
      case ADD_QUEUE_QUEUE: {
        return {
          ...state,
          queue: [...state.queue, action.data] // creating new reference

      }
  
      case SET_CURRENT_STATE: {
        return {
          ...state,
          current: action.data,
        };
      }
  
      case REMOVE_FROM_QUEUE: {
        return {
          ...state,
          current: null,
          queue: [...state.queue].filter(x => x.uuid !== action.data.uuid)
        };
      }
  
      case SET_VIDEO_LIST: {
        return {
          ...state,
          videoList: action.data
        };
      }
  
      default: {
        return state;
      }
    }
  };
  