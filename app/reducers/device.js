import {
    DEVICE_GET_CHECK_SUCCESS,
    DEVICE_POST_CHECK_SUCCESS,
    DEVICE_UPDATE_CHECK_SUCCESS,
    DEVICE_DELETE_CHECK_SUCCESS,
    DEVICE_SET_DATA,
} from '../actions/actionTypes';

const initialState = {
    deviceGetCheckSuccess: false,
    deviceData: [],
    devicePostCheckSuccess: false,
    deviceUpdateCheckSuccess: false,
    deviceDeleteCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case DEVICE_GET_CHECK_SUCCESS:
            return {
                ...state,
                deviceGetCheckSuccess: action.payload,
            };
        case DEVICE_POST_CHECK_SUCCESS:
            return {
                ...state,
                devicePostCheckSuccess: action.payload,
            };
        case DEVICE_UPDATE_CHECK_SUCCESS:
            return {
                ...state,
                deviceUpdateCheckSuccess: action.payload,
            };
        case DEVICE_DELETE_CHECK_SUCCESS:
            return {
                ...state,
                deviceDeleteCheckSuccess: action.payload,
            };

        case DEVICE_SET_DATA:
            return {
                ...state,
                deviceData: action.payload,
            };
        default:
            return state;
    }
};

export default reducer;
