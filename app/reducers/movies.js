import {
    MOVIES_GET_CHECK_SUCCESS,
    MOVIES_POST_CHECK_SUCCESS,
    MOVIES_UPDATE_CHECK_SUCCESS,
    MOVIES_DELETE_CHECK_SUCCESS,
    MOVIES_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   moviesGetCheckSuccess: false,
   moviesData: [],
   moviesPostCheckSuccess: false,
   moviesUpdateCheckSuccess: false,
   moviesDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case MOVIES_GET_CHECK_SUCCESS:
        return {
          ...state,
         moviesGetCheckSuccess: action.payload,
        };
        case MOVIES_POST_CHECK_SUCCESS:
        return {
          ...state,
         moviesPostCheckSuccess: action.payload,
        };
        case MOVIES_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         moviesUpdateCheckSuccess: action.payload,
        };
        case MOVIES_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             moviesDeleteCheckSuccess: action.payload,
            };
        
      case MOVIES_SET_DATA:
        return {
          ...state,
         moviesData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  