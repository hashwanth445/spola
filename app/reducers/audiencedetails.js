import {
    AUDIENCEDETAILS_GET_CHECK_SUCCESS,
    AUDIENCEDETAILS_POST_CHECK_SUCCESS,
    AUDIENCEDETAILS_UPDATE_CHECK_SUCCESS,
    AUDIENCEDETAILS_DELETE_CHECK_SUCCESS,
    AUDIENCEDETAILS_SET_DATA,
  } from '../actions/actionTypes';
  
  const initialState = {
   audiencedetailsGetCheckSuccess: false,
   audiencedetailsData: [],
   audiencedetailsPostCheckSuccess: false,
   audiencedetailsUpdateCheckSuccess: false,
   audiencedetailsDeleteCheckSuccess: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case AUDIENCEDETAILS_GET_CHECK_SUCCESS:
        return {
          ...state,
         audiencedetailsGetCheckSuccess: action.payload,
        };
        case AUDIENCEDETAILS_POST_CHECK_SUCCESS:
        return {
          ...state,
         audiencedetailsPostCheckSuccess: action.payload,
        };
        case AUDIENCEDETAILS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
         audiencedetailsUpdateCheckSuccess: action.payload,
        };
        case AUDIENCEDETAILS_DELETE_CHECK_SUCCESS:
            return {
              ...state,
             audiencedetailsDeleteCheckSuccess: action.payload,
            };
        
      case AUDIENCEDETAILS_SET_DATA:
        return {
          ...state,
         audiencedetailsData: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  