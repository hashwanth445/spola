import React, {useState} from 'react';
import {View, TouchableOpacity, StatusBar, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {useSelector} from 'react-redux';
import {BaseColor, useTheme, useFont} from '@config';
import {useTranslation} from 'react-i18next';
import {Icon, Image} from '@components';
import {FloatingMenu} from 'react-native-floating-action-menu';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import {Neomorph, NeomorphBlur} from 'react-native-neomorph-shadows';
import {
  Menu,
  Button,
  VStack,
  Select,
  CheckIcon,
  Center,
  NativeBaseProvider,
} from 'native-base';
import {style, width} from 'styled-system';
import styles from 'react-native-floating-action-menu/dist/src/styles';
import {faBars, faTimes, faUserPlus} from '@fortawesome/free-solid-svg-icons';
// import { NeuView } from 'react-native-neu-element';
/* Stack Screen */
import * as Utils from '@utils';
import axios from 'axios';
import {
  VESDK,
  VideoEditorModal,
  Configuration,
} from 'react-native-videoeditorsdk';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

import AboutUs from '@screens/AboutUs';

import SpolaScreen from '../screens/SpolaScreen';
import Messages from '@screens/Messages';
import Notification from '@screens/Notification';
import Walkthrough from '@screens/Walkthrough';
import SignUp from '@screens/SignUp';
import SignIn from '@screens/SignIn';
import ResetPassword from '@screens/ResetPassword';
import ChangePassword from '@screens/ChangePassword';
import OnboardingScreenOneProfileDetails from '@screens/OnboardingScreenOneProfileDetails';
import OnboardingScreenThreeProfilePic from '@screens/OnboardingScreenThreeProfilePic';
import OnboardingScreenSevenCreateChannel from '@screens/OnboardingScreenSevenCreateChannel';


import ChangeLanguage from '@screens/ChangeLanguage';
import ContactUs from '@screens/ContactUs';
import PostDetail from '@screens/PostDetail';
import Setting from '@screens/Setting';
import ThemeSetting from '@screens/ThemeSetting';
import NotFound from '@screens/NotFound';
// /* Bottom Screen */
import Home from '@screens/Home';
import VideoUploadingScreen from '../screens/VideoUploadingScreen';
import VideoUploading from '../screens/VideoUploading';
import Messenger from '@screens/Messenger';
import Post from '@screens/Post';
import VerifyOTP from '@screens/VerifyOTP';
import ForgotPassword from '@screens/ForgotPassword';
import OnboardingScreenTwoGender from '@screens/OnboardingScreenTwoGender';
import OnboardingScreenFourSelectCategories from '@screens/OnboardingScreenFourSelectCategories';
import OnboardingScreenFiveFollowCretors from '@screens/OnboardingScreenFiveFollowCretors';
import OnboardingScreenSixFollowChannels from '@screens/OnboardingScreenSixFollowChannels';
import ThankyouScreen from '@screens/ThankyouScreen';
import Otp from '@screens/Otp';
import AudioRoom from '@screens/AudioRoom';
import RoomWithFriends from '@screens/RoomWithFriends';
import Rewards from '@screens/Rewards';
import VideoEditor from '@screens/VideoEditor';
import CreateChannel from '@screens/CreateChannel';
import Followers from '@screens/Followers';
import Following from '@screens/Following';
import Subscribers from '@screens/Subscribers';
import ImagePicker from 'react-native-image-crop-picker';
import {PermissionsAndroid} from 'react-native';
import Comments from '@screens/Comments';
import UserFollowers from '@screens/UserFollowers';
import UserFollowing from '@screens/UserFollowing';
import RewardedVideoPlayer from '@screens/RewardedVideoPlayer';
import Viewer from '../screens/Home/Viewer'
import {uploadFilesPostRequest} from '../api/uploadApi';
import SearchHistory from '@screens/SearchHistory';
import HomeDetail from '@screens/HomeDetail';
import ProductDetail from '@screens/ProductDetail';
import createFeed from '@screens/CreateFeed';
import SpolaComments from '@screens/SpolaComments';
import SpolaScreen1 from '@screens/SpolaScreen1';
import Golive from '../screens/Golive';
import Carts from '@screens/Carts';
import Orders from '../screens/Orders';
import Address from '../screens/Address';
import LoggedInUserProfile from '@screens/LoggedInUserProfile';
import CommonProfile from '@screens/CommonProfile';
import CreateSpola from '../screens/CreateSpola';

const MainStack = createStackNavigator();
const BottomTab = createBottomTabNavigator();
const Stack = createStackNavigator();

export default function Main() {
  return (
    <MainStack.Navigator
      headerMode="none"
      initialRouteName="BottomTabNavigator">
      <MainStack.Screen
        tabBarOptions={{
          style: {
            backgroundColor: 'red',
            height: Utils.scaleWithPixel(80),
            width: '100%',
          },
        }}
        name="BottomTabNavigator"
        component={BottomTabNavigator}
      />
      <MainStack.Screen name="ProductDetail" component={ProductDetail} />
      {/* <MainStack.Screen name="CheckOut" component={CheckOut} /> */}
      <MainStack.Screen name="Otp" component={Otp} />
      <MainStack.Screen name="Address" component={Address} />
      <MainStack.Screen
        name="VideoUploadingScreen"
        component={VideoUploadingScreen}
      />
      <MainStack.Screen
        name="RewardedVideoPlayer"
        component={RewardedVideoPlayer}
      />
      <MainStack.Screen name="Orders" component={Orders} />
      <MainStack.Screen name="Carts" component={Carts} />
      <MainStack.Screen name="UserFollowing" component={UserFollowing} />
      <MainStack.Screen name="VideoEditor" component={VideoEditor} />
      <MainStack.Screen name="LoggedInUserProfile" component={LoggedInUserProfile} />
      <MainStack.Screen name="Messenger" component={Messenger} />
      <MainStack.Screen name="CommonProfile" component={CommonProfile} />
      <MainStack.Screen name="CreateChannel" component={CreateChannel} />
      <MainStack.Screen name="SpolaScreen" component={SpolaScreen} />
      <MainStack.Screen name="Golive" component={Golive} />
      <MainStack.Screen name="SpolaComments" component={SpolaComments} />
      <MainStack.Screen name="createFeed" component={createFeed} />
      <MainStack.Screen name="Messages" component={Messages} />
      <MainStack.Screen name="Notification" component={Notification} />
      <MainStack.Screen name="Walkthrough" component={Walkthrough} />
      <MainStack.Screen name="SignUp" component={SignUp} />
      <MainStack.Screen name="OnboardingScreenThreeProfilePic" component={OnboardingScreenThreeProfilePic} />
      <MainStack.Screen name="OnboardingScreenSevenCreateChannel" component={OnboardingScreenSevenCreateChannel} />

      <MainStack.Screen name="SignIn" component={SignIn} />
      <MainStack.Screen name="VerifyOTP" component={VerifyOTP} />
      <MainStack.Screen name="OnboardingScreenFourSelectCategories" component={OnboardingScreenFourSelectCategories} />
      <MainStack.Screen name="ForgotPassword" component={ForgotPassword} />
      <MainStack.Screen name="ResetPassword" component={ResetPassword} />
      <MainStack.Screen name="ChangePassword" component={ChangePassword} />
      <MainStack.Screen name="OnboardingScreenOneProfileDetails" component={OnboardingScreenOneProfileDetails} />
      <MainStack.Screen name="ChangeLanguage" component={ChangeLanguage} />
      <MainStack.Screen name="ContactUs" component={ContactUs} />
      <MainStack.Screen name="PostDetail" component={PostDetail} />
      <MainStack.Screen name="AboutUs" component={AboutUs} />
      <MainStack.Screen name="OnboardingScreenTwoGender" component={OnboardingScreenTwoGender} />
      <MainStack.Screen name="SearchHistory" component={SearchHistory} />
      <MainStack.Screen name="Viewer" component={Viewer} />
      <MainStack.Screen name="OnboardingScreenFiveFollowCretors" component={OnboardingScreenFiveFollowCretors} />
      <MainStack.Screen name="OnboardingScreenSixFollowChannels" component={OnboardingScreenSixFollowChannels} />
      <MainStack.Screen name="ThankyouScreen" component={ThankyouScreen} />

      <MainStack.Screen name="AudioRoom" component={AudioRoom} />
      <MainStack.Screen name="RoomWithFriends" component={RoomWithFriends} />
      <MainStack.Screen name="Rewards" component={Rewards} />
      <MainStack.Screen name="HomeDetail" component={HomeDetail} />
      <MainStack.Screen name="Following" component={Following} />
      <MainStack.Screen name="Followers" component={Followers} />
      <MainStack.Screen name="Subscribers" component={Subscribers} />
      <MainStack.Screen name="CreateSpola" component={CreateSpola} />
      <MainStack.Screen name="CreateFeed" component={createFeed} />
      <MainStack.Screen name="VideoUploading" component={VideoUploading} />
      <MainStack.Screen name="SpolaScreen1" component={SpolaScreen1} />
      <MainStack.Screen name="Setting" component={Setting} />
      <MainStack.Screen name="ThemeSetting" component={ThemeSetting} />
      <MainStack.Screen name="NotFound" component={NotFound} />
      <MainStack.Screen name="Comments" component={Comments} />
      <MainStack.Screen name="UserFollowers" component={UserFollowers} />
    </MainStack.Navigator>
  );
}


function BottomTabNavigator({navigation}) {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const font = useFont();
  const auth = useSelector(state => state.auth);
  const login = auth.login.success;
  const accessToken = useSelector(
    state => state.accessTokenReducer.accessToken,
  );
  const [image, setImage] = useState([]);
  const [loading, setLoading] = useState(false);

  console.log(login, 'navigation page');
 
  const [shouldOverlapWithTrigger] = useState(false);
  const [position, setPosition] = useState('auto');

  selectImage = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, response => {
      console.log(response.assets, 'all response', response.assets.uri);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        console.log(source);
        setImage(source);
      }
    });
  };
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write camera err', err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  console.log(image, 'image.......');
  const captureImage = async type => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      let formData = new FormData();
      ImagePicker.openPicker({
        multiple: true,
      }).then(images => {
        console.log('imgApi');

        toNextPage(images);
      });
    }
  };

  const toNextPage = async images => {
    console.log('imgApi2', images);
    let formData = new FormData();
    setLoading(true);

    console.log('lopala', images);

    formData.append('files', {
      name: images[0].modificationDate,
      type: images[0].mime,
      uri: images[0].path,
    });
    console.log(formData);

    let imgApi = await uploadFilesPostRequest(formData);
    console.log('imgApi', imgApi);
    setLoading(false);
    openEditor(imgApi);
  };
  const openEditor = url => {
    // this.props.navigation.navigate('VideoUploadingScreen', {url});
    VESDK.openEditor(url).then(
      result => {
        navigation.navigate('VideoUploadingScreen', {result});
        console.log('result', result);
      },
      error => {
        console.log(error);
      },
    );
  };
  const createFormData = (photo, body = {}) => {
    const data = new FormData();

    data.append('photo', {
      name: photo.fileName,
      type: photo.type,
      uri: Platform.OS === 'ios' ? photo.uri.replace('file://', '') : photo.uri,
    });

    Object.keys(body).forEach(key => {
      data.append(key, body[key]);
    });

    return data;
  };

  const captureImage2 = async type => {
    // let options = {
    //   mediaType: type,
    //   maxWidth: 300,
    //   maxHeight: 550,
    //   quality: 1,
    //   videoQuality: 'low',
    //   durationLimit: 30, //Video max duration in seconds
    //   saveToPhotos: true,
    // };
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      ImagePicker.openPicker({
        multiple: true,
      }).then(images => {
        let imgd = image.concat(images);
        setImage(imgd);
        if (imgd) {
          navigation.navigate('VideoUploading', {image: imgd});
          console.log(image);
        }
      });
    }
  };

  const navigatePopup = () => {};

  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      headerMode="none"
      tabBarOptions={{
        showIcon: true,

        showLabel: false,
        activeTintColor: colors.primary,
        inactiveTintColor: BaseColor.grayColor,
        style: {
          height: Utils.scaleWithPixel(60),

          borderRadius: 20,
          borderTopRightRadius: 40,
          borderTopLeftRadius: 40,
          backgroundColor: colors.neoThemebg,
        },

        labelStyle: {
          fontSize: 12,
          fontFamily: font,
        },
      }}>
      <BottomTab.Screen
        name="Home"
        component={Home}
        options={{
          title: t('home'),
          tabBarIcon: ({color}) => {
            return (
              <View
                style={{
                  marginBottom: Utils.scaleWithPixel(0),
                }}>
                  <TouchableOpacity onPress={() => navigation.navigate('Home')}> 
                  <NeomorphBlur
                    inner={false}
                    style={{
                      shadowRadius: 3,
                      borderRadius: 100,
                      backgroundColor: colors.neoThemebg,
                      width: 52,
                      height: 52,
                      shadowOpacity: 0.1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Neomorph
                      inner={true}
                      // darkShadowColor="#1d2024" // <- set this
                      // lightShadowColor="#2d4660"
                      darkShadowColor="#2d4660" // <- set this
                      lightShadowColor="#1d2024"
                      swapShadows="false"
                      style={{
                        shadowRadius: 10,
                        borderRadius: 100,
                        backgroundColor: colors.neoThemebg,
                        width: 51,
                        height: 51,
                        shadowOpacity: 0.2,
                        ShadowColor: 'black',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <IconSimple color="#808080" name="home" size={20} />
                      {/* <Image source={require('../assets/images2/home_icn.png')} style={{ height: 20, width: 20 }} /> */}
                    </Neomorph>
                  </NeomorphBlur>
                </TouchableOpacity>
              </View>
            );
          },
        }}
      />
      <BottomTab.Screen
        name="Booking"
        component={SpolaScreen1}
        options={{
          title: t('shorts'),

          tabBarIcon: ({color}) => {
            return (
              
            <View
                style={{
                  marginBottom: Utils.scaleWithPixel(0),
                }}>
                <TouchableOpacity onPress={() => navigation.navigate('SpolaScreen1')
                                    }>
                  <NeomorphBlur
                    inner={false}
                    style={{
                      shadowRadius: 3,
                      borderRadius: 100,
                      backgroundColor: colors.neoThemebg,
                      width: 52,
                      height: 52,
                      shadowOpacity: 0.1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Neomorph
                      inner={true}
                      // darkShadowColor="#1d2024" // <- set this
                      // lightShadowColor="#2d4660"
                      darkShadowColor="#2d4660" // <- set this
                      lightShadowColor="#1d2024"
                      swapShadows="false"
                      style={{
                        shadowRadius: 10,
                        borderRadius: 100,
                        backgroundColor: colors.neoThemebg,
                        width: 51,
                        height: 51,
                        shadowOpacity: 0.2,
                        ShadowColor: 'black',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <IconSimple color="#808080" name="energy" size={20} />

                      {/* <Image source={require('../assets/images2/reels_icn.png')} style={{ height: 20, width: 20 }} /> */}
                      {/* <Icon solid color="#2E2B2A" name="video" size={20} solid /> */}
                    </Neomorph>
                  </NeomorphBlur>
                </TouchableOpacity>
              </View>
            );
          },
        }}
      />

      <BottomTab.Screen
        name="welcome"
        component={VideoUploading}
        options={{
          title: t('create'),
          // name="VideoUploading"
          // component={VideoUploading}
          // options={{
          //   title: t('create'),
          tabBarIcon: ({color}) => {
            return (
              <View
                style={{
                  marginBottom: Utils.scaleWithPixel(0),
                }}>
                <TouchableOpacity>
                  <NeomorphBlur
                    inner={false}
                    style={{
                      shadowRadius: 3,
                      borderRadius: 100,
                      backgroundColor: colors.neoThemebg,
                      width: Utils.scaleWithPixel(62),
                      height: Utils.scaleWithPixel(62),
                      shadowOpacity: 0.1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Neomorph
                      inner={true}
                      darkShadowColor="#1d2024" // <- set this
                      lightShadowColor="#2d4660"
                      // darkShadowColor="#2d4660" // <- set this
                      // lightShadowColor="#1d2024"
                      swapShadows="false"
                      style={{
                        shadowRadius: 10,
                        borderRadius: 100,
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(60),
                        height: Utils.scaleWithPixel(60),
                        shadowOpacity: 0.2,
                        ShadowColor: 'black',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      
                      <NativeBaseProvider>
                        <Center flex={1} px="3">
                          <VStack space={6} alignSelf="flex-start" w="100%">
                            <Menu
                              style={{
                                padding: 0,
                                backgroundColor: 'rgba(105,105,105, 0.7)',
                                blurRadius: 90,
                                blurType: 'light',
                                blurAmount: 50,
                                alignItems: 'center',
                                borderWidth: 0,
                                borderRadius: 10,
                                width: 120,
                                top: 0,
                                left: -28,
                              }}
                              shouldOverlapWithTrigger={
                                shouldOverlapWithTrigger
                              } // @ts-ignore
                              placement={
                                position == 'auto' ? undefined : position
                              }
                              trigger={triggerProps => {
                                return (
                                  <Button
                                    style={{
                                      backgroundColor: 'transparent',
                                    }}
                                    {...triggerProps}>
                                    <Neomorph
                                      concave
                                      style={{
                                        width: Utils.scaleWithPixel(45),
                                        height: Utils.scaleWithPixel(45),
                                        borderRadius: Utils.scaleWithPixel(60),
                                        borderWidth: Utils.scaleWithPixel(2),
                                        borderColor: colors.neoThemeBorderColor,
                                        backgroundColor: colors.neoThemebg,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                      }}>
                                      <Neomorph
                                        inner={true}
                                        // darkShadowColor="#1d2024" // <- set this
                                        // lightShadowColor="#2d4660"
                                        darkShadowColor="#2aa9f9" // <- set this
                                        lightShadowColor="#83cfff"
                                        swapShadows="false"
                                        style={{
                                          shadowRadius: 10,
                                          borderRadius: 100,
                                          backgroundColor: '#2aa9f9',
                                          width: 51,
                                          height: 51,
                                          borderWidth: 0,
                                          borderColor: '#b1d9f2',
                                          shadowOpacity: 0.2,
                                          justifyContent: 'center',
                                          alignItems: 'center',
                                        }}>
                                        {/* <Icon solid color="#e2e2e2" name="plus-thick" size={20} solid /> */}
                                        {/* <Image source={require('../assets/images2/videocam_icn.png')} style={{ height: 20, width: 24 }} /> */}
                                        <IconSimple
                                          color="#fff"
                                          name="menu"
                                          size={20}
                                        />
                                      </Neomorph>
                                    </Neomorph>
                                  </Button>
                                );
                              }}>
                              <Menu.Item
                                onPress={() => {
                                  accessToken === null
                                    ? navigation.navigate('SignIn')
                                    : navigation.navigate('Golive');
                                }}>
                                <View
                                  style={{
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    padding: 6,
                                  }}>
                                  {/* <Image source={require('../assets/images2/startlive_icn.png')}
                                  onPress={() => { navigation.navigate('VideoEditor') }} style={{ height: 13, width: 20, left: -5, top: 0 }} /> */}
                                  <IconSimple
                                    color="#fff"
                                    name="screen-smartphone"
                                    size={12}
                                    style={{left: -5, top: 2}}
                                  />

                                  <Text style={{color: '#d8d8d8'}}>
                                    Go live
                                  </Text>
                                </View>
                              </Menu.Item>

                              <View
                                style={{
                                  height: 1,
                                  width: 85,
                                  left: 3,
                                  backgroundColor: 'rgb(192,192,192)',
                                }}></View>

                              <Menu.Item
                                onPress={() => {
                                  accessToken === null
                                    ? navigation.navigate('SignIn')
                                    : navigation.navigate('CreateSpola');
                                }}>
                                <View
                                  style={{
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    padding: 6,
                                  }}>
                                  {/* <Image source={require('../assets/images2/addshorts_icn.png')}
                                  onPress={() => { navigation.navigate('Profile') }} style={{ height: 16, width: 16, left: -5, top: 0 }} /> */}
                                  <IconSimple
                                    color="#fff"
                                    name="disc"
                                    size={12}
                                    style={{left: -5, top: 2}}
                                  />

                                  <Text style={{color: '#d8d8d8'}}>Spola</Text>
                                </View>
                              </Menu.Item>

                              <View
                                style={{
                                  height: 1,
                                  width: 85,
                                  left: 3,
                                  backgroundColor: 'rgb(192,192,192)',
                                }}></View>

                              <Menu.Item
                                onPress={() => {
                                  accessToken === null
                                    ? navigation.navigate('SignIn')
                                    : navigation.navigate('VideoUploadingScreen');
                                }}>
                                <View
                                  style={{
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    padding: 6,
                                  }}>
                                  {/* <Image source={require('../assets/images2/cameraimage_icn.png')}
                                  onPress={() => { navigation.navigate('VideoEditor') }} style={{ height: 18, width: 18, left: -5, top: 0 }} /> */}
                                  <IconSimple
                                    color="#fff"
                                    name="cloud-upload"
                                    size={12}
                                    style={{left: -5, top: 2}}
                                  />

                                  <Text style={{color: '#d8d8d8'}}>Upload</Text>
                                </View>
                              </Menu.Item>

                              <View
                                style={{
                                  height: 1,
                                  width: 85,
                                  left: 3,
                                  backgroundColor: 'rgb(192,192,192)',
                                }}></View>

                              <Menu.Item
                                onPress={() => {
                                  accessToken === null
                                    ? navigation.navigate('SignIn')
                                    : navigation.navigate('CreateFeed');
                                }}>
                                <View
                                  style={{
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    padding: 6,
                                  }}>
                                  {/* <Image source={require('../assets/images2/video_icn.png')}
                                  onPress={() => { navigation.navigate('Post') }} style={{ height: 12, width: 18, left: -5, top: 2 }} /> */}
                                  <IconSimple
                                    color="#fff"
                                    name="plus"
                                    size={12}
                                    style={{left: -5, top: 2}}
                                  />

                                  <Text
                                    style={{
                                      textAlign: 'center',
                                      color: 'white',
                                    }}>
                                    Post
                                  </Text>
                                </View>
                              </Menu.Item>
                            </Menu>
                          </VStack>
                        </Center>
                      </NativeBaseProvider>
                    </Neomorph>
                  </NeomorphBlur>
                </TouchableOpacity>

                
              </View>
            );
          },
        }}
        listeners={({navigation}) => ({
          tabPress: e => {
            e.preventDefault();
            navigation.navigate('CreateNew');
          },
        })}
      />
      <BottomTab.Screen
        name="Post"
        component={accessToken === null ? SignIn : Post}
        options={{
          title: t('news feed'),
          tabBarIcon: ({color}) => {
            return (
              <View
                style={{
                  marginBottom: Utils.scaleWithPixel(0),
                }}>
                <TouchableOpacity onPress={() => navigation.navigate('Post')}> 
                  <NeomorphBlur
                    inner={false}
                    style={{
                      shadowRadius: 3,
                      borderRadius: 100,
                      backgroundColor: colors.neoThemebg,
                      width: 52,
                      height: 52,
                      shadowOpacity: 0.1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Neomorph
                      inner={true}
                      // darkShadowColor="#1d2024" // <- set this
                      // lightShadowColor="#2d4660"
                      darkShadowColor="#2d4660" // <- set this
                      lightShadowColor="#1d2024"
                      swapShadows="false"
                      style={{
                        shadowRadius: 10,
                        borderRadius: 100,
                        backgroundColor: colors.neoThemebg,
                        width: 51,
                        height: 51,
                        shadowOpacity: 0.2,
                        ShadowColor: 'black',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      {/* <Icon color="#2E2B2A" name="file-multiple" size={20} solid /> */}
                      {/* <Image source={require('../assets/images2/tv_icn.png')} style={{ height: 18, width: 18 }} /> */}
                      <IconSimple color="#808080" name="feed" size={20} />
                    </Neomorph>
                  </NeomorphBlur>
                </TouchableOpacity>
              </View>
            );
          },
        }}
      />
      <BottomTab.Screen
        name="LoggedInUserProfile"
        component={accessToken === null ? SignIn : LoggedInUserProfile}
        options={{
          tabBarVisible: accessToken === null ? false : true,
          title: t('account'),
          tabBarIcon: ({color}) => {
            return (
              <View
                style={{
                  marginBottom: Utils.scaleWithPixel(0),
                }}>
                <TouchableOpacity onPress={() => {accessToken === null ? navigation.navigate('SignIn') : navigation.navigate('LoggedInUserProfile')}
                }> 
                <NeomorphBlur
                  inner={false}
                  style={{
                    shadowRadius: 3,
                    borderRadius: 100,
                    backgroundColor: colors.neoThemebg,
                    width: 52,
                    height: 52,
                    shadowOpacity: 0.1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Neomorph
                    inner={true}
                    // darkShadowColor="#1d2024" // <- set this
                    // lightShadowColor="#2d4660"
                    darkShadowColor="#2d4660" // <- set this
                    lightShadowColor="#1d2024"
                    swapShadows="false"
                    style={{
                      shadowRadius: 10,
                      borderRadius: 100,
                      backgroundColor: colors.neoThemebg,
                      width: 51,
                      height: 51,
                      shadowOpacity: 0.2,
                      ShadowColor: 'black',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {/* <Icon solid color="#2E2B2A" name="account-circle" size={25} style={{ alignItems: "center" }} /> */}
                    {/* <Image source={require('../assets/images2/profile_icn.png')} style={{ height: 20, width: 16 }} /> */}
                    <IconSimple color="#808080" name="user" size={20} />
                  </Neomorph>
                </NeomorphBlur>
                </TouchableOpacity>
              </View>
            );
          },
        }}
      />
    </BottomTab.Navigator>
  );
}
