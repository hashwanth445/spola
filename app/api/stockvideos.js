import{ 
    stockvideosGetCheckSuccess,
    stockvideosSetData,
    stockvideosPostCheckSuccess,
    stockvideosUpdateCheckSuccess,
    stockvideosDeleteCheckSuccess,
    
      } from '../actions';
      import axios from 'axios';
      let a = [];
      
      export function stockvideosGetRequest(element,token) {
      return dispatch => {
        dispatch(stockvideosGetCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `stock-videos-settings`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            // console.log(data)
            // console.log(element,"api data",data.data[0])
            // const propertyValues = Object.values(data.data);
            // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            // a.push(obj)
            dispatch(stockvideosSetData(data));
            dispatch(stockvideosGetCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,'furnishings')
            dispatch(stockvideosGetCheckSuccess(false));
          });
      };
      };
      
      export function stockvideosPostRequest(element) {
          return dispatch => {
            dispatch(stockvideosPostCheckSuccess(false));
            const axiosLogin = axios.create();
              return axiosLogin({
                method: 'post',
                url: `stock-videos-settings`,
                headers:{
                  // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                  "Content-Type" : "application/json"}
               
                })
              .then(response => {
                if (response.status !== 200) {
                  throw Error(response.statusText);
                }
                return response.data;
              })
              .then(data => {
                const propertyValues = Object.values(data.data);
                const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                a.push(obj)
                dispatch(stockvideosSetData(a));
                dispatch(stockvideosPostCheckSuccess(true));
              })
              .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(stockvideosPostCheckSuccess(false));
               
              });
          };
      }
      
      export function stockvideosUpdateRequest(element) {
          console.log(element.accessToken)
          const accessToken = element.accessToken;
          delete element.accessToken;
          return dispatch => {
              dispatch(stockvideosSetData(element));
              dispatch(stockvideosUpdateCheckSuccess(false));
              const axiosLogin = axios.create();
              return axiosLogin({
                  method: 'put',
                  url: `stock-videos-settings/610ceec93be536107bcb5c80`,
                  headers:{
                  Authorization: `Bearer ${accessToken}`,
                  "Content-Type" : "application/json"},
                  data:element,
                  })
              .then(response => {
                  // console.log(response)
                  if (response.status !== 200) {
                  throw Error(response.statusText);
                  }
                  return response.data;
              })
              .then(data => {
                  // console.log("data",data)
                  dispatch(stockvideosSetData(a));
                  dispatch(stockvideosUpdateCheckSuccess(true));
              })
              .catch((error) => {
                  console.log(error,axiosLogin)
                  dispatch(stockvideosUpdateCheckSuccess(false));
                  
              });
          };
      }
    
      export function stockvideosDeleteRequest(element) {
          return dispatch => {
            dispatch(stockvideosDeleteCheckSuccess(false));
            const axiosLogin = axios.create();
              return axiosLogin({
                method: 'delete',
                url: `stock-videos-settings/610ceecc3be536107bcb5c81`,
                headers:{
                  // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                  "Content-Type" : "application/json"}
               
                })
              .then(response => {
                if (response.status !== 200) {
                  throw Error(response.statusText);
                }
                return response.data;
              })
              .then(data => {
                const propertyValues = Object.values(data.data);
                const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                a.push(obj)
                dispatch(stockvideosSetData(a));
                dispatch(stockvideosDeleteCheckSuccess(true));
              })
              .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(stockvideosDeleteCheckSuccess(false));
               
              });
          };
      }