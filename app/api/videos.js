import {
  videosGetCheckSuccess,
  videosSetData,
  videosSearchSetData,
  videosPostCheckSuccess,
  videosUpdateCheckSuccess,
  videosDeleteCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

export function videosGetRequest(element, token) {
  console.log('videos without auth enter ----------------');

  return dispatch => {
    dispatch(videosGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `videos/`,
      headers: {
        Authorization: ``,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log(response, 'videos without auth enter');
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data, 'data');

        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(videosSetData(data));
        dispatch(videosGetCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'videos without auth catch------');
        dispatch(videosGetCheckSuccess(false));
      });
  };
}

export function videosSearchRequest(element) {
  console.log('videos without auth enter ----------------');

  return dispatch => {
    dispatch(videosGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `videos/`,
      headers: {
        Authorization: ``,
        'Content-Type': 'application/json',
      },
      params:element
    })
      .then(response => {
        console.log(response, 'videos without auth enter');
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data, 'data');

        dispatch(videosSearchSetData(data));
        dispatch(videosGetCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'videos without auth catch------');
        dispatch(videosGetCheckSuccess(false));
      });
  };
}

export function videosPostRequest(videoApi, title, description, userData) {
  const dataObject = {};
  dataObject.backBlazeVideoUrl = videoApi[0].url;
  dataObject.title = title;
  dataObject.description = description;
  dataObject.uploadedBy = userData?.user?.id;

  console.log('object inside', dataObject);
  return dispatch => {
    dispatch(videosPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `videos`,
      data: dataObject,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log('response inside api', response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data);
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(videosSetData(data));
        dispatch(videosPostCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(videosPostCheckSuccess(true));
      });
  };
}

export function videosUpdateRequest(element) {
  console.log(element.accessToken);
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(videosSetData(element));
    dispatch(videosUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `videos/611bb1274161ced881fda01c`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(videosSetData(a));
        dispatch(videosUpdateCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(videosUpdateCheckSuccess(false));
      });
  };
}

export function videosDeleteRequest(element) {
  return dispatch => {
    dispatch(videosDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `videos/611bb1274161ced881fda01c`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        const propertyValues = Object.values(data.data);
        const obj = {
          categoryID: element.id,
          categoryName: element.name,
          categoryImage: element.icon,
          subCategory: propertyValues,
        };
        a.push(obj);
        dispatch(videosSetData(a));
        dispatch(videosDeleteCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(videosDeleteCheckSuccess(false));
      });
  };
}
