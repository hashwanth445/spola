import{ 
    categoriesGetCheckSuccess,
    categoriesSetData,
    categoriesPostCheckSuccess,
    categoriesUpdateCheckSuccess,
    categoriesDeleteCheckSuccess,
    creatorsSetData,

    } from '../actions';
    import axios from 'axios';
    // import { useDispatch, useSelector } from 'react-redux';
  
    let a = [];
    // const userToken =  useSelector(state => state.accessTokenReducer.accessToken)
    export function categoriesGetRequest() {
      console.log("get category api hit");
    return dispatch => {
      dispatch(categoriesGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `categories`,
          headers:{
            //  Authorization: `Bearer ${userToken}`,
            // Authorization:`Bearer ${userToken}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          console.log("get cate from api",response)
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
           console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          // category.map(item => { 
          
          // return {
          //   ...item,
          //   checked: false,
          // };
        
      // })
          dispatch(categoriesSetData(data));
          dispatch(categoriesGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(categoriesGetCheckSuccess(false));
        });
    };
    };
    
    export function categoriesPostRequest(element) {
        return dispatch => {
          dispatch(categoriesPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `categories`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(categoriesSetData(a));
              dispatch(categoriesPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(categoriesPostCheckSuccess(false));
             
            });
        };
    }
    
    export function categoriesUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(categoriesSetData(element));
            dispatch(categoriesUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `categories/610cf7783be536107bcb5c96`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(categoriesSetData(a));
                dispatch(categoriesUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(categoriesUpdateCheckSuccess(false));
                
            });
        };
    }

    export function categoriesDeleteRequest(element) {
        return dispatch => {
          dispatch(categoriesDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `categories/610cf78a3be536107bcb5c98`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(categoriesSetData(a));
              dispatch(categoriesDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(categoriesDeleteCheckSuccess(false));
             
            });
        };
    }
    // users get for creaters
    export function creatorsGetRequest(token) {
      console.log("get creators api hit");
    return dispatch => {
      // dispatch(categoriesGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `users`,
          headers:{
            //  Authorization: `Bearer ${userToken}`,
            Authorization:``,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          console.log("get creators from api",response)
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          console.log("response crestors data",data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(creatorsSetData(data));
          // dispatch(categoriesGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          // dispatch(categoriesGetCheckSuccess(false));
        });
    };
    };