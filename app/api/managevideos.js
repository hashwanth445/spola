import{ 
    managevideosGetCheckSuccess,
    managevideosSetData,
    managevideosPostCheckSuccess,
    managevideosUpdateCheckSuccess,
    managevideosDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function managevideosGetRequest(element,token) {
    return dispatch => {
      dispatch(managevideosGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `manage-videos`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(managevideosSetData(data));
          dispatch(managevideosGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(managevideosGetCheckSuccess(false));
        });
    };
    };
    
    export function managevideosPostRequest(element) {
        return dispatch => {
          dispatch(managevideosPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `manage-videos`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(managevideosSetData(a));
              dispatch(managevideosPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(managevideosPostCheckSuccess(false));
             
            });
        };
    }
    
    export function managevideosUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(managevideosSetData(element));
            dispatch(managevideosUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `manage-videos/610ce48a3be536107bcb5c58`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(managevideosSetData(a));
                dispatch(managevideosUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(managevideosUpdateCheckSuccess(false));
                
            });
        };
    }

    export function managevideosDeleteRequest(element) {
        return dispatch => {
          dispatch(managevideosDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `manage-videos/610ce48c3be536107bcb5c59`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(managevideosSetData(a));
              dispatch(managevideosDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(managevideosDeleteCheckSuccess(false));
             
            });
        };
    }