import{ 
    paidsubscriberssettingGetCheckSuccess,
    paidsubscriberssettingSetData,
    paidsubscriberssettingPostCheckSuccess,
    paidsubscriberssettingUpdateCheckSuccess,
    paidsubscriberssettingDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function paidsubscriberssettingGetRequest(element,token) {
    return dispatch => {
      dispatch(paidsubscriberssettingGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `paid-subscribers-settings`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(paidsubscriberssettingSetData(data));
          dispatch(paidsubscriberssettingGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(paidsubscriberssettingGetCheckSuccess(false));
        });
    };
    };
    
    export function paidsubscriberssettingPostRequest(element) {
        return dispatch => {
          dispatch(paidsubscriberssettingPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `paid-subscribers-settings`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(paidsubscriberssettingSetData(a));
              dispatch(paidsubscriberssettingPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(paidsubscriberssettingPostCheckSuccess(false));
             
            });
        };
    }
    
    export function paidsubscriberssettingUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(paidsubscriberssettingSetData(element));
            dispatch(paidsubscriberssettingUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `paid-subscribers-settings/610ce5cc3be536107bcb5c5f`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(paidsubscriberssettingSetData(a));
                dispatch(paidsubscriberssettingUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(paidsubscriberssettingUpdateCheckSuccess(false));
                
            });
        };
    }

    export function paidsubscriberssettingDeleteRequest(element) {
        return dispatch => {
          dispatch(paidsubscriberssettingDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `paid-subscribers-settings/610ce5cd3be536107bcb5c60`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(paidsubscriberssettingSetData(a));
              dispatch(paidsubscriberssettingDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(paidsubscriberssettingDeleteCheckSuccess(false));
             
            });
        };
    }