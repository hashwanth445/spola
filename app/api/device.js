import{ 
    deviceGetCheckSuccess,
    deviceSetData,
    devicePostCheckSuccess,
    deviceUpdateCheckSuccess,
    deviceDeleteCheckSuccess,
    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function deviceGetRequest(userId) {
        const objectValue = {};
        objectValue.userId = userId;
    return dispatch => {
      dispatch(deviceGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `devices`,
          params:objectValue,
          headers:{
              // Authorization: ``,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"user data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(deviceSetData(data));
          dispatch(deviceGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(deviceGetCheckSuccess(false));
        });
    };
    };   
    export function devicePostRequest(objectValue) {
        return dispatch => {
          dispatch(devicePostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `devices`,
              data:objectValue,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              console.log("response",response);
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              console.log("data",data);
              // const propertyValues = Object.values(data.data);
              // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              // a.push(obj)
              dispatch(deviceSetData(data));
              dispatch(devicePostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(devicePostCheckSuccess(false));
             
            });
        };
    }
    
    export function deviceUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(deviceSetData(element));
            dispatch(deviceUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `devices`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(deviceSetData(data));
                dispatch(deviceUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(deviceUpdateCheckSuccess(false));
                
            });
        };
    }
  
    export function deviceDeleteRequest(id,objectValue) {
        return dispatch => {
          dispatch(deviceDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `devices/${id}`,
              data:objectValue,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              // const propertyValues = Object.values(data.data);
              // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              // a.push(obj)
              dispatch(deviceSetData(data));
              dispatch(deviceDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(deviceDeleteCheckSuccess(false));
             
            });
        };
    }