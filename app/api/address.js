import{ 
    addressGetCheckSuccess,
    addressSetData,
    addressPostCheckSuccess,
    addressUpdateCheckSuccess,
    addressDeleteCheckSuccess,
    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function addressGetRequest(userId) {
        const objectValue = {};
        objectValue.userId = userId;
    return dispatch => {
      dispatch(addressGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `addresses`,
          params:objectValue,
          headers:{
              // Authorization: ``,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"user data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(addressSetData(data));
          dispatch(addressGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(addressGetCheckSuccess(false));
        });
    };
    };   
    export function addressPostRequest(objectValue) {
        return dispatch => {
          dispatch(addressPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `addresses`,
              data:objectValue,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              console.log("response",response);
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              console.log("data",data);
              // const propertyValues = Object.values(data.data);
              // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              // a.push(obj)
              dispatch(addressSetData(data));
              dispatch(addressPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(addressPostCheckSuccess(false));
             
            });
        };
    }
    
    export function addressUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(addressSetData(element));
            dispatch(addressUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `address`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(addressSetData(data));
                dispatch(addressUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(addressUpdateCheckSuccess(false));
                
            });
        };
    }
  
    export function addressDeleteRequest(id,objectValue) {
        return dispatch => {
          dispatch(addressDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `address/${id}`,
              data:objectValue,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              // const propertyValues = Object.values(data.data);
              // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              // a.push(obj)
              dispatch(addressSetData(data));
              dispatch(addressDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(addressDeleteCheckSuccess(false));
             
            });
        };
    }