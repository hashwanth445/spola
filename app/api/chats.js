import{ 
    chatsGetCheckSuccess,
    chatsSetData,
    chatsPostCheckSuccess,
    chatsUpdateCheckSuccess,
    chatsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function chatsGetRequest(element,token) {
    return dispatch => {
      dispatch(chatsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `chats`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(chatsSetData(data));
          dispatch(chatsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(chatsGetCheckSuccess(false));
        });
    };
    };
    
    export function chatsPostRequest(element) {
        return dispatch => {
          dispatch(chatsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `chats`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(chatsSetData(a));
              dispatch(chatsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(chatsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function chatsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(chatsSetData(element));
            dispatch(chatsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `chats/611bb1b84161ced881fda01f`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(chatsSetData(a));
                dispatch(chatsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(chatsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function chatsDeleteRequest(element) {
        return dispatch => {
          dispatch(chatsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `chats/611bb1a94161ced881fda01e`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(chatsSetData(a));
              dispatch(chatsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(chatsDeleteCheckSuccess(false));
             
            });
        };
    }