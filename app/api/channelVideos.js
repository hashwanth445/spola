import {
  channelVideoGetCheckSuccess,
  channelVideoGetCheckSuccessForUser,
  channelVideoSetData,
  channelVideoSetDataForReward,
  channelVideoPostCheckSuccess,
  channelVideoPostCheckSuccessForReward,
  channelVideoUpdateCheckSuccess,
  channelVideoDeleteCheckSuccess,
  channelVideoSetDataForUser,
} from '../actions';
import axios from 'axios';
let a = [];

export function channelVideoGetRequest(element, token) {
  console.log("channels get request");
  return dispatch => {
    dispatch(channelVideoGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `channel-videos`,
      headers: {
        Authorization: ``,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("get channels from api", response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(channelVideoSetData(data));
        dispatch(channelVideoGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(channelVideoGetCheckSuccess(false));
      });
  };
};
export function channelVideoGetRequestForUser(userId) {
  console.log("channels video get request");
  const dataObject = {};
  dataObject.channelCreatedBy = userId;
  return dispatch => {
    dispatch(channelVideoGetCheckSuccessForUser(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `channel-videos`,
      params: dataObject,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("get channels from api", response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(channelVideoSetDataForUser(data));
        dispatch(channelVideoGetCheckSuccessForUser(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(channelVideoGetCheckSuccessForUser(false));
      });
  };
};

export function channelVideoPostRequest(selectedValue, videoApi) {
  const dataObject = {}
  dataObject.channel = selectedValue;
  dataObject.uploadedVideo = videoApi[0]?.id;
  console.log("dataObject", dataObject);
  return dispatch => {
    dispatch(channelVideoPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `channel-videos`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        "Content-Type": "application/json"
      },
      data: dataObject

    })
      .then(response => {
        console.log("response", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(channelVideoSetData(data));
        dispatch(channelVideoPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(channelVideoPostCheckSuccess(false));

      });
  };
}

export function channelVideoPostRequestForReward(selectedValue, videoApi) {
  const dataObject = {}
  dataObject.channel = selectedValue;
  dataObject.postType = 'rewardedVideo'
  dataObject.rewardVideo = videoApi[0]?.id;
  console.log("dataObject", dataObject);
  return dispatch => {
    dispatch(channelVideoPostCheckSuccessForReward(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `channel-videos`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        "Content-Type": "application/json"
      },
      data: dataObject

    })
      .then(response => {
        console.log("response", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(channelVideoSetDataForReward(data));
        dispatch(channelVideoPostCheckSuccessForReward(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(channelVideoPostCheckSuccessForReward(false));

      });
  };
}

export function channelVideoUpdateRequest(element) {
  console.log(element.accessToken)
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(channelVideoSetData(element));
    dispatch(channelVideoUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `channel-videos/611baca84161ced881fda003`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(channelVideoSetData(a));
        dispatch(channelVideoUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(channelVideoUpdateCheckSuccess(false));

      });
  };
}

export function channelVideoDeleteRequest(element) {
  return dispatch => {
    dispatch(channelVideoDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `channel-videos/611bac954161ced881fda002`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        const propertyValues = Object.values(data.data);
        const obj = { categoryID: element.id, categoryName: element.name, categoryImage: element.icon, subCategory: propertyValues }
        a.push(obj)
        dispatch(channelVideoSetData(a));
        dispatch(channelVideoDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(channelVideoDeleteCheckSuccess(false));

      });
  };
}