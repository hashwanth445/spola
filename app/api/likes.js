import {
  likesGetCheckSuccess,
  likesSetData,
  likesPostCheckSuccess,
  likesUpdateCheckSuccess,
  likesDeleteCheckSuccess,
  likesGetCheckSuccessByUser,
  likesSetDataByUser,
  likesGetCheckSuccessForFeed,
  likesSetDataForFeed,
  likesGetCheckSuccessForShort,
  likesSetDataForShort,
} from '../actions';
import axios from 'axios';
let a = [];

export function likesGetRequest(element, token) {
  return dispatch => {
    dispatch(likesGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `likes`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(likesSetData(data));
        dispatch(likesGetCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings');
        dispatch(likesGetCheckSuccess(false));
      });
  };
}
export function likesGetRequestByUser(userId, videoId) {
  const objectvalue = {};
  objectvalue.likedBy = userId;
  objectvalue.likedUploadVideo = videoId;
  console.log('objectvalue', objectvalue);
  return dispatch => {
    dispatch(likesGetCheckSuccessByUser(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `likes`,
      params: objectvalue,
      hepaders: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log("response inside",response.data)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log('data from like single user', data);
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(likesSetDataByUser(data));
        dispatch(likesGetCheckSuccessByUser(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings');
        dispatch(likesGetCheckSuccessByUser(false));
      });
  };
}
export function likesGetRequestForFeed(userId, feedId) {
  const objectvalue = {};
  objectvalue.likedBy = userId;
  objectvalue.likeFeed = feedId;
  console.log('objectvalue', objectvalue);
  return dispatch => {
    dispatch(likesGetCheckSuccessForFeed(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `likes`,
      params: objectvalue,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log('data fromlike single user', data);
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(likesSetDataForFeed(data));
        dispatch(likesGetCheckSuccessForFeed(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings');
        dispatch(likesGetCheckSuccessForFeed(false));
      });
  };
}


export function likesGetRequestForShort(userId, shortId) {
  const objectvalue = {};
  objectvalue.likedBy = userId;
  objectvalue.likedShort = shortId;
  console.log('objectvalue', objectvalue);
  return dispatch => {
    dispatch(likesGetCheckSuccessForShort(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `likes`,
      params: objectvalue,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log('data fromlike single user', data);
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(likesSetDataForShort(data));
        dispatch(likesGetCheckSuccessForShort(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings');
        dispatch(likesGetCheckSuccessForShort(false));
      });
  };
}

export function likesPostRequest(objectValue) {
  const likeObeject = {};
  likeObeject.likedBy = objectValue.likedBy;

  if (objectValue.postType == 'feed') {
    likeObeject.likeFeed = objectValue.likeFeed;
    likeObeject.postType = objectValue.postType;
  }
  if (objectValue.postType == 'video') {
    likeObeject.likedUploadVideo = objectValue.likedUploadVideo;
    likeObeject.postType = objectValue.postType;
  }
  if (objectValue.postType == 'short') {
    likeObeject.likedShort = objectValue.likedShort;
    likeObeject.postType = objectValue.postType;
  }
  console.log('likeObeject', likeObeject);
  return dispatch => {
    dispatch(likesPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `likes`,
      data: likeObeject,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log("getting or not",response.data);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(likesSetData(data));
        dispatch(likesPostCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(likesPostCheckSuccess(false));
      });
  };
}

export function likesUpdateRequest(likedata, likeId) {
  console.log(likedata, likeId);
  // console.log(element.accessToken)
  // const accessToken = element.accessToken;
  // delete element.accessToken;
  return dispatch => {
    dispatch(likesSetData(element));
    dispatch(likesUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `likes/${likeId}`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
      data: likedata,
    })
      .then(response => {
        console.log('from update response', response);
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(likesSetData(data));
        dispatch(likesUpdateCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(likesUpdateCheckSuccess(false));
      });
  };
}

export function likesDeleteRequest(element) {
  console.log('element', element);
  return dispatch => {
    dispatch(likesDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `likes/${element}`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(likesSetData(data));
        dispatch(likesDeleteCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(likesDeleteCheckSuccess(false));
      });
  };
}
