import{ 
    checkoutpaymentsGetCheckSuccess,
    checkoutpaymentsSetData,
    checkoutpaymentsPostCheckSuccess,
    checkoutpaymentsUpdateCheckSuccess,
    checkoutpaymentsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function checkoutpaymentsGetRequest(element,token) {
    return dispatch => {
      dispatch(checkoutpaymentsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `checkout-payments`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(checkoutpaymentsSetData(data));
          dispatch(checkoutpaymentsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(checkoutpaymentsGetCheckSuccess(false));
        });
    };
    };
    
    export function checkoutpaymentsPostRequest(element) {
        return dispatch => {
          dispatch(checkoutpaymentsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `checkout-payments`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(checkoutpaymentsSetData(a));
              dispatch(checkoutpaymentsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(checkoutpaymentsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function checkoutpaymentsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(checkoutpaymentsSetData(element));
            dispatch(checkoutpaymentsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `checkout-payments/610cd1843be536107bcb5c03`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(checkoutpaymentsSetData(a));
                dispatch(checkoutpaymentsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(checkoutpaymentsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function checkoutpaymentsDeleteRequest(element) {
        return dispatch => {
          dispatch(checkoutpaymentsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `checkout-payments/610cd1943be536107bcb5c04`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(checkoutpaymentsSetData(a));
              dispatch(checkoutpaymentsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(checkoutpaymentsDeleteCheckSuccess(false));
             
            });
        };
    }