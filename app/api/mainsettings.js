import{ 
    mainsettingsGetCheckSuccess,
    mainsettingsSetData,
    mainsettingsPostCheckSuccess,
    mainsettingsUpdateCheckSuccess,
    mainsettingsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function mainsettingsGetRequest(element,token) {
    return dispatch => {
      dispatch(mainsettingsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `main-settings`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"user data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(mainsettingsSetData(data));
          dispatch(mainsettingsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(mainsettingsGetCheckSuccess(false));
        });
    };
    };
    
    export function mainsettingsPostRequest(element) {
        return dispatch => {
          dispatch(mainsettingsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `main-settings`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(mainsettingsSetData(a));
              dispatch(mainsettingsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(mainsettingsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function mainsettingsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(mainsettingsSetData(element));
            dispatch(mainsettingsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `main-settings/610cc89d3be536107bcb5bf0`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(mainsettingsSetData(a));
                dispatch(mainsettingsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(mainsettingsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function mainsettingsDeleteRequest(element) {
        return dispatch => {
          dispatch(mainsettingsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `main-settings/6107b73cf331412de18c1eba`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(mainsettingsSetData(a));
              dispatch(mainsettingsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(mainsettingsDeleteCheckSuccess(false));
             
            });
        };
    }