import{ 
  dislikeGetCheckSuccess,
  dislikeSetData,
  dislikePostCheckSuccess,
  dislikeUpdateCheckSuccess,
  dislikeDeleteCheckSuccess,
  dislikeGetCheckSuccessByUser,
  dislikeSetDataByUser

  } from '../actions';
  import axios from 'axios';
  let a = [];
  
  export function dislikeGetRequest(element,token) {
  return dispatch => {
    dispatch(dislikeGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `dislikes`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(dislikeSetData(data));
        dispatch(dislikeGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(dislikeGetCheckSuccess(false));
      });
  };
  };
  export function dislikeGetRequestByUser(userId,videoId) {
    const objectvalue={};
    objectvalue.disLikedBy=userId;
    objectvalue.dislikeUploadVideo=videoId;
  return dispatch => {
    dispatch(dislikeGetCheckSuccessByUser(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `dislikes`,
        params:objectvalue,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(dislikeSetDataByUser(data));
        dispatch(dislikeGetCheckSuccessByUser(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(dislikeGetCheckSuccessByUser(false));
      });
  };
  };
  
  export function dislikePostRequest(objectValue) {
      return dispatch => {
        dispatch(dislikePostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `dislikes`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"},
           data: objectValue
            })
          .then(response => {
            console.log("response feed",response)
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
           console.log("data",data);
            dispatch(dislikeSetData(data));
            dispatch(dislikePostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(dislikePostCheckSuccess(false));
           
          });
      };
  }
  
  export function dislikeUpdateRequest(element) {
      console.log(element.accessToken)
      const accessToken = element.accessToken;
      delete element.accessToken;
      return dispatch => {
          dispatch(dislikeSetData(element));
          dispatch(dislikeUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `dislikes/611bae3b4161ced881fda009`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(dislikeSetData(data));
              dispatch(dislikeUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(dislikeUpdateCheckSuccess(false));
              
          });
      };
  }

  export function dislikeDeleteRequest(dislikeId) {
      return dispatch => {
        dispatch(dislikeDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'delete',
            url: `dislikes/${dislikeId}`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(dislikeSetData(data));
            dispatch(dislikeDeleteCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(dislikeDeleteCheckSuccess(false));
           
          });
      };
  }