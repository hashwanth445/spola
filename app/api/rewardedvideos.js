import{ 
  rewardedvideosGetCheckSuccess,
  rewardedvideosSetData,
  rewardedvideosPostCheckSuccess,
  rewardedvideosUpdateCheckSuccess,
  rewardedvideosDeleteCheckSuccess,

  } from '../actions';
  import axios from 'axios';
  let a = [];
  
  export function rewardedvideosGetRequest(element,token) {
  return dispatch => {
    dispatch(rewardedvideosGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `rewards`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(rewardedvideosSetData(data));
        dispatch(rewardedvideosGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(rewardedvideosGetCheckSuccess(false));
      });
  };
  };
  
  export function rewardedvideosPostRequest(videoApi, title, description, userData) {
    const object = {};
    object.videoUrl = videoApi[0]?.url;
    object.title = title;
    object.description = description;
    object.uploadedBy = userData?.user?.id;
      return dispatch => {
        dispatch(rewardedvideosPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `rewards`,
            data:object,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            // const propertyValues = Object.values(data.data);
            // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            // a.push(obj)
            dispatch(rewardedvideosSetData(data));
            dispatch(rewardedvideosPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(rewardedvideosPostCheckSuccess(false));
           
          });
      };
  }
  
  export function rewardedvideosUpdateRequest(element) {
      console.log(element.accessToken)
      const accessToken = element.accessToken;
      delete element.accessToken;
      return dispatch => {
          dispatch(rewardedvideosSetData(element));
          dispatch(rewardedvideosUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `rewards/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(rewardedvideosSetData(data));
              dispatch(rewardedvideosUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(rewardedvideosUpdateCheckSuccess(false));
              
          });
      };
  }

  export function rewardedvideosDeleteRequest(element) {
      return dispatch => {
        dispatch(rewardedvideosDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'delete',
            url: `rewards/${id}`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            // const propertyValues = Object.values(data.data);
            // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            // a.push(obj)
            dispatch(rewardedvideosSetData(data));
            dispatch(rewardedvideosDeleteCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(rewardedvideosDeleteCheckSuccess(false));
           
          });
      };
  }