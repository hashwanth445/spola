import {
  commentsGetCheckSuccess,
  commentsSetData,
  commentsSetDataForFeed,
  commentsPostCheckSuccess,
  commentsUpdateCheckSuccess,
  commentsDeleteCheckSuccess,
  commentsGetCheckSuccessForFeed,
} from '../actions';
import axios from 'axios';
let a = [];

export function commentsGetRequest(element, token) {
  return dispatch => {
    dispatch(commentsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `comments`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(commentsSetData(data));
        dispatch(commentsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(commentsGetCheckSuccess(false));
      });
  };
};

export function commentsGetRequestForFeed(feedId) {
  const objectvalue = {};
  objectvalue.commentFeed = feedId;
  console.log('objectvalue', objectvalue);
  return dispatch => {
    dispatch(commentsGetCheckSuccessForFeed(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `comments`,
      params: objectvalue,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(commentsSetDataForFeed(data));
        dispatch(commentsGetCheckSuccessForFeed(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(commentsGetCheckSuccessForFeed(false));
      });
  };
};

export function commentsPostRequest(objectValue) {

  const objectUrl = {};
  objectUrl.message = objectValue.message;
  objectUrl.commentedBy = objectValue.commentedBy;
  if (objectValue.postType == "feed") {
    objectUrl.commentFeed = objectValue.commentFeed;
  }
  if (objectValue.postType == "video") {
    objectUrl.commentVideo = objectValue.commentVideo;
  }
  console.log("objectUrl", objectUrl);
  return dispatch => {
    dispatch(commentsPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `comments`,
      data: objectUrl,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        console.log(response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj);
        dispatch(commentsSetData(data));
        dispatch(commentsPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(commentsPostCheckSuccess(false));

      });
  };
}

export function commentsUpdateRequest(element) {
  console.log(element.accessToken)
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(commentsSetData(element));
    dispatch(commentsUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `comments/611bad454161ced881fda005`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(commentsSetData(a));
        dispatch(commentsUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(commentsUpdateCheckSuccess(false));

      });
  };
}

export function commentsDeleteRequest(element) {
  return dispatch => {
    dispatch(commentsDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `comments/611bad274161ced881fda004`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        const propertyValues = Object.values(data.data);
        const obj = { categoryID: element.id, categoryName: element.name, categoryImage: element.icon, subCategory: propertyValues }
        a.push(obj)
        dispatch(commentsSetData(a));
        dispatch(commentsDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(commentsDeleteCheckSuccess(false));

      });
  };
}