import{ 
    paystackpaymentsGetCheckSuccess,
    paystackpaymentsSetData,
    paystackpaymentsPostCheckSuccess,
    paystackpaymentsUpdateCheckSuccess,
    paystackpaymentsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function paystackpaymentsGetRequest(element,token) {
    return dispatch => {
      dispatch(paystackpaymentsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `pay-stack-payments`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(paystackpaymentsSetData(data));
          dispatch(paystackpaymentsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(paystackpaymentsGetCheckSuccess(false));
        });
    };
    };
    
    export function paystackpaymentsPostRequest(element) {
        return dispatch => {
          dispatch(paystackpaymentsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `pay-stack-payments`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(paystackpaymentsSetData(a));
              dispatch(paystackpaymentsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(paystackpaymentsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function paystackpaymentsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(paystackpaymentsSetData(element));
            dispatch(paystackpaymentsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `pay-stack-payments/610ce7a13be536107bcb5c67`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(paystackpaymentsSetData(a));
                dispatch(paystackpaymentsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(paystackpaymentsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function paystackpaymentsDeleteRequest(element) {
        return dispatch => {
          dispatch(paystackpaymentsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `pay-stack-payments/610ce7a53be536107bcb5c68`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(paystackpaymentsSetData(a));
              dispatch(paystackpaymentsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(paystackpaymentsDeleteCheckSuccess(false));
             
            });
        };
    }