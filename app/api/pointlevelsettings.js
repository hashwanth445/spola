import{ 
    pointlevelsettingsGetCheckSuccess,
    pointlevelsettingsSetData,
    pointlevelsettingsPostCheckSuccess,
    pointlevelsettingsUpdateCheckSuccess,
    pointlevelsettingsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function pointlevelsettingsGetRequest(element,token) {
    return dispatch => {
      dispatch(pointlevelsettingsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `point-level-settings`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(pointlevelsettingsSetData(data));
          dispatch(pointlevelsettingsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(pointlevelsettingsGetCheckSuccess(false));
        });
    };
    };
    
    export function pointlevelsettingsPostRequest(element) {
        return dispatch => {
          dispatch(pointlevelsettingsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `point-level-settings`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(pointlevelsettingsSetData(a));
              dispatch(pointlevelsettingsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(pointlevelsettingsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function pointlevelsettingsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(pointlevelsettingsSetData(element));
            dispatch(pointlevelsettingsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `point-level-settings/6107a456f331412de18c1eb4`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(pointlevelsettingsSetData(a));
                dispatch(pointlevelsettingsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(pointlevelsettingsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function pointlevelsettingsDeleteRequest(element) {
        return dispatch => {
          dispatch(pointlevelsettingsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `point-level-settings/610ce9023be536107bcb5c6b`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(pointlevelsettingsSetData(a));
              dispatch(pointlevelsettingsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(pointlevelsettingsDeleteCheckSuccess(false));
             
            });
        };
    }