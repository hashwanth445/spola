import {
  shortsGetCheckSuccess,
  shortsSetData,
  shortsPostCheckSuccess,
  shortsUpdateCheckSuccess,
  shortsDeleteCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

export function shortsGetRequest(element, token) {
  return dispatch => {
    dispatch(shortsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `shorts`,
      headers: {
          Authorization: ``,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(shortsSetData(data));
        dispatch(shortsGetCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings dshoooortssss-----------------');
        dispatch(shortsGetCheckSuccess(false));
      });
  };
}

export function shortsPostRequest(videoApi, description, userData) {
  const dataObject = {};
  dataObject.backBlazeSpolaUrl = videoApi[0].url;
  dataObject.description = description;
  dataObject.uploadedBy = userData?.user?.id;

  console.log("object inside",dataObject)
  return dispatch => {
    dispatch(shortsPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `shorts`,
      data: dataObject,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log("response inside api",response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data);
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(shortsSetData(data));
        dispatch(shortsPostCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(shortsPostCheckSuccess(false));
      });
  };
}

export function shortsUpdateRequest(element) {
  console.log(element.accessToken);
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(shortsSetData(element));
    dispatch(shortsUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `shorts/611bb27a4161ced881fda023`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(shortsSetData(a));
        dispatch(shortsUpdateCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(shortsUpdateCheckSuccess(false));
      });
  };
}

export function shortsDeleteRequest(element) {
  return dispatch => {
    dispatch(shortsDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `shorts/611bb26f4161ced881fda022`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        const propertyValues = Object.values(data.data);
        const obj = {
          categoryID: element.id,
          categoryName: element.name,
          categoryImage: element.icon,
          subCategory: propertyValues,
        };
        a.push(obj);
        dispatch(shortsSetData(a));
        dispatch(shortsDeleteCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(shortsDeleteCheckSuccess(false));
      });
  };
}
