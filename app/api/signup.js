
import axios from 'axios';
import { Alert } from "react-native";
import { signupFailure, signupData, signupLoading, signupSuccess,signupPutLoading,signupPutChannelDataSuccess,signupPutSuccess,signupPutFailure, verifyOtpSucess, verifyOtpFailure } from '../actions';
import { setAccessToken, setUserData } from '../actions';
// import AsyncStorage from '@react-native-community/async-storage';
// const userToken =  useSelector(state => state.accessTokenReducer.accessToken)

export const signupRequest = ( name, email, phone, password, ) => {
    const dataObject = {}
    dataObject.username = name;
    dataObject.email = email;
    dataObject.MobileNumber = phone;
    dataObject.password = password;
   
    console.log("signup post",dataObject);
    return dispatch => {
        dispatch(signupLoading(true));
        dispatch(signupSuccess(false));
        dispatch(signupFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'post',
            url: 'auth/local/register',
            data: dataObject,
            headers: {
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                console.log("response from signup",response)
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                console.log("response data",data)
                dispatch(setUserData(data));
                dispatch(setAccessToken(data.jwt));
                dispatch(signupData(data));
                dispatch(signupLoading(false));
                dispatch(signupSuccess(true));
                dispatch(signupFailure(false));
                dispatch(sendOtpRequest(dataObject.MobileNumber));
                
                 
            })
            .catch((err) => {
                console.log("error", err);
                dispatch(signupLoading(false));
                dispatch(signupFailure(true));
                dispatch(signupSuccess(false));
               
                if (err?.response?.status === 400) {
                    alert("Please Fill Valid Data")
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};
//update profile
export const signupRequestPut = (  firstName, lastName, gender,userId, userToken, image ) => {
    console.log("userId",userId)
    const dataObject = {}

    dataObject.firstName = firstName;
    dataObject.lastName = lastName;
    dataObject.gender = gender;
    dataObject.backBlazeImageUrl = image;
    // dataObject.followingCategories = selectedCat;
   
    console.log("signup put",dataObject, 'users/'+ userId);
    return dispatch => {
        dispatch(signupPutLoading(true));
        dispatch(signupPutSuccess(false));
        dispatch(signupPutFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'put',
            url: `users/${userId}`,
            data: dataObject,
            headers: {
                Authorization:`Bearer ${userToken}`,
                "Content-Type": "application/json",
                
            },

        })
            .then(response => {
                console.log("response from profile update",response)
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                console.log("response data",data)
             
                dispatch(signupData(data));
                dispatch(signupPutLoading(false));
                dispatch(signupPutSuccess(true));
                dispatch(signupPutFailure(false));
               
                
                 
            })
            .catch((err) => {
                console.log("error", err.response);
                dispatch(signupPutLoading(false));
                dispatch(signupPutSuccess(false));
                dispatch(signupPutFailure(true));

                if (err.response.status === 400) {
                    alert("Please Fill Valid Data")
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};
//update api for chaneel,categories,creaters in user
export const signupRequestPutForChannel = (  selectedCat,selectedFollowing,selectedChannel,userToken,userId ) => {
    console.log("userId",userId)
    const dataObject = {}

    dataObject.followingCategories = selectedCat;
    dataObject.followingChannels = selectedChannel;
    dataObject.followingProfiles = selectedFollowing;
   
    console.log("signup put",dataObject, 'users/'+ userId);
    return dispatch => {
        dispatch(signupLoading(true));
        dispatch(signupPutChannelDataSuccess(false));
        dispatch(signupFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'put',
            url: `users/${userId}`,
            data: dataObject,
            headers: {
                Authorization:`Bearer ${userToken}`,
                "Content-Type": "application/json",
                
            },

        })
            .then(response => {
                console.log("response from profile update",response)
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                console.log("response data",data)
             
                dispatch(signupData(data));
                dispatch(signupLoading(false));
                dispatch(signupPutChannelDataSuccess(true));
                dispatch(signupFailure(false));
               
                
                 
            })
            .catch((err) => {
                console.log("error", err.response);
                dispatch(signupLoading(false));
                dispatch(signupFailure(true));
                dispatch(signupSsignupPutChannelDataSuccessuccess(false));

                if (err.response.status === 400) {
                    alert("Please Fill Valid Data")
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};
export const sendOtpRequest = (MobileNumber) => {
   
    return dispatch => {
        dispatch(signupLoading(true));
        // dispatch(signupSuccess(false));
        // dispatch(signupFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'GET',
            url: `https://api.msg91.com/api/v5/otp?template_id=6130a7bb390c095dbd546af6&mobile=91${MobileNumber}&authkey=366475AiwGawQc612c9aa8P1`,
            // data: data,
            headers: {
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                // dispatch(signupData(data));
                console.log("++++++++", data);
                dispatch(signupLoading(false));
                // dispatch(signupSuccess(true));
                // dispatch(signupFailure(false));
            })
            .catch((err) => {
                console.log("error", err.response);
                dispatch(signupLoading(false));
                s
                if (err.response.status === 400) {
                    alert("Please Fill Valid Data")
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};
export const sendVerifyOtpRequest = (MobileNumber, enterOtp ) => {
    
     
    return dispatch => {
        dispatch(signupLoading(true));
        // dispatch(signupSuccess(false));
        // dispatch(signupFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'post',
            url: `https://api.msg91.com/api/v5/otp/verify?authkey=366475AiwGawQc612c9aa8P1&mobile=91${MobileNumber}&otp=${enterOtp}`,
            // data: data,
            headers: {
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                dispatch(signupData(data));
                console.log("+++++verifyotp+++", data);
                console.log("dnfkjdsgfuidsgfkj1===========", data.message);
                console.log("dnfkjdsgfuidsgfkj1-------------------", data.type);
                if(data.message == "OTP verified success")
                {
                    
                    alert('OTP Vefication Success');   
                    // navigation.navigate('OnboardingScreenOneProfileDetails')
                    dispatch(verifyOtpSucess(true))
                }
                else
                {
                      alert('OTP invaild');
                      dispatch(verifyOtpFailure(true))
                   
                }
                // dispatch(signupLoading(false));
                // dispatch(signupSuccess(true));
                // dispatch(signupFailure(false));
            })
            .catch((err) => {
                console.log("error", err.response);
                dispatch(signupLoading(false));
                // dispatch(signupFailure(true));
                // dispatch(signupSuccess(false));
                if (err.response.status === 400) {
                    alert("Please Fill Valid Data")
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};
