import{ 
    payserapaymentsGetCheckSuccess,
    payserapaymentsSetData,
    payserapaymentsPostCheckSuccess,
    payserapaymentsUpdateCheckSuccess,
    payserapaymentsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function payserapaymentsGetRequest(element,token) {
    return dispatch => {
      dispatch(payserapaymentsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `paysera-payments`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(payserapaymentsSetData(data));
          dispatch(payserapaymentsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(payserapaymentsGetCheckSuccess(false));
        });
    };
    };
    
    export function payserapaymentsPostRequest(element) {
        return dispatch => {
          dispatch(payserapaymentsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `paysera-payments`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(payserapaymentsSetData(a));
              dispatch(payserapaymentsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(payserapaymentsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function payserapaymentsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(payserapaymentsSetData(element));
            dispatch(payserapaymentsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `paysera-payments/610ce7453be536107bcb5c65`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(payserapaymentsSetData(a));
                dispatch(payserapaymentsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(payserapaymentsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function payserapaymentsDeleteRequest(element) {
        return dispatch => {
          dispatch(payserapaymentsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `paysera-payments/610ce7463be536107bcb5c66`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(payserapaymentsSetData(a));
              dispatch(payserapaymentsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(payserapaymentsDeleteCheckSuccess(false));
             
            });
        };
    }