import{ 
  commentsettingsGetCheckSuccess,
  commentsettingsSetData,
  commentsettingsPostCheckSuccess,
  commentsettingsUpdateCheckSuccess,
  commentsettingsDeleteCheckSuccess,

  } from '../actions';
  import axios from 'axios';
  let a = [];
  
  export function commentsettingsGetRequest(element,token) {
  return dispatch => {
    dispatch(commentsettingsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `comment-settings`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(commentsettingsSetData(data));
        dispatch(commentsettingsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(commentsettingsGetCheckSuccess(false));
      });
  };
  };
  
  export function commentsettingsPostRequest(element) {
      return dispatch => {
        dispatch(commentsettingsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `comment-settings`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(commentsettingsSetData(a));
            dispatch(commentsettingsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(commentsettingsPostCheckSuccess(false));
           
          });
      };
  }
  
  export function commentsettingsUpdateRequest(element) {
      console.log(element.accessToken)
      const accessToken = element.accessToken;
      delete element.accessToken;
      return dispatch => {
          dispatch(commentsettingsSetData(element));
          dispatch(commentsettingsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `comment-settings/610cd8313be536107bcb5c20`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(commentsettingsSetData(a));
              dispatch(commentsettingsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(commentsettingsUpdateCheckSuccess(false));
              
          });
      };
  }

  export function commentsettingsDeleteRequest(element) {
      return dispatch => {
        dispatch(commentsettingsDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'delete',
            url: `comment-settings/610cd8343be536107bcb5c21`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(commentsettingsSetData(a));
            dispatch(commentsettingsDeleteCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(commentsettingsDeleteCheckSuccess(false));
           
          });
      };
  }