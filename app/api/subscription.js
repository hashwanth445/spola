import{ 
    subscriptionGetCheckSuccess,
    subscriptionSetData,
    subscriptionPostCheckSuccess,
    subscriptionUpdateCheckSuccess,
    subscriptionDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function subscriptionGetRequest(element,token) {
    return dispatch => {
      dispatch(subscriptionGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `subscriptions`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"user data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(subscriptionSetData(data));
          dispatch(subscriptionGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(subscriptionGetCheckSuccess(false));
        });
    };
    };
    
    export function subscriptionPostRequest(element) {
        return dispatch => {
          dispatch(subscriptionPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `subscriptions`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(subscriptionSetData(a));
              dispatch(subscriptionPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(subscriptionPostCheckSuccess(false));
             
            });
        };
    }
    
    export function subscriptionUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(subscriptionSetData(element));
            dispatch(subscriptionUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `subscriptions/611bb0374161ced881fda015`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(subscriptionSetData(a));
                dispatch(subscriptionUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(subscriptionUpdateCheckSuccess(false));
                
            });
        };
    }

    export function subscriptionDeleteRequest(element) {
        return dispatch => {
          dispatch(subscriptionDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `subscriptions/611bb0554161ced881fda016`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(subscriptionSetData(a));
              dispatch(subscriptionDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(subscriptionDeleteCheckSuccess(false));
             
            });
        };
    }