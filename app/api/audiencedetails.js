import{ 
    audiencedetailsGetCheckSuccess,
    audiencedetailsSetData,
    audiencedetailsPostCheckSuccess,
    audiencedetailsUpdateCheckSuccess,
    audiencedetailsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function audiencedetailsGetRequest(element,token) {
    return dispatch => {
      dispatch(audiencedetailsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `audience-details`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(audiencedetailsSetData(data));
          dispatch(audiencedetailsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(audiencedetailsGetCheckSuccess(false));
        });
    };
    };
    
    export function audiencedetailsPostRequest(element) {
        return dispatch => {
          dispatch(audiencedetailsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `audience-details`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(audiencedetailsSetData(a));
              dispatch(audiencedetailsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(audiencedetailsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function audiencedetailsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(audiencedetailsSetData(element));
            dispatch(audiencedetailsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `audience-details/610cd7193be536107bcb5c1c`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(audiencedetailsSetData(a));
                dispatch(audiencedetailsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(audiencedetailsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function audiencedetailsDeleteRequest(element) {
        return dispatch => {
          dispatch(audiencedetailsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `audience-details/610cd75b3be536107bcb5c1d`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(audiencedetailsSetData(a));
              dispatch(audiencedetailsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(audiencedetailsDeleteCheckSuccess(false));
             
            });
        };
    }