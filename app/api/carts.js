import{ 
  cartsGetCheckSuccess,
  cartsGetCheckSuccessForUser,
  cartsSetData,
  cartsSetDataForUser,
  cartsPostCheckSuccess,
  cartsUpdateCheckSuccess,
  cartsDeleteCheckSuccess,

  } from '../actions';
  import axios from 'axios';
  let a = [];
  
  export function cartsGetRequest(element,token) {
  return dispatch => {
    dispatch(cartsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `carts`,
        headers:{
            // Authorization: ``,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(cartsSetData(data));
        dispatch(cartsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(cartsGetCheckSuccess(false));
      });
  };
  };
  export function cartsGetRequestForUser(userId) {
    const objectValue={};
    objectValue.userId = userId;
  return dispatch => {
    dispatch(cartsGetCheckSuccessForUser(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `carts`,
        params:objectValue,
        headers:{
            // Authorization: ``,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(cartsSetDataForUser(data));
        dispatch(cartsGetCheckSuccessForUser(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(cartsGetCheckSuccessForUser(false));
      });
  };
  };
  
  export function cartsPostRequest(objectValue) {
      return dispatch => {
        dispatch(cartsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `carts`,
            data:objectValue,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            console.log("response",response);
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            console.log("data",data);
            // const propertyValues = Object.values(data.data);
            // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            // a.push(obj)
            dispatch(cartsSetData(data));
            dispatch(cartsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(cartsPostCheckSuccess(false));
           
          });
      };
  }
  
  export function cartsUpdateRequest(element) {
      console.log(element.accessToken)
      const accessToken = element.accessToken;
      delete element.accessToken;
      return dispatch => {
          dispatch(cartsSetData(element));
          dispatch(cartsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `carts`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(cartsSetData(data));
              dispatch(cartsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(cartsUpdateCheckSuccess(false));
              
          });
      };
  }

  export function cartsDeleteRequest(id,objectValue) {
      return dispatch => {
        dispatch(cartsDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'delete',
            url: `carts/${id}`,
            data:objectValue,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            // const propertyValues = Object.values(data.data);
            // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            // a.push(obj)
            dispatch(cartsSetData(data));
            dispatch(cartsDeleteCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(cartsDeleteCheckSuccess(false));
           
          });
      };
  }