import{ 
    uploadlimitGetCheckSuccess,
    uploadlimitSetData,
    uploadlimitPostCheckSuccess,
    uploadlimitUpdateCheckSuccess,
    uploadlimitDeleteCheckSuccess,
    
      } from '../actions';
      import axios from 'axios';
      let a = [];
      
      export function uploadlimitGetRequest(element,token) {
      return dispatch => {
        dispatch(uploadlimitGetCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `upload-limits`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            // console.log(data)
            // console.log(element,"api data",data.data[0])
            // const propertyValues = Object.values(data.data);
            // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            // a.push(obj)
            dispatch(uploadlimitSetData(data));
            dispatch(uploadlimitGetCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,'furnishings')
            dispatch(uploadlimitGetCheckSuccess(false));
          });
      };
      };
      
      export function uploadlimitPostRequest(element) {
          return dispatch => {
            dispatch(uploadlimitPostCheckSuccess(false));
            const axiosLogin = axios.create();
              return axiosLogin({
                method: 'post',
                url: `upload-limits`,
                headers:{
                  // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                  "Content-Type" : "application/json"}
               
                })
              .then(response => {
                if (response.status !== 200) {
                  throw Error(response.statusText);
                }
                return response.data;
              })
              .then(data => {
                const propertyValues = Object.values(data.data);
                const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                a.push(obj)
                dispatch(uploadlimitSetData(a));
                dispatch(uploadlimitPostCheckSuccess(true));
              })
              .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(uploadlimitPostCheckSuccess(false));
               
              });
          };
      }
      
      export function uploadlimitUpdateRequest(element) {
          console.log(element.accessToken)
          const accessToken = element.accessToken;
          delete element.accessToken;
          return dispatch => {
              dispatch(uploadlimitSetData(element));
              dispatch(uploadlimitUpdateCheckSuccess(false));
              const axiosLogin = axios.create();
              return axiosLogin({
                  method: 'put',
                  url: `upload-limits/610ceb4c3be536107bcb5c73`,
                  headers:{
                  Authorization: `Bearer ${accessToken}`,
                  "Content-Type" : "application/json"},
                  data:element,
                  })
              .then(response => {
                  // console.log(response)
                  if (response.status !== 200) {
                  throw Error(response.statusText);
                  }
                  return response.data;
              })
              .then(data => {
                  // console.log("data",data)
                  dispatch(uploadlimitSetData(a));
                  dispatch(uploadlimitUpdateCheckSuccess(true));
              })
              .catch((error) => {
                  console.log(error,axiosLogin)
                  dispatch(uploadlimitUpdateCheckSuccess(false));
                  
              });
          };
      }
    
      export function uploadlimitDeleteRequest(element) {
          return dispatch => {
            dispatch(uploadlimitDeleteCheckSuccess(false));
            const axiosLogin = axios.create();
              return axiosLogin({
                method: 'delete',
                url: `upload-limits/610ceb513be536107bcb5c74`,
                headers:{
                  // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                  "Content-Type" : "application/json"}
               
                })
              .then(response => {
                if (response.status !== 200) {
                  throw Error(response.statusText);
                }
                return response.data;
              })
              .then(data => {
                const propertyValues = Object.values(data.data);
                const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                a.push(obj)
                dispatch(uploadlimitSetData(a));
                dispatch(uploadlimitDeleteCheckSuccess(true));
              })
              .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(uploadlimitDeleteCheckSuccess(false));
               
              });
          };
      }