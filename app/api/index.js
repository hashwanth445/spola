export {loginRequest} from './login';
export {signupRequest} from './signup';
export {furnishingsGetRequest} from './sample';
export {
  mainsettingsGetRequest,
  mainsettingsPostRequest,
  mainsettingsUpdateRequest,
  mainsettingsDeleteRequest,
} from './mainsettings';
export {
  addressGetRequest,
  addressPostRequest,
  addressUpdateRequest,
  addressDeleteRequest,
} from './address';

export {
  deviceGetRequest,
  devicePostRequest,
  deviceUpdateRequest,
  deviceDeleteRequest,
} from './device';

export {
  manageusersGetRequest,
  manageusersPostRequest,
  manageusersUpdateRequest,
  manageusersDeleteRequest,
} from './manageusers';

export {
  playersettingsGetRequest,
  playersettingsPostRequest,
  playersettingsUpdateRequest,
  playersettingsDeleteRequest,
} from './playersettings';

export {
  uploadsettingsGetRequest,
  uploadsettingsPostRequest,
  uploadsettingsUpdateRequest,
  uploadsettingsDeleteRequest,
} from './uploadsettings';

export {
  usersettingsGetRequest,
  usersettingsPostRequest,
  usersettingsUpdateRequest,
  usersettingsDeleteRequest,
} from './usersettings';

export {
  checkoutpaymentsGetRequest,
  checkoutpaymentsPostRequest,
  checkoutpaymentsUpdateRequest,
  checkoutpaymentsDeleteRequest,
} from './checkoutpayments';

export {
  audiencedetailsGetRequest,
  audiencedetailsPostRequest,
  audiencedetailsUpdateRequest,
  audiencedetailsDeleteRequest,
} from './audiencedetails';

export {
  cashfreepaymentsGetRequest,
  cashfreepaymentsPostRequest,
  cashfreepaymentsUpdateRequest,
  cashfreepaymentsDeleteRequest,
} from './cashfreepayments';

export {
  commentsettingsGetRequest,
  commentsettingsPostRequest,
  commentsettingsUpdateRequest,
  commentsettingsDeleteRequest,
} from './commentsettings';

export {
  limitperpageGetRequest,
  limitperpagePostRequest,
  limitperpageUpdateRequest,
  limitperpageDeleteRequest,
} from './limitperpage';

export {
  livesettingsGetRequest,
  livesettingsPostRequest,
  livesettingsUpdateRequest,
  livesettingsDeleteRequest,
} from './livesettings';

export {
  manageuseradsGetRequest,
  manageuseradsPostRequest,
  manageuseradsUpdateRequest,
  manageuseradsDeleteRequest,
} from './manageuserads';

export {
  managevideoadsGetRequest,
  managevideoadsPostRequest,
  managevideoadsUpdateRequest,
  managevideoadsDeleteRequest,
} from './managevideoads';

export {
  managevideoreportsGetRequest,
  managevideoreportsPostRequest,
  managevideoreportsUpdateRequest,
  managevideoreportsDeleteRequest,
} from './managevideoreports';

export {
  managevideosGetRequest,
  managevideosPostRequest,
  managevideosUpdateRequest,
  managevideosDeleteRequest,
} from './managevideos';

export {
  maxuploadsizeGetRequest,
  maxuploadsizePostRequest,
  maxuploadsizeUpdateRequest,
  maxuploadsizeDeleteRequest,
} from './maxuploadsize';

export {
  othersettingsGetRequest,
  othersettingsPostRequest,
  othersettingsUpdateRequest,
  othersettingsDeleteRequest,
} from './othersettings';

export {
  paidsubscriberssettingGetRequest,
  paidsubscriberssettingPostRequest,
  paidsubscriberssettingUpdateRequest,
  paidsubscriberssettingDeleteRequest,
} from './paidsubscriberssetting';

export {
  rewardedvideosGetRequest,
  rewardedvideosPostRequest,
  rewardedvideosUpdateRequest,
  rewardedvideosDeleteRequest,
} from './rewardedvideos';

export {
  paymentsettingsGetRequest,
  paymentsettingsPostRequest,
  paymentsettingsUpdateRequest,
  paymentsettingsDeleteRequest,
} from './paymentsettings';

export {
  payserapaymentsGetRequest,
  payserapaymentsPostRequest,
  payserapaymentsUpdateRequest,
  payserapaymentsDeleteRequest,
} from './payserapayments';

export {
  paystackpaymentsGetRequest,
  paystackpaymentsPostRequest,
  paystackpaymentsUpdateRequest,
  paystackpaymentsDeleteRequest,
} from './paystackpayments';

export {
  payupaymentsGetRequest,
  payupaymentsPostRequest,
  payupaymentsUpdateRequest,
  payupaymentsDeleteRequest,
} from './payupayments';

export {
  pointlevelsettingsGetRequest,
  pointlevelsettingsPostRequest,
  pointlevelsettingsUpdateRequest,
  pointlevelsettingsDeleteRequest,
} from './pointlevelsettings';

export {
  razorpaypaymentsGetRequest,
  razorpaypaymentsPostRequest,
  razorpaypaymentsUpdateRequest,
  razorpaypaymentsDeleteRequest,
} from './razorpaypayments';

export {
  socialloginsettingsGetRequest,
  socialloginsettingsPostRequest,
  socialloginsettingsUpdateRequest,
  socialloginsettingsDeleteRequest,
} from './socialloginsettings';

export {
  chatsGetRequest,
  chatsPostRequest,
  chatsUpdateRequest,
  chatsDeleteRequest,
} from './chats';

export {
  friendsGetRequest,
  friendsPostRequest,
  friendsUpdateRequest,
  friendsDeleteRequest,
} from './friends';
export {
  usersPostRequest,
  usersCountRequest,
  usersDeleteRequest,
  usersFindOneRequest,
  usersGetRequest,
  usersUpdateRequest,
}
  from './users'; 

export {
  categoriesGetRequest,
  categoriesPostRequest,
  categoriesUpdateRequest,
  categoriesDeleteRequest,
} from './categories';

export {
  channelGetRequest,
  channelGetRequestForUser,
  channelPostRequest,
  channelUpdateRequest,
  channelDeleteRequest,
} from './channel';
export {
  channelVideoGetRequest,
  channelVideoGetRequestForUser,
  channelVideoPostRequest,
  channelVideoPostRequestForReward,
  channelVideoUpdateRequest,
  channelVideoDeleteRequest,
} from './channelVideos';

export {
  commentsGetRequest,
  commentsPostRequest,
  commentsUpdateRequest,
  commentsDeleteRequest,
  commentsGetRequestForFeed,
} from './comments';

export {
  filesGetRequest,
  filesPostRequest,
  filesUpdateRequest,
  filesDeleteRequest,
} from './files';

export {
  feedsGetRequest,
  feedsPostRequest,
  feedsUpdateRequest,
  feedsDeleteRequest,
} from './feeds';

export {
  likesGetRequest,
  likesPostRequest,
  likesUpdateRequest,
  likesDeleteRequest,
  likesGetRequestByUser,
  likesGetRequestForFeed,
  likesGetRequestForShort,
} from './likes';

export {
  uploadlimitGetRequest,
  uploadlimitPostRequest,
  uploadlimitUpdateRequest,
  uploadlimitDeleteRequest,
} from './uploadlimit';

export {
  uploadFilesGetRequest,
  uploadFilesPostRequest,
  uploadFilesUpdateRequest,
  uploadFilesDeleteRequest,
} from './uploadApi';

export {
  stockvideosGetRequest,
  stockvideosPostRequest,
  stockvideosUpdateRequest,
  stockvideosDeleteRequest,
} from './stockvideos';

export {
  notificationGetRequest,
  notificationPostRequest,
  notificationUpdateRequest,
  notificationDeleteRequest,
} from './notification';

export {
  playlistsGetRequest,
  playlistsPostRequest,
  playlistsUpdateRequest,
  playlistsDeleteRequest,
} from './playlists';

export {
  moviesGetRequest,
  moviesPostRequest,
  moviesUpdateRequest,
  moviesDeleteRequest,
} from './movies';

export {
  subscriptionGetRequest,
  subscriptionPostRequest,
  subscriptionUpdateRequest,
  subscriptionDeleteRequest,
} from './subscription';

export {
  walletGetRequest,
  walletPostRequest,
  walletUpdateRequest,
  walletDeleteRequest,
} from './wallet';

export {
  videosGetRequest,
  videosPostRequest,
  videosSearchRequest,
  videosUpdateRequest,
  videosDeleteRequest,
} from './videos';


export {
  shortsGetRequest,
  shortsPostRequest,
  shortsUpdateRequest,
  shortsDeleteRequest,
} from './shorts';

export {
  bgmsGetRequest,
  bgmsPostRequest,
  bgmsUpdateRequest,
  bgmsDeleteRequest,
} from './bgms';

export {
  trendingsGetRequest,
  trendingsPostRequest,
  trendingsUpdateRequest,
  trendingsDeleteRequest,
} from './trendings';

export {
  historiesGetRequest,
  historiesPostRequest,
  historiesUpdateRequest,
  historiesDeleteRequest,
} from './histories';

export {
  audioroomGetRequest,
  audioroomPostRequest,
  audioroomUpdateRequest,
  audioroomDeleteRequest,
} from './audioroom';

export {
  productsGetRequest,
  productsPostRequest,
  productsUpdateRequest,
  productsDeleteRequest,
} from './products';
export {
  ordersGetRequest,
  ordersGetRequestForUser,
  ordersPostRequest,
  ordersUpdateRequest,
  ordersDeleteRequest,
} from './orders';
export {
  cartsGetRequest,
  cartsGetRequestForUser,
  cartsPostRequest,
  cartsUpdateRequest,
  cartsDeleteRequest,
} from './carts';

export {
  bookmarksGetRequest,
  bookmarksGetRequestByUser,
  bookmarksPostRequest,
  bookmarksUpdateRequest,
  bookmarksDeleteRequest,
  bookmarksGetRequestForFeed,
} from './bookmarks';

export {
  dislikeGetRequest,
  dislikePostRequest,
  dislikeUpdateRequest,
  dislikeDeleteRequest,
  dislikeGetRequestByUser,
} from './dislike';
