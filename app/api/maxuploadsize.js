import{ 
    maxuploadsizeGetCheckSuccess,
    maxuploadsizeSetData,
    maxuploadsizePostCheckSuccess,
    maxuploadsizeUpdateCheckSuccess,
    maxuploadsizeDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function maxuploadsizeGetRequest(element,token) {
    return dispatch => {
      dispatch(maxuploadsizeGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `max-upload-sizes`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(maxuploadsizeSetData(data));
          dispatch(maxuploadsizeGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(maxuploadsizeGetCheckSuccess(false));
        });
    };
    };
    
    export function maxuploadsizePostRequest(element) {
        return dispatch => {
          dispatch(maxuploadsizePostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `max-upload-sizes`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(maxuploadsizeSetData(a));
              dispatch(maxuploadsizePostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(maxuploadsizePostCheckSuccess(false));
             
            });
        };
    }
    
    export function maxuploadsizeUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(maxuploadsizeSetData(element));
            dispatch(maxuploadsizeUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `max-upload-sizes/610ce5073be536107bcb5c5a`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(maxuploadsizeSetData(a));
                dispatch(maxuploadsizeUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(maxuploadsizeUpdateCheckSuccess(false));
                
            });
        };
    }

    export function maxuploadsizeDeleteRequest(element) {
        return dispatch => {
          dispatch(maxuploadsizeDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `max-upload-sizes/610ce5073be536107bcb5c5a`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(maxuploadsizeSetData(a));
              dispatch(maxuploadsizeDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(maxuploadsizeDeleteCheckSuccess(false));
             
            });
        };
    }