import{ 
    bgmsGetCheckSuccess,
    bgmsSetData,
    bgmsPostCheckSuccess,
    bgmsUpdateCheckSuccess,
    bgmsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];


    export function bgmsGetRequest() {
      return new Promise((resolve, reject) => {
          const axiosLogin = axios.create();
          
            axiosLogin({
              method: 'get',
              url: `bgms`,
              headers:{
                
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(rrr=>{
              resolve(rrr)
            })
            .catch((error) => {
              console.log(error)
             
             
            });
          });
        
    }
    // export function bgmsGetRequest(element,token) {
    // return dispatch => {
    //   dispatch(bgmsGetCheckSuccess(false));
    //   const axiosLogin = axios.create();
    //     return axiosLogin({
    //       method: 'get',
    //       url: `bgms`,
    //       headers:{
    //         //  Authorization: `Bearer ${token}`,
    //         "Content-Type" : "application/json"
    //       }
    //       })
    //     .then(response => {
    //       if (response.status !== 200) {
    //         throw Error(response.statusText);
    //       }
    //       return response.data;
    //     })
    //     .then(data => {
    //       console.log(data)
    //       // console.log(element,"api data",data.data[0])
    //       // const propertyValues = Object.values(data.data);
    //       // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
    //       // a.push(obj)
    //       dispatch(bgmsSetData(data));
    //       dispatch(bgmsGetCheckSuccess(true));
    //     })
    //     .catch((error) => {
    //       console.log(error,axiosLogin,'furnishings')
    //       dispatch(bgmsGetCheckSuccess(false));
    //     });
    // };
    // };
    
    export function bgmsPostRequest(element,bgmName) {
      return new Promise((resolve, reject) => {
          const axiosLogin = axios.create();
          const data={bgmBackblazeUrl:element,bgmName}
            axiosLogin({
              method: 'post',
              url: `bgms`,
              headers:{
                
                "Content-Type" : "application/json"},
                data:data
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(rrr=>{
              resolve(rrr)
            })
            .catch((error) => {
              console.log(error)
             
             
            });
          });
        
    }

    
    export function bgmsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(bgmsSetData(element));
            dispatch(bgmsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `bgms/611bb3684161ced881fda029`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(bgmsSetData(a));
                dispatch(bgmsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(bgmsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function bgmsDeleteRequest(element) {
        return dispatch => {
          dispatch(bgmsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `bgms/611bb3624161ced881fda027`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(bgmsSetData(a));
              dispatch(bgmsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(bgmsDeleteCheckSuccess(false));
             
            });
        };
    }