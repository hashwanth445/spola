import {
  ordersGetCheckSuccess,
  ordersGetCheckSuccessForUser,
  ordersSetData,
  ordersSetDataForUser,
  ordersPostCheckSuccess,
  ordersUpdateCheckSuccess,
  ordersDeleteCheckSuccess,

} from '../actions';
import axios from 'axios';
let a = [];

export function ordersGetRequest(element, token) {
  return dispatch => {
    dispatch(ordersGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `orders`,
      headers: {
        // Authorization: ``,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(ordersSetData(data));
        dispatch(ordersGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(ordersGetCheckSuccess(false));
      });
  };
};
export function ordersGetRequestForUser(userId) {
  const objectValue = {};
  objectValue.OrderedBy = userId;
  return dispatch => {
    dispatch(ordersGetCheckSuccessForUser(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `orders`,
      params:objectValue,
      headers: {
        // Authorization: ``,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(ordersSetDataForUser(data));
        dispatch(ordersGetCheckSuccessForUser(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(ordersGetCheckSuccessForUser(false));
      });
  };
};

export function ordersPostRequest(objectValue) {
  return dispatch => {
    dispatch(ordersPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `orders`,
      data: objectValue,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        console.log("response", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("data", data);
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(ordersSetData(data));
        dispatch(ordersPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(ordersPostCheckSuccess(false));

      });
  };
}

export function ordersUpdateRequest(element) {
  console.log(element.accessToken)
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(ordersSetData(element));
    dispatch(ordersUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `orders`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(ordersSetData(data));
        dispatch(ordersUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(ordersUpdateCheckSuccess(false));

      });
  };
}

export function ordersDeleteRequest(element) {
  return dispatch => {
    dispatch(ordersDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `orders`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(ordersSetData(data));
        dispatch(ordersDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(ordersDeleteCheckSuccess(false));

      });
  };
}