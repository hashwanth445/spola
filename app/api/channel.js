import {
  channelGetCheckSuccess,
  channelGetCheckSuccessForUser,
  channelSetData,
  channelPostCheckSuccess,
  channelUpdateCheckSuccess,
  channelDeleteCheckSuccess,
  channelSetDataForUser,
} from '../actions';
import axios from 'axios';
let a = [];

export function channelGetRequest(element, token) {
  console.log("channels get request");
  return dispatch => {
    dispatch(channelGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `channels`,
      headers: {
        Authorization: ``,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("get channels from api", response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(channelSetData(data));
        dispatch(channelGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(channelGetCheckSuccess(false));
      });
  };
};
export function channelGetRequestForUser(userId) {
  console.log("channels get request");
  const dataObject = {};
  dataObject.channelCreatedBy = userId;
  return dispatch => {
    dispatch(channelGetCheckSuccessForUser(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `channels`,
      params: dataObject,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("get channels from api", response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(channelSetDataForUser(data));
        dispatch(channelGetCheckSuccessForUser(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(channelGetCheckSuccessForUser(false));
      });
  };
};

export function channelPostRequest(channelName, channelDesc, image,userToken, userId) {
  const dataObject = {}
  dataObject.channelName = channelName;
  dataObject.channelDesc = channelDesc;
  dataObject.backBlazeCoverImg = image;
  dataObject.channelCreatedBy = userId;
  console.log("dataObject", dataObject);
  return dispatch => {
    dispatch(channelPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `channels`,
      headers: {
        Authorization: `Basic ${userToken}`,
        "Content-Type": "application/json"
      },
      data: dataObject

    })
      .then(response => {
        console.log("response", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(channelSetData(data));
        dispatch(channelPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(channelPostCheckSuccess(false));

      });
  };
}

export function channelUpdateRequest(element) {
  console.log(element.accessToken)
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(channelSetData(element));
    dispatch(channelUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `channels/611baca84161ced881fda003`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(channelSetData(a));
        dispatch(channelUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(channelUpdateCheckSuccess(false));

      });
  };
}

export function channelDeleteRequest(element) {
  return dispatch => {
    dispatch(channelDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `channels/611bac954161ced881fda002`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        const propertyValues = Object.values(data.data);
        const obj = { categoryID: element.id, categoryName: element.name, categoryImage: element.icon, subCategory: propertyValues }
        a.push(obj)
        dispatch(channelSetData(a));
        dispatch(channelDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(channelDeleteCheckSuccess(false));

      });
  };
}