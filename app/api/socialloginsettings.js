import{ 
socialloginsettingsGetCheckSuccess,
socialloginsettingsSetData,
socialloginsettingsPostCheckSuccess,
socialloginsettingsUpdateCheckSuccess,
socialloginsettingsDeleteCheckSuccess,

  } from '../actions';
  import axios from 'axios';
  let a = [];
  
  export function socialloginsettingsGetRequest(element,token) {
  return dispatch => {
    dispatch(socialloginsettingsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `social-login-settings`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(socialloginsettingsSetData(data));
        dispatch(socialloginsettingsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(socialloginsettingsGetCheckSuccess(false));
      });
  };
  };
  
  export function socialloginsettingsPostRequest(element) {
      return dispatch => {
        dispatch(socialloginsettingsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `social-login-settings`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(socialloginsettingsSetData(a));
            dispatch(socialloginsettingsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(socialloginsettingsPostCheckSuccess(false));
           
          });
      };
  }
  
  export function socialloginsettingsUpdateRequest(element) {
      console.log(element.accessToken)
      const accessToken = element.accessToken;
      delete element.accessToken;
      return dispatch => {
          dispatch(socialloginsettingsSetData(element));
          dispatch(socialloginsettingsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `social-login-settings/610cea8f3be536107bcb5c6f`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(socialloginsettingsSetData(a));
              dispatch(socialloginsettingsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(socialloginsettingsUpdateCheckSuccess(false));
              
          });
      };
  }

  export function socialloginsettingsDeleteRequest(element) {
      return dispatch => {
        dispatch(socialloginsettingsDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'delete',
            url: `social-login-settings/610cea913be536107bcb5c70`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(socialloginsettingsSetData(a));
            dispatch(socialloginsettingsDeleteCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(socialloginsettingsDeleteCheckSuccess(false));
           
          });
      };
  }