import{ 
  razorpaypaymentsGetCheckSuccess,
  razorpaypaymentsSetData,
  razorpaypaymentsPostCheckSuccess,
  razorpaypaymentsUpdateCheckSuccess,
  razorpaypaymentsDeleteCheckSuccess,

  } from '../actions';
  import axios from 'axios';
  let a = [];
  
  export function razorpaypaymentsGetRequest(element,token) {
  return dispatch => {
    dispatch(razorpaypaymentsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `razorpay-payments`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(razorpaypaymentsSetData(data));
        dispatch(razorpaypaymentsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(razorpaypaymentsGetCheckSuccess(false));
      });
  };
  };
  
  export function razorpaypaymentsPostRequest(element) {
      return dispatch => {
        dispatch(razorpaypaymentsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `razorpay-payments`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(razorpaypaymentsSetData(a));
            dispatch(razorpaypaymentsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(razorpaypaymentsPostCheckSuccess(false));
           
          });
      };
  }
  
  export function razorpaypaymentsUpdateRequest(element) {
      console.log(element.accessToken)
      const accessToken = element.accessToken;
      delete element.accessToken;
      return dispatch => {
          dispatch(razorpaypaymentsSetData(element));
          dispatch(razorpaypaymentsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `razorpay-payments/610ce96c3be536107bcb5c6d`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(razorpaypaymentsSetData(a));
              dispatch(razorpaypaymentsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(razorpaypaymentsUpdateCheckSuccess(false));
              
          });
      };
  }

  export function razorpaypaymentsDeleteRequest(element) {
      return dispatch => {
        dispatch(razorpaypaymentsDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'delete',
            url: `http://52.66.255.72:1337/razorpay-payments/610ce96e3be536107bcb5c6e`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(razorpaypaymentsSetData(a));
            dispatch(razorpaypaymentsDeleteCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(razorpaypaymentsDeleteCheckSuccess(false));
           
          });
      };
  }