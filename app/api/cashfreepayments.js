import{ 
    cashfreepaymentsGetCheckSuccess,
    cashfreepaymentsSetData,
    cashfreepaymentsPostCheckSuccess,
    cashfreepaymentsUpdateCheckSuccess,
    cashfreepaymentsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function cashfreepaymentsGetRequest(element,token) {
    return dispatch => {
      dispatch(cashfreepaymentsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `cash-free-payments`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(cashfreepaymentsSetData(data));
          dispatch(cashfreepaymentsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(cashfreepaymentsGetCheckSuccess(false));
        });
    };
    };
    
    export function cashfreepaymentsPostRequest(element) {
        return dispatch => {
          dispatch(cashfreepaymentsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `cash-free-payments`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(cashfreepaymentsSetData(a));
              dispatch(cashfreepaymentsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(cashfreepaymentsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function cashfreepaymentsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(cashfreepaymentsSetData(element));
            dispatch(cashfreepaymentsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `cash-free-payments/610cd7bf3be536107bcb5c1e`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(cashfreepaymentsSetData(a));
                dispatch(cashfreepaymentsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(cashfreepaymentsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function cashfreepaymentsDeleteRequest(element) {
        return dispatch => {
          dispatch(cashfreepaymentsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `cash-free-payments/610cd7c13be536107bcb5c1f`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(cashfreepaymentsSetData(a));
              dispatch(cashfreepaymentsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(cashfreepaymentsDeleteCheckSuccess(false));
             
            });
        };
    }