import {
  audioroomGetCheckSuccess,
  audioroomSetData,
  audioroomPostCheckSuccess,
  audioroomUpdateCheckSuccess,
  audioroomDeleteCheckSuccess,

} from '../actions';
import axios from 'axios';
let a = [];

export function audioroomGetRequest(element, token) {
  return dispatch => {
    dispatch(audioroomGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `audio-rooms`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(audioroomSetData(data));
        dispatch(audioroomGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(audioroomGetCheckSuccess(false));
      });
  };
};

export function audioroomPostRequest(objectValue) {
  console.log("objectValue",objectValue);
  return dispatch => {
    dispatch(audioroomPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `audio-rooms`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        "Content-Type": "application/json"
      },
      data: objectValue,
      
    })
      .then(response => {
        console.log("respponse", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(audioroomSetData(data));
        dispatch(audioroomPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(audioroomPostCheckSuccess(false));
      });
  };
}

export function audioroomUpdateRequest(element) {
  console.log(element.accessToken)
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(audioroomSetData(element));
    dispatch(audioroomUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: ``,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(audioroomSetData(a));
        dispatch(audioroomUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(audioroomUpdateCheckSuccess(false));

      });
  };
}

export function audioroomDeleteRequest(element) {
  return dispatch => {
    dispatch(audioroomDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: ``,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        const propertyValues = Object.values(data.data);
        const obj = { categoryID: element.id, categoryName: element.name, categoryImage: element.icon, subCategory: propertyValues }
        a.push(obj)
        dispatch(audioroomSetData(a));
        dispatch(audioroomDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(audioroomDeleteCheckSuccess(false));

      });
  };
}