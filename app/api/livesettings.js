import{ 
  livesettingsGetCheckSuccess,
  livesettingsSetData,
  livesettingsPostCheckSuccess,
  livesettingsUpdateCheckSuccess,
  livesettingsDeleteCheckSuccess,

  } from '../actions';
  import axios from 'axios';
  let a = [];
  
  export function livesettingsGetRequest(element,token) {
  return dispatch => {
    dispatch(livesettingsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `live-settings`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(livesettingsSetData(data));
        dispatch(livesettingsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(livesettingsGetCheckSuccess(false));
      });
  };
  };
  
  export function livesettingsPostRequest(element) {
      return dispatch => {
        dispatch(livesettingsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `live-settings`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(livesettingsSetData(a));
            dispatch(livesettingsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(livesettingsPostCheckSuccess(false));
           
          });
      };
  }
  
  export function livesettingsUpdateRequest(element) {
      console.log(element.accessToken)
      const accessToken = element.accessToken;
      delete element.accessToken;
      return dispatch => {
          dispatch(livesettingsSetData(element));
          dispatch(livesettingsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `live-settings/610cdc4b3be536107bcb5c34`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(livesettingsSetData(a));
              dispatch(livesettingsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(livesettingsUpdateCheckSuccess(false));
              
          });
      };
  }

  export function livesettingsDeleteRequest(element) {
      return dispatch => {
        dispatch(livesettingsDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'delete',
            url: `live-settings/610cdc4d3be536107bcb5c35`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(livesettingsSetData(a));
            dispatch(livesettingsDeleteCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(livesettingsDeleteCheckSuccess(false));
           
          });
      };
  }