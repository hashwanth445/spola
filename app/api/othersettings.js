import{ 
  othersettingsGetCheckSuccess,
  othersettingsSetData,
  othersettingsPostCheckSuccess,
  othersettingsUpdateCheckSuccess,
  othersettingsDeleteCheckSuccess,

  } from '../actions';
  import axios from 'axios';
  let a = [];
  
  export function othersettingsGetRequest(element,token) {
  return dispatch => {
    dispatch(othersettingsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `other-settings`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json"
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(othersettingsSetData(data));
        dispatch(othersettingsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin,'furnishings')
        dispatch(othersettingsGetCheckSuccess(false));
      });
  };
  };
  
  export function othersettingsPostRequest(element) {
      return dispatch => {
        dispatch(othersettingsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `other-settings`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(othersettingsSetData(a));
            dispatch(othersettingsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(othersettingsPostCheckSuccess(false));
           
          });
      };
  }
  
  export function othersettingsUpdateRequest(element) {
      console.log(element.accessToken)
      const accessToken = element.accessToken;
      delete element.accessToken;
      return dispatch => {
          dispatch(othersettingsSetData(element));
          dispatch(othersettingsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `other-settings/6107a4a2f331412de18c1eb5`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(othersettingsSetData(a));
              dispatch(othersettingsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(othersettingsUpdateCheckSuccess(false));
              
          });
      };
  }

  export function othersettingsDeleteRequest(element) {
      return dispatch => {
        dispatch(othersettingsDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'delete',
            url: `other-settings/610ce5523be536107bcb5c5c`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            const propertyValues = Object.values(data.data);
            const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
            a.push(obj)
            dispatch(othersettingsSetData(a));
            dispatch(othersettingsDeleteCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(othersettingsDeleteCheckSuccess(false));
           
          });
      };
  }