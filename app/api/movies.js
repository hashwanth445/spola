import{ 
    moviesGetCheckSuccess,
    moviesSetData,
    moviesPostCheckSuccess,
    moviesUpdateCheckSuccess,
    moviesDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function moviesGetRequest(element,token) {
    return dispatch => {
      dispatch(moviesGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `movies`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(moviesSetData(data));
          dispatch(moviesGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(moviesGetCheckSuccess(false));
        });
    };
    };
    
    export function moviesPostRequest(element) {
        return dispatch => {
          dispatch(moviesPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `movies`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(moviesSetData(a));
              dispatch(moviesPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(moviesPostCheckSuccess(false));
             
            });
        };
    }
    
    export function moviesUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(moviesSetData(element));
            dispatch(moviesUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `movies/611bafec4161ced881fda013`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(moviesSetData(a));
                dispatch(moviesUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(moviesUpdateCheckSuccess(false));
                
            });
        };
    }

    export function moviesDeleteRequest(element) {
        return dispatch => {
          dispatch(moviesDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `movies/611baff24161ced881fda014`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(moviesSetData(a));
              dispatch(moviesDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(moviesDeleteCheckSuccess(false));
             
            });
        };
    }