import{ 
    feedsGetCheckSuccess,
    feedsSetData,
    feedsPostCheckSuccess,
    feedsUpdateCheckSuccess,
    feedsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function feedsGetRequest(element,token) {
    return dispatch => {
      dispatch(feedsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `feeds?_sort=createdAt:DESC`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"api data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(feedsSetData(data));
          dispatch(feedsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(feedsGetCheckSuccess(false));
        });
    };
    };
    
    export function feedsPostRequest(selectedImage,title,description,userData) {
      console.log("elements",selectedImage,title,description,userData)
      const dataObj={
               feedDesc:description,
               name:title,
               backBlazeImageUrl:selectedImage,
               feedPostedBy:userData?.user?.id}
               console.log("dataObj",dataObj);
        return dispatch => {
          dispatch(feedsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `feeds`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"},
             data: dataObj
              })
            .then(response => {
              console.log("response feed",response)
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
             
              dispatch(feedsSetData(data));
              dispatch(feedsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(feedsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function feedsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(feedsSetData(element));
            dispatch(feedsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `feeds/611bae3b4161ced881fda009`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(feedsSetData(a));
                dispatch(feedsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(feedsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function feedsDeleteRequest(element) {
        return dispatch => {
          dispatch(feedsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `feeds/611bae424161ced881fda00a`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(feedsSetData(a));
              dispatch(feedsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(feedsDeleteCheckSuccess(false));
             
            });
        };
    }