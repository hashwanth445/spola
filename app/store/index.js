import AsyncStorage from '@react-native-async-storage/async-storage';
import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from '../reducers';
import axios from 'axios';

/**
 * Redux Setting
 */
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  timeout: 0,
  blacklist: [
    'loginReducer',
  ], // values will not be persisted
  whitelist: [
    "auth",
  "application",
  
  "accessTokenReducer",
  "logoutReducer",
  
  ], 
};

// Axios Middleware to send token
function createAxiosAuthMiddleware() {
  console.log("createAxiosAuthMiddleware");
  return ({getState}) => next => action => {
    const {accessToken} = getState().accessTokenReducer;
    console.log(accessToken)
    axios.defaults.headers.common.Authorization = accessToken
      ? `Bearer ${accessToken}`
      : null;

    return next(action);
  };
}

const axiosAuth = createAxiosAuthMiddleware();

const persistedReducer = persistReducer(persistConfig, rootReducer);

function configureStore(initialState = {}) {
  return createStore(
    persistedReducer,
    initialState,
    //composeEnhancers(applyMiddleware(thunk, axiosAuth))
   applyMiddleware(thunk, axiosAuth),
  );
}

// let middleware = [thunk];
// if (process.env.NODE_ENV === `development`) {
//   middleware.push(logger);
// }


const store = configureStore();
const persistor = persistStore(store);

export {store, persistor};
