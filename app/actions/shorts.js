import {
    SHORTS_GET_CHECK_SUCCESS,
    SHORTS_POST_CHECK_SUCCESS,
    SHORTS_UPDATE_CHECK_SUCCESS,
    SHORTS_DELETE_CHECK_SUCCESS,
    SHORTS_SET_DATA,
  } from './actionTypes';

  
  
  export function shortsGetCheckSuccess(bool = false) {
    return {
      type: SHORTS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function shortsPostCheckSuccess(bool = false) {
    return {
      type: SHORTS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function shortsUpdateCheckSuccess(bool = false) {
    return {
      type: SHORTS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function shortsDeleteCheckSuccess(bool = false) {
    return {
      type: SHORTS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function shortsSetData(object) {
    return {
      type: SHORTS_SET_DATA,
      payload: object,
    };
  }
  