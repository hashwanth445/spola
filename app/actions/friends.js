import {
    FRIENDS_GET_CHECK_SUCCESS,
    FRIENDS_POST_CHECK_SUCCESS,
    FRIENDS_UPDATE_CHECK_SUCCESS,
    FRIENDS_DELETE_CHECK_SUCCESS,
    FRIENDS_SET_DATA,
  } from './actionTypes';

  
  
  export function friendsGetCheckSuccess(bool = false) {
    return {
      type: FRIENDS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function friendsPostCheckSuccess(bool = false) {
    return {
      type: FRIENDS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function friendsUpdateCheckSuccess(bool = false) {
    return {
      type: FRIENDS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function friendsDeleteCheckSuccess(bool = false) {
    return {
      type: FRIENDS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function friendsSetData(object) {
    return {
      type: FRIENDS_SET_DATA,
      payload: object,
    };
  }
  