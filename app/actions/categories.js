import {
    CATEGORIES_GET_CHECK_SUCCESS,
    CATEGORIES_POST_CHECK_SUCCESS,
    CATEGORIES_UPDATE_CHECK_SUCCESS,
    CATEGORIES_DELETE_CHECK_SUCCESS,
    CATEGORIES_SET_DATA,
    CREATORS_SET_DATA,
  } from './actionTypes';

  
  
  export function categoriesGetCheckSuccess(bool = false) {
    return {
      type: CATEGORIES_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function categoriesPostCheckSuccess(bool = false) {
    return {
      type: CATEGORIES_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function categoriesUpdateCheckSuccess(bool = false) {
    return {
      type: CATEGORIES_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function categoriesDeleteCheckSuccess(bool = false) {
    return {
      type: CATEGORIES_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function categoriesSetData(object) {
    return {
      type: CATEGORIES_SET_DATA,
      payload: object,
    };
  }
  export function creatorsSetData(object) {
    return {
      type: CREATORS_SET_DATA,
      payload: object,
    };
  }
  