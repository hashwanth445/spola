import {
    PAYSERAPAYMENTS_GET_CHECK_SUCCESS,
    PAYSERAPAYMENTS_POST_CHECK_SUCCESS,
    PAYSERAPAYMENTS_UPDATE_CHECK_SUCCESS,
    PAYSERAPAYMENTS_DELETE_CHECK_SUCCESS,
    PAYSERAPAYMENTS_SET_DATA,
  } from './actionTypes';
  
  
  
  export function payserapaymentsGetCheckSuccess(bool = false) {
    return {
      type: PAYSERAPAYMENTS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function payserapaymentsPostCheckSuccess(bool = false) {
    return {
      type: PAYSERAPAYMENTS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function payserapaymentsUpdateCheckSuccess(bool = false) {
    return {
      type: PAYSERAPAYMENTS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function payserapaymentsDeleteCheckSuccess(bool = false) {
    return {
      type: PAYSERAPAYMENTS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function payserapaymentsSetData(object) {
    return {
      type: PAYSERAPAYMENTS_SET_DATA,
      payload: object,
    };
  }
  
  