import {
    MANAGEMOVIES_GET_CHECK_SUCCESS,
    MANAGEMOVIES_POST_CHECK_SUCCESS,
    MANAGEMOVIES_UPDATE_CHECK_SUCCESS,
    MANAGEMOVIES_DELETE_CHECK_SUCCESS,
    MANAGEMOVIES_SET_DATA,
  } from './actionTypes';

  
  
  export function managemoviesGetCheckSuccess(bool = false) {
    return {
      type: MANAGEMOVIES_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function managemoviesPostCheckSuccess(bool = false) {
    return {
      type: MANAGEMOVIES_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function managemoviesUpdateCheckSuccess(bool = false) {
    return {
      type: MANAGEMOVIES_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function managemoviesDeleteCheckSuccess(bool = false) {
    return {
      type: MANAGEMOVIES_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function managemoviesSetData(object) {
    return {
      type: MANAGEMOVIES_SET_DATA,
      payload: object,
    };
  }
  