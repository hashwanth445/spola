import {SIGNUP_DATA,SIGNUP_FAILURE, SIGNUP_SUCCESS, SIGNUP_LOADING,SIGNUP_PUT_LOADING,SIGNUP_PUT_CHANNEL_DATA_SUCCESS,SIGNUP_PUT_SUCCESS,SIGNUP_PUT_FAILURE, VERIFYOTP_SUCCESS, VERIFYOTP_FAILURE} from './actionTypes';

export function signupData(object) {
  return {
    type: SIGNUP_DATA,
    payload: object,
  };
}

export function signupLoading(bool = false) {
  return {
    type: SIGNUP_LOADING,
    payload: bool,
  };
}

export function signupSuccess(bool = false) {
  return {
    type: SIGNUP_SUCCESS,
    payload: bool,
  };
}
export function verifyOtpSucess(bool = false) {
  return {
    type: VERIFYOTP_SUCCESS,
    payload: bool,
  };
}
 
export function signupFailure(bool = false) {
  return {
    type: SIGNUP_FAILURE,
    payload: bool,
  };
}
export function signupPutLoading(bool = false) {
  return {
    type: SIGNUP_PUT_LOADING,
    payload: bool,
  };
}
export function signupPutSuccess(bool = false) {
  return {
    type: SIGNUP_PUT_SUCCESS,
    payload: bool,
  };
}


export function signupPutChannelDataSuccess(bool = false) {
  return {
    type: SIGNUP_PUT_CHANNEL_DATA_SUCCESS,
    payload: bool,
  };
}
export function signupPutFailure(bool = false) {
  return {
    type: SIGNUP_PUT_FAILURE,
    payload: bool,
  };
}
 
export function verifyOtpFailure(bool = false) {
  return {
    type: VERIFYOTP_FAILURE,
    payload: bool,
  };
}
