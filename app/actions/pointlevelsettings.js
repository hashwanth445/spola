import {
    POINTLEVELSETTINGS_GET_CHECK_SUCCESS,
    POINTLEVELSETTINGS_POST_CHECK_SUCCESS,
    POINTLEVELSETTINGS_UPDATE_CHECK_SUCCESS,
    POINTLEVELSETTINGS_DELETE_CHECK_SUCCESS,
    POINTLEVELSETTINGS_SET_DATA,
  } from './actionTypes';
  
  
  
  export function pointlevelsettingsGetCheckSuccess(bool = false) {
    return {
      type: POINTLEVELSETTINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function pointlevelsettingsPostCheckSuccess(bool = false) {
    return {
      type: POINTLEVELSETTINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function pointlevelsettingsUpdateCheckSuccess(bool = false) {
    return {
      type: POINTLEVELSETTINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function pointlevelsettingsDeleteCheckSuccess(bool = false) {
    return {
      type: POINTLEVELSETTINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function pointlevelsettingsSetData(object) {
    return {
      type: POINTLEVELSETTINGS_SET_DATA,
      payload: object,
    };
  }
  
  