import {
    DEVICE_GET_CHECK_SUCCESS,
    DEVICE_POST_CHECK_SUCCESS,
    DEVICE_UPDATE_CHECK_SUCCESS,
    DEVICE_DELETE_CHECK_SUCCESS,
    DEVICE_SET_DATA,
  } from './actionTypes';
  
  
  
  export function deviceGetCheckSuccess(bool = false) {
    return {
      type: DEVICE_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function devicePostCheckSuccess(bool = false) {
    return {
      type: DEVICE_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function deviceUpdateCheckSuccess(bool = false) {
    return {
      type: DEVICE_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function deviceDeleteCheckSuccess(bool = false) {
    return {
      type: DEVICE_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function deviceSetData(object) {
    return {
      type: DEVICE_SET_DATA,
      payload: object,
    };
  }
  