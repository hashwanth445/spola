import {ADD_QUEUE_QUEUE, SET_CURRENT_STATE, REMOVE_FROM_QUEUE, SET_VIDEO_LIST } from './actionTypes';



export const addToQueue = (data) => {
    return { type: ADD_QUEUE_QUEUE, data }
  }
  
export const setCurrent = (data) => {
    return { type: SET_CURRENT_STATE, data }
  };
  
  export const removeFromQueue = (data) => {
    return { type: REMOVE_FROM_QUEUE, data }
  };
  
  export const setVideoList = (data) => {
    return { type: SET_VIDEO_LIST, data }
  };
  