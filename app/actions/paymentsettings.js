import {
    PAYMENTSETTINGS_GET_CHECK_SUCCESS,
    PAYMENTSETTINGS_POST_CHECK_SUCCESS,
    PAYMENTSETTINGS_UPDATE_CHECK_SUCCESS,
    PAYMENTSETTINGS_DELETE_CHECK_SUCCESS,
    PAYMENTSETTINGS_SET_DATA,
  } from './actionTypes';
  
  
  
  export function paymentsettingsGetCheckSuccess(bool = false) {
    return {
      type: PAYMENTSETTINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function paymentsettingsPostCheckSuccess(bool = false) {
    return {
      type: PAYMENTSETTINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function paymentsettingsUpdateCheckSuccess(bool = false) {
    return {
      type: PAYMENTSETTINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function paymentsettingsDeleteCheckSuccess(bool = false) {
    return {
      type: PAYMENTSETTINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function paymentsettingsSetData(object) {
    return {
      type: PAYMENTSETTINGS_SET_DATA,
      payload: object,
    };
  }
  
  