import {
  CHANNEL_VIDEO_GET_CHECK_SUCCESS,
  CHANNEL_VIDEO_GET_CHECK_SUCCESS_FOR_USER,
  CHANNEL_VIDEO_POST_CHECK_SUCCESS,
  CHANNEL_VIDEO_POST_CHECK_SUCCESS_FOR_REWARD,
  CHANNEL_VIDEO_UPDATE_CHECK_SUCCESS,
  CHANNEL_VIDEO_DELETE_CHECK_SUCCESS,
  CHANNEL_VIDEO_SET_DATA,
  CHANNEL_VIDEO_SET_DATA_FOR_USER,
  CHANNEL_VIDEO_SET_DATA_FOR_REWARD,
} from './actionTypes';



export function channelVideoGetCheckSuccess(bool = false) {
  return {
    type: CHANNEL_VIDEO_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function channelVideoGetCheckSuccessForUser(bool = false) {
  return {
    type: CHANNEL_VIDEO_GET_CHECK_SUCCESS_FOR_USER,
    payload: bool,
  };
}
export function channelVideoPostCheckSuccess(bool = false) {
  return {
    type: CHANNEL_VIDEO_POST_CHECK_SUCCESS,
    payload: bool,
  };
}
export function channelVideoPostCheckSuccessForReward(bool = false) {
  return {
    type: CHANNEL_VIDEO_POST_CHECK_SUCCESS_FOR_REWARD,
    payload: bool,
  };
}

export function channelVideoUpdateCheckSuccess(bool = false) {
  return {
    type: CHANNEL_VIDEO_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function channelVideoDeleteCheckSuccess(bool = false) {
  return {
    type: CHANNEL_VIDEO_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function channelVideoSetData(object) {
  return {
    type: CHANNEL_VIDEO_SET_DATA,
    payload: object,
  };
}
export function channelVideoSetDataForReward(object) {
  return {
    type: CHANNEL_VIDEO_SET_DATA_FOR_REWARD,
    payload: object,
  };
}

export function channelVideoSetDataForUser(object) {
  return {
    type: CHANNEL_VIDEO_SET_DATA_FOR_USER,
    payload: object,
  };
}
