import {
  ORDERS_GET_CHECK_SUCCESS,
  ORDERS_GET_CHECK_SUCCESS_FOR_USER,
  ORDERS_POST_CHECK_SUCCESS,
  ORDERS_UPDATE_CHECK_SUCCESS,
  ORDERS_DELETE_CHECK_SUCCESS,
  ORDERS_SET_DATA,
  ORDERS_SET_DATA_FOR_USER,
} from './actionTypes';



export function ordersGetCheckSuccess(bool = false) {
  return {
    type: ORDERS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function ordersGetCheckSuccessForUser(bool = false) {
  return {
    type: ORDERS_GET_CHECK_SUCCESS_FOR_USER,
    payload: bool,
  };
}
export function ordersPostCheckSuccess(bool = false) {
  return {
    type: ORDERS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function ordersUpdateCheckSuccess(bool = false) {
  return {
    type: ORDERS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function ordersDeleteCheckSuccess(bool = false) {
  return {
    type: ORDERS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function ordersSetData(object) {
  return {
    type: ORDERS_SET_DATA,
    payload: object,
  };
}
export function ordersSetDataForUser(object) {
  return {
    type: ORDERS_SET_DATA_FOR_USER,
    payload: object,
  };
}
