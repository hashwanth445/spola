import {LOGIN_FAILURE, LOGIN_SUCCESS, LOGIN_LOADING} from './actionTypes';

export function loginLoading(bool = false) {
  console.log('comi')
  return {
    type: LOGIN_LOADING,
    payload: bool,
  };
}

export function loginSuccess(bool = false) {
  return {
    type: LOGIN_SUCCESS,
    payload: bool,
  };
}

export function loginFailure(bool = false) {
  return {
    type: LOGIN_FAILURE,
    payload: bool,
  };
}
