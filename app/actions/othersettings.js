import {
  OTHERSETTINGS_GET_CHECK_SUCCESS,
  OTHERSETTINGS_POST_CHECK_SUCCESS,
  OTHERSETTINGS_UPDATE_CHECK_SUCCESS,
  OTHERSETTINGS_DELETE_CHECK_SUCCESS,
  OTHERSETTINGS_SET_DATA,
} from './actionTypes';



export function othersettingsGetCheckSuccess(bool = false) {
  return {
    type: OTHERSETTINGS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function othersettingsPostCheckSuccess(bool = false) {
  return {
    type: OTHERSETTINGS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function othersettingsUpdateCheckSuccess(bool = false) {
  return {
    type: OTHERSETTINGS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function othersettingsDeleteCheckSuccess(bool = false) {
  return {
    type: OTHERSETTINGS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function othersettingsSetData(object) {
  return {
    type: OTHERSETTINGS_SET_DATA,
    payload: object,
  };
}

