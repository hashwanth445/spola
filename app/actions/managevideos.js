import {
  MANAGEVIDEOS_GET_CHECK_SUCCESS,
  MANAGEVIDEOS_POST_CHECK_SUCCESS,
  MANAGEVIDEOS_UPDATE_CHECK_SUCCESS,
  MANAGEVIDEOS_DELETE_CHECK_SUCCESS,
  MANAGEVIDEOS_SET_DATA,
} from './actionTypes';



export function managevideosGetCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function managevideosPostCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function managevideosUpdateCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function managevideosDeleteCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function managevideosSetData(object) {
  return {
    type: MANAGEVIDEOS_SET_DATA,
    payload: object,
  };
}
