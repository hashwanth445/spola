import {
  PRODUCTS_GET_CHECK_SUCCESS,
  PRODUCTS_POST_CHECK_SUCCESS,
  PRODUCTS_UPDATE_CHECK_SUCCESS,
  PRODUCTS_DELETE_CHECK_SUCCESS,
  PRODUCTS_SET_DATA,
} from './actionTypes';



export function productsGetCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function productsPostCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productsUpdateCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productsDeleteCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function productsSetData(object) {
  return {
    type: PRODUCTS_SET_DATA,
    payload: object,
  };
}
