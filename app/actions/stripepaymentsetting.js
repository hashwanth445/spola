import {
    STRIPEPAYMENTSETTING_GET_CHECK_SUCCESS,
    STRIPEPAYMENTSETTING_POST_CHECK_SUCCESS,
    STRIPEPAYMENTSETTING_UPDATE_CHECK_SUCCESS,
    STRIPEPAYMENTSETTING_DELETE_CHECK_SUCCESS,
    STRIPEPAYMENTSETTING_SET_DATA,
  } from './actionTypes';
  
  
  
  export function stripepaymentsettingGetCheckSuccess(bool = false) {
    return {
      type: STRIPEPAYMENTSETTING_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function stripepaymentsettingPostCheckSuccess(bool = false) {
    return {
      type: STRIPEPAYMENTSETTING_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function stripepaymentsettingUpdateCheckSuccess(bool = false) {
    return {
      type: STRIPEPAYMENTSETTING_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function stripepaymentsettingDeleteCheckSuccess(bool = false) {
    return {
      type: STRIPEPAYMENTSETTING_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function stripepaymentsettingSetData(object) {
    return {
      type: STRIPEPAYMENTSETTING_SET_DATA,
      payload: object,
    };
  }
  
  