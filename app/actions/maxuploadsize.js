import {
  MAXUPLOADSIZE_GET_CHECK_SUCCESS,
  MAXUPLOADSIZE_POST_CHECK_SUCCESS,
  MAXUPLOADSIZE_UPDATE_CHECK_SUCCESS,
  MAXUPLOADSIZE_DELETE_CHECK_SUCCESS,
  MAXUPLOADSIZE_SET_DATA,
} from './actionTypes';



export function maxuploadsizeGetCheckSuccess(bool = false) {
  return {
    type: MAXUPLOADSIZE_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function maxuploadsizePostCheckSuccess(bool = false) {
  return {
    type: MAXUPLOADSIZE_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function maxuploadsizeUpdateCheckSuccess(bool = false) {
  return {
    type: MAXUPLOADSIZE_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function maxuploadsizeDeleteCheckSuccess(bool = false) {
  return {
    type: MAXUPLOADSIZE_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function maxuploadsizeSetData(object) {
  return {
    type: MAXUPLOADSIZE_SET_DATA,
    payload: object,
  };
}
