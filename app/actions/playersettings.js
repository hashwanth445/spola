import {
    PLAYERSETTINGS_GET_CHECK_SUCCESS,
    PLAYERSETTINGS_POST_CHECK_SUCCESS,
    PLAYERSETTINGS_UPDATE_CHECK_SUCCESS,
    PLAYERSETTINGS_DELETE_CHECK_SUCCESS,
    PLAYERSETTINGS_SET_DATA,
  } from './actionTypes';

  
  
  export function playersettingsGetCheckSuccess(bool = false) {
    return {
      type: PLAYERSETTINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function playersettingsPostCheckSuccess(bool = false) {
    return {
      type: PLAYERSETTINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function playersettingsUpdateCheckSuccess(bool = false) {
    return {
      type: PLAYERSETTINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function playersettingsDeleteCheckSuccess(bool = false) {
    return {
      type: PLAYERSETTINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function playersettingsSetData(object) {
    return {
      type: PLAYERSETTINGS_SET_DATA,
      payload: object,
    };
  }
  