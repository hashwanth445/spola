import {
    MOVIES_GET_CHECK_SUCCESS,
    MOVIES_POST_CHECK_SUCCESS,
    MOVIES_UPDATE_CHECK_SUCCESS,
    MOVIES_DELETE_CHECK_SUCCESS,
    MOVIES_SET_DATA,
  } from './actionTypes';
  
  
  
  export function moviesGetCheckSuccess(bool = false) {
    return {
      type: MOVIES_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function moviesPostCheckSuccess(bool = false) {
    return {
      type: MOVIES_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function moviesUpdateCheckSuccess(bool = false) {
    return {
      type: MOVIES_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function moviesDeleteCheckSuccess(bool = false) {
    return {
      type: MOVIES_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function moviesSetData(object) {
    return {
      type: MOVIES_SET_DATA,
      payload: object,
    };
  }
  