import {
    MANAGEUSERADS_GET_CHECK_SUCCESS,
    MANAGEUSERADS_POST_CHECK_SUCCESS,
    MANAGEUSERADS_UPDATE_CHECK_SUCCESS,
    MANAGEUSERADS_DELETE_CHECK_SUCCESS,
    MANAGEUSERADS_SET_DATA,
  } from './actionTypes';

  
  
  export function manageuseradsGetCheckSuccess(bool = false) {
    return {
      type: MANAGEUSERADS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function manageuseradsPostCheckSuccess(bool = false) {
    return {
      type: MANAGEUSERADS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function manageuseradsUpdateCheckSuccess(bool = false) {
    return {
      type: MANAGEUSERADS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function manageuseradsDeleteCheckSuccess(bool = false) {
    return {
      type: MANAGEUSERADS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function manageuseradsSetData(object) {
    return {
      type: MANAGEUSERADS_SET_DATA,
      payload: object,
    };
  }
  