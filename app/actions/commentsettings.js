import {
    COMMENTSETTINGS_GET_CHECK_SUCCESS,
    COMMENTSETTINGS_POST_CHECK_SUCCESS,
    COMMENTSETTINGS_UPDATE_CHECK_SUCCESS,
    COMMENTSETTINGS_DELETE_CHECK_SUCCESS,
    COMMENTSETTINGS_SET_DATA,
  } from './actionTypes';

  
  
  export function commentsettingsGetCheckSuccess(bool = false) {
    return {
      type: COMMENTSETTINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function commentsettingsPostCheckSuccess(bool = false) {
    return {
      type: COMMENTSETTINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function commentsettingsUpdateCheckSuccess(bool = false) {
    return {
      type: COMMENTSETTINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function commentsettingsDeleteCheckSuccess(bool = false) {
    return {
      type: COMMENTSETTINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function commentsettingsSetData(object) {
    return {
      type: COMMENTSETTINGS_SET_DATA,
      payload: object,
    };
  }
  