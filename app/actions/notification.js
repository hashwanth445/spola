import {
    NOTIFICATION_GET_CHECK_SUCCESS,
    NOTIFICATION_POST_CHECK_SUCCESS,
    NOTIFICATION_UPDATE_CHECK_SUCCESS,
    NOTIFICATION_DELETE_CHECK_SUCCESS,
    NOTIFICATION_SET_DATA,
  } from './actionTypes';

  
  
  export function notificationGetCheckSuccess(bool = false) {
    return {
      type: NOTIFICATION_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function notificationPostCheckSuccess(bool = false) {
    return {
      type: NOTIFICATION_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function notificationUpdateCheckSuccess(bool = false) {
    return {
      type: NOTIFICATION_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function notificationDeleteCheckSuccess(bool = false) {
    return {
      type: NOTIFICATION_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function notificationSetData(object) {
    return {
      type: NOTIFICATION_SET_DATA,
      payload: object,
    };
  }
  