import {
    CHATS_GET_CHECK_SUCCESS,
    CHATS_POST_CHECK_SUCCESS,
    CHATS_UPDATE_CHECK_SUCCESS,
    CHATS_DELETE_CHECK_SUCCESS,
    CHATS_SET_DATA,
  } from './actionTypes';
  
  
  //chats
  export function chatsGetCheckSuccess(bool = false) {
    return {
      type: CHATS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function chatsPostCheckSuccess(bool = false) {
    return {
      type: CHATS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function chatsUpdateCheckSuccess(bool = false) {
    return {
      type: CHATS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function chatsDeleteCheckSuccess(bool = false) {
    return {
      type: CHATS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function chatsSetData(object) {
    return {
      type: CHATS_SET_DATA,
      payload: object,
    };
  }
  