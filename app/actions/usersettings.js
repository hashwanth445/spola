import {
    USERSETTINGS_GET_CHECK_SUCCESS,
    USERSETTINGS_POST_CHECK_SUCCESS,
    USERSETTINGS_UPDATE_CHECK_SUCCESS,
    USERSETTINGS_DELETE_CHECK_SUCCESS,
    USERSETTINGS_SET_DATA,
  } from './actionTypes';

  
  
  export function usersettingsGetCheckSuccess(bool = false) {
    return {
      type: USERSETTINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function usersettingsPostCheckSuccess(bool = false) {
    return {
      type: USERSETTINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function usersettingsUpdateCheckSuccess(bool = false) {
    return {
      type: USERSETTINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function usersettingsDeleteCheckSuccess(bool = false) {
    return {
      type: USERSETTINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function usersettingsSetData(object) {
    return {
      type: USERSETTINGS_SET_DATA,
      payload: object,
    };
  }
  