import {
    BGMS_GET_CHECK_SUCCESS,
    BGMS_POST_CHECK_SUCCESS,
    BGMS_UPDATE_CHECK_SUCCESS,
    BGMS_DELETE_CHECK_SUCCESS,
    BGMS_SET_DATA,
  } from './actionTypes';

  
  
  export function bgmsGetCheckSuccess(bool = false) {
    return {
      type: BGMS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function bgmsPostCheckSuccess(bool = false) {
    return {
      type: BGMS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function bgmsUpdateCheckSuccess(bool = false) {
    return {
      type: BGMS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function bgmsDeleteCheckSuccess(bool = false) {
    return {
      type: BGMS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function bgmsSetData(object) {
    return {
      type: BGMS_SET_DATA,
      payload: object,
    };
  }
  