import {
    WALLET_GET_CHECK_SUCCESS,
    WALLET_POST_CHECK_SUCCESS,
    WALLET_UPDATE_CHECK_SUCCESS,
    WALLET_DELETE_CHECK_SUCCESS,
    WALLET_SET_DATA,
  } from './actionTypes';

  
  
  export function walletGetCheckSuccess(bool = false) {
    return {
      type: WALLET_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function walletPostCheckSuccess(bool = false) {
    return {
      type: WALLET_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function walletUpdateCheckSuccess(bool = false) {
    return {
      type: WALLET_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function walletDeleteCheckSuccess(bool = false) {
    return {
      type: WALLET_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function walletSetData(object) {
    return {
      type: WALLET_SET_DATA,
      payload: object,
    };
  }
  