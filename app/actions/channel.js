import {
  CHANNEL_GET_CHECK_SUCCESS,
  CHANNEL_GET_CHECK_SUCCESS_FOR_USER,
  CHANNEL_POST_CHECK_SUCCESS,
  CHANNEL_UPDATE_CHECK_SUCCESS,
  CHANNEL_DELETE_CHECK_SUCCESS,
  CHANNEL_SET_DATA,
  CHANNEL_SET_DATA_FOR_USER,
} from './actionTypes';



export function channelGetCheckSuccess(bool = false) {
  return {
    type: CHANNEL_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function channelGetCheckSuccessForUser(bool = false) {
  return {
    type: CHANNEL_GET_CHECK_SUCCESS_FOR_USER,
    payload: bool,
  };
}
export function channelPostCheckSuccess(bool = false) {
  return {
    type: CHANNEL_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function channelUpdateCheckSuccess(bool = false) {
  return {
    type: CHANNEL_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function channelDeleteCheckSuccess(bool = false) {
  return {
    type: CHANNEL_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function channelSetData(object) {
  return {
    type: CHANNEL_SET_DATA,
    payload: object,
  };
}
export function channelSetDataForUser(object) {
  return {
    type: CHANNEL_SET_DATA_FOR_USER,
    payload: object,
  };
}
