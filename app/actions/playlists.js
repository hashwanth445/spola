import {
    PLAYLISTS_GET_CHECK_SUCCESS,
    PLAYLISTS_POST_CHECK_SUCCESS,
    PLAYLISTS_UPDATE_CHECK_SUCCESS,
    PLAYLISTS_DELETE_CHECK_SUCCESS,
    PLAYLISTS_SET_DATA,
  } from './actionTypes';

  
  
  export function playlistsGetCheckSuccess(bool = false) {
    return {
      type: PLAYLISTS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function playlistsPostCheckSuccess(bool = false) {
    return {
      type: PLAYLISTS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function playlistsUpdateCheckSuccess(bool = false) {
    return {
      type: PLAYLISTS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function playlistsDeleteCheckSuccess(bool = false) {
    return {
      type: PLAYLISTS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function playlistsSetData(object) {
    return {
      type: PLAYLISTS_SET_DATA,
      payload: object,
    };
  }
  