import {
    PAYSTACKPAYMENTS_GET_CHECK_SUCCESS,
    PAYSTACKPAYMENTS_POST_CHECK_SUCCESS,
    PAYSTACKPAYMENTS_UPDATE_CHECK_SUCCESS,
    PAYSTACKPAYMENTS_DELETE_CHECK_SUCCESS,
    PAYSTACKPAYMENTS_SET_DATA,
  } from './actionTypes';
  
  
  
  export function paystackpaymentsGetCheckSuccess(bool = false) {
    return {
      type: PAYSTACKPAYMENTS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function paystackpaymentsPostCheckSuccess(bool = false) {
    return {
      type: PAYSTACKPAYMENTS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function paystackpaymentsUpdateCheckSuccess(bool = false) {
    return {
      type: PAYSTACKPAYMENTS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function paystackpaymentsDeleteCheckSuccess(bool = false) {
    return {
      type: PAYSTACKPAYMENTS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function paystackpaymentsSetData(object) {
    return {
      type: PAYSTACKPAYMENTS_SET_DATA,
      payload: object,
    };
  }
  
  