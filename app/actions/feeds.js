import {
    FEEDS_GET_CHECK_SUCCESS,
    FEEDS_POST_CHECK_SUCCESS,
    FEEDS_UPDATE_CHECK_SUCCESS,
    FEEDS_DELETE_CHECK_SUCCESS,
    FEEDS_SET_DATA,
  } from './actionTypes';

  
  
  export function feedsGetCheckSuccess(bool = false) {
    return {
      type: FEEDS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function feedsPostCheckSuccess(bool = false) {
    return {
      type: FEEDS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function feedsUpdateCheckSuccess(bool = false) {
    return {
      type: FEEDS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function feedsDeleteCheckSuccess(bool = false) {
    return {
      type: FEEDS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function feedsSetData(object) {
    return {
      type: FEEDS_SET_DATA,
      payload: object,
    };
  }
  