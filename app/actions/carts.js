import {
  CARTS_GET_CHECK_SUCCESS,
  CARTS_GET_CHECK_SUCCESS_FOR_USER,
  CARTS_POST_CHECK_SUCCESS,
  CARTS_UPDATE_CHECK_SUCCESS,
  CARTS_DELETE_CHECK_SUCCESS,
  CARTS_SET_DATA,
  CARTS_SET_DATA_FOR_USER,
} from './actionTypes';



export function cartsGetCheckSuccess(bool = false) {
  return {
    type: CARTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function cartsGetCheckSuccessForUser(bool = false) {
  return {
    type: CARTS_GET_CHECK_SUCCESS_FOR_USER,
    payload: bool,
  };
}
export function cartsPostCheckSuccess(bool = false) {
  return {
    type: CARTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function cartsUpdateCheckSuccess(bool = false) {
  return {
    type: CARTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function cartsDeleteCheckSuccess(bool = false) {
  return {
    type: CARTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function cartsSetData(object) {
  return {
    type: CARTS_SET_DATA,
    payload: object,
  };
}
export function cartsSetDataForUser(object) {
  return {
    type: CARTS_SET_DATA_FOR_USER,
    payload: object,
  };
}
