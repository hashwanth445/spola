import {
    AUDIOROOM_GET_CHECK_SUCCESS,
    AUDIOROOM_POST_CHECK_SUCCESS,
    AUDIOROOM_UPDATE_CHECK_SUCCESS,
    AUDIOROOM_DELETE_CHECK_SUCCESS,
    AUDIOROOM_SET_DATA,
  } from './actionTypes';

  
  
  export function audioroomGetCheckSuccess(bool = false) {
    return {
      type: AUDIOROOM_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function audioroomPostCheckSuccess(bool = false) {
    return {
      type: AUDIOROOM_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function audioroomUpdateCheckSuccess(bool = false) {
    return {
      type: AUDIOROOM_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function audioroomDeleteCheckSuccess(bool = false) {
    return {
      type: AUDIOROOM_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function audioroomSetData(object) {
    return {
      type: AUDIOROOM_SET_DATA,
      payload: object,
    };
  }
  