import {
    MAINSETTINGS_GET_CHECK_SUCCESS,
    MAINSETTINGS_POST_CHECK_SUCCESS,
    MAINSETTINGS_UPDATE_CHECK_SUCCESS,
    MAINSETTINGS_DELETE_CHECK_SUCCESS,

    MAINSETTINGS_SET_DATA,
  } from './actionTypes';

  
  
  export function mainsettingsGetCheckSuccess(bool = false) {
    return {
      type:  MAINSETTINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function mainsettingsPostCheckSuccess(bool = false) {
    return {
      type:  MAINSETTINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function mainsettingsUpdateCheckSuccess(bool = false) {
    return {
      type:  MAINSETTINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function mainsettingsDeleteCheckSuccess(bool = false) {
    return {
      type:  MAINSETTINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function mainsettingsSetData(object) {
    return {
      type:  MAINSETTINGS_SET_DATA,
      payload: object,
    };
  }
  