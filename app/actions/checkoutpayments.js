import {
    CHECKOUTPAYMENTS_GET_CHECK_SUCCESS,
    CHECKOUTPAYMENTS_POST_CHECK_SUCCESS,
    CHECKOUTPAYMENTS_UPDATE_CHECK_SUCCESS,
    CHECKOUTPAYMENTS_DELETE_CHECK_SUCCESS,
    CHECKOUTPAYMENTS_SET_DATA,
  } from './actionTypes';

  
  
  export function checkoutpaymentsGetCheckSuccess(bool = false) {
    return {
      type: CHECKOUTPAYMENTS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function checkoutpaymentsPostCheckSuccess(bool = false) {
    return {
      type: CHECKOUTPAYMENTS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function checkoutpaymentsUpdateCheckSuccess(bool = false) {
    return {
      type: CHECKOUTPAYMENTS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function checkoutpaymentsDeleteCheckSuccess(bool = false) {
    return {
      type: CHECKOUTPAYMENTS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function checkoutpaymentsSetData(object) {
    return {
      type: CHECKOUTPAYMENTS_SET_DATA,
      payload: object,
    };
  }
  