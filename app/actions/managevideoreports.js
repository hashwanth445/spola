import {
  MANAGEVIDEOREPORTS_GET_CHECK_SUCCESS,
  MANAGEVIDEOREPORTS_POST_CHECK_SUCCESS,
  MANAGEVIDEOREPORTS_UPDATE_CHECK_SUCCESS,
  MANAGEVIDEOREPORTS_DELETE_CHECK_SUCCESS,
  MANAGEVIDEOREPORTS_SET_DATA,
} from './actionTypes';



export function managevideoreportsGetCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOREPORTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function managevideoreportsPostCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOREPORTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function managevideoreportsUpdateCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOREPORTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function managevideoreportsDeleteCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOREPORTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function managevideoreportsSetData(object) {
  return {
    type: MANAGEVIDEOREPORTS_SET_DATA,
    payload: object,
  };
}
