import {
    LIVESETTINGS_GET_CHECK_SUCCESS,
    LIVESETTINGS_POST_CHECK_SUCCESS,
    LIVESETTINGS_UPDATE_CHECK_SUCCESS,
    LIVESETTINGS_DELETE_CHECK_SUCCESS,
    LIVESETTINGS_SET_DATA,
  } from './actionTypes';

  
  
  export function livesettingsGetCheckSuccess(bool = false) {
    return {
      type: LIVESETTINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function livesettingsPostCheckSuccess(bool = false) {
    return {
      type: LIVESETTINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function livesettingsUpdateCheckSuccess(bool = false) {
    return {
      type: LIVESETTINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function livesettingsDeleteCheckSuccess(bool = false) {
    return {
      type: LIVESETTINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function livesettingsSetData(object) {
    return {
      type: LIVESETTINGS_SET_DATA,
      payload: object,
    };
  }
  