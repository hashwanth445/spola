import {
    UPLOADLIMIT_GET_CHECK_SUCCESS,
    UPLOADLIMIT_POST_CHECK_SUCCESS,
    UPLOADLIMIT_UPDATE_CHECK_SUCCESS,
    UPLOADLIMIT_DELETE_CHECK_SUCCESS,
    UPLOADLIMIT_SET_DATA,
  } from './actionTypes';
  
  
  
  export function uploadlimitGetCheckSuccess(bool = false) {
    return {
      type: UPLOADLIMIT_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function uploadlimitPostCheckSuccess(bool = false) {
    return {
      type: UPLOADLIMIT_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function uploadlimitUpdateCheckSuccess(bool = false) {
    return {
      type: UPLOADLIMIT_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function uploadlimitDeleteCheckSuccess(bool = false) {
    return {
      type: UPLOADLIMIT_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function uploadlimitSetData(object) {
    return {
      type: UPLOADLIMIT_SET_DATA,
      payload: object,
    };
  }
  
  