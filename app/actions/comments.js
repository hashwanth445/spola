import {
    COMMENTS_GET_CHECK_SUCCESS,
    COMMENTS_POST_CHECK_SUCCESS,
    COMMENTS_UPDATE_CHECK_SUCCESS,
    COMMENTS_DELETE_CHECK_SUCCESS,
    COMMENTS_SET_DATA,
    COMMENTS_SET_DATA_FOR_FEED,
    COMMENTS_GET_CHECK_SUCCESS_FOR_FEED,
  } from './actionTypes';
  
  
  
  export function commentsGetCheckSuccess(bool = false) {
    return {
      type: COMMENTS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function commentsGetCheckSuccessForFeed(bool = false) {
    return {
      type: COMMENTS_GET_CHECK_SUCCESS_FOR_FEED,
      payload: bool,
    };
  }
  export function commentsPostCheckSuccess(bool = false) {
    return {
      type: COMMENTS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function commentsUpdateCheckSuccess(bool = false) {
    return {
      type: COMMENTS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function commentsDeleteCheckSuccess(bool = false) {
    return {
      type: COMMENTS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function commentsSetData(object) {
    return {
      type: COMMENTS_SET_DATA,
      payload: object,
    };
  }
  export function commentsSetDataForFeed(object) {
    return {
      type: COMMENTS_SET_DATA_FOR_FEED,
      payload: object,
    };
  }
  