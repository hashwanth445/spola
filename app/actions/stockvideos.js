import {
    STOCKVIDEOS_GET_CHECK_SUCCESS,
    STOCKVIDEOS_POST_CHECK_SUCCESS,
    STOCKVIDEOS_UPDATE_CHECK_SUCCESS,
    STOCKVIDEOS_DELETE_CHECK_SUCCESS,
    STOCKVIDEOS_SET_DATA,
  } from './actionTypes';
  
  
  
  export function stockvideosGetCheckSuccess(bool = false) {
    return {
      type: STOCKVIDEOS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function stockvideosPostCheckSuccess(bool = false) {
    return {
      type: STOCKVIDEOS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function stockvideosUpdateCheckSuccess(bool = false) {
    return {
      type: STOCKVIDEOS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function stockvideosDeleteCheckSuccess(bool = false) {
    return {
      type: STOCKVIDEOS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function stockvideosSetData(object) {
    return {
      type: STOCKVIDEOS_SET_DATA,
      payload: object,
    };
  }
  
  