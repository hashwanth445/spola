import {
  VIDEOS_GET_CHECK_SUCCESS,
  VIDEOS_POST_CHECK_SUCCESS,
  VIDEOS_UPDATE_CHECK_SUCCESS,
  VIDEOS_DELETE_CHECK_SUCCESS,
  VIDEOS_SET_DATA,
  VIDEOS_SEARCH_SET_DATA,
} from './actionTypes';

export function videosGetCheckSuccess(bool = false) {
  return {
    type: VIDEOS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function videosPostCheckSuccess(bool = false) {
  return {
    type: VIDEOS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function videosUpdateCheckSuccess(bool = false) {
  return {
    type: VIDEOS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function videosDeleteCheckSuccess(bool = false) {
  return {
    type: VIDEOS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function videosSetData(object) {
  return {
    type: VIDEOS_SET_DATA,
    payload: object,
  };
}

export function videosSearchSetData(object) {
  return {
    type: VIDEOS_SEARCH_SET_DATA,
    payload: object,
  };
}
