import {
    FILES_GET_CHECK_SUCCESS,
    FILES_POST_CHECK_SUCCESS,
    FILES_UPDATE_CHECK_SUCCESS,
    FILES_DELETE_CHECK_SUCCESS,
    FILES_SET_DATA,
  } from './actionTypes';

  
  
  export function filesGetCheckSuccess(bool = false) {
    return {
      type: FILES_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function filesPostCheckSuccess(bool = false) {
    return {
      type: FILES_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function filesUpdateCheckSuccess(bool = false) {
    return {
      type: FILES_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function filesDeleteCheckSuccess(bool = false) {
    return {
      type: FILES_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function filesSetData(object) {
    return {
      type: FILES_SET_DATA,
      payload: object,
    };
  }
  