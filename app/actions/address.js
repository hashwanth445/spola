import {
    ADDRESS_GET_CHECK_SUCCESS,
    ADDRESS_POST_CHECK_SUCCESS,
    ADDRESS_UPDATE_CHECK_SUCCESS,
    ADDRESS_DELETE_CHECK_SUCCESS,
    ADDRESS_SET_DATA,
  } from './actionTypes';
  
  
  
  export function addressGetCheckSuccess(bool = false) {
    return {
      type: ADDRESS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function addressPostCheckSuccess(bool = false) {
    return {
      type: ADDRESS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function addressUpdateCheckSuccess(bool = false) {
    return {
      type: ADDRESS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function addressDeleteCheckSuccess(bool = false) {
    return {
      type: ADDRESS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function addressSetData(object) {
    return {
      type: ADDRESS_SET_DATA,
      payload: object,
    };
  }
  