import {
  DISLIKE_GET_CHECK_SUCCESS,
  DISLIKE_POST_CHECK_SUCCESS,
  DISLIKE_UPDATE_CHECK_SUCCESS,
  DISLIKE_DELETE_CHECK_SUCCESS,
  DISLIKE_SET_DATA,
  DISLIKE_GET_CHECK_SUCCESS_BY_USER,
  DISLIKE_SET_DATA_BY_USER
} from './actionTypes';



export function dislikeGetCheckSuccess(bool = false) {
  return {
    type: DISLIKE_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function dislikeGetCheckSuccessByUser(bool = false) {
  return {
    type: DISLIKE_GET_CHECK_SUCCESS_BY_USER,
    payload: bool,
  };
}
export function dislikePostCheckSuccess(bool = false) {
  return {
    type: DISLIKE_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function dislikeUpdateCheckSuccess(bool = false) {
  return {
    type: DISLIKE_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function dislikeDeleteCheckSuccess(bool = false) {
  return {
    type: DISLIKE_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function dislikeSetData(object) {
  return {
    type: DISLIKE_SET_DATA,
    payload: object,
  };
}
export function dislikeSetDataByUser(object) {
  return {
    type: DISLIKE_SET_DATA_BY_USER,
    payload: object,
  };
}
