import {
    HISTORIES_GET_CHECK_SUCCESS,
    HISTORIES_POST_CHECK_SUCCESS,
    HISTORIES_UPDATE_CHECK_SUCCESS,
    HISTORIES_DELETE_CHECK_SUCCESS,
    HISTORIES_SET_DATA,
  } from './actionTypes';

  
  
  export function historiesGetCheckSuccess(bool = false) {
    return {
      type: HISTORIES_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function historiesPostCheckSuccess(bool = false) {
    return {
      type: HISTORIES_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function historiesUpdateCheckSuccess(bool = false) {
    return {
      type: HISTORIES_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function historiesDeleteCheckSuccess(bool = false) {
    return {
      type: HISTORIES_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function historiesSetData(object) {
    return {
      type: HISTORIES_SET_DATA,
      payload: object,
    };
  }
  