import {
  MANAGEVIDEOADS_GET_CHECK_SUCCESS,
  MANAGEVIDEOADS_POST_CHECK_SUCCESS,
  MANAGEVIDEOADS_UPDATE_CHECK_SUCCESS,
  MANAGEVIDEOADS_DELETE_CHECK_SUCCESS,
  MANAGEVIDEOADS_SET_DATA,
} from './actionTypes';



export function managevideoadsGetCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOADS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function managevideoadsPostCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOADS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function managevideoadsUpdateCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOADS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function managevideoadsDeleteCheckSuccess(bool = false) {
  return {
    type: MANAGEVIDEOADS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function managevideoadsSetData(object) {
  return {
    type: MANAGEVIDEOADS_SET_DATA,
    payload: object,
  };
}
