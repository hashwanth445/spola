import {
    TRENDINGS_GET_CHECK_SUCCESS,
    TRENDINGS_POST_CHECK_SUCCESS,
    TRENDINGS_UPDATE_CHECK_SUCCESS,
    TRENDINGS_DELETE_CHECK_SUCCESS,
    TRENDINGS_SET_DATA,
  } from './actionTypes';

  
  
  export function trendingsGetCheckSuccess(bool = false) {
    return {
      type: TRENDINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function trendingsPostCheckSuccess(bool = false) {
    return {
      type: TRENDINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function trendingsUpdateCheckSuccess(bool = false) {
    return {
      type: TRENDINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function trendingsDeleteCheckSuccess(bool = false) {
    return {
      type: TRENDINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function trendingsSetData(object) {
    return {
      type: TRENDINGS_SET_DATA,
      payload: object,
    };
  }
  