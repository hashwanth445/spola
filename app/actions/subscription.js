import {
    SUBSCRIPTION_GET_CHECK_SUCCESS,
    SUBSCRIPTION_POST_CHECK_SUCCESS,
    SUBSCRIPTION_UPDATE_CHECK_SUCCESS,
    SUBSCRIPTION_DELETE_CHECK_SUCCESS,
    SUBSCRIPTION_SET_DATA,
  } from './actionTypes';

  
  
  export function subscriptionGetCheckSuccess(bool = false) {
    return {
      type: SUBSCRIPTION_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function subscriptionPostCheckSuccess(bool = false) {
    return {
      type: SUBSCRIPTION_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function subscriptionUpdateCheckSuccess(bool = false) {
    return {
      type: SUBSCRIPTION_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function subscriptionDeleteCheckSuccess(bool = false) {
    return {
      type: SUBSCRIPTION_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function subscriptionSetData(object) {
    return {
      type: SUBSCRIPTION_SET_DATA,
      payload: object,
    };
  }
  