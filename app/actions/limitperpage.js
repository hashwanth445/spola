import {
    LIMITPERPAGE_GET_CHECK_SUCCESS,
    LIMITPERPAGE_POST_CHECK_SUCCESS,
    LIMITPERPAGE_UPDATE_CHECK_SUCCESS,
    LIMITPERPAGE_DELETE_CHECK_SUCCESS,
    LIMITPERPAGE_SET_DATA,
  } from './actionTypes';

  
  
  export function limitperpageGetCheckSuccess(bool = false) {
    return {
      type: LIMITPERPAGE_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function limitperpagePostCheckSuccess(bool = false) {
    return {
      type: LIMITPERPAGE_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function limitperpageUpdateCheckSuccess(bool = false) {
    return {
      type: LIMITPERPAGE_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function limitperpageDeleteCheckSuccess(bool = false) {
    return {
      type: LIMITPERPAGE_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function limitperpageSetData(object) {
    return {
      type: LIMITPERPAGE_SET_DATA,
      payload: object,
    };
  }
  