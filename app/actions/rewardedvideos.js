import {
    REWARDEDVIDEOS_GET_CHECK_SUCCESS,
    REWARDEDVIDEOS_POST_CHECK_SUCCESS,
    REWARDEDVIDEOS_UPDATE_CHECK_SUCCESS,
    REWARDEDVIDEOS_DELETE_CHECK_SUCCESS,
    REWARDEDVIDEOS_SET_DATA,
  } from './actionTypes';
  
  
  
  export function rewardedvideosGetCheckSuccess(bool = false) {
    return {
      type: REWARDEDVIDEOS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function rewardedvideosPostCheckSuccess(bool = false) {
    return {
      type: REWARDEDVIDEOS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function rewardedvideosUpdateCheckSuccess(bool = false) {
    return {
      type: REWARDEDVIDEOS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function rewardedvideosDeleteCheckSuccess(bool = false) {
    return {
      type: REWARDEDVIDEOS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function rewardedvideosSetData(object) {
    return {
      type: REWARDEDVIDEOS_SET_DATA,
      payload: object,
    };
  }
  
  