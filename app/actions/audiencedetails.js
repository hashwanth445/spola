import {
    AUDIENCEDETAILS_GET_CHECK_SUCCESS,
    AUDIENCEDETAILS_POST_CHECK_SUCCESS,
    AUDIENCEDETAILS_UPDATE_CHECK_SUCCESS,
    AUDIENCEDETAILS_DELETE_CHECK_SUCCESS,
    AUDIENCEDETAILS_SET_DATA,
  } from './actionTypes';

  
  
  export function audiencedetailsGetCheckSuccess(bool = false) {
    return {
      type: AUDIENCEDETAILS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function audiencedetailsPostCheckSuccess(bool = false) {
    return {
      type: AUDIENCEDETAILS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function audiencedetailsUpdateCheckSuccess(bool = false) {
    return {
      type: AUDIENCEDETAILS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function audiencedetailsDeleteCheckSuccess(bool = false) {
    return {
      type: AUDIENCEDETAILS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function audiencedetailsSetData(object) {
    return {
      type: AUDIENCEDETAILS_SET_DATA,
      payload: object,
    };
  }
  