import {
  PAIDSUBSCRIBERSSETTING_GET_CHECK_SUCCESS,
  PAIDSUBSCRIBERSSETTING_POST_CHECK_SUCCESS,
  PAIDSUBSCRIBERSSETTING_UPDATE_CHECK_SUCCESS,
  PAIDSUBSCRIBERSSETTING_DELETE_CHECK_SUCCESS,
  PAIDSUBSCRIBERSSETTING_SET_DATA,
} from './actionTypes';



export function paidsubscriberssettingGetCheckSuccess(bool = false) {
  return {
    type: PAIDSUBSCRIBERSSETTING_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function paidsubscriberssettingPostCheckSuccess(bool = false) {
  return {
    type: PAIDSUBSCRIBERSSETTING_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function paidsubscriberssettingUpdateCheckSuccess(bool = false) {
  return {
    type: PAIDSUBSCRIBERSSETTING_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function paidsubscriberssettingDeleteCheckSuccess(bool = false) {
  return {
    type: PAIDSUBSCRIBERSSETTING_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function paidsubscriberssettingSetData(object) {
  return {
    type: PAIDSUBSCRIBERSSETTING_SET_DATA,
    payload: object,
  };
}

