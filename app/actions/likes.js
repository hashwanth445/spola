import {
    LIKES_GET_CHECK_SUCCESS,
    LIKES_POST_CHECK_SUCCESS,
    LIKES_UPDATE_CHECK_SUCCESS,
    LIKES_DELETE_CHECK_SUCCESS,
    LIKES_SET_DATA,
    LIKES_GET_CHECK_SUCCESS_BY_USER,
    LIKES_SET_DATA_BY_USER,
    LIKES_GET_CHECK_SUCCESS_FOR_FEED,
    LIKES_SET_DATA_FOR_FEED,
    LIKES_GET_CHECK_SUCCESS_FOR_SHORT,
    LIKES_SET_DATA_FOR_SHORT,
  } from './actionTypes';

  
  
  export function likesGetCheckSuccess(bool = false) {
    return {
      type: LIKES_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function likesPostCheckSuccess(bool = false) {
    return {
      type: LIKES_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function likesGetCheckSuccessByUser(bool = false) {
    return {
      type: LIKES_GET_CHECK_SUCCESS_BY_USER,
      payload: bool,
    };
  }
  export function likesGetCheckSuccessForFeed(bool = false) {
    return {
      type: LIKES_GET_CHECK_SUCCESS_FOR_FEED,
      payload: bool,
    };
  } 
  export function likesGetCheckSuccessForShort(bool = false) {
    return {
      type: LIKES_GET_CHECK_SUCCESS_FOR_SHORT,
      payload: bool,
    };
  } 
  export function likesUpdateCheckSuccess(bool = false) {
    return {
      type: LIKES_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function likesDeleteCheckSuccess(bool = false) {
    return {
      type: LIKES_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function likesSetData(object) {
    return {
      type: LIKES_SET_DATA,
      payload: object,
    };
  }
  export function likesSetDataByUser(object) {
    return {
      type: LIKES_SET_DATA_BY_USER,
      payload: object,
    };
  }
  export function likesSetDataForFeed(object) {
    return {
      type: LIKES_SET_DATA_FOR_FEED,
      payload: object,
    };
  }
  export function likesSetDataForShort(object) {
    return {
      type: LIKES_SET_DATA_FOR_SHORT,
      payload: object,
    };
  }
  