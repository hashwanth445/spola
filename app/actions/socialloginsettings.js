import {
    SOCIALLOGINSETTINGS_GET_CHECK_SUCCESS,
    SOCIALLOGINSETTINGS_POST_CHECK_SUCCESS,
    SOCIALLOGINSETTINGS_UPDATE_CHECK_SUCCESS,
    SOCIALLOGINSETTINGS_DELETE_CHECK_SUCCESS,
    SOCIALLOGINSETTINGS_SET_DATA,
  } from './actionTypes';
  
  
  
  export function socialloginsettingsGetCheckSuccess(bool = false) {
    return {
      type: SOCIALLOGINSETTINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function socialloginsettingsPostCheckSuccess(bool = false) {
    return {
      type: SOCIALLOGINSETTINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function socialloginsettingsUpdateCheckSuccess(bool = false) {
    return {
      type: SOCIALLOGINSETTINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function socialloginsettingsDeleteCheckSuccess(bool = false) {
    return {
      type: SOCIALLOGINSETTINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function socialloginsettingsSetData(object) {
    return {
      type: SOCIALLOGINSETTINGS_SET_DATA,
      payload: object,
    };
  }
  
  