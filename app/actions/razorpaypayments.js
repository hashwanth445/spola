import {
    RAZORPAYPAYMENTS_GET_CHECK_SUCCESS,
    RAZORPAYPAYMENTS_POST_CHECK_SUCCESS,
    RAZORPAYPAYMENTS_UPDATE_CHECK_SUCCESS,
    RAZORPAYPAYMENTS_DELETE_CHECK_SUCCESS,
    RAZORPAYPAYMENTS_SET_DATA,
  } from './actionTypes';
  
  
  
  export function razorpaypaymentsGetCheckSuccess(bool = false) {
    return {
      type: RAZORPAYPAYMENTS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function razorpaypaymentsPostCheckSuccess(bool = false) {
    return {
      type: RAZORPAYPAYMENTS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function razorpaypaymentsUpdateCheckSuccess(bool = false) {
    return {
      type: RAZORPAYPAYMENTS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function razorpaypaymentsDeleteCheckSuccess(bool = false) {
    return {
      type: RAZORPAYPAYMENTS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function razorpaypaymentsSetData(object) {
    return {
      type: RAZORPAYPAYMENTS_SET_DATA,
      payload: object,
    };
  }
  
  