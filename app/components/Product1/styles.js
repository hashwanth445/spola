import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {paddingVertical: Utils.scaleWithPixel(10), flexDirection: 'row'},
  contentLeft: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  thumb: {
    borderWidth: Utils.scaleWithPixel(1),
    borderColor: BaseColor.whiteColor,
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    top:Utils.scaleWithPixel(45)
  },
  contentRight: {
    padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
imageBanner: {flex: 1, borderRadius: 20,borderWidth:1.9,borderColor:"#2c3036",width:130,height:50},

  card: {
    width: Utils.scaleWithPixel(125),
    height: Utils.scaleWithPixel(210),
    borderRadius: Utils.scaleWithPixel(0),
    margin:Utils.scaleWithPixel(10),
  
  },
  content: {
    alignItems:"center",
    marginTop:-70,
    marginBottom:10
  }

});
