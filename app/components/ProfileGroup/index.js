import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text, Icon } from '@components';
import { useTheme } from '@config';
import styles from './styles';
import PropTypes from 'prop-types';
import * as Utils from '@utils';

export default function ProfileGroup(props) {
  const { colors } = useTheme();
  const {
    style,
    users,
    styleLeft,
    styleThumb,
    styleRight,
    onPress,
    onPressLove,
    name,
    detail,
    audioroom
  } = props;
  console.log("audioroom", audioroom);
  const audioPeople = audioroom?.audioRoomPeople.slice(0, 2);
  return (
    <View style={[styles.contain, style]}>
      <View style={styles.contentLeft, { flexDirection: "column",alignItems:"center" }}>
        <TouchableOpacity style={{alignItems:"center"}}>
          {audioPeople.map((item, index) => {
            return (
              <View style={{ marginTop: -20,marginHorizontal:-5 }}>
                <Image
                  key={index}
                  source={{ uri: item?.backBlazeImageUrl ? item?.backBlazeImageUrl : '' }}
                  style={[
                    styles.thumb,
                    index != 0 ? { left: Utils.scaleWithPixel(15), marginTop: -15 } : {},
                    styleThumb,
                  ]}
                />
              </View>
            );

          })}
          <View style={{ alignSelf: "center" }}>
            <Text numberOfLines={1} style={{ fontFamily: "ProximaNova", textAlign: "center", fontSize: 15, color: "white", marginTop: 10 }}>{detail}</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

ProfileGroup.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  users: PropTypes.array,
  name: PropTypes.string,
  detail: PropTypes.string,
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
  onPressLove: PropTypes.func,
};

ProfileGroup.defaultProps = {
  style: {},
  users: [],
  name: '',
  detail: '',
  styleLeft: {},
  styleThumb: {},
  styleRight: {},
  onPress: () => { },
  onPressLove: () => { },
};
