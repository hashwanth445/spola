import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {paddingVertical: Utils.scaleWithPixel(25)},
  contentLeft: {
    flex: 2,
    // justifyContent: 'flex-start',
    // alignItems: 'center',
    marginBottom:Utils.scaleWithPixel(80),
  },
  thumb: {
    // borderWidth: 1,
    // borderColor: BaseColor.whiteColor,
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    // marginLeft:Utils.scaleWithPixel(85)
  },
  contentRight: {
    padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
});
