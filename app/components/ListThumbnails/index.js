import React,{useState} from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text, Icon } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
// import { Icon } from 'react-native-elements';

export default function ListThumbnails(props) {
  const { colors } = useTheme();
  const [secure, setSecure] = useState(secure);
  const {
    style,
    imageStyle,
    image,
    txtLeftTitle,
    txtContent,
    txtRight,
    onPress,
  } = props;
  return (
    
    <TouchableOpacity
      style={[styles.item, style]}
      onPress={onPress}
      activeOpacity={0.9}>
      
        <View style={[styles.contain, { borderBottomColor: colors.border }]}>
         
          <Image source={image} style={[styles.thumb, imageStyle]} />
          <Icon name={ secure ? "microphone" : "microphone-off" }
                onPress={() => setSecure(!secure)} size={20} color="white" style={{left:Utils.scaleWithPixel(-20),margin:Utils.scaleWithPixel(-10),top:Utils.scaleWithPixel(55)}} />
          <Image source={image} style={[styles.thumb, imageStyle]} />
          
          <Image source={image} style={[styles.thumb, imageStyle]} />
          
          <Image source={image} style={[styles.thumb, imageStyle]} />
          
        </View>
     
    </TouchableOpacity >
 
  );
}

ListThumbnails.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  imageStyle: PropTypes.object,
  image: PropTypes.node.isRequired,
  txtLeftTitle: PropTypes.string,
  txtContent: PropTypes.string,
  txtRight: PropTypes.string,
  onPress: PropTypes.func,
};

ListThumbnails.defaultProps = {
  style: {},
  imageStyle: {},
  image: '',
  txtLeftTitle: '',
  txtContent: '',
  txtRight: '',
  onPress: () => { },
};
