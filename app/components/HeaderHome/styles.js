
import { StyleSheet } from 'react-native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { BaseStyle, Images, BaseColor, useTheme } from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: { height: Utils.scaleWithPixel(70), flexDirection: 'row', borderBottomColor: "gray", borderBottomWidth: Utils.scaleWithPixel(0.1) },
  contentLeft: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: Utils.scaleWithPixel(20),
    width: Utils.scaleWithPixel(60),
  },
  contentCenter: {
    flex: 2,
    fontSize: 25,
    color: BaseColor.primary,
    paddingLeft: Utils.scaleWithPixel(20),
    paddingRight: Utils.scaleWithPixel(10),
    paddingTop:  Utils.scaleWithPixel(10),
    justifyContent: 'center',
    alignItems: 'center',


  },
  contentRight: {
    marginRight: Utils.scaleWithPixel(20),
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingLeft: Utils.scaleWithPixel(20),
    paddingRight: Utils.scaleWithPixel(20),
    height: '100%',
    backgroundColor: BaseColor.primary,
    shadowColor: '#e2e2e2',
    shadowOffset: { width: Utils.scaleWithPixel(0), height: Utils.scaleWithPixel(2) },
    shadowOpacity: Utils.scaleWithPixel(100),
    shadowRadius: Utils.scaleWithPixel(10),
    elevation: Utils.scaleWithPixel(40),
    borderBottomLeftRadius: Utils.scaleWithPixel(30),
    borderBottomRightRadius: Utils.scaleWithPixel(30),
    borderTopRightRadius: Utils.scaleWithPixel(30),
    borderTopLeftRadius: Utils.scaleWithPixel(30),
    borderColor: 'white',
  },
  contentRightSecond: {
    marginRight: Utils.scaleWithPixel(10),
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingLeft: Utils.scaleWithPixel(20),
    paddingRight: Utils.scaleWithPixel(20),
    height: '100%',
    backgroundColor: 'white',
    shadowColor: '#e2e2e2',
    shadowOffset: { width: Utils.scaleWithPixel(0), height: Utils.scaleWithPixel(2) },
    shadowOpacity: Utils.scaleWithPixel(100),
    shadowRadius: Utils.scaleWithPixel(10),
    elevation: Utils.scaleWithPixel(40),
    borderBottomLeftRadius: Utils.scaleWithPixel(30),
    borderBottomRightRadius: Utils.scaleWithPixel(30),
    borderTopRightRadius: Utils.scaleWithPixel(30),
    borderTopLeftRadius: Utils.scaleWithPixel(30),
    borderColor: 'white',


  },
  right: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});
