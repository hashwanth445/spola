
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { View, TouchableOpacity, StatusBar } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import { Text, SafeAreaView,Image } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { BaseStyle, Images, BaseColor, useTheme } from '@config';
import { Neomorph, NeomorphBlur, Shadow } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';


export default function HeaderHome(props) {
  const forceDark = useSelector(state => state.application.force_dark);
  const {
    style,
    styleLeft,
    styleCenter,
    styleRight,
    styleRightSecond,
    title,
    subTitle,
    onPressLeft,
    onPressRight,
    onPressRightSecond,
    renderLeft,
    renderRightSecond,
    renderRight,
    barStyle,
  } = props;
  const { colors } = useTheme();

  const isDarkMode = useColorScheme() === 'dark';

  useEffect(() => {
    let option = isDarkMode ? 'light-content' : 'dark-content';
    if (forceDark) {
      option = 'light-content';
    }
    if (forceDark == false) {
      option = 'dark-content';
    }
    if (barStyle) {
      option = barStyle;
    }
    StatusBar.setBarStyle(option, true);
  }, [barStyle, forceDark, isDarkMode]);

  return (
    <SafeAreaView style={{ width: '100%' }} edges={['top', 'left', 'right']}>
      <View style={[styles.contain, style]}>

        {/* <View style={[styles.contentCenter]}>
          <Text title1 bold numberOfLines={1} style={{
            
          }}>
              Spola
          </Text>
          {subTitle != '' && (
            <Text>
              the app
            </Text>
          )}
        </View> */}
        {/* <View style={{ paddingLeft: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(10), }}>

          <NeomorphBlur
            style={{
              shadowOffset: { width: 3, height: 4 },
              shadowOpacity: 0.10,
              shadowColor: colors.neoThemebg,
              shadowRadius: 8,
              borderRadius: 60,
              Shadow: 3.4,
              backgroundColor: colors.neoThemebg,
              width: 50,
              height: 50,
              justifyContent: 'center',
              alignItems: 'center',

            }}
          > */}
        {/* <Neomorph
              style={{
                width: 50,
                height: 50,
                borderRadius: 95,
                borderWidth: 0.1,
                borderColor: colors.neoThemeBorderColor,
                backgroundColor: colors.neoThemebg,
                justifyContent: 'center',
                alignItems: 'center',

              }}
            > */}
        {/* <TouchableOpacity
              style={[styleLeft]}
              onPress={onPressLeft}>
              {renderLeft()}
            </TouchableOpacity> */}
        {/* </Neomorph> */}



        {/* </NeomorphBlur>
        </View> */}
        <View style={{ paddingRight: Utils.scaleWithPixel(10), right: Utils.scaleWithPixel(-10), top: Utils.scaleWithPixel(10), }}>

          {/* <NeomorphBlur
            inner={false}
            // darkShadowColor="#1d2024" // <- set this
            // lightShadowColor="#2d4660"
            style={{
              shadowRadius: Utils.scaleWithPixel(3),
              borderRadius: Utils.scaleWithPixel(100),
              backgroundColor: colors.neoThemebg,
              width: Utils.scaleWithPixel(45),
              height: Utils.scaleWithPixel(45),

              justifyContent: 'center',
              alignItems: 'center',

            }}
          > */}
             <TouchableOpacity
              style={[styleLeft]}
              onPress={onPressLeft}>
              {renderLeft()}
            </TouchableOpacity>
       

          {/* <Neomorph
                  style={{
                    width: 50,
                    height: 50,
                    borderRadius: 100,
                    borderWidth: 0.1,
                    borderColor: colors.neoThemeBorderColor,
                    backgroundColor: colors.neoThemebg,
                    justifyContent: 'center',
                    alignItems: 'center',

                  }}
                >
                  <TouchableOpacity
                    style={[styleRightSecond]}
                    onPress={onPressRightSecond}>
                    {renderRightSecond()}
                  </TouchableOpacity>
                </Neomorph> */}
        {/* </NeomorphBlur> */}
      </View>
      <View style={[styles.contentCenter]}>
        {/* <Text title1 bold numberOfLines={1} style={{

        }}>
          SPOLA
        </Text> */}
        <Image source={Images.logo} style={{height:Utils.scaleWithPixel(100),width:Utils.scaleWithPixel(100),top:Utils.scaleWithPixel(-5)}} />

      </View>
      {/* <View style={styles.right,{top:Utils.scaleWithPixel(10)}}>
          <TouchableOpacity
            style={[styles.contentRightSecond, styleRightSecond]}
            onPress={onPressRightSecond}>
            {renderRightSecond()}
          </TouchableOpacity>
         
          <TouchableOpacity
            style={[styles.contentRight, styleRight]}
            onPress={onPressRight}>
            {renderRight()}
          </TouchableOpacity>
         
        </View> */}
      <View style={{ paddingRight: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(10), }}>

        {/* <NeomorphBlur
          inner={false}
          // darkShadowColor="#1d2024" // <- set this
          // lightShadowColor="#2d4660"
          style={{
            shadowRadius: Utils.scaleWithPixel(3),
            borderRadius: Utils.scaleWithPixel(100),
            backgroundColor: colors.neoThemebg,
            width: Utils.scaleWithPixel(45),
            height: Utils.scaleWithPixel(45),

            justifyContent: 'center',
            alignItems: 'center',

          }}
        > */}
          {/* <Neomorph
             inner={false}
             darkShadowColor="#1d2024" // <- set this
             lightShadowColor="#2d4660"
             
               style={{
                 
                 shadowRadius: 10,
                 borderRadius: 100,
                 backgroundColor: colors.neoThemebg,
                 width: 51,
                 height: 51,
                 shadowOpacity: 0.2,
                 ShadowColor:"black",
                 justifyContent: 'center',
                 alignItems: 'center',

               }}
            > */}
          <TouchableOpacity
            style={[styleRightSecond]}
            onPress={onPressRightSecond}>
            {renderRightSecond()}
          </TouchableOpacity>
          {/* </Neomorph> */}
        {/* </NeomorphBlur> */}
      </View>
      <View style={{ paddingRight: Utils.scaleWithPixel(5), top: Utils.scaleWithPixel(10), }}>

        {/* <NeomorphBlur
          style={{
            shadowRadius: Utils.scaleWithPixel(3),
            borderRadius: Utils.scaleWithPixel(100),
            backgroundColor: colors.neoThemebg,
            width: Utils.scaleWithPixel(45),
            height: Utils.scaleWithPixel(45),

            justifyContent: 'center',
            alignItems: 'center',

          }}
        > */}
          {/* <Neomorph
              style={{
                width: 50,
                height: 50,
                borderRadius: 100,
                borderWidth: 0.1,
                borderColor: colors.neoThemeBorderColor,
                backgroundColor: colors.neoThemebg,
                justifyContent: 'center',
                alignItems: 'center',

              }}
            > */}
          <TouchableOpacity
            style={[styleRight]}
            onPress={onPressRight}>
            {renderRight()}
          </TouchableOpacity>
          {/* </Neomorph> */}



        {/* </NeomorphBlur> */}
      </View>




    </View>
    </SafeAreaView >
  );
}

HeaderHome.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleCenter: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRightSecond: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  renderLeft: PropTypes.func,
  renderRight: PropTypes.func,
  renderRightSecond: PropTypes.func,
  onPressRightSecond: PropTypes.func,
  onPressLeft: PropTypes.func,
  onPressRight: PropTypes.func,
  title: PropTypes.string,
  subTitle: PropTypes.string,
  barStyle: PropTypes.string,
};

HeaderHome.defaultProps = {
  style: {},
  styleLeft: {},
  styleCenter: {},
  styleRight: {},
  styleRightSecond: {},
  renderLeft: () => { },
  renderRight: () => { },
  renderRightSecond: () => { },
  onPressLeft: () => { },
  onPressRight: () => { },
  onPressRightSecond: () => { },
  title: 'Title',
  subTitle: '',
  barStyle: '',
};
