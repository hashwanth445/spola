import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import {Image,Icon} from '@components';
import {Images, useTheme} from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
// import { Icon } from 'react-native-elements/dist/icons/Icon';

export default function ProfileCard(props) {
  const {colors} = useTheme();
  const {style, children, styleContent, image, onPress,time} = props;
  return (
    <View style={{flexDirection:"column",height:250}}>
    
    <TouchableOpacity
      style={[styles.card, {borderColor: colors.border}, style]}
      onPress={onPress}
      activeOpacity={0.9}>
      <Image source={{uri : image ? image : ''}} style={styles.imageBanner} />
      
    </TouchableOpacity>

      <View style={[styles.content, styleContent]}>{children}</View>
    </View>
  );
}

ProfileCard.propTypes = {
  image: PropTypes.node.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleContent: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  onPress: PropTypes.func,
};

ProfileCard.defaultProps = {
  image: Images.profile2,
  style: {},
  styleContent: {},
  onPress: () => {},
};
