import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  imageBanner: {flex: 1,width:160, borderRadius: 20,top:-25,left:7,borderWidth:1.9,borderColor:"#2c3036"},
  card: {
    width: 155,
    height: 250,
    borderRadius: 0,
    marginTop:25,
  
  },
  content: {
    position: 'absolute',
    alignItems: 'flex-start',
    bottom: -10,
    left:10,
    padding: 10,
  },
});
