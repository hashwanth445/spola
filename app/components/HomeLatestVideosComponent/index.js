import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text, Icon } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import * as Utils from '@utils';
import { BaseColor, useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import { useNavigation } from '@react-navigation/native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ProfileAuthor } from '..';
import { format, render, cancel, register } from 'timeago.js';


export default function HomeLatestVideosComponent(props) {
  const { colors } = useTheme();
  const [liked, setLiked] = useState(liked);
  const [saved, setSaved] = useState(saved);
  const navigation = useNavigation();
  const { 
    
    style, children, title, description,
    styleLeft,
    styleThumb,
    onPress,
    name,
    dateAndTime,
    channelImage,
    textRight, onPressImage, image, styleRight,item, time } = props;
    const newtime = format(dateAndTime, 'en_US');

  return (

    <View>
      {children}
      <TouchableOpacity onPress={onPress} activeOpacity={0.9}>
        <Image style={styles.imagePost} source={{ uri: image ? image : '' }} />
        {/* <Icon
            name="dots-horizontal"
            solid
            size={28}
            color={BaseColor.whiteColor}
            style={{ position: 'absolute', top: -40, right: 20 }}
          /> */}

      </TouchableOpacity>
      <View style={[styles.content, { borderBottomColor: colors.border }]}>
        {/* <View style={{alignContent:"center",alignSelf:"flex-end", top: Utils.scaleWithPixel(-170), borderWidth: Utils.scaleWithPixel(1), borderColor: "#999999",width: wp('12%'),height: hp('3.5%'), borderRadius: Utils.scaleWithPixel(5), backgroundColor: "#000000", paddingLeft: Utils.scaleWithPixel(5)}}>
          <Text body2 style={{  fontSize: 14,  fontFamily: "ProximaNova", }}>
            {time}
          </Text>
        </View> */}
        <View style={{alignContent:"center",alignSelf:"flex-end", top: Utils.scaleWithPixel(-175), borderWidth: Utils.scaleWithPixel(1), borderColor: "#999999",width: Utils.scaleWithPixel(30), borderRadius: Utils.scaleWithPixel(5), backgroundColor: "#000000"}}>
          <Text body2 style={{ fontSize: 14,  fontFamily: "ProximaNova",alignSelf:"center" }}>
            {time}
          </Text>
        </View>
        {/* <Icon name="youtube" color="red" size={32} style={{ top: Utils.scaleWithPixel(-135), left: Utils.scaleWithPixel(120) }} /> */}
        <Text headline bold style={{ fontFamily: "ProximaNova", }}>
          {/* {title} */}
        </Text>


      </View>
        <TouchableOpacity
        style={[styles.contain, style]}
        onPress={onPress}
        activeOpacity={0.9}>
        <View style={[styles.contentLeft,styleLeft]}>
          <Image source={{ uri: channelImage ? channelImage : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg' }} style={[styles.thumb, styleThumb]} />
          <View>
            <Text bold style={{ fontFamily: "ProximaNova", fontSize: 14, color: "#E2E2E2",letterSpacing:0.5 }}>
              {title}
            </Text>
            <Text style={{ fontFamily: "ProximaNova", color: "#B3B6B7", fontSize: 12}} numberOfLines={0} >
              {name}  {description} views | {newtime}
            </Text>
          </View>
        </View>
        <View style={[styles.contentRight, styleRight]}>
          <Text caption2 grayColor numberOfLines={1}>
            {textRight}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

HomeLatestVideosComponent.propTypes = {
  image: PropTypes.node.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  title: PropTypes.string,
  description: PropTypes.string,
  time: PropTypes.string,
  date: PropTypes.string,
  onPress: PropTypes.func,
};

HomeLatestVideosComponent.defaultProps = {
  image: '',
  title: '',
  description: '',
  style: {},
  date: '',
  onPress: () => { },
};
