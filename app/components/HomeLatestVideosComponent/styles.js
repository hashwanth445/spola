import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
  imagePost: {width: wp('90%'), height: hp('23%'),borderRadius:Utils.scaleWithPixel(10),margin:Utils.scaleWithPixel(10),marginTop:80,marginLeft:20},
  content: {
    // marginHorizontal: 20,
    // paddingVertical: 10,
    // borderBottomWidth: 1,
    margin:Utils.scaleWithPixel(20)
  },
  contain: {
    marginLeft: 10,
    marginTop: -80,
    marginBottom: 80,
    // marginHorizontal: 80,
    // paddingVertical: 10,
    // borderBottomWidth: 1,
    // margin:Utils.scaleWithPixel(-20)
  },
  contentRight: {
    padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-end',
    
  },
  contentLeft: {
    flex: 8,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  thumb: {
    width: Utils.scaleWithPixel(30),
    height: Utils.scaleWithPixel(30),
    borderRadius: Utils.scaleWithPixel(20),
    marginRight: Utils.scaleWithPixel(5),
    borderWidth:Utils.scaleWithPixel(0.5),
    borderColor:"#ffffff"
  },
  contentRight: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
});
