import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: { 
    flexDirection: 'row'
  },
  thumb: {
    // borderWidth: 1,
    // borderColor: BaseColor.whiteColor,
    width: Utils.scaleWithPixel(70),
    height: Utils.scaleWithPixel(70),
    borderRadius: Utils.scaleWithPixel(40),
    margin:15,
  },
  contentRight: {
    // padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  contentLeft: {
    alignContent:"flex-start"
  }
});
