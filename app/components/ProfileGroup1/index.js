import React, { useState } from 'react';
import { View, TouchableOpacity, FlatList } from 'react-native';
import { Image, Text, Icon } from '@components';
import { useTheme } from '@config';
import styles from './styles';
import PropTypes from 'prop-types';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';

export default function ProfileGroup1(props) {
  const { colors } = useTheme();
  const {
    style,
    users,
    styleLeft,
    styleThumb,
    styleRight,
    onPress,
    onPressLove,
    name,
    detail,
    audioObj
  } = props;
  const audioRoomPeople = audioObj?.audioRoomPeople
  console.log("audioObj", audioObj);
  const [click, setClick] = useState(false);
  return (
    <View style={[styles.contain, style]}>
      <TouchableOpacity
        style={[styles.contentLeft, styleLeft]}
        onPress={onPress}
        activeOpacity={0.9}
      >
        {/* <Neomorph
          style={{
            shadowRadius: Utils.scaleWithPixel(3),
            borderRadius: Utils.scaleWithPixel(100),
            // backgroundColor: colors.card,
            backgroundColor: colors.neoThemebg,
            width: Utils.scaleWithPixel(60),
            height: Utils.scaleWithPixel(60),
            justifyContent: 'center',
            alignItems: 'center',
            // top: Utils.scaleWithPixel(-5),
            // margin: Utils.scaleWithPixel(-30),
            // left: Utils.scaleWithPixel(-80)
          }}
        > */}

        {/* {audioRoomPeople.map((item, index) => { */}
        {/* return ( */}
        <View style={{ width: 350, }}>
          <FlatList
            data={audioRoomPeople}
            // contentContainerStyle={{ width: '100%' }}
            numColumns={3}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item, index }) => (
              <View style={{ flexDirection: "column" }}>
                <Image
                  key={index}

                  source={{ uri: item?.backBlazeImageUrl ? item?.backBlazeImageUrl : '' }}
                  style={[
                    styles.thumb,
                    // index != 0 ? { marginLeft: Utils.scaleWithPixel(-15) } : {},
                    styleThumb,

                  ]}
                />
                <View style={{ backgroundColor: "white", height: 25, width: 25, borderRadius: 20, marginTop: -30, top: -10, left: -20, alignSelf: "flex-end", justifyContent: "center" }}>
                  <TouchableOpacity onPress={() => setClick(!click)}>
                    <Icon name={click && item ? 'microphone' : 'microphone-off'} size={19} color="black" style={{ alignSelf: "center", }} />
                  </TouchableOpacity>
                </View>
                <Text headline semibold numberOfLines={1} style={{ fontFamily: "ProximaNova", fontSize: 16, alignSelf: "center" }}>
                  {item?.username}
                </Text>
              </View>
            )}
          />
        </View>
        {/* );
          })} */}

        {/* </Neomorph> */}
      </TouchableOpacity>
      <View
        style={{
          flex: 1,
          alignItems: 'flex-start',
        }}>

        <Text footnote grayColor numberOfLines={1}>
          {detail}
        </Text>
      </View>
      {/* <TouchableOpacity
        style={[styles.contentRight, styleRight]}
        onPress={onPressLove}
        activeOpacity={0.9}>
        <Icon name="heart" color={colors.text} size={18} />
      </TouchableOpacity> */}
    </View>
  );
}

ProfileGroup1.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  users: PropTypes.array,
  name: PropTypes.string,
  detail: PropTypes.string,
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
  onPressLove: PropTypes.func,
};

ProfileGroup1.defaultProps = {
  style: {},
  users: [],
  name: '',
  detail: '',
  styleLeft: {},
  styleThumb: {},
  styleRight: {},
  onPress: () => { },
  onPressLove: () => { },
};
