import {StyleSheet} from 'react-native';
import * as Utils from '@utils';


export default StyleSheet.create({
  contain: {
    flexDirection: 'row',
    marginEnd:Utils.scaleWithPixel(-60),
    paddingTop: Utils.scaleWithPixel(5),
    paddingBottom: Utils.scaleWithPixel(5),
    left:Utils.scaleWithPixel(10)
  },
  thumb: {width: Utils.scaleWithPixel(40), height: Utils.scaleWithPixel(40), borderRadius: Utils.scaleWithPixel(50), marginLeft:Utils.scaleWithPixel(50),left:Utils.scaleWithPixel(-50), marginTop:Utils.scaleWithPixel(30),margin:Utils.scaleWithPixel(40)},
  // thumb1: {width: 40, height: 40, borderRadius: 50, marginLeft:0,left:-80, marginTop:50},
  content: {
    flex: 1,
    flexDirection: 'row',
  },
  left: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    top:Utils.scaleWithPixel(20),
    left:Utils.scaleWithPixel(-65)
  },
  right: {
    flex: 2.5,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  content1: {
    position: 'absolute',
    alignItems: 'flex-start',
    bottom: Utils.scaleWithPixel(0),
    left:Utils.scaleWithPixel(10),
    padding: Utils.scaleWithPixel(10),
  },
});
