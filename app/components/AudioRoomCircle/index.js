import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import { ScrollView } from 'react-native-gesture-handler';
import * as Utils from '@utils';


export default function AudioRoomCircle(props) {
  const { colors } = useTheme();
  const {
    style,
    imageStyle,
    image,
    txtLeftTitle,
    txtContent,
    txtRight,
    children,
    onPress,
    styleContent,
  } = props;
  return (
        <TouchableOpacity
          style={[
            styles.contain,
            // {borderBottomWidth: 1, borderBottomColor: colors.border},
            style,
          ]}
          onPress={onPress}
          activeOpacity={0.9}>
          <Image source={image} style={[styles.thumb, imageStyle]} />
          {/* <Image source={image} style={[styles.thumb1, imageStyle]} /> */}
          <View style={styles.content}>
            <View style={styles.left}>
            
              <Text
                note
                numberOfLines={1}
                footnote
                grayColor
                style={{
                  paddingTop: Utils.scaleWithPixel(5),
                  fontFamily:"Proxima-Nova"
                }}>
                {txtContent}
              </Text>
            </View>
          </View>
          <View style={[styles.content1, styleContent]}>{children}</View>
        </TouchableOpacity>

  );
}

AudioRoomCircle.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  imageStyle: PropTypes.object,
  image: PropTypes.node.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  txtContent: PropTypes.string,
  txtRight: PropTypes.string,
  onPress: PropTypes.func,
};

AudioRoomCircle.defaultProps = {
  style: {},
  imageStyle: {},
  image: '',
  txtLeftTitle: '',
  txtContent: '',
  txtRight: '',
  styleContent: {},
  onPress: () => { },
};
