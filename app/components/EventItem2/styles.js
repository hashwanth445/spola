import {StyleSheet} from 'react-native';
import * as Utils from '@utils';
import {BaseColor} from '@config';

export default StyleSheet.create({
  //block css
  blockImage: {
    height: Utils.scaleWithPixel(200),
    width: '100%',
  },
  blockContent: {
    paddingLeft: Utils.scaleWithPixel(20),
    paddingRight: Utils.scaleWithPixel(10),
    paddingTop: Utils.scaleWithPixel(10),
    paddingBottom: Utils.scaleWithPixel(10),
  },
  tagStatus: {
    position: 'absolute',
    top: Utils.scaleWithPixel(10),
    left: Utils.scaleWithPixel(10),
  },
  iconLike: {
    position: 'absolute',
    top: Utils.scaleWithPixel(10),
    right: Utils.scaleWithPixel(10),
  },
  blockLineMap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  blockContentStatus: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: Utils.scaleWithPixel(10),
  },
  blockContentPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: Utils.scaleWithPixel(5),
  },
  //list css
  listContent: {
    padding: Utils.scaleWithPixel(10),
    borderRadius: Utils.scaleWithPixel(10),
  },
  listLineMap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  listRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: Utils.scaleWithPixel(10),
  },
  //gird css
  girdImage: {
    borderRadius: Utils.scaleWithPixel(8),
    height: Utils.scaleWithPixel(120),
    width: '100%',
  },
  girdContent: {
    flex: 1,
  },
  tagGirdStatus: {
    position: 'absolute',
    top: Utils.scaleWithPixel(5),
    left: Utils.scaleWithPixel(5),
  },
  iconGirdLike: {
    position: 'absolute',
    bottom: Utils.scaleWithPixel(5),
    right: Utils.scaleWithPixel(5),
  },
  girdRowRate: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: Utils.scaleWithPixel(5),
    justifyContent: 'space-between',
  },
});
