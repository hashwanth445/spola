import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {paddingVertical: Utils.scaleWithPixel(10), flexDirection: 'row'},
  contentLeft: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  thumb: {
    borderWidth: Utils.scaleWithPixel(1),
    borderColor: BaseColor.whiteColor,
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    top:Utils.scaleWithPixel(45)
  },
  contentRight: {
    padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  image: {
    height: Utils.scaleWithPixel(80),
    width: Utils.scaleWithPixel(80),
    borderRadius: Utils.scaleWithPixel(70)

}
});
