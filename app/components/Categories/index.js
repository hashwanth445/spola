import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text, Icon } from '@components';
import { useTheme } from '@config';
import styles from './styles';
import PropTypes from 'prop-types';
import { Neomorph } from 'react-native-neomorph-shadows';
import { ScrollView } from 'react-native-gesture-handler';
import * as Utils from '@utils';

export default function Category(props) {
    const { colors } = useTheme();
    const {
        style,
        users,
        styleLeft,
        styleThumb,
        styleRight,
        onPress,
        onPressLove,
        name,
        detail,
    } = props;
    const [shouldShowGaming, setShouldShowGaming] = useState(true);
    const [shouldShowFood, setShouldShowFood] = useState(true);
    const [shouldShowDIY, setShouldShowDIY] = useState(true);
    return (
        <ScrollView horizontal={true}>
            <View style={[styles.contain, style]}>
                <TouchableOpacity
                    style={[styles.contentLeft, styleLeft]}
                    onPress={onPress}
                    activeOpacity={0.9}>
                    <View style={{ flexDirection: 'row', marginLeft: Utils.scaleWithPixel(10) }}>
                        {users.map((item, index) => {
                            return (
                                <View style={{
                                    marginTop: Utils.scaleWithPixel(30),
                                    
                                }}>
                                    {shouldShowGaming ? (
                                        <Neomorph
                                            outer
                                            style={{
                                                shadowRadius: Utils.scaleWithPixel(7),
                                                borderRadius: Utils.scaleWithPixel(20),
                                                backgroundColor: colors.neoThemebg,
                                                width: Utils.scaleWithPixel(115),
                                                height: Utils.scaleWithPixel(115),
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: Utils.scaleWithPixel(10)
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => setShouldShowGaming(!shouldShowGaming)}>
                                                <Icon name="gamepad-variant" size={70} style={styles.Icon} />
                                                <Text bold style={{ fontSize: 24, color: colors.text }}>Gaming</Text>
                                            </TouchableOpacity>
                                        </Neomorph>) : (<Neomorph
                                            inner
                                            style={{
                                                shadowRadius: Utils.scaleWithPixel(7),
                                                borderRadius: Utils.scaleWithPixel(20),
                                                backgroundColor: colors.neoThemebg,
                                                width: Utils.scaleWithPixel(115),
                                                height: Utils.scaleWithPixel(115),
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: Utils.scaleWithPixel(10)
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => setShouldShowGaming(!shouldShowGaming)}>
                                                <Icon name="gamepad-variant" size={70} style={styles.Icon} />
                                                <Text bold style={{ fontSize: 24, color: "#008cff" }}>Gaming</Text>
                                            </TouchableOpacity>
                                        </Neomorph>)}
                                    {/* {shouldShowFood ? (
                                        <Neomorph
                                            outer
                                            style={{
                                                shadowRadius: 7,
                                                borderRadius: 20,
                                                backgroundColor: colors.neoThemebg,
                                                width: 115,
                                                height: 115,
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: 10
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => setShouldShowFood(!shouldShowFood)}>
                                                <Icon name="food" size={70} style={styles.Icon} />
                                                <Text bold style={{ fontSize: 24, color: "#6a6c6e", marginLeft: 10 }}>Food</Text>
                                            </TouchableOpacity>
                                        </Neomorph>) : (<Neomorph
                                            inner
                                            style={{
                                                shadowRadius: 7,
                                                borderRadius: 20,
                                                backgroundColor: colors.neoThemebg,
                                                width: 115,
                                                height: 115,
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: 10
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => setShouldShowFood(!shouldShowFood)}>
                                                <Icon name="food" size={70} style={styles.Icon} />
                                                <Text bold style={{ fontSize: 24, color: "#008cff", marginLeft: 10 }}>Food</Text>
                                            </TouchableOpacity>
                                        </Neomorph>)} */}
                                    <View style={{flexDirection:"row"}}>
                                        <Neomorph
                                            outer
                                            style={{
                                                shadowRadius: Utils.scaleWithPixel(7),
                                                borderRadius: Utils.scaleWithPixel(20),
                                                backgroundColor: colors.neoThemebg,
                                                width: Utils.scaleWithPixel(115),
                                                height: Utils.scaleWithPixel(115),
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: Utils.scaleWithPixel(10)
                                            }}
                                        >
                                            <Icon name="bookshelf" size={70} style={styles.Icon} />
                                            <Text bold style={{ fontSize: 24, color: colors.text }}>Books</Text>
                                        </Neomorph>
                                    </View>
                                </View>
                            );
                        })}
                    </View>
                    {/* <View
                        style={{
                            flex: 1,
                            alignItems: 'flex-start',
                        }}>
                        <Text headline semibold numberOfLines={2} style={{ top: -15, left: -90, fontSize: 18 }}>
                            {name} and 25 others
                        </Text>
                        <Text title2 semibold numberOfLines={1} style={{ top: -70, left: -70, fontSize: 20, margin: -20 }}>
                            {detail}
                        </Text>
                    </View> */}
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

Category.propTypes = {
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    users: PropTypes.array,
    name: PropTypes.string,
    detail: PropTypes.string,
    styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    onPress: PropTypes.func,
    onPressLove: PropTypes.func,
};

Category.defaultProps = {
    style: {},
    users: [],
    name: '',
    detail: '',
    styleLeft: {},
    styleThumb: {},
    styleRight: {},
    onPress: () => { },
    onPressLove: () => { },
};
