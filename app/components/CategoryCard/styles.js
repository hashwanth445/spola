import {StyleSheet} from 'react-native';
import * as Utils from '@utils';
import {BaseColor} from '../../config' 

export default StyleSheet.create({
  imageBanner: {flex: 0.6,width:Utils.scaleWithPixel(50),top:Utils.scaleWithPixel(-10),marginBottom:Utils.scaleWithPixel(-10)},
  imageBanner1: {flex: 0.6,width:Utils.scaleWithPixel(50),top:Utils.scaleWithPixel(-20),marginBottom:Utils.scaleWithPixel(-10)},
  card: {
//    height:40,
    borderRadius: Utils.scaleWithPixel(0),
    margin:Utils.scaleWithPixel(5),
    marginTop:Utils.scaleWithPixel(-40),
  
  },
  content: {
    position: 'absolute',
    alignSelf:"center",
    top:Utils.scaleWithPixel(65),
    // left:Utils.scaleWithPixel(50)
  },
  content1: {
    position: 'absolute',
    alignSelf:"center",
    top:Utils.scaleWithPixel(55),
    
    // left:Utils.scaleWithPixel(50)
  },
});
