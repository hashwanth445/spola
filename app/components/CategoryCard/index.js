import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Image, Icon } from '@components';
import { Images, useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import { BaseColor } from '../../config/theme';
// import { Icon } from 'react-native-elements/dist/icons/Icon';



export default function CategoryCard(props) {
    const { colors } = useTheme();
    // const [shouldShowCat, setShouldShowCat] = useState(cateObj)
    // const [selectedCat, setSelectedCat] = useState([]);
    const { style, children, styleContent, image, onPress, time, item, cateObj } = props;
    console.log("cateobj", cateObj);
    // const selectCategories = async data => {
    //     let selectedCategories = selectedCat;
    //     selectedCategories.push({ "_id": data });
    //     setSelectedCat(selectedCategories);


    const select = () => {
        <TouchableOpacity
            style={[styles.card, { borderColor: colors.border }, style]}
            // onPress={onPress}
            activeOpacity={0.9}>
            <Neomorph
                inner
                style={{
                    shadowRadius: Utils.scaleWithPixel(7),
                    borderRadius: Utils.scaleWithPixel(20),
                    backgroundColor: colors.neoThemebg,
                    width: Utils.scaleWithPixel(85),
                    height: Utils.scaleWithPixel(85),
                    justifyContent: 'center',
                    alignItems: 'center',
                    // margin: 10,
                    left: Utils.scaleWithPixel(20)

                }}
            >
                <Icon name="check-circle" size={20} color={BaseColor.blueColor} style={{ alignSelf: "flex-start", left: Utils.scaleWithPixel(5), top: Utils.scaleWithPixel(-8) }} />
                <View style={{ top: Utils.scaleWithPixel(10) }}>
                    <Image source={{ uri: image ? image : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg' }}
                        style={styles.imageBanner1}
                    />
                    <View style={styles.content1}>{children}</View>
                </View>
            </Neomorph>
        </TouchableOpacity>
    }
    return (
        <View >
            <TouchableOpacity
                style={[styles.card, { borderColor: colors.border }, style]}
                onClick={select()}
                onPress={onPress}
                activeOpacity={0.9}>
                <Neomorph
                    outer
                    style={{
                        shadowRadius: Utils.scaleWithPixel(7),
                        borderRadius: Utils.scaleWithPixel(20),
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(85),
                        height: Utils.scaleWithPixel(85),
                        justifyContent: 'center',
                        alignItems: 'center',
                        // margin: 10,
                        left: Utils.scaleWithPixel(20)

                    }}
                >
                    <View style={{ top: 10, height: 60 }}>
                        <Image source={{ uri: image ? image : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg' }}
                            style={styles.imageBanner}
                        />
                        <View style={[styles.content, styleContent]}>{children}</View>
                    </View>
                </Neomorph>
            </TouchableOpacity >



        </View >
    );
}

CategoryCard.propTypes = {
    image: PropTypes.node.isRequired,
    item: PropTypes.object,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleContent: PropTypes.object,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element),
    ]),
    onPress: PropTypes.func,
};

CategoryCard.defaultProps = {
    image: Images.profile2,
    item: {},
    style: {},
    styleContent: {},
    onPress: () => { },
};
