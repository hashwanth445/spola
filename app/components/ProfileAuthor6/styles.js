import React from 'react';
import {StyleSheet} from 'react-native';
import * as Utils from '@utils';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


export default StyleSheet.create({
  contain: {
    flexDirection: 'row',
    paddingVertical: Utils.scaleWithPixel(10),
  },
  contentLeft: {
    flex: 8,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  thumb: {
    width: wp('12%'),
    height: hp('6%'),
    borderRadius: Utils.scaleWithPixel(25),
    // marginRight: Utils.scaleWithPixel(5),
    borderWidth:Utils.scaleWithPixel(0.5),
    borderColor:"#ffffff",
    margin:0

  },
  contentRight: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
});
