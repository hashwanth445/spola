import React from 'react';
import { View, TouchableOpacity, FlatList, ImageBackground } from 'react-native';
import { Image, Text, Icon } from '@components';
import { useTheme } from '@config';
import styles from './styles';
import PropTypes from 'prop-types';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import {
  Menu,
  HamburgerIcon,
  Box,
  Pressable,
  Center,
  NativeBaseProvider,
} from "native-base"

export default function AudioGroup(props) {
  const { colors } = useTheme();
  const {
    style,
    users,
    styleLeft,
    styleThumb,
    styleRight,
    onPress,
    onPressLove,
    name,
    detail,
    audioroomObj
  } = props;
  console.log("audioroomObj", audioroomObj);
  const audioRoomPeople = audioroomObj?.audioRoomPeople
  const audio = audioroomObj?.audioRoomPeople.slice(2).length
  const names = audioroomObj?.audioRoomPeople.slice(0, 3);


  const Example = () => {
    return (
      <Box h="80%" w="90%" alignItems="flex-end">
        <Menu
          w="190"
          trigger={(triggerProps) => {
            return (
              <TouchableOpacity accessibilityLabel="More options menu" {...triggerProps}>
                <Icon name="dots-horizontal" size={25} color="white" style={{ alignSelf: "flex-start" }} />
              </TouchableOpacity>
            )
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Menu.Item>See fewer rooms like this</Menu.Item>
            <Icon name="eye-off" size={16} color="gray" style={{ alignSelf: "center" }} />
          </View>
          <View style={{ flexDirection: "row" }}>
            <Menu.Item>Report this Title</Menu.Item>
            <Icon name="flag" size={16} color="gray" style={{ alignSelf: "center" }} />
          </View>
        </Menu>
      </Box>
    )
  }

  return (

    <View style={[styles.contain, style]}>
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.9}>
        <Neomorph
          outer
          style={{
            shadowRadius: Utils.scaleWithPixel(7),
            borderRadius: Utils.scaleWithPixel(20),
            backgroundColor: colors.neoThemebg,
            width: Utils.scaleWithPixel(300),
            height: Utils.scaleWithPixel(135),
            // justifyContent: 'center',
            // alignItems: 'center',
            margin: Utils.scaleWithPixel(5)
          }}
        >

          <View style={{ marginLeft: 20, marginTop: 15 }}>
            <Text bold style={{ fontSize: 16, fontFamily: "ProximaNova", alignSelf: "flex-start" }}>
              {detail}
            </Text>
          </View>


          <View style={{ flexDirection: "row" }}>

            <View style={{ flex: 1, flexDirection: 'row', marginTop: 20, top: -20, marginLeft: 20 }}>

              {audioroomObj?.audioRoomPeople.slice(0, 2).map((item, index) => {
                return (
                  <Image
                    key={index}
                    source={{ uri: item?.backBlazeImageUrl ? item?.backBlazeImageUrl : '' }}
                    style={[
                      styles.thumb,

                      index != 0 ? { left: Utils.scaleWithPixel(-20), marginTop: 15 } : {},
                      styleThumb,
                    ]}
                  />
                );
              })}
              {/* {audioroomObj?.audioRoomPeople?.length > 2 ? (

                <View style={{ alignContent: "flex-start", top: 45, left: -20 }}>
                  {audio > 1 ? (
                    <Text bold style={{ fontFamily: "ProximaNova", fontSize: 16, alignSelf: "center", opacity: 1 }}>
                      + {audio} others
                    </Text>
                  ) : (
                    <Text bold style={{ fontFamily: "ProximaNova", fontSize: 16, alignSelf: "center", opacity: 1 }}>
                      + {audio} other
                    </Text>
                  )}
                </View>
              ) : null} */}

            </View>

            <View
              style={{
                flex: 1,
                alignItems: "flex-start",
                left:-60
              }}>

              {audioroomObj?.audioRoomPeople?.length > 2 ? (
                <View style={{ flex: 1, top: 25, }}>

                  {names.map((item, key) => (
                    <Text style={{ fontSize: 20, textAlign: "left", color: "white", fontFamily: "ProximaNova" }}>
                      {item?.username}
                    </Text>
                  ))}
                  {/* <Text style={{ fontSize: 20, fontFamily: "ProximaNova",color:"white", alignSelf: "center",  }}>
                  & {audio} Others
                </Text> */}
                </View>
              ) : (

                audioRoomPeople.map((item, key) => (
                  <Text semibold style={{ fontSize: 15, marginTop: 10, fontFamily: "ProximaNova" }}>
                    {item?.username}
                  </Text>
                ))
              )}

            </View>
          </View>
          <View style={{ flexDirection: "row", alignSelf: "center", top: 20, left: -40 }}>
            <Icon name="account" size={15} color="white" style={{}} />
            <Text style={{ fontFamily: "ProximaNova", marginLeft: 3 }}>{audioRoomPeople?.length}</Text>
          </View>

          <NativeBaseProvider>
            <Center style={{ alignContent: "flex-start", top: -120 }}>
              <Example />
            </Center>
          </NativeBaseProvider>

          {/* <TouchableOpacity
        style={[styles.contentRight, styleRight]}
        onPress={onPressLove}
        activeOpacity={0.9}>
        <Icon name="heart" color={colors.text} size={18} />
      </TouchableOpacity> */}
        </Neomorph >
      </TouchableOpacity>
    </View >
  );
}

AudioGroup.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  users: PropTypes.array,
  name: PropTypes.string,
  detail: PropTypes.string,
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
  onPressLove: PropTypes.func,
};

AudioGroup.defaultProps = {
  style: {},
  users: [],
  name: '',
  detail: '',
  styleLeft: {},
  styleThumb: {},
  styleRight: {},
  onPress: () => { },
  onPressLove: () => { },
};
