import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  ScrollView,
  FlatList,
} from 'react-native';
import {
  Image,
  Text,
  Icon,
  TextInput,
  ProfileCard,
  HomeItem1,
  ChannelsCard,
  ProfileAuthor1,
} from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import * as Utils from '@utils';
import {TabView, TabBar} from 'react-native-tab-view';
import {BaseColor, useTheme, Images} from '@config';
import {useTranslation} from 'react-i18next';
import {Neomorph, NeomorphBlur} from 'react-native-neomorph-shadows';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import Video from 'react-native-video';
import {
  likesGetRequestForShort,
  likesDeleteRequest,
  likesGetRequest,
  likesPostRequest,
} from '../../api/likes';
import {
  bookmarksGetRequestForFeed,
  bookmarksPostRequest,
  bookmarksGetRequest,
  bookmarksDeleteRequest,
} from '../../api/bookmarks';
import _ from 'lodash';
import Modal from 'react-native-modal';
import {SwiperFlatList} from 'react-native-swiper-flatlist';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {format, render, cancel, register} from 'timeago.js';
import {Avatar} from 'react-native-paper';
import {shortsGetRequest} from '../../api/shorts';
import {videosGetRequest} from '../../api/videos';
import {channelGetRequestForUser} from '../../api/channel';
import LinearGradient from 'react-native-linear-gradient';


export default function SpolaItem(props) {
  const { width, height } = Dimensions.get('window');
  console.log("spola item h,w",width, height)
  const { style, children, title, description, onPress, image, styleRight, spolaUrlObject, index, currindex } = props;
  console.log("spolaUrlObject", spolaUrlObject);
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [modalVisible, setModalVisible] = useState(false);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  const likesdataFromUser = useSelector(state => state.LikesReducer.likesDataForFeed)
  const bookmarkdataFromUser = useSelector(state => state.BookmarksReducer.bookmarksDataForFeed)
 
  // console.log("bookmarkdataFromUser,bookmark", bookmarkdataFromUser);
  // console.log("likesdataFromUser", likesdataFromUser);
  const [saved, setSaved] = useState(saved);
  const [paused, setPaused] = useState(false);
  const [follow, setFollow] = useState();
  const [shouldShowFollow, setShouldShowFollow] = useState(true);
  const navigation = useNavigation();
  const [Loading, setLoading] = useState();
  const [thumbnail, setThumbnail] = useState();
  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const userId = UserData?.user?.id;
  const videoRef = useRef(null);
  const dispatch = useDispatch();
  useEffect(() => {
    console.log("thumb00")

    generateThumbnail()

    dispatch(likesGetRequestForShort(userId, shortId));
    
  }, [dispatch])
  useEffect(() => {
    console.log("thumb01")

    generateThumbnail()
    dispatch(bookmarksGetRequestForFeed(userId));
  }, [dispatch]);
  useEffect(() => {
    if (!!videoRef.current) {
      videoRef.current.seek(0);
    }
  }, [currindex]);

  const shortId = spolaUrlObject?.id;
  const [likecount, setLikeCount] = useState(spolaUrlObject?.likes?.length);
  const commentscount = spolaUrlObject?.comments?.length;
  const spolaVideo = spolaUrlObject?.backBlazeSpolaUrl;
  console.log("videoooooooooooooooooo",spolaUrlObject)
  const [lasttap, setLastTap] = useState();
  const handleDoubleTap = () => {
    const now = Date.now();
    const DOUBLE_PRESS_DELAY = 300;
    if (lasttap && now - lasttap < DOUBLE_PRESS_DELAY) {
      setLiked(!likes);
      setPaused(false);
    } else {
      setLastTap(now);
      paused ? setPaused(false) : setPaused(true);
    }
  };

  const commonUser = async userObjectUrl => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('CommonProfile', {userObjectUrl});
    setLoading(false);
  };

  const comments = async () => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('SpolaComments', {spolaUrlObject});
    setLoading(false);
  };


  const [likelistId, setLikelistId] = useState("")
  const [likes, setLiked] = useState(false)

  const generateThumbnail = async () => {
    console.log("thumb0",spolaVideo)
    // await LinkPreview.getPreview('https://www.youtube.com/watch?v=MejbOFk7H6c')
    //     .then(data => console.log("thumbsssss",data));
    // const url = 'https://www.apple.com/ipad/';

    //     const metadata = await LinkPreview.generate(url);
    //     console.log(metadata,"thumbbb111111")
    // try {
    //   console.log("thumb-------")

    //   const  uri  = await VideoThumbnails.getThumbnailAsync(decodeURI("https://f004.backblazeb2.com/file/spolaApp/short_10_23250d8285.mp4"))
    //   setThumbnail(uri)
    //   console.log("thumb",uri)
    // } catch (e) {
    //   console.warn(e,"thumb2");
    // }
  };


  useEffect(() => {
    generateThumbnail()
    const shorted = _.find(likesdataFromUser, function (o) { return o.likedShort?.id === spolaUrlObject.id; });
    if (shorted) {
      setLikelistId(shorted.id);
      setLiked(true);
    }
  }, []);

  useEffect(() => {
    const shorted = _.find(likesdataFromUser, function (o) {
      return o.likedShort?.id === spolaUrlObject.id;
    });
    if (shorted) {
      setLikelistId(shorted.id);
      setLiked(true);
    }
  }, [likesdataFromUser]);
  const saveLike = async () => {
    if (likes) {
      console.log('likePressed update');
      // const likeId = likesdataFromUser[0]?.id;
      setLiked(false);
      setLikeCount(likecount - 1);
      setLoading(true);
      // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
      await dispatch(likesDeleteRequest(likelistId));
      await dispatch(likesGetRequest());
      await dispatch(likesGetRequestForShort(userId, shortId));
    } else {
      setLiked(true);
      let objectValue = {};
      objectValue.likedShort = shortId;
      objectValue.likedBy = userId;
      objectValue.postType = 'short';

      console.log('likePressed post');
      setLoading(true);
      // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
      await dispatch(likesPostRequest(objectValue));
      await dispatch(likesGetRequestForShort(userId, shortId));
      setLikeCount(likecount + 1);
    }
  };

  const [routes] = useState([
    {key: 'booking', title: t('Shorts'), icon: 'video-wireless'},
    {key: 'profile', title: t('Videos'), icon: 'video'},
    {key: 'setting', title: t('Channels'), icon: 'youtube'},
    // { key: 'activity', title: t('setting'), icon: 'cog' },
  ]);
  const options = {
    
    title: 'Title',
    message: 'Message',
    // uri:base64.uri
    // urls: reviewShare.state.blobArr[0],
  };
  const share = async (customOptions = options) => {
    try {
      await Share.open(customOptions);
    } catch (err) {
      console.log(err);
    }
  };
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, {backgroundColor: colors.text}]}
      style={[styles.tabbar, {backgroundColor: colors.neoThemeBg}]}
      tabStyle={styles.tab}
      getLabelText={({route}) => route.title}
      inactiveColor={BaseColor.grayColor}
      activeColor={colors.text}
      renderLabel={({route, focused, color}) => (
        <View
          style={{
            flex: 1,
            width: Utils.scaleWithPixel(350),
            alignItems: 'center',
            left: Utils.scaleWithPixel(15),
          }}>
          {focused ? (
            <View>
              <Text headline bold={focused} style={{fontFamily: 'ProximaNova'}}>
                {route.title}
              </Text>
              <Icon
                name={route.icon}
                color="#DDDDDD"
                size={24}
                style={{top: -22, left: -30}}
              />
            </View>
          ) : (
            <Icon
              name={route.icon}
              color="#909090"
              size={24}
              style={{top: 0, left: -15}}
            />
          )}
        </View>
      )}
    />
  );

  const renderScene = ({route, jumpTo}) => {
    switch (route.key) {
      case 'booking':
        return <BookingTab jumpTo={jumpTo} navigation={navigation} />;
      case 'profile':
        return <ProfileTab jumpTo={jumpTo} navigation={navigation} />;
      case 'setting':
        return <SettingTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };

  const openModal = modal => {
    setModalVisible(modal);
  };

  const handleIndexChange = index => setIndex(index);
  const renderModal = () => {
    return (
      <ScrollView vertical={true}>
        <View>
          <Modal
            isVisible={modalVisible === 'roomtype'}
            animationType="fade"
            // propagateSwipe
            onSwipeComplete={() => setModalVisible(false)}
            swipeDirection={['down']}
            backdropColor="transparent"
            onBackdropPress={() => setModalVisible(false)}
            // customBackdrop={<View style={{ flex: 1 }} />}
            style={styles.bottomModal}>
            <View
              style={
                (styles.contain,
                {backgroundColor: colors.neoThemebg, height: 400})
              }>
              <View>
                <TouchableOpacity
                  onPress={() => commonUser(spolaUrlObject?.uploadedBy)}>
                  <View style={{alignItems: 'center'}}>
                    <Neomorph
                      inner
                      style={{
                        shadowRadius: Utils.scaleWithPixel(7),
                        borderRadius: Utils.scaleWithPixel(100),
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(88),
                        height: Utils.scaleWithPixel(88),
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: -50,
                      }}>
                      <Avatar.Image
                        source={{
                          uri: spolaUrlObject?.uploadedBy?.backBlazeImageUrl
                            ? spolaUrlObject?.uploadedBy?.backBlazeImageUrl
                            : '',
                        }}
                        size={80}
                      />
                    </Neomorph>
                  </View>
                  <View>
                    <Text
                      title3
                      bold
                      style={{
                        color: 'white',
                        marginTop: 20,
                        marginBottom: 10,
                        alignSelf: 'center',
                        fontFamily: 'ProximaNova',
                      }}>
                      {spolaUrlObject?.uploadedBy?.username}
                    </Text>
                    <Text
                      grayColor
                      semibold
                      style={{alignSelf: 'center', fontFamily: 'ProximaNova'}}>
                      {spolaUrlObject?.uploadedBy?.email}
                    </Text>
                  </View>
                </TouchableOpacity>
                <View
                  style={{
                    height: 1,
                    width: 380,
                    left: 10,
                    top: 30,
                    backgroundColor: '#373a3c',
                    marginBottom: 40,
                  }}></View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    marginLeft: 10,
                    justifyContent: 'space-between',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <NeomorphBlur
                      style={{
                        shadowRadius: 3,
                        borderRadius: 100,
                        backgroundColor: colors.neoThemebg,
                        width: 52,
                        height: 48,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        style={{
                          borderRadius: 30,
                          height: 55,
                          width: 55,
                          justifyContent: 'center',
                        }}>
                        {/* <Icon name="account" size={28} color="#DDDDDD" style={{ left: 15 }} /> */}
                        <Image
                          source={require('../../assets/images2/twopeople_icn.png')}
                          style={{height: 25, width: 25, alignSelf: 'center'}}
                        />
                      </TouchableOpacity>
                    </NeomorphBlur>
                    <Text
                      grayColor
                      semibold
                      style={{
                        fontFamily: 'ProximaNova',
                        fontSize: 18,
                        alignSelf: 'center',
                      }}>
                      {spolaUrlObject?.uploadedBy?.followersProfiles?.length}{'\n'}Followers
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <NeomorphBlur
                      style={{
                        shadowRadius: 3,
                        borderRadius: 100,
                        backgroundColor: colors.neoThemebg,
                        width: 52,
                        height: 48,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        style={{
                          borderRadius: 30,
                          height: 55,
                          width: 55,
                          justifyContent: 'center',
                        }}>
                        {/* <Icon name="account" size={28} color="#DDDDDD" style={{ left: 15 }} /> */}
                        <Image
                          source={require('../../assets/images2/heartgrey_icn.png')}
                          style={{height: 20, width: 25, alignSelf: 'center'}}
                        />
                      </TouchableOpacity>
                    </NeomorphBlur>
                    <Text
                      grayColor
                      semibold
                      style={{
                        fontFamily: 'ProximaNova',
                        fontSize: 18,
                        alignSelf: 'center',
                      }}>
                      {spolaUrlObject?.likes.length} {'\n'} Likes
                    </Text>
                  </View>
                  <View>
                    <NeomorphBlur
                      outer
                      style={{
                        shadowRadius: Utils.scaleWithPixel(3),
                        borderRadius: Utils.scaleWithPixel(100),
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(115),
                        height: Utils.scaleWithPixel(35),
                        justifyContent: 'center',
                        alignItems: 'center',
                        // margin: Utils.scaleWithPixel(10),
                      }}>
                      <TouchableOpacity
                        onPress={() => setShouldShowFollow(!shouldShowFollow)}>
                        <Text
                          bold
                          style={{
                            fontSize: 20,
                            color: shouldShowFollow ? 'white' : '#e3dce1',
                            fontFamily: 'ProximaNova',
                          }}>
                          {shouldShowFollow ? 'Follow' : 'Following'}
                        </Text>
                      </TouchableOpacity>
                    </NeomorphBlur>
                  </View>

                  {/* <Text style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", fontWeight: "bold", left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(15) }}>Using 400Spola coins (Balance 3042)</Text> */}
                </View>
                <View
                  style={{
                    height: 1,
                    width: 380,
                    left: 10,
                    top: 30,
                    backgroundColor: '#373a3c',
                    marginTop: -10,
                    marginBottom: 40,
                  }}></View>
              </View>
              <TabView
                lazy
                navigationState={{index, routes}}
                renderScene={renderScene}
                renderTabBar={renderTabBar}
                onIndexChange={handleIndexChange}
                style={{height: '100%'}}
                tabBarShowLabel
                tabBarIcon
              />
            </View>
          </Modal>
        </View>
      </ScrollView>
    );
  };

  /*
   * @description Show when tab Booking activated
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   * @class PreviewTab
   * @extends {Component}
   */
  function BookingTab({navigation}) {
    const UserData = useSelector(state => state.accessTokenReducer.userData);
    const shortsData = useSelector(state => state.ShortsReducer.shortsData);
    console.log('UserData', UserData);
    const shorts = UserData?.user?.shorts;
    console.log('shorts', shorts);

    console.log('coming to BookingTab ------------------');
    return (
      <ScrollView>
        <FlatList
          contentContainerStyle={{
            paddingLeft: 30,
            margin: 10,
            // paddingRight: 0,
            paddingTop: 37,
          }}
          numColumns={2}
          // vertical={true}
          // showsHorizontalScrollIndicator={false}
          data={shorts}
          keyExtractor={(item, index) => item.id}
          renderItem={({item, index}) => (
            <ProfileCard
              grid
              image={
                item?.backBlazeSpolaUrl
                  ? item?.backBlazeSpolaUrl
                  : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
              }
              style={{margin: 10, left: -30, paddingTop: 20, top: -30}}
              // onPress={() => navigation.navigate('HotelDetail')}
            >
              <View>
                <Icon
                  name="play-outline"
                  color="#dddddd"
                  size={22}
                  style={{top: -20, left: -15}}
                />
                <Text
                  subhead
                  whiteColor
                  style={{top: -40, left: 5, fontFamily: 'ProximaNova-Medium'}}>
                  {item?.viewCount}
                </Text>
              </View>
            </ProfileCard>
          )}
        />
      </ScrollView>
    );
  }

  /**
   * @description Show when tab Profile activated
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   * @class PreviewTab
   * @extends {Component}
   */
  function ProfileTab({navigation}) {
    const {colors} = useTheme();
    const {t} = useTranslation();
    const UserData = useSelector(state => state.accessTokenReducer.userData);
    const videos = UserData?.user?.videos;
    console.log('videos', videos);
    // const [post3] = useState(PostData3);

    return (
      <View style={{left: -8, marginTop: -5, height: '100%'}}>
        <FlatList
          data={videos}
          // horizontal={true}
          contentContainerStyle={{
            // paddingLeft: 25,

            // paddingRight: 0,
            paddingTop: 50,
          }}
          keyExtractor={(item, index) => item.id}
          renderItem={({item, index}) => (
            <HomeItem1
              image={
                item?.backBlazeVideoUrl
                  ? item?.backBlazeVideoUrl
                  : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
              }
              // title={item.title}
              time={item?.Duration}
              // description={item.description}
              onPress={() => navigation.navigate('HomeDetail')}></HomeItem1>
          )}
        />
      </View>
    );
  }

  /**
   * @description Show when tab Setting activated
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   * @class PreviewTab
   * @extends {Component}
   */
  function SettingTab({navigation}) {
    const {colors} = useTheme();
    const {t} = useTranslation();
    const [refreshing] = useState(false);
    const channeldata = useSelector(
      state => state.ChannelReducer.channelDataForUser,
    );
    console.log('channeldata', channeldata);
    return (
      <View style={{alignItems: 'flex-end', top: 20}}>
        <NeomorphBlur
          style={{
            shadowRadius: 3,
            borderRadius: 100,
            backgroundColor: colors.neoThemebg,
            width: 150,
            height: 56,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => navigation.navigate('CreateChannel')}>
            <Text style={{fontSize: 20, fontFamily: 'ProximaNova'}}>
              {' '}
              + Channel
            </Text>
          </TouchableOpacity>
        </NeomorphBlur>

        <FlatList
          numColumns={2}
          contentContainerStyle={{
            alignSelf: 'flex-start',
            marginTop: Utils.scaleWithPixel(-15),
          }}
          showsHorizontalScrollIndicator={false}
          data={channeldata}
          keyExtractor={(item, index) => item.id}
          renderItem={({item, index}) => (
            <ChannelsCard
              style={{margin: Utils.scaleWithPixel(10), left: 15}}
              image={
                item?.backBlazeCoverImg
                  ? item?.backBlazeCoverImg
                  : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
              }
              // name={item.name}
            >
              <Text
                style={{
                  top: Utils.scaleWithPixel(29),
                  left: Utils.scaleWithPixel(5),
                  alignSelf: 'center',
                  color: '#909090',
                  fontFamily: 'ProximaNova-Medium',
                  fontSize: 14,
                }}>
                {item.channelDesc}
              </Text>
              <Text
                style={{
                  top: Utils.scaleWithPixel(30),
                  left: Utils.scaleWithPixel(5),
                  alignSelf: 'center',
                  color: '#e3dce1',
                  fontWeight: 'bold',
                  fontFamily: 'ProximaNova',
                  fontSize: 16,
                }}>
                {item.title}
              </Text>
              <Text
                style={{
                  top: Utils.scaleWithPixel(35),
                  left: Utils.scaleWithPixel(5),
                  alignSelf: 'center',
                  color: '#909090',
                  fontFamily: 'ProximaNova-Medium',
                  fontSize: 14,
                }}>
                {item.channelName}
              </Text>

              <ProfileAuthor1
                style={{
                  left: Utils.scaleWithPixel(40),
                  top: Utils.scaleWithPixel(-90),
                  height: Utils.scaleWithPixel(100),
                  width: Utils.scaleWithPixel(100),
                }}
                image={
                  item?.backBlazeCoverImg
                    ? item?.backBlazeCoverImg
                    : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                }
                // name={item.name}
                // description={item.detail}
              />
            </ChannelsCard>
          )}
        />
      </View>
    );
  }


 

  return (
    <View>
      {renderModal()}
      <View style={{height: height}}>
        {children}
        <TouchableOpacity onPress={onPress} activeOpacity={0.9}>
          <TouchableWithoutFeedback onPress={() => handleDoubleTap()}>
            <Video
              source={{
                uri: spolaVideo ? spolaVideo : 'video',
              }}
              // poster={{
              //   uri: spolaVideo ? spolaVideo : 'video',
              // }}
              style={styles.backgroundVideo}
              ref={videoRef}
              resizeMode="cover"
              selectedVideoTrack={{
                type: "resolution",
                value: 140
              }}
              paused={paused || currindex != index}
              repeat
            />
          </TouchableWithoutFeedback>
        </TouchableOpacity>
        <View style={styles.container}>
          <TouchableOpacity onPress={() => saveLike()}>
            <Icon
              name={likes ? 'heart' : 'heart-outline'}
              size={38}
              style={{color: likes ? 'red' : '#DDDDDD', marginVertical: '2%'}}
            />
            <Text
              style={{
                fontFamily: 'ProximaNova',
                alignSelf: 'center',
                fontSize: 18,
                top: -5,
              }}>
              {likecount}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => comments()}>
            <Icon
              name={'message-text-outline'}
              size={38}
              style={{color: '#DDDDDD', marginVertical: '2%'}}
            />
            <Text
              style={{
                fontFamily: 'ProximaNova',
                alignSelf: 'center',
                fontSize: 18,
                top: -5,
              }}>
              {commentscount}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => async () => {
              await share();
            }}>
            <Icon
              name={'share-outline'}
              size={38}
              style={{color: '#DDDDDD', marginVertical: '2%'}}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon
              name={'dots-vertical'}
              size={34}
              style={{color: '#DDDDDD', marginVertical: '2%'}}
            />
          </TouchableOpacity>
        </View>
        {/* <LinearGradient style={{ opacity: 0.4, width: '100%', }} colors={['#7f8c8d', '#434343']} > */}
        <View style={{paddingHorizontal: '5%', top: -50}}>
          <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
            <TouchableOpacity onPress={() => openModal('roomtype')}>
              <Image
                source={{
                  uri: spolaUrlObject?.uploadedBy?.backBlazeImageUrl
                    ? spolaUrlObject?.uploadedBy?.backBlazeImageUrl
                    : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png',
                }}
                style={{borderRadius: 30, height: 45, width: 45}}
                size={40}
              />
              <Text
                title3
                bold
                style={{
                  color: 'white',
                  marginTop: -35,
                  marginLeft: 50,
                  alignSelf: 'flex-start',
                  fontFamily: 'ProximaNova',
                }}>
                {spolaUrlObject?.uploadedBy?.username}
              </Text>
            </TouchableOpacity>
            <View
              style={{
                borderWidth: 1,
                borderRadius: 5,
                height: 20,
                width: 66,
                borderColor: 'white',
                alignContent: 'flex-end',
                marginTop: 15,
                marginLeft: 10,
                backgroundColor: colors.card,
              }}>
              <TouchableOpacity onPress={() => setFollow(!follow)}>
                <Text
                  semibold
                  style={{
                    color: follow ? 'white' : BaseColor.greenColor,
                    fontFamily: 'ProximaNova',
                    alignSelf: 'center',
                  }}>
                  {follow ? '+ Follow' : '✓Following'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text
            style={{
              color: 'white',
              marginTop: 20,
              fontFamily: 'ProximaNova',
              fontSize: 18,
            }}>
            {spolaUrlObject?.description}
          </Text>
        </View>
        {/* </LinearGradient> */}
        {/* <View style={[styles.content, { borderBottomColor: colors.border }]}>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity onPress={() => saveLike()}>
              {likes ? (<Icon name="heart" size={28} style={{ color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(-5) }} />)
                : (<Icon name="heart-outline" size={28} style={{ color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(-5) }} />)}

            </TouchableOpacity>
            <TouchableOpacity onPress={() => success()}>
              <Icon name="message-text-outline" size={28} style={{ color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(10) }} />
            </TouchableOpacity>
            <Icon name="share-outline" size={28} style={{ color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(10) }} />

          </View>
          <TouchableOpacity onPress={() => hitShotList()} >
            {shortlist ? (<Icon
              name="bookmark"
              solid
              size={28}
              color={BaseColor.whiteColor}
              style={{ position: 'absolute', alignSelf: "flex-end", top: Utils.scaleWithPixel(-25) }}
            />) : (<Icon
              name="bookmark-outline"
              solid
              size={28}
              color={BaseColor.whiteColor}
              style={{ position: 'absolute', alignSelf: "flex-end", top: Utils.scaleWithPixel(-25) }}
            />)}

          </TouchableOpacity>
          <View>
            {likeCount == 0 ? null :
              (likeCount > 1 ? (
                <Text headline bold style={{ marginTop: 5, fontFamily: "ProximaNova", alignSelf: "flex-start" }}>
                  {likeCount} likes
                </Text>
              ) : (
                <Text headline bold style={{ marginTop: 5, fontFamily: "ProximaNova", alignSelf: "flex-start" }}>
                  {likeCount} like
                </Text>
              ))}

            <Text headline bold style={{ marginBottom: Utils.scaleWithPixel(6), marginTop: Utils.scaleWithPixel(5), fontFamily: "ProximaNova" }}>
              {title}
            </Text>

            <Text style={{ fontFamily: "ProximaNova", fontSize: 18 }} numberOfLines={1} body2>{description}</Text>

            <View style={{ marginTop: 10 }}>
              {commentscount == 0 ? (
                <Text bold style={{ fontFamily: "ProximaNova", fontSize: 18 }} body2>No Comments</Text>)
                : (commentscount > 1 ? (
                  <TouchableOpacity onPress={() => success()}>
                    <Text bold style={{ fontFamily: "ProximaNova", fontSize: 18 }} body2>View all {commentscount} comments</Text>
                  </TouchableOpacity>
                ) :
                  (
                    <TouchableOpacity onPress={() => success()}>
                      <Text bold style={{ fontFamily: "ProximaNova", fontSize: 18 }} body2>{commentscount} comment</Text>
                    </TouchableOpacity>)
                )}
            </View>
            <Text style={{ fontFamily: "ProximaNova", fontSize: 16, marginTop: 5 }} body2>{time}</Text>
          </View>
        </View> */}
      </View>
    </View>
  );
}

SpolaItem.propTypes = {
  image: PropTypes.node.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  title: PropTypes.string,
  description: PropTypes.string,
  onPress: PropTypes.func,
};

SpolaItem.defaultProps = {
  image: '',
  title: '',
  description: '',
  style: {},
  onPress: () => {},
};
