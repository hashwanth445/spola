import {StyleSheet,Dimensions} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';
const {width,height}=Dimensions.get('window')
export default StyleSheet.create({
  backgroundVideo: {
    top:0,
    bottom:0,
    left:0,
    right:0,
    height: height,
    width:width,
    position: "absolute",
    backgroundColor:'black'
  },
  bottomModal: {
    justifyContent: 'flex-end',
   margin:0,
   
  },
  tabbar: {
    height: 40,
    marginTop:15,
    
  },
  tab: {
    width: 120,
    marginLeft:0,
    justifyContent:"center"
    
  },
  indicator: {
    height: 1,
    width:45,
   left:45
  },
  label: {
    fontWeight: '400',
  },
  imagePost: {width: 350, height: Utils.scaleWithPixel(150),borderRadius:Utils.scaleWithPixel(10),alignSelf:"center"},
  content: {
    marginHorizontal: Utils.scaleWithPixel(20),
    paddingVertical: Utils.scaleWithPixel(10),
    // borderBottomWidth: 1,
  },
  contentRight: {
    padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-end',
    
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin:0,
  },
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
    // marginTop:200,

  },
  contentSwipeDown: {
    paddingTop: 20,
    height:100,
    alignItems: 'center',
  },
  lineSwipeDown: {
    width: 40,
    height: 3.5,
    borderRadius:20,
    backgroundColor: BaseColor.whiteColor,
    bottom:10
  },

  img: {
    width:352, height: Utils.scaleWithPixel(150),borderRadius:Utils.scaleWithPixel(10),alignSelf:"center"
  },
  slide: {
    alignItems: 'center',
    left:15,
    marginRight:10,
    justifyContent: 'center',
    // flex: 1,
    fontFamily:"ProximaNova"
  },
  contentPage: {
    bottom: Utils.scaleWithPixel(0),
  },
  container:{
    marginTop:'110%',
    alignItems:'flex-end',
    padding:'6%'
  },
});
