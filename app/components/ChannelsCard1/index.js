import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Image, Icon, Text } from '@components';
import { Images, useTheme,BaseColor } from '@config';
import { Neomorph,NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
// import { Icon } from 'react-native-elements/dist/icons/Icon';

export default function ChannelsCard1(props) {
    const { colors } = useTheme();
    const { style, children, styleContent, image, onPress, time } = props;
    const [shouldShowFollow, setShouldShowFollow] = useState(true);
    return (
        <View style={{ flexDirection: "column", height: Utils.scaleWithPixel(250) }}>

            <TouchableOpacity
                style={[styles.card, { borderColor: colors.border }, style]}
                onPress={onPress}
                activeOpacity={0.9}>
                <Neomorph
                    outer
                    style={{
                        shadowRadius: Utils.scaleWithPixel(7),
                        borderRadius: Utils.scaleWithPixel(20),
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(140),
                        height: Utils.scaleWithPixel(180),
                        justifyContent: 'center',
                        alignItems: 'center',
                        left: Utils.scaleWithPixel(-10),
                        margin: Utils.scaleWithPixel(0),
                        marginBottom:Utils.scaleWithPixel(10)
                    }}
                >
                    {/* <Neomorph
                        inner
                        style={{
                            shadowRadius: 7,
                            borderRadius: 70,
                            backgroundColor: colors.neoThemebg,
                            width: 65,
                            height: 65,
                            justifyContent: 'center',
                            alignItems: 'center',
                            top: -30
                        }}
                    > */}
                        <Image source={{uri:image ? image : ''}} style={{height:Utils.scaleWithPixel(80),width:Utils.scaleWithPixel(120),borderRadius:Utils.scaleWithPixel(20),top:Utils.scaleWithPixel(-45)}} />
                    {/* </Neomorph> */}
                </Neomorph>

            </TouchableOpacity>

            <View style={[styles.content, styleContent]}>{children}</View>
        </View>
    );
}

ChannelsCard1.propTypes = {
    image: PropTypes.node.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleContent: PropTypes.object,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element),
    ]),
    onPress: PropTypes.func,
};

ChannelsCard1.defaultProps = {
    image: Images.profile2,
    style: {},
    styleContent: {},
    onPress: () => { },
};
