import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';


export default StyleSheet.create({
  contain: {paddingVertical: Utils.scaleWithPixel(5)},
  contentLeft: {
    flex: 1,
    flexDirection: 'row',
    // justifyContent: 'flex-start',
    // alignItems: 'center',
  marginTop:-60
  },
  thumb: {
    // borderWidth: 1,
    borderColor: BaseColor.whiteColor,
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    top:Utils.scaleWithPixel(25),
    left:Utils.scaleWithPixel(10)
  },
  contentRight: {
    padding: Utils.scaleWithPixel(7),
    top:-10,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
});
