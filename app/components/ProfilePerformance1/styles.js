import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    flexDirection: 'row',
    // borderColor:"#ffffff",
    // borderWidth:Utils.scaleWithPixel(0.2),
    // height:Utils.scaleWithPixel(58),
    // width:Utils.scaleWithPixel(290),
    marginLeft:Utils.scaleWithPixel(45),
    // borderRadius:Utils.scaleWithPixel(9),
  },
  contentLeft: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: Utils.scaleWithPixel(0)
  },
  contentCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:Utils.scaleWithPixel(0)
  },
  contentRight: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: Utils.scaleWithPixel(10)
  },
  itemInfor: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
