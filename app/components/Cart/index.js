import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text, Icon } from '@components';
import Icon1 from 'react-native-vector-icons/Ionicons';
import { useDispatch, useSelector } from 'react-redux';
import styles from './styles';
import PropTypes from 'prop-types';
import { useTheme } from '@config';
import * as Utils from '@utils';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { cartsDeleteRequest } from '../../api/carts'

export default function Cart(props) {
  const { colors } = useTheme();
  const {
    style,
    imageStyle,
    image,
    txtLeftTitle,
    txtContent,
    txtRight,
    onPress,
    productObj
  } = props;
  console.log("product", productObj)
  const [loading, setLoading] = useState(true);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(cartsDeleteRequest());
  }, [dispatch]);
  const id = productObj?.id
  const productId = productObj?.productId?.id
  const deleteProduct = async () => {
    // console.log("commentPressed");
    // if (!order) {
    //   // alert('Order Placed')

    // }
    // else 
    {
      let objectValue = {};
      objectValue.productId = productId;
      setLoading(true);
      // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
      await dispatch(cartsDeleteRequest(id, objectValue));

    }

  };
  const UserData = useSelector(state => state.accessTokenReducer.userData)
  return (
    <TouchableOpacity
      style={[styles.item, style]}
      onPress={onPress}
      activeOpacity={0.9}>

      <Neomorph
        outer
        style={{
          shadowRadius: Utils.scaleWithPixel(7),
          borderRadius: Utils.scaleWithPixel(20),
          backgroundColor: colors.neoThemebg,
          width: Utils.scaleWithPixel(300),
          height: Utils.scaleWithPixel(60),
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: Utils.scaleWithPixel(-10),
          marginTop: Utils.scaleWithPixel(10),

        }}
      >
        <View style={[styles.contain, { borderBottomColor: colors.border }]}>
          <Image source={{ uri: image ? image : '' }} style={[styles.thumb, imageStyle]} />
          <View style={styles.content}>
            <View style={styles.left}>
              <Text bold title3 style={{ fontFamily: "ProximaNova", marginBottom: Utils.scaleWithPixel(5), }}>
                {txtLeftTitle}
              </Text>
              {/* <Text
                numberOfLines={1}
                
                grayColor
                style={{
                  paddingTop: Utils.scaleWithPixel(0),
                  fontFamily:"ProximaNova"
                }}>
                {txtContent}
              </Text> */}
              <Text body1 grayColor numberOfLines={1} style={{ fontFamily: "ProximaNova" }}>
                Price : {txtRight}
              </Text>
            </View>
            <View style={styles.right}>
              <TouchableOpacity onPress={() => deleteProduct()}>
                <NeomorphBlur
                  style={{
                    shadowRadius: 3,
                    borderRadius: 20,
                    backgroundColor: colors.neoThemebg,
                    width: 50,
                    height: 50,

                    justifyContent: 'center',
                    alignItems: 'center',
                  
                  }}
                >
                  <Icon1 name="trash-outline" size={20} color="white" />
                </NeomorphBlur>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Neomorph>
    </TouchableOpacity >

  );
}

Cart.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  imageStyle: PropTypes.object,
  image: PropTypes.node.isRequired,
  txtLeftTitle: PropTypes.string,
  txtContent: PropTypes.string,
  txtRight: PropTypes.string,
  onPress: PropTypes.func,
};

Cart.defaultProps = {
  style: {},
  imageStyle: {},
  image: '',
  txtLeftTitle: '',
  txtContent: '',
  txtRight: '',
  onPress: () => { },
};
