import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Image, Icon, Text } from '@components';
import { Images, useTheme } from '@config';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import { BaseColor } from '../../config/theme';
// import { Icon } from 'react-native-elements/dist/icons/Icon';

export default function ProductsCard(props) {
    const { colors } = useTheme();
    const { style, children, styleContent, image, onPress, time, price,prouctsObject } = props;
    const [shouldShowFollow, setShouldShowFollow] = useState(true);
    // console.log("prouctsObject",prouctsObject);
    return (
        <View style={{ flexDirection: "column", height: Utils.scaleWithPixel(250) }}>

            <TouchableOpacity
                style={[styles.card, { borderColor: colors.border }, style]}
                onPress={onPress}
                activeOpacity={0.9}>
                <Neomorph
                    outer
                    style={{
                        shadowRadius: Utils.scaleWithPixel(7),
                        borderRadius: Utils.scaleWithPixel(20),
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(150),
                        height: Utils.scaleWithPixel(225),
                        justifyContent: 'center',
                        alignItems: 'center',
                        left: Utils.scaleWithPixel(0),
                        margin: Utils.scaleWithPixel(10),
                        marginBottom: Utils.scaleWithPixel(10)
                    }}
                >
                    {/* <Neomorph
                        inner
                        style={{
                            shadowRadius: 7,
                            borderRadius: 70,
                            backgroundColor: colors.neoThemebg,
                            width: 65,
                            height: 65,
                            justifyContent: 'center',
                            alignItems: 'center',
                            top: -30
                        }}
                    > */}
                    {/* <Image source={image} style={styles.imageBanner} /> */}
                    {/* </Neomorph> */}
                    <View style={{ top: 95 }}>
                        {shouldShowFollow ? (
                            <NeomorphBlur
                                style={{
                                    shadowRadius: 3,
                                    borderRadius: 100,
                                    backgroundColor: colors.neoThemebg,
                                    width: 155,
                                    height: 45,
                                    top: 10,
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => setShouldShowFollow(!shouldShowFollow)}>
                                    <Text semibold style={{ fontSize: 20, color: "#e3dce1", fontFamily: "ProximaNova" }}>Use {prouctsObject?.productRequiredRewardedPoints}</Text>
                                </TouchableOpacity>
                            </NeomorphBlur>) : (
                            <NeomorphBlur
                            style={{
                                shadowRadius: 3,
                                borderRadius: 100,
                                backgroundColor: colors.neoThemebg,
                                width: 155,
                                height: 45,
                                top: 10,
                                justifyContent: 'center',
                                alignItems: 'center',

                            }}
                        >
                                <TouchableOpacity style={{ backgroundColor: BaseColor.blueColor, width: 140, height: 35, borderRadius: 20 }}
                                    onPress={() => setShouldShowFollow(!shouldShowFollow)}>
                                    <Text semibold style={{ fontSize: 20, left: 40, fontFamily: "ProximaNova", top: 5 }}>Use {prouctsObject?.productRequiredRewardedPoints}</Text>
                                </TouchableOpacity>
                            </NeomorphBlur>)}
                    </View>
                </Neomorph>

            </TouchableOpacity>

            <View style={[styles.content, styleContent]}>{children}</View>
        </View>
    );
}

ProductsCard.propTypes = {
    image: PropTypes.node.isRequired,
    price: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleContent: PropTypes.object,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element),
    ]),
    onPress: PropTypes.func,
};

ProductsCard.defaultProps = {
    image: Images.profile2,
    price: '',
    style: {},
    styleContent: {},
    onPress: () => { },
};
