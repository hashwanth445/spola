import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  imageBanner: {flex: 1, borderRadius: Utils.scaleWithPixel(40),top:Utils.scaleWithPixel(0),width:Utils.scaleWithPixel(65)},
  card: {
    // borderRadius: 0,
    marginTop:Utils.scaleWithPixel(25),
  
  },
  content: {
    position: 'absolute',
    alignItems: 'flex-start',
    bottom: Utils.scaleWithPixel(-10),
    left:Utils.scaleWithPixel(10),
    padding: Utils.scaleWithPixel(10),
  },
});
