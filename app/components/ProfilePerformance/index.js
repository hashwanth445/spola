import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import * as Utils from '@utils';
import { useNavigation } from '@react-navigation/native';

export default function ProfilePerformance(props) {
  const navigation = useNavigation();
  const renderValue = (type, value) => {
    switch (type) {
      case 'primary':
        return (
          <Text primaryColor>
            {value}
          </Text>
        );
      case 'small':
        return (
          <Text body1>
            {value}
          </Text>
        );
      default:
        return (
          <Text headline style={{ fontFamily: "ProximaNova-Semibold", alignSelf: "center" }}>
            {value}
          </Text>
        );
    }
  };

  const renderTitle = (type, value) => {
    switch (type) {
      case 'primary':
        return (
          <Text body2 grayColor>
            {value}
          </Text>
        );
      case 'small':
        return (
          <Text caption1 grayColor>
            {value}
          </Text>
        );
      default:
        return (
          <Text color="#ffffff" style={{ fontFamily: "ProximaNova-Semibold", fontSize: 16, fontWeight: "bold" }}>
            {value}
          </Text>
        );
    }
  };

  const {
    style,
    contentLeft,
    contentCenter,
    contentRight,
    data,
    type,
    flexDirection,
    profileDataObject
  } = props;
  console.log("profileDataObject", profileDataObject);
  const [Loading, setLoading] = useState();

  const success = async () => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('Rewards', { profileDataObject })
    setLoading(false);

  };
  const success1 = async () => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('UserFollowers', { profileDataObject })
    setLoading(false);

  };
  const success2 = async () => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('UserFollowing', { profileDataObject })
    setLoading(false);

  };

  switch (flexDirection) {
    case 'row':
      return (
        <View style={[styles.contain, style]}>
          {data.map((item, index) => {
            if (index == 0) {
              return (
                <TouchableOpacity onPress={() => success2()}>
                  <View style={[styles.contentLeft, contentLeft]} key={index}>
                    {renderValue(type, item.value)}
                    {renderTitle(type, item.title)}
                  </View>
                </TouchableOpacity>
              );
            } else if (index == data.length - 1) {
              return (
                <TouchableOpacity onPress={() => success()}>
                  <View style={[styles.contentRight, contentRight]} key={index}>
                    {renderValue(type, item.value)}
                    {renderTitle(type, item.title)}
                  </View>
                </TouchableOpacity>
              );
            } else {
              return (
                <View style={[styles.contentCenter, contentCenter]} key={index}>
                  <TouchableOpacity onPress={() => success1()}>
                    {renderValue(type, item.value)}
                    {renderTitle(type, item.title)}
                  </TouchableOpacity>
                </View>
              );
            }
          })}
        </View>
      );
    case 'column':
      return (
        <View style={[{ justifyContent: 'space-between', flex: 1 }, style]}>
          {data.map((item, index) => (
            <View style={styles.itemInfor} key={index}>
              {renderTitle(type, item.title)}
              {renderValue(type, item.value)}
            </View>
          ))}
        </View>
      );
  }
}

ProfilePerformance.propTypes = {
  flexDirection: PropTypes.string,
  type: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  data: PropTypes.array,
  contentLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  contentCenter: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  contentRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

ProfilePerformance.defaultProps = {
  flexDirection: 'row',
  type: 'medium',
  style: {},
  data: [],
  contentLeft: {},
  contentCenter: {},
  contentRight: {},
};
