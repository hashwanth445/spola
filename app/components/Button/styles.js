import {StyleSheet} from 'react-native';
import {BaseColor, Typography, FontWeight} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  default: {
    height: Utils.scaleWithPixel(36),
    borderRadius: Utils.scaleWithPixel(8),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: Utils.scaleWithPixel(20),
  },
  textDefault: {
    ...Typography.title2,
    color: BaseColor.buttonTextColor,
    fontWeight: FontWeight.semibold,
    fontFamily:"ProximaNova"
  },
  outline: {
    borderWidth: Utils.scaleWithPixel(1),
  },

  full: {
    // width: '100%',
    alignSelf: 'auto',
  },
  round: {
    borderRadius: Utils.scaleWithPixel(28),
  },
});
