import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {paddingVertical: Utils.scaleWithPixel(10), flexDirection: 'row'},
  contentLeft: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  thumb: {
    borderWidth: Utils.scaleWithPixel(1),
    borderColor: BaseColor.whiteColor,
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    top:Utils.scaleWithPixel(45)
  },
  contentRight: {
    padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
imageBanner: {flex: 1, borderRadius: 20,borderWidth:1.9,borderColor:"#2c3036",width:130,height:50},

  card: {
    width: Utils.scaleWithPixel(155),
    height: Utils.scaleWithPixel(230),
    borderRadius: Utils.scaleWithPixel(0),
    marginTop:Utils.scaleWithPixel(15),
  
  },
  content: {
    position: 'absolute',
    alignItems: 'center',
    alignContent:"center",
    bottom: Utils.scaleWithPixel(-10),
    // padding: Utils.scaleWithPixel(10),
  },

});
