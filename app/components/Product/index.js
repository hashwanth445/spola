import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text, Icon } from '@components';
import { useTheme } from '@config';
import styles from './styles';
import PropTypes from 'prop-types';
import { Neomorph } from 'react-native-neomorph-shadows';
import { ScrollView } from 'react-native-gesture-handler';
import * as Utils from '@utils';

export default function Product(props) {
    const { colors } = useTheme();
    const { style, children, styleContent, image, onPress, time } = props;
    return (
        <View style={{ flexDirection: "column", height: Utils.scaleWithPixel(250) }}>
            <Neomorph
                outer
                style={{
                    shadowRadius: Utils.scaleWithPixel(7),
                    borderRadius: Utils.scaleWithPixel(20),
                    backgroundColor: colors.neoThemebg,
                    width: Utils.scaleWithPixel(150),
                    height: Utils.scaleWithPixel(180),
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10,
                    marginLeft: 5
                }}
            >
                <TouchableOpacity
                    style={[styles.card, { borderColor: colors.border }, style]}
                    onPress={onPress}
                // activeOpacity={0.9}
                >
                    <Neomorph
                        inner
                        style={{
                            shadowRadius: Utils.scaleWithPixel(7),
                            borderRadius: Utils.scaleWithPixel(10),
                            backgroundColor: colors.neoThemebg,
                            width: Utils.scaleWithPixel(125),
                            height: Utils.scaleWithPixel(100),
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginLeft: 18,
                            marginTop: 30
                        }}
                    >
                        <Image source={{ uri: image ? image : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg' }} style={{ width: 130, height: 100, borderRadius: 10,top:-3 }} />
                    </Neomorph>

                </TouchableOpacity>

                <View style={[styles.content, styleContent]}>{children}</View>
            </Neomorph>
        </View>
    );
}

Product.propTypes = {
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    users: PropTypes.array,
    name: PropTypes.string,
    detail: PropTypes.string,
    styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    onPress: PropTypes.func,
    onPressLove: PropTypes.func,
};

Product.defaultProps = {
    style: {},
    users: [],
    name: '',
    detail: '',
    styleLeft: {},
    styleThumb: {},
    styleRight: {},
    onPress: () => { },
    onPressLove: () => { },
};
