import {BaseColor} from '@config';
import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  thumb: {
    width: Utils.scaleWithPixel(80),
    height: Utils.scaleWithPixel(80),
    borderRadius: Utils.scaleWithPixel(40),
    marginRight: Utils.scaleWithPixel(10),
  },
});
