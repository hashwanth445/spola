import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Image, Icon, Text } from '@components';
import { Images, useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import Video from 'react-native-video';
// import VideoPlayer from 'react-native-video-controls';
import Swiper from 'react-native-swiper'
import IconSimple from 'react-native-vector-icons/SimpleLineIcons'
import * as Utils from '@utils';

// import { Icon } from 'react-native-elements/dist/icons/Icon';

export default function PostItem1(props,{navigation}) {
  const { colors } = useTheme();
  const [liked, setLiked] = useState(liked);
  const [saved, setSaved] = useState(saved);
  const { style, children, styleContent, image, onPress, description, title, video } = props;
  return (
    <Swiper horizontal={false} pagingEnabled={true} scrollEnabled={true}>
      <View style={{ flexDirection: "column",height:Utils.scaleWithPixel(1080) }}>

        {/* <TouchableOpacity
        style={[styles.card, { borderColor: colors.border }, style]}
        onPress={onPress}
        activeOpacity={0.9}> */}
        {/* <Image source={image} style={styles.imageBanner} /> */}
        {/* <VideoPlayer
          source={video}
          autoplay={false}
          defaultMuted={true}
          disableBack
          disableFullscreen
          // thumbnail={require('../../assets/images/avata.png')}
          // videoHeight='2000'
          // controls={true}
          // style={styles.backgroundVideo, { height: 1000 }}
          // playWhenInactive={true}
          // repeat={true}
          resizeMode={'cover'}
          tapAnywhereToPause={true}
          // rate={1.0}
          // ignoreSilentSwitch={'obey'}
        /> */}

        {/* </TouchableOpacity> */}
        <View style={[styles.content, { borderBottomColor: colors.border }]}>
          <View style={{ flexDirection: "column", left: Utils.scaleWithPixel(320), top: Utils.scaleWithPixel(-350) }}>
            <Icon name={liked ? "heart" : "heart-outline"}
              onPress={() => setLiked(!liked)}
              size={34} style={{ color: "#DDDDDD", left: Utils.scaleWithPixel(5), top: Utils.scaleWithPixel(-30) }} />
            <TouchableOpacity>
              {/* <Image source={Images.message} style={{ marginLeft: 10, top: 10, height: 25, width: 30 }}  onPress={() => navigation.navigate('Messages')}/> */}
              <IconSimple name="speech" size={30} style={{ color: "#DDDDDD", left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(10) }} />
            </TouchableOpacity>
            {/* <Icon name="share-outline" size={34} style={{ color: "#DDDDDD", left: 5, top: 50 }} /> */}
            <Image source={require('../../assets/images2/share_icn.png')} style={{height:Utils.scaleWithPixel(24),width:Utils.scaleWithPixel(26),top:Utils.scaleWithPixel(50),left:Utils.scaleWithPixel(10)}} />
          </View>
          <View>
            <Icon name="music" color="#ffffff" size={18} style={{ top: Utils.scaleWithPixel(-170), left: Utils.scaleWithPixel(-5) }} />
            <Text headline style={{ marginBottom: Utils.scaleWithPixel(6), left: Utils.scaleWithPixel(15), top: Utils.scaleWithPixel(-190), fontFamily: "ProximaNova-Regular" }}>
              {title}
            </Text>
            <Text body2 style={{ marginBottom: Utils.scaleWithPixel(6), top: Utils.scaleWithPixel(-260), fontFamily: "ProximaNova-Bold" }}>{description}</Text>
          </View>
        </View>

        <View style={[styles.content, styleContent]}>{children}</View>
      </View>
    </Swiper>
  );
}

PostItem1.propTypes = {
  image: PropTypes.node.isRequired,
  video: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleContent: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  title: PropTypes.string,
  description: PropTypes.string,
  onPress: PropTypes.func,
};

PostItem1.defaultProps = {
  image: Images.profile2,
  style: {},
  title: '',
  description: '',
  video: '',
  styleContent: {},
  onPress: () => { },
};
