import {StyleSheet,Dimensions} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
  imagePost: {width: '100%', height: Utils.scaleWithPixel(150),borderRadius:Utils.scaleWithPixel(10)},
  content: {
    // position: 'absolute',
    // alignItems: 'flex-start',
    bottom: Utils.scaleWithPixel(-10),
    left:Utils.scaleWithPixel(10),
    padding: Utils.scaleWithPixel(10),
  },
  contentRight: {
    padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-end',
    
  },
  backgroundVideo: {
    // height: height,
    position: "absolute",
    top: Utils.scaleWithPixel(0),
    left: Utils.scaleWithPixel(0),
    alignItems: "stretch",
    bottom: Utils.scaleWithPixel(0),
    right: Utils.scaleWithPixel(0),
  },
});
