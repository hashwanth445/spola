import React, { useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import * as Utils from '@utils';
import { useSelector, useDispatch } from 'react-redux';
import { format, render, cancel, register } from 'timeago.js';

export default function ProfileAuthor(props) {
  const {
    style,
    image,
    title,
    styleLeft,
    styleThumb,
    styleRight,
    onPress,
    name,
    description,
    dateAndTime,
    textRight,
  } = props;
  const newtime = format(dateAndTime, 'en_US');
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const dispatch = useDispatch();
  // useEffect(() => {
  //     dispatch(videosGetRequest(userToken));

  //   }, [dispatch])
  // const {videoUrlObject}= route.params;
  // const videoViewCount = videoUrlObject?.viewCount
  return (
    <TouchableOpacity
      style={[styles.contain, style]}
      onPress={onPress}
      activeOpacity={0.9}>
      <View style={[styles.contentLeft, styleLeft]}>
        <Image source={{ uri: image ? image : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg' }} style={[styles.thumb, styleThumb]} />
        <View>
          <Text bold style={{ fontFamily: "ProximaNova", fontSize: 14, color: "#E2E2E2",letterSpacing:0.5 }}>
            {title}
          </Text>
          <Text style={{ fontFamily: "ProximaNova", color: "#B3B6B7", fontSize: 12}} numberOfLines={0} >
            {name}  {description} views | {newtime}
          </Text>
        </View>
      </View>
      <View style={[styles.contentRight, styleRight]}>
        <Text caption2 grayColor numberOfLines={1}>
          {textRight}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

ProfileAuthor.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  image: PropTypes.node.isRequired,
  description: PropTypes.string,
  title: PropTypes.string,
  name: PropTypes.string,
  dateAndTime: PropTypes.string,
  textRight: PropTypes.string,
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

ProfileAuthor.defaultProps = {
  image: '',
  name: '',
  title: '',
  description: '',
  dateAndTime: '',
  textRight: '',
  styleLeft: {},
  styleThumb: {},
  styleRight: {},
  style: {},
  onPress: () => { },
};
