import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import {Image, Text,Icon} from '@components';
import {Images, useTheme} from '@config';

export default function EventCard(props) {
  const {colors} = useTheme();
  const {style, title, location,viewsCount,styleContent, time, image, children, onPress} = props;
  return (
    <TouchableOpacity
      style={[styles.content, {borderColor: colors.border}, style]}
      onPress={onPress}
      activeOpacity={0.9}>
      <Image source={image} style={styles.imageBanner} />
      <View
        style={{
          padding: 10,
          marginTop:-60,
          flexDirection: 'row',
        }}>
       
        <View style={{flex: 1, alignItems: 'flex-start'}}>
        <Icon name="youtube" color="red" size={32} style={{left:150}} />
        <View style={[styles.content1, styleContent]}>{children}</View>
          {/* <Text body1 bold numberOfLines={1}>
            {title}
          </Text>
          <Text overline grayColor style={{marginVertical: 5}}>
            {time}
          </Text>
          <Text overline grayColor>
            {viewsCount} Views
          </Text> */}
        </View>
        <View style={{flex: 1, alignItems: 'flex-start'}}>
          
        </View>
      </View>
    </TouchableOpacity>
  );
}

EventCard.propTypes = {
  image: PropTypes.node.isRequired,
  // title: PropTypes.string,
  // time: PropTypes.string,
  // location: PropTypes.string,
  // viewsCount:PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

EventCard.defaultProps = {
  image: Images.profile2,
  // title: 'BBC Music Introducing',
  // time: '7 hrs ago',
  // location: 'Tobacco Dock, London',
  // viewsCount: "200",
  style: {},
  onPress: () => {},
};
