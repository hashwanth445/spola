import {StyleSheet} from 'react-native';
import * as Utils from '@utils';
export default StyleSheet.create({
  imageBanner: {
    height: Utils.scaleWithPixel(200),
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    top:60,
    
  },
  content: {
    borderRadius: 5,
    borderWidth: 0.5,
    width: Utils.scaleWithPixel(350),
    height: Utils.scaleWithPixel(320),
  },
  content1: {
    position: 'absolute',
    alignItems: 'flex-start',
    bottom: -10,
    left:10,
    padding: 10,
  },
});
