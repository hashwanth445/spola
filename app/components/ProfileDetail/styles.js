import {BaseColor} from '@config';
import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {flexDirection: 'row'},
  contentLeft: {
    flex: 8,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  thumb: {
    width: Utils.scaleWithPixel(60),
    height: Utils.scaleWithPixel(60),
    borderRadius: Utils.scaleWithPixel(30),
    marginRight: Utils.scaleWithPixel(10),
  },
  contentRight: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  point: {
    width: Utils.scaleWithPixel(18),
    height: Utils.scaleWithPixel(18),
    borderRadius: Utils.scaleWithPixel(9),
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: Utils.scaleWithPixel(9),
    bottom: Utils.scaleWithPixel(0),
  },
});
