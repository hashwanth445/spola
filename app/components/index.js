import Text from './Text';
import Button from './Button';
import Tag from './Tag';
import Icon from './Icon';
import HomeLatestVideosComponent from './HomeLatestVideosComponent';
// import BookingHistory from './BookingHistory';
// import StepProgress from './StepProgress';
// import PackageItem from './PackageItem';
// import TourItem from './TourItem';
// import TourDay from './TourDay';
// import CarItem from './CarItem';
// import RoomType from './RoomType';
// import HelpBlock from './HelpBlock';
// import StarRating from './StarRating';
// import ProfileDescription from './ProfileDescription';
// import Coupon from './Coupon';
// import Coupon from './Coupon';
// import FlightPlan from './FlightPlan';
// import FormOption from './FormOption';
// import QuantityPicker from './QuantityPicker';
// import FlightItem from './FlightItem';
// import CruiseItem from './CruiseItem';
// import DatePicker from './DatePicker';
// import BusItem from './BusItem';
// import BusPlan from './BusPlan';
// import EventItem2 from './EventItem2';
// import TrophyCard from './TrophyCard';

import CommentItem from './CommentItem';
import ProfilePerformance from './ProfilePerformance';
import CommonUserProfilePerformance from './CommonUserProfilePerformance';
import ListThumbSquare from './ListThumbSquare';
import Header from './Header';
import HeaderHome from './HeaderHome';
import {SafeAreaView} from 'react-native-safe-area-context';
import ProfileAuthor from './ProfileAuthor';
import ProfileDetail from './ProfileDetail';
import ProfileGroup from './ProfileGroup';
import ProfileGroupSmall from './ProfileGroupSmall';
import RateDetail from './RateDetail';
import ListThumbCircle from './ListThumbCircle';
import PostListItem from './PostListItem';
import PostItem from './PostItem';
import Card from './Card';
import MediaUploadComponent from './MediaUploadComponent'
import FilterSort from './FilterSort';
import Image from './Image';
import BookingTime from './BookingTime';

import EventItem from './EventItem';
import SpolasCard from './SpolasCard';
import EventCard from './EventCard';
import TextInput from './TextInput';
import RangeSlider from './RangeSlider';
import ListThumbnails from './ListThumbnails';
import ProfileAuthor2 from './ProfileAuthor2';
import AudioGroup from './AudioGroup';
import ListThumbnails1 from './ListThumbnails1';
import ProfileGroup1 from './ProfileGroup1';
import CardRewards from './CardRewards';
import ProfilePerformance1 from './ProfilePerformance1';
import Search from './Search';
import Category from './Categories';
import HomeItem from './HomeLatestVideosComponent';
import HomeItem1 from './HomeItem1';
import Product from './Product';
import MessengerCircle from './MessengerCircle';
import AudioRoomCircle from './AudioRoomCircle';
import CategoryCard from './CategoryCard';
import CreatorsCard from './CreatorsCard';
import ChannelsCard from './ChannelsCard';
import PostItem1 from './VideoItem';
import ProfileAuthor1 from './ProfileAuthor1';
import ProfileCard from './ProfileCard';
import ProductsCard from './ProductsCard';
import ProfileAuthor3 from './ProfileAuthor3';
import ProfileAuthor4 from './ProfileAuthor4';
import ProfileAuthor5 from './ProfileAuthor5';
import ProfileAuthor6 from './ProfileAuthor6';
import SpolaItem from './SpolaItem';
import Product1 from './Product1';
import Cart from './Cart'
import OrderComp from './Orders';
import ChannelsCard1 from './ChannelsCard1';

export {
  ChannelsCard1,
  OrderComp,
  Cart,
  Product1,
  SpolaItem,
  ProfileAuthor5,
  ProfileAuthor6,
  // BusPlan,
  // BusItem,
  // DatePicker,
  // CruiseItem,
  // FlightItem,
  // QuantityPicker,
  // FormOption,
  // TourItem,
  // TourDay,
  // CarItem,
  // RoomType,
    // TrophyCard,
      // FlightPlan,
  // EventItem2,
// StepProgress,
  // PackageItem,
    // HelpBlock,
  // StarRating,
  // ProfileDescription,
  // Coupon,
  // BookingHistory,

  BookingTime,
  Image,
  Text,
  Button,
  Tag,
  Icon,
  MediaUploadComponent,
  HomeLatestVideosComponent,
  CommentItem,
  ProfilePerformance,
  CommonUserProfilePerformance,
  ListThumbSquare,
  ListThumbCircle,
  Header,
  HeaderHome,
  SafeAreaView,
  ProfileAuthor,
  ProfileDetail,
  ProfileGroup,
  RateDetail,
  PostListItem,
  PostItem,
  Card,
  FilterSort,
  EventItem,
  SpolasCard,
  ProfileGroupSmall,
  EventCard,
  TextInput,
  RangeSlider,
  ListThumbnails,
  ProfileAuthor2,
  AudioGroup,
  ListThumbnails1,
  ProfileGroup1,
  CardRewards,
  ProfilePerformance1,
  Search,
  Category,
  HomeItem,
  HomeItem1,
  Product,
  MessengerCircle,
  AudioRoomCircle,
  CategoryCard,
  CreatorsCard,
  ChannelsCard,
  PostItem1,
  ProfileAuthor1,
  ProfileCard,
  ProductsCard,
  ProfileAuthor3,
  ProfileAuthor4,
};
