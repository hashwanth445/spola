import {StyleSheet} from 'react-native';
import * as Utils from '@utils';


export default StyleSheet.create({
  contain: {height: Utils.scaleWithPixel(45), flexDirection: 'row'},
  contentLeft: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: Utils.scaleWithPixel(20),
    width: Utils.scaleWithPixel(60),
    left:-10
  },
  contentCenter: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  contentRight: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingLeft: Utils.scaleWithPixel(20),
    paddingRight: Utils.scaleWithPixel(10),
    paddingTop:0,
    height: '100%',

  },
  contentRightSecond: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingLeft: Utils.scaleWithPixel(10),
    paddingRight: Utils.scaleWithPixel(10),
    height: '100%',
  },
  right: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});
