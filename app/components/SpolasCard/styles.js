import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  imageBanner: {flex: 1, borderRadius: Utils.scaleWithPixel(20),top:Utils.scaleWithPixel(-25),left:Utils.scaleWithPixel(7),borderWidth:Utils.scaleWithPixel(1.9),borderColor:"#2c3036"},
  card: {
    width: Utils.scaleWithPixel(155),
    height: Utils.scaleWithPixel(250),
    borderRadius: Utils.scaleWithPixel(0),
    marginTop:Utils.scaleWithPixel(25),
  
  },
  content: {
    position: 'absolute',
    alignItems: 'flex-start',
    bottom: Utils.scaleWithPixel(-10),
    left:Utils.scaleWithPixel(10),
    padding: Utils.scaleWithPixel(10),
  },
});
