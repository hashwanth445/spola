import React, { useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import * as Utils from '@utils';
import { useSelector, useDispatch } from 'react-redux';


export default function ProfileAuthor4(props) {
  const {
    style,
    image,
    title,
    styleLeft,
    styleThumb,
    styleRight,
    onPress,
    name,
    description,
    dateAndTime,
    textRight,
  } = props;
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const dispatch = useDispatch();
  // useEffect(() => {
  //     dispatch(videosGetRequest(userToken));

  //   }, [dispatch])
  // const {videoUrlObject}= route.params;
  // const videoViewCount = videoUrlObject?.viewCount
  return (
    <TouchableOpacity
      style={[styles.contain, style]}
      onPress={onPress}
      activeOpacity={0.9}>
      <View style={[styles.contentLeft, styleLeft]}>
        <Image source={{ uri: image ? image : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg' }} style={[styles.thumb, styleThumb]} />
        <View>
          <Text semibold style={{ fontFamily: "ProximaNova", fontSize: 18, color: "#ffffff" }} numberOfLines={1}>
            {title}
          </Text>
          <Text style={{ fontFamily: "ProximaNova-Medium", color: "#909090", fontSize: 16, }} >
            {name} subscribers
          </Text>
        </View>
      </View>
      <View style={[styles.contentRight, styleRight]}>
        <Text caption2 grayColor numberOfLines={1}>
          {textRight}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

ProfileAuthor4.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  image: PropTypes.node.isRequired,
  description: PropTypes.string,
  title: PropTypes.string,
  name: PropTypes.string,
  dateAndTime: PropTypes.string,
  textRight: PropTypes.string,
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

ProfileAuthor4.defaultProps = {
  image: '',
  name: '',
  title: '',
  description: '',
  dateAndTime: '',
  textRight: '',
  styleLeft: {},
  styleThumb: {},
  styleRight: {},
  style: {},
  onPress: () => { },
};
