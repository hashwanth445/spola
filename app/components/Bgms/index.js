import React, { useState } from 'react'
import {View,Text,StyleSheet,TouchableOpacity,Image} from 'react-native'
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/AntDesign';
export default function Bgms({index,fun,fun2}){
  
  const [playpause,setPlayPause]=useState(false)
    onselecting=(value)=>{
    fun(value)
    }
    playsong=(value,value2)=>{
      fun2(value,value2)
    }
 
  return(
        <View  style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
         
        }}>
          <TouchableOpacity
                    onPress={() => {
                     
                      onselecting(index._id)
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={require('../../assets/music/music.png')}
                        style={{width: 50, height: 50}}
                      />
                      <Text style={{color: 'white',alignSelf:'center'}}>
                        {'\t'} {index.bgmName}
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {setPlayPause(!(playpause));playsong(index._id,playpause)}}>
                    <Icon
                      name={
                        playpause ? 'pausecircleo' : 'playcircleo'
                      }
                      size={25}
                      color={'white'}
                    />
                  </TouchableOpacity>
        </View>
  )

}