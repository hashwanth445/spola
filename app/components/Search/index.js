import React, {useState} from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text, Icon } from '@components';
import { useTheme } from '@config';
import styles from './styles';
import PropTypes from 'prop-types';
import { Neomorph } from 'react-native-neomorph-shadows';
import { ScrollView } from 'react-native-gesture-handler';

export default function Search(props) {
    const { colors } = useTheme();
    const {
        style,
        users,
        styleLeft,
        styleThumb,
        styleRight,
        onPress,
        onPressLove,
        name,
        detail,
        image
    } = props;
    const [shouldShowFollow, setShouldShowFollow] = useState(true);
    return (
        <ScrollView horizontal={true}>
            <View style={[styles.contain, style]}>
                <TouchableOpacity
                    style={[styles.contentLeft, styleLeft]}
                    onPress={onPress}
                    activeOpacity={0.9}>
                    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        {users.map((item, index) => {
                            return (
                                <Neomorph
                                    outer
                                    style={{
                                        shadowRadius: 7,
                                        borderRadius: 20,
                                        backgroundColor: colors.neoThemebg,
                                        width: 155,
                                        height: 215,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginTop: 20,
                                        margin:10
                                    }}
                                >
                                    <Neomorph
                                        inner
                                        style={{
                                            shadowRadius: 7,
                                            borderRadius: 70,
                                            backgroundColor: colors.neoThemebg,
                                            width: 75,
                                            height: 75,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginTop: 15
                                        }}
                                    >
                                        <Image source={image} style={styles.image} />
                                    </Neomorph>
                                    <Text style={{ fontSize: 16, color: "#909090",marginTop:20,fontFamily:"Proxima-Nova" }}>3M Followers</Text>
                                    <Text style={{ fontSize: 20,margin:5, color: "#e3dce1",fontFamily:"Proxima-Nova",fontWeight:"bold" }}>Creator 1</Text>
                                    <Text style={{ fontSize: 20,marginTop:-10, color: "#909090",fontFamily:"Proxima-Nova" }}>@creator1</Text>
                                    {shouldShowFollow ? (
                                        <Neomorph
                                            outer
                                            style={{
                                                shadowRadius: 7,
                                                borderRadius: 20,
                                                backgroundColor: colors.neoThemebg,
                                                width: 125,
                                                height: 35,
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: 10,
                                                marginTop:5
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => setShouldShowFollow(!shouldShowFollow)}>
                                            <Text style={{ fontSize: 20, color: "#e3dce1",fontFamily:"Proxima-Nova",fontWeight:"bold" }}>Follow</Text>
                                            </TouchableOpacity>
                                        </Neomorph>) : (<Neomorph
                                            inner
                                            style={{
                                                shadowRadius: 7,
                                                borderRadius: 20,
                                                backgroundColor: colors.neoThemebg,
                                                width: 125,
                                                height: 35,
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: 10,
                                                marginTop:5
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => setShouldShowFollow(!shouldShowFollow)}>
                                            <Text style={{ fontSize: 20, color: "#e3dce1",fontFamily:"Proxima-Nova",fontWeight:"bold" }}>Follow</Text>
                                            </TouchableOpacity>
                                        </Neomorph>)}
                                </Neomorph>
                            );
                        })}
                    </View>
                    {/* <View
                        style={{
                            flex: 1,
                            alignItems: 'flex-start',
                        }}>
                        <Text headline semibold numberOfLines={2} style={{ top: -15, left: -90, fontSize: 18 }}>
                            {name} and 25 others
                        </Text>
                        <Text title2 semibold numberOfLines={1} style={{ top: -70, left: -70, fontSize: 20, margin: -20 }}>
                            {detail}
                        </Text>
                    </View> */}
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

Search.propTypes = {
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    users: PropTypes.array,
    image: PropTypes.string,
    name: PropTypes.string,
    detail: PropTypes.string,
    styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    onPress: PropTypes.func,
    onPressLove: PropTypes.func,
};

Search.defaultProps = {
    style: {},
    users: [],
    image:'',
    name: '',
    detail: '',
    styleLeft: {},
    styleThumb: {},
    styleRight: {},
    onPress: () => { },
    onPressLove: () => { },
};
