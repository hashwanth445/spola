import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  content: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  thumb: {
    borderWidth: Utils.scaleWithPixel(1),
    borderColor: BaseColor.whiteColor,
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
  },
  couter: {
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.fieldColor,
    marginLeft: Utils.scaleWithPixel(5),
  },
});
