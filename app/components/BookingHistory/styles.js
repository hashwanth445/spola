import {StyleSheet} from 'react-native';
import * as Utils from '@utils';


export default StyleSheet.create({
  contain: {
    shadowOffset: {height: Utils.scaleWithPixel(1)},
    shadowOpacity: Utils.scaleWithPixel(1.0),
    elevation: Utils.scaleWithPixel(5),
  },
  nameContent: {
    alignItems: 'flex-start',
    borderBottomWidth: Utils.scaleWithPixel(1),
    paddingHorizontal: Utils.scaleWithPixel(10),
    paddingVertical: Utils.scaleWithPixel(8),
    borderTopRightRadius: Utils.scaleWithPixel(8),
    borderTopLeftRadius: Utils.scaleWithPixel(8),
  },
  validContent: {
    flexDirection: 'row',
    paddingHorizontal: Utils.scaleWithPixel(12),
    paddingVertical: Utils.scaleWithPixel(7),
    justifyContent: 'space-between',
    borderBottomRightRadius: Utils.scaleWithPixel(8),
    borderBottomLeftRadius: Utils.scaleWithPixel(8),
  },
  mainContent: {
    paddingHorizontal: Utils.scaleWithPixel(12),
    paddingVertical: Utils.scaleWithPixel(20),
    flexDirection: 'row',
  },
});
