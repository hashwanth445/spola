import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  imageBanner: {flex: 1, borderRadius: 20,top:-25,left:7,borderWidth:1.9,borderColor:"#2c3036",opacity:0.6},
  card: {
    width: Utils.scaleWithPixel(145),
    height: Utils.scaleWithPixel(230),
    borderRadius: Utils.scaleWithPixel(0),
    marginTop:Utils.scaleWithPixel(25),
  
  },
  content: {
    position: 'absolute',
    alignItems: 'flex-start',
    bottom: Utils.scaleWithPixel(10),
    left:Utils.scaleWithPixel(10),
    padding: Utils.scaleWithPixel(10),
  },
});
