import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contentPickDate: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: Utils.scaleWithPixel(8),
    padding: Utils.scaleWithPixel(10),
  },
  itemPick: {
    flex: 1,
    justifyContent: 'center',
  },
  linePick: {
    width: Utils.scaleWithPixel(1),
    marginRight: Utils.scaleWithPixel(10),
  },
  contentCalendar: {
    borderRadius: Utils.scaleWithPixel(8),
    width: '100%',
  },
  contentActionCalendar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: Utils.scaleWithPixel(15),
  },
});
