import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    flexDirection: 'row',

    paddingTop: Utils.scaleWithPixel(5),
    paddingBottom: Utils.scaleWithPixel(5),
  },
  thumb: {width: Utils.scaleWithPixel(40), height: Utils.scaleWithPixel(40), marginRight: Utils.scaleWithPixel(10), borderRadius: Utils.scaleWithPixel(24), marginLeft: Utils.scaleWithPixel(5), marginTop:Utils.scaleWithPixel(5)},
  content: {
    flex: 1,
    flexDirection: 'row',
  },
  left: {
    flex: 7.5,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  right: {
    flex: 2.5,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
});
