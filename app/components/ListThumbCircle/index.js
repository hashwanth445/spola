import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import { ScrollView } from 'react-native-gesture-handler';
import * as Utils from '@utils';



export default function ListThumbCircle(props) {
  const { colors } = useTheme();
  const {
    style,
    imageStyle,
    image,
    txtLeftTitle,
    txtContent,
    txtRight,
    onPress,
  } = props;
  return (
    
      <TouchableOpacity
        style={[
          styles.contain,
          // {borderBottomWidth: 1, borderBottomColor: colors.border},
          style,
        ]}
        onPress={onPress}
        activeOpacity={0.9}>
        <Image source={image} style={[styles.thumb, imageStyle]} />
        <View style={styles.content}>
          <View style={styles.left}>
            <Text headline bold style={{fontFamily:"ProximaNova"}}>
              {txtLeftTitle}
            </Text>
            {/* <Text
              note
              numberOfLines={1}
              footnote
              grayColor
              style={{
                paddingTop: Utils.scaleWithPixel(5),
                fontFamily:"ProximaNova"
              }}>
              {txtContent}
            </Text> */}
          </View>
          <View style={styles.right}>
            <Neomorph
              outer
              style={{
                shadowRadius: Utils.scaleWithPixel(7),
                borderRadius: Utils.scaleWithPixel(15),
                backgroundColor: colors.neoThemebg,
                width: Utils.scaleWithPixel(90),
                height: Utils.scaleWithPixel(35),
                justifyContent: 'center',
                alignItems: 'center',
                margin: Utils.scaleWithPixel(8),
                marginLeft: Utils.scaleWithPixel(-10)
              }}
            >
              <Text caption2 grayColor bold numberOfLines={1} style={{ fontSize: 15 }}>
                Follow
              </Text>
            </Neomorph>

          </View>
        </View>
      </TouchableOpacity>
    
  );
}

ListThumbCircle.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  imageStyle: PropTypes.object,
  image: PropTypes.node.isRequired,
  txtLeftTitle: PropTypes.string,
  txtContent: PropTypes.string,
  txtRight: PropTypes.string,
  onPress: PropTypes.func,
};

ListThumbCircle.defaultProps = {
  style: {},
  imageStyle: {},
  image: '',
  txtLeftTitle: '',
  txtContent: '',
  txtRight: '',
  onPress: () => { },
};
