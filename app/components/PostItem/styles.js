import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  imagePost: {width: 350, height: Utils.scaleWithPixel(150),borderRadius:Utils.scaleWithPixel(10),alignSelf:"center"},
  content: {
    marginHorizontal: Utils.scaleWithPixel(20),
    paddingVertical: Utils.scaleWithPixel(10),
    // borderBottomWidth: 1,
  },
  contentRight: {
    padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-end',
    
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin:0,
  },
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    flex: 1,
  },
  contentSwipeDown: {
    paddingTop: 20,
    height:100,
    alignItems: 'center',
  },
  lineSwipeDown: {
    width: 40,
    height: 3.5,
    borderRadius:20,
    backgroundColor: BaseColor.whiteColor,
    bottom:10
  },
  wrapper: {
    // width: 375,
    // height: Utils.scaleWithPixel(150),
  },

  img: {
    width:352, height: Utils.scaleWithPixel(150),borderRadius:Utils.scaleWithPixel(10),alignSelf:"center"
  },
  slide: {
    alignItems: 'center',
    left:15,
    marginRight:10,
    justifyContent: 'center',
    // flex: 1,
    fontFamily:"ProximaNova"
  },
  contentPage: {
    bottom: Utils.scaleWithPixel(0),
  },
});
