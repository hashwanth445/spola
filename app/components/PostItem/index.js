import React, { useState, useEffect, useCallback } from 'react';
import { View, TouchableOpacity, TouchableWithoutFeedback, Share } from 'react-native';
import { Image, Text, Icon, TextInput } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import * as Utils from '@utils';
import { BaseColor, useTheme, Images } from '@config';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { likesGetRequestForFeed, likesDeleteRequest, likesGetRequest, likesPostRequest } from '../../api/likes';
import { bookmarksGetRequestForFeed, bookmarksPostRequest, bookmarksGetRequest, bookmarksDeleteRequest } from '../../api/bookmarks';
import _ from 'lodash';
import Modal from 'react-native-modal';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { format, render, cancel, register } from 'timeago.js';
import { commentsGetRequestForFeed } from '../../api/comments';


export default function PostItem(props) {
  const { style, children, title, description, onPress, image, styleRight, feedDataObject } = props;
  const { colors } = useTheme();
  const { t } = useTranslation();
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  const likesdataFromUser = useSelector(state => state.LikesReducer.likesDataForFeed)
  const bookmarkdataFromUser = useSelector(state => state.BookmarksReducer.bookmarksDataForFeed)
  // console.log("bookmarkdataFromUser,bookmark", bookmarkdataFromUser);
  console.log("likesdataFromUser", likesdataFromUser);
  const [saved, setSaved] = useState(saved);
  const navigation = useNavigation();
  const [Loading, setLoading] = useState();
  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const userId = UserData?.user?.id;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(likesGetRequestForFeed(userId));

  }, [dispatch])

  useEffect(() => {
    dispatch(bookmarksGetRequestForFeed(userId));

  }, [dispatch])

  console.log("feedDataObject", feedDataObject);
  useEffect(() => {
    dispatch(commentsGetRequestForFeed(feedId));

  }, [dispatch])

  const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          `http://www.thespola.com/feeds/${feedId}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  // const commentDataFromUser = useSelector(state => state.CommentsReducer.commentsDataForFeed)
  // console.log("commentDataFromUser",commentDataFromUser);
  const feedId = feedDataObject?.id
  const [likeCount, setLikeCount] = useState(feedDataObject?.likes?.length);
  const commentscount = feedDataObject?.comments?.length
  const postedtime = feedDataObject?.createdAt
  const time = format(postedtime, 'en_US');
  const feedImage = feedDataObject?.backBlazeImageUrl;


  const [lasttap, setLastTap] = useState();
  const handleDoubleTap = () => {
    const now = Date.now();
    const DOUBLE_PRESS_DELAY = 300;
    if (lasttap && now - lasttap < DOUBLE_PRESS_DELAY) {
      setLiked(!likes);
    } else {
      setLastTap(now);
    }
  };

  const [modalVisible, setModalVisible] = useState(false);

  const openModal = (modal) => {
    setModalVisible(modal);
  };

  const renderModal = () => {
    return (
      <View>
        <Modal
          isVisible={modalVisible === 'roomtype'}
          onSwipeComplete={() => setModalVisible(false)}
          swipeDirection={['down']}
          style={styles.bottomModal}>
          <View style={styles.contain, { backgroundColor: colors.neoThemebg }}>
            <View style={styles.contentSwipeDown}>
              <View style={styles.lineSwipeDown} />
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-around", top: -60 }}>
              <View style={{ flexDirection: "column" }}>
                <View style={{ borderRadius: 30, borderWidth: 1, borderColor: colors.text, height: 60, width: 60 }}>
                  <Icon name="link-variant" size={25} color={colors.text} style={{ alignSelf: "center", top: 18 }} />
                </View>
                <Text style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", fontWeight: "bold", alignSelf: "center" }}>Link</Text>
              </View>
              <View style={{ flexDirection: "column" }}>
                <View style={{ borderRadius: 30, borderWidth: 1, borderColor: colors.text, height: 60, width: 60 }}>
                  <Icon name="share-variant" size={25} color={colors.text} style={{ alignSelf: "center", top: 18 }} />
                </View>
                <Text style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", fontWeight: "bold", alignSelf: "center" }}>Link</Text>
              </View>
              <View style={{ flexDirection: "column" }}>
                <View style={{ borderRadius: 30, borderWidth: 1, borderColor: "pink", height: 60, width: 60 }}>
                  <Icon name="comment-alert" size={25} color="red" style={{ alignSelf: "center", top: 18 }} />
                </View>
                <Text style={{ fontSize: 20, color: "red", fontFamily: "ProximaNova", fontWeight: "bold", alignSelf: "center" }}>Report</Text>
              </View>
            </View>
            <View
              style={{
                height: 1,
                width: 355,
                left: 20,
                top: -40,
                backgroundColor: colors.border,
              }}></View>

            <View style={{ flexDirection: "column", alignItems: "flex-start", left: 10, margin: 10, top: -60 }}>
              <Text style={{ top: Utils.scaleWithPixel(15), fontSize: 20, fontFamily: "ProximaNova" }}>Why you're seeing this post</Text>
              <Text style={{ top: Utils.scaleWithPixel(25), fontSize: 20, fontFamily: "ProximaNova" }}>Hide</Text>
              <Text style={{ top: Utils.scaleWithPixel(35), fontSize: 20, fontFamily: "ProximaNova" }}>About this Account</Text>
              <Text style={{ top: Utils.scaleWithPixel(45), fontSize: 20, fontFamily: "ProximaNova" }}>Unfollow</Text>

            </View>
          </View>
        </Modal>
      </View>

    )
  }

  const success = async () => {
    // console.log("feedDataObject", feedDataObject)
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('Comments', { feedDataObject })
    setLoading(false);

  };


  const [likelistId, setLikelistId] = useState("")
  const [likes, setLiked] = useState(false);


  useEffect(() => {
    const shorted = _.find(likesdataFromUser, function (o) { return o.likeFeed?.id === feedDataObject.id; });
    if (shorted) {
      setLikelistId(shorted.id);
      setLiked(true)
    }
  }, []);

  useEffect(() => {
    const shorted = _.find(likesdataFromUser, function (o) { return o.likeFeed?.id === feedDataObject.id; });
    if (shorted) {
      setLikelistId(shorted.id);
      setLiked(true)
    }
  }, [likesdataFromUser]);


  const saveLike = async () => {
    if (likes) {
      setLiked(false);
      setLikeCount(likeCount - 1)
      setLoading(true);
      await dispatch(likesDeleteRequest(likelistId));
      await dispatch(likesGetRequest());
      await dispatch(likesGetRequestForFeed(userId, feedId))
    } else {
      setLiked(true)
      let objectValue = {};
      objectValue.likeFeed = feedId;
      objectValue.likedBy = userId;
      objectValue.postType = 'feed'
      setLoading(true);
      await dispatch(likesPostRequest(objectValue));
      await dispatch(likesGetRequestForFeed(userId, feedId));
      setLikeCount(likeCount + 1)
    }



  }


  //stateyards code

  const [shortlist, setShortlist] = useState(false)
  const [shortlistId, setShortlistId] = useState("")


  useEffect(() => {
    // console.log("hellooooo use eff")
    const shorted = _.find(bookmarkdataFromUser, function (o) { return o.savedFeed?.id === feedDataObject.id; });
    // console.log("shorted", shorted);
    if (shorted) {
      setShortlistId(shorted.id);
      setShortlist(true)
    }
  }, []);

  useEffect(() => {
    // console.log("hellooooo use ssseff")
    const shorted = _.find(bookmarkdataFromUser, function (o) { return o.savedFeed?.id === feedDataObject.id; });
    if (shorted) {
      setShortlistId(shorted.id);
      setShortlist(true)
    }
  }, [bookmarkdataFromUser]);


  const hitShotList = async () => {

    if (shortlist) {
      // console.log("shortlistIdshortlistIdshortlistId", shortlistId)
      setShortlist(!shortlist)

      await dispatch(bookmarksDeleteRequest(shortlistId));
    } else {
      setShortlist(!shortlist)

      // console.log("bookmark post hit", feedId);
      const objectValue = {};
      objectValue.addToBookmark = true;
      objectValue.savedFeed = feedId;
      objectValue.bookmarkedBy = userId;
      objectValue.postType = "feed"
      // console.log("bookmark post hit ----", objectValue);
      await dispatch(bookmarksPostRequest(objectValue));
    }
    await dispatch(bookmarksGetRequestForFeed(userId));



  }
  //stateyrdscode


  return (
    <View>
      {renderModal()}
      <Neomorph
        outer
        style={{
          shadowRadius: Utils.scaleWithPixel(7),
          borderRadius: Utils.scaleWithPixel(20),
          backgroundColor: colors.neoThemebg,
          width: Utils.scaleWithPixel(310),
          height: Utils.scaleWithPixel(350),
          // justifyContent: 'center',
          // alignItems: 'center',
          marginLeft: Utils.scaleWithPixel(8),
          margin: Utils.scaleWithPixel(10)
        }}
      >
        <View style={{ marginTop: -5 }}>
          {children}
          <TouchableOpacity onPress={onPress} activeOpacity={0.9}>

            {/* <SwiperFlatList
                
                data={feedDataObject}
                index={0}
                // autoplay={true}
                showPagination
                renderItem={({ item }) => (
                    <View style={styles.slide}> */}
            <TouchableWithoutFeedback onPress={() => handleDoubleTap()}>
              <Image source={{ uri: feedImage ? feedImage : '' }} style={styles.img} />
            </TouchableWithoutFeedback>
            {/* </View> */}
            {/* )}
            /> */}

            {/* <Image style={styles.imagePost} source={{ uri: image ? image : '' }} /> */}
            <TouchableOpacity onPress={() => openModal('roomtype')} style={{ top: -220, right: 50, alignSelf: "flex-end" }}>
              <Icon
                name="dots-horizontal"
                solid
                size={28}
                color={BaseColor.whiteColor}
                style={{ position: 'absolute' }}
              />
            </TouchableOpacity>
          </TouchableOpacity>
          <View style={[styles.content, { borderBottomColor: colors.border }]}>
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity onPress={() => saveLike()}>
                {likes ? (<Icon name="heart" size={28} style={{ color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(-5) }} />)
                  : (<Icon name="heart-outline" size={28} style={{ color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(-5) }} />)}

              </TouchableOpacity>
              <TouchableOpacity onPress={() => success()}>
                <Icon name="message-text-outline" size={28} style={{ color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(10) }} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => onShare()}>
                <Icon name="share-outline" size={28} style={{ color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(10) }} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => hitShotList()} >
              {shortlist ? (<Icon
                name="bookmark"
                solid
                size={28}
                color={BaseColor.whiteColor}
                style={{ position: 'absolute', alignSelf: "flex-end", top: Utils.scaleWithPixel(-25) }}
              />) : (<Icon
                name="bookmark-outline"
                solid
                size={28}
                color={BaseColor.whiteColor}
                style={{ position: 'absolute', alignSelf: "flex-end", top: Utils.scaleWithPixel(-25) }}
              />)}

            </TouchableOpacity>
            <View>
              {likeCount == 0 ? null :
                (likeCount > 1 ? (
                  <Text headline bold style={{ marginTop: 5, fontFamily: "ProximaNova", alignSelf: "flex-start" }}>
                    {likeCount} likes
                  </Text>
                ) : (
                  <Text headline bold style={{ marginTop: 5, fontFamily: "ProximaNova", alignSelf: "flex-start" }}>
                    {likeCount} like
                  </Text>
                ))}

              <Text headline bold style={{ marginBottom: Utils.scaleWithPixel(6), marginTop: Utils.scaleWithPixel(5), fontFamily: "ProximaNova" }}>
                {title}
              </Text>

              <Text style={{ fontFamily: "ProximaNova", fontSize: 18 }} numberOfLines={1} body2>{description}</Text>

              <View style={{ marginTop: 10 }}>
                {commentscount == 0 ? (
                  <Text bold style={{ fontFamily: "ProximaNova", fontSize: 18 }} body2>No Comments</Text>)
                  : (commentscount > 1 ? (
                    <TouchableOpacity onPress={() => success()}>
                      <Text bold style={{ fontFamily: "ProximaNova", fontSize: 18 }} body2>View all {commentscount} comments</Text>
                    </TouchableOpacity>
                  ) :
                    (
                      <TouchableOpacity onPress={() => success()}>
                        <Text bold style={{ fontFamily: "ProximaNova", fontSize: 18 }} body2>{commentscount} comment</Text>
                      </TouchableOpacity>)
                  )}
              </View>
              <Text style={{ fontFamily: "ProximaNova", fontSize: 16, marginTop: 5 }} body2>{time}</Text>
            </View>
          </View>
        </View>
      </Neomorph>

    </View>
  );
}

PostItem.propTypes = {
  image: PropTypes.node.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  title: PropTypes.string,
  description: PropTypes.string,
  onPress: PropTypes.func,
};

PostItem.defaultProps = {
  image: '',
  title: '',
  description: '',
  style: {},
  onPress: () => { },
};
