import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    flexDirection: 'row',
    marginEnd:Utils.scaleWithPixel(-40),
    paddingTop: Utils.scaleWithPixel(5),
    paddingBottom: Utils.scaleWithPixel(5),
  },
  thumb: {width: Utils.scaleWithPixel(50), height: Utils.scaleWithPixel(50), borderRadius: Utils.scaleWithPixel(50), marginLeft:Utils.scaleWithPixel(0), marginTop:Utils.scaleWithPixel(0)},
  content: {
    flex: 1,
    flexDirection: 'row',
  },
  left: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    top:Utils.scaleWithPixel(20),
    left:Utils.scaleWithPixel(-65)
  },
  right: {
    flex: 2.5,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  content1: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: Utils.scaleWithPixel(0),
    // left:Utils.scaleWithPixel(10),
    // padding: Utils.scaleWithPixel(10),
  },
});
