import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import { ScrollView } from 'react-native-gesture-handler';
import * as Utils from '@utils';


export default function MessengerCircle(props) {
  const { colors } = useTheme();
  const {
    style,
    imageStyle,
    image,
    txtLeftTitle,
    txtContent,
    txtRight,
    children,
    onPress,
    styleContent,
  } = props;
  return (
    <Neomorph
      style={{
        shadowRadius: Utils.scaleWithPixel(3),
        borderRadius: Utils.scaleWithPixel(100),
        // backgroundColor: colors.card,
        backgroundColor: colors.neoThemebg,
        width: Utils.scaleWithPixel(60),
        height: Utils.scaleWithPixel(60),
        justifyContent: 'center',
        alignItems: 'center',
        top: Utils.scaleWithPixel(10),
        left:Utils.scaleWithPixel(10),
        margin:Utils.scaleWithPixel(5) 
      }}
    >
      <Neomorph
        style={{
          width: Utils.scaleWithPixel(50),
          height: Utils.scaleWithPixel(50),
          borderRadius: Utils.scaleWithPixel(100),
          // borderWidth: 2,
          borderColor: colors.neoThemeBorderColor,
          backgroundColor: colors.neoThemebg,
          justifyContent: 'center',
          alignItems: 'center',

        }}
      >
        <TouchableOpacity
          style={[
            styles.contain,
            // {borderBottomWidth: 1, borderBottomColor: colors.border},
            style,
          ]}
          onPress={onPress}
          activeOpacity={0.9}>
          <Image source={{uri: image ? image : ''}} style={[styles.thumb, imageStyle]} />
          <View style={styles.content}>
            <View style={styles.left}>
            
              <Text
                note
                numberOfLines={1}
                footnote
                grayColor
                style={{
                  paddingTop: Utils.scaleWithPixel(5),
                  fontFamily:"Proxima-Nova"
                }}>
                {txtContent}
              </Text>
            </View>
          </View>
          <View style={[styles.content1, styleContent]}>{children}</View>
        </TouchableOpacity>
      </Neomorph>
    </Neomorph>

  );
}

MessengerCircle.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  imageStyle: PropTypes.object,
  image: PropTypes.node.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  txtContent: PropTypes.string,
  txtRight: PropTypes.string,
  onPress: PropTypes.func,
};

MessengerCircle.defaultProps = {
  style: {},
  imageStyle: {},
  image: '',
  txtLeftTitle: '',
  txtContent: '',
  txtRight: '',
  styleContent: {},
  onPress: () => { },
};
