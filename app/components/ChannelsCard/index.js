import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Image, Icon, Text } from '@components';
import { Images, useTheme,BaseColor } from '@config';
import { Neomorph,NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
// import { Icon } from 'react-native-elements/dist/icons/Icon';

export default function ChannelsCard(props) {
    const { colors } = useTheme();
    const { style, children, styleContent, image, onPress, time } = props;
    const [shouldShowFollow, setShouldShowFollow] = useState(true);
    return (
        <View style={{ flexDirection: "column", height: Utils.scaleWithPixel(250) }}>

            <TouchableOpacity
                style={[styles.card, { borderColor: colors.border }, style]}
                onPress={onPress}
                activeOpacity={0.9}>
                <Neomorph
                    outer
                    style={{
                        shadowRadius: Utils.scaleWithPixel(7),
                        borderRadius: Utils.scaleWithPixel(20),
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(140),
                        height: Utils.scaleWithPixel(220),
                        justifyContent: 'center',
                        alignItems: 'center',
                        left: Utils.scaleWithPixel(-10),
                        margin: Utils.scaleWithPixel(0),
                        marginBottom:Utils.scaleWithPixel(10)
                    }}
                >
                    {/* <Neomorph
                        inner
                        style={{
                            shadowRadius: 7,
                            borderRadius: 70,
                            backgroundColor: colors.neoThemebg,
                            width: 65,
                            height: 65,
                            justifyContent: 'center',
                            alignItems: 'center',
                            top: -30
                        }}
                    > */}
                        <Image source={{uri:image ? image : ''}} style={{height:Utils.scaleWithPixel(80),width:Utils.scaleWithPixel(120),borderRadius:Utils.scaleWithPixel(20),top:Utils.scaleWithPixel(-25)}} />
                    {/* </Neomorph> */}
                    <View style={{top:Utils.scaleWithPixel(40)}}>
                        {shouldShowFollow ? (
                            <Neomorph
                                outer
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(20),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(125),
                                    height: Utils.scaleWithPixel(35),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    top:5,
                                    margin: Utils.scaleWithPixel(10)
                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => setShouldShowFollow(!shouldShowFollow)}>
                                    <Text bold style={{ fontSize: 20, color: "#e3dce1" }}>Follow</Text>
                                </TouchableOpacity>
                            </Neomorph>) : (<Neomorph
                                inner
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(20),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(125),
                                    height: Utils.scaleWithPixel(35),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    top:5,
                                    margin: Utils.scaleWithPixel(10)
                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => setShouldShowFollow(!shouldShowFollow)}>
                                   <View style={{flexDirection:"row"}}>
                                        <Text bold style={{ fontSize: 20, color: BaseColor.blueColor }}>Following </Text>
                                        <Icon name="check-circle-outline" size={20} color={BaseColor.blueColor} style={{alignSelf:"center",top:2}} />
                                    </View>
                                </TouchableOpacity>
                            </Neomorph>)}
                    </View>
                </Neomorph>

            </TouchableOpacity>

            <View style={[styles.content, styleContent]}>{children}</View>
        </View>
    );
}

ChannelsCard.propTypes = {
    image: PropTypes.node.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleContent: PropTypes.object,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element),
    ]),
    onPress: PropTypes.func,
};

ChannelsCard.defaultProps = {
    image: Images.profile2,
    style: {},
    styleContent: {},
    onPress: () => { },
};
