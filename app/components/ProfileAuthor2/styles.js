import React from 'react';
import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    flexDirection: 'row',
    paddingVertical: Utils.scaleWithPixel(10),
  },
  contentLeft: {
    flex: 8,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  thumb: {
    width: Utils.scaleWithPixel(50),
    height: Utils.scaleWithPixel(50),
    borderWidth:Utils.scaleWithPixel(0.4),
    borderColor:"#DDDDDD",
    left:Utils.scaleWithPixel(2),
    top:Utils.scaleWithPixel(0),
    borderRadius: Utils.scaleWithPixel(40),
    marginRight: Utils.scaleWithPixel(5),
  },
  contentRight: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
});