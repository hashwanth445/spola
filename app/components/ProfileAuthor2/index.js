import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text } from '@components';
import styles from './styles';
import { BaseStyle, useTheme } from '@config';
import PropTypes from 'prop-types';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';

export default function ProfileAuthor2(props) {
  const {
    style,
    image,
    styleLeft,
    styleThumb,
    styleRight,
    onPress,
    name,
    description,
    textRight,
  } = props;
  const { colors } = useTheme();
  return (
    <TouchableOpacity
      style={[styles.contain, style]}
      onPress={onPress}
      activeOpacity={0.9}>
      <View style={[styles.contentLeft, styleLeft]}>
        <Neomorph
          inner
          style={{
            shadowRadius: Utils.scaleWithPixel(7),
            borderRadius: Utils.scaleWithPixel(100),
            backgroundColor: colors.neoThemebg,
            width: Utils.scaleWithPixel(68),
            height: Utils.scaleWithPixel(68),
            left: -5,
            justifyContent: 'center',
            alignItems: 'center',

          }}
        >
          <Image source={{ uri: image ? image : '' }} style={[styles.thumb, styleThumb]} />
        </Neomorph>
        <View>
          {/* <Text headline semibold numberOfLines={1}>
              {name}
            </Text> */}
          <Text footnote grayColor numberOfLines={1}>
            {description}
          </Text>
        </View>
      </View>
      <View style={[styles.contentRight, styleRight]}>
        <Text caption2 grayColor numberOfLines={1}>
          {textRight}
        </Text>
      </View>
    </TouchableOpacity>

  );
}

ProfileAuthor2.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  image: PropTypes.node.isRequired,
  name: PropTypes.string,
  description: PropTypes.string,
  textRight: PropTypes.string,
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

ProfileAuthor2.defaultProps = {
  image: '',
  name: '',
  description: '',  textRight: '',  styleLeft: {},
  styleThumb: {},
  styleRight: {},
  style: {},
  onPress: () => { },
};
