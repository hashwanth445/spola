import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Image } from '@components';
import { Images, useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';

export default function TrophyCard(props) {
    const { colors } = useTheme();
    const { style, children, styleContent, image, onPress } = props;
    return (
        <View style={{ flexDirection: "column" }}>

            <View style={{ flexDirection: "row", top: -10 }}>
                <Neomorph
                    outer
                    style={{
                        padding: 20,
                        shadowRadius: 7,
                        borderRadius: 20,
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(300),
                        height: Utils.scaleWithPixel(100),
                        justifyContent: 'center',
                        alignItems: 'center',
                        left: 5,
                        margin:10,
                        top: 10,
                    }}
                >
                    {/* <Image source={Images.car1} style={{ height: 140, width: 140, borderRadius: 20 }} /> */}
                </Neomorph>
                {/* <Neomorph
                    outer
                    style={{
                        shadowRadius: 7,
                        borderRadius: 20,
                        backgroundColor: colors.neoThemebg,
                        width: 150,
                        height: 150,
                        justifyContent: 'center',
                        alignItems: 'center',
                        margin: 10,
                        top: 10
                    }}
                >
                    <Image source={Images.car2} style={{ height: 140, width: 140, borderRadius: 20 }} />
                </Neomorph> */}
            </View>


        </View>
    );
}

TrophyCard.propTypes = {
    image: PropTypes.node.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleContent: PropTypes.object,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element),
    ]),
    onPress: PropTypes.func,
};

TrophyCard.defaultProps = {
    image: Images.profile2,
    style: {},
    styleContent: {},
    onPress: () => { },
};
