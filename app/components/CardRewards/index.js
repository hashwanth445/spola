import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Image } from '@components';
import { Images, useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';

export default function CardRewards(props) {
  const { colors } = useTheme();
  const { style, children, styleContent, image, onPress } = props;
  return (
    <View style={{ flexDirection: "column" }}>

      <View style={{ flexDirection: "row", top: Utils.scaleWithPixel(-20),left:Utils.scaleWithPixel(0) }}>
        <Neomorph
          outer
          style={{
            shadowRadius: Utils.scaleWithPixel(7),
            borderRadius: Utils.scaleWithPixel(20),
            backgroundColor: colors.neoThemebg,
            width: Utils.scaleWithPixel(120),
            height: Utils.scaleWithPixel(150),
            justifyContent: 'center',
            alignItems: 'center',
            margin: Utils.scaleWithPixel(10),
            top: Utils.scaleWithPixel(10)
          }}
        >
          <Image source={image} style={{ height: Utils.scaleWithPixel(140), width: Utils.scaleWithPixel(110),borderRadius:Utils.scaleWithPixel(20)}} />
        </Neomorph>
        {/* <Neomorph
          outer
          style={{
            shadowRadius: 7,
            borderRadius: 20,
            backgroundColor: colors.neoThemebg,
            width: 155,
            height: 170,
            justifyContent: 'center',
            alignItems: 'center',
            margin: 10,
            top: 10
          }}
        >
          <Image source={image} style={{ height: 175, width: 150,borderRadius:20 }} />
        </Neomorph> */}
      </View>


    </View>
  );
}

CardRewards.propTypes = {
  image: PropTypes.node.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleContent: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  onPress: PropTypes.func,
};

CardRewards.defaultProps = {
  image: Images.profile2,
  style: {},
  styleContent: {},
  onPress: () => { },
};
