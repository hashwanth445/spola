import {StyleSheet} from 'react-native';
import * as Utils from '@utils';


export default StyleSheet.create({
  imageBanner: {flex: 1, borderRadius: Utils.scaleWithPixel(8),},
  card: {
    width: '100%',
    height: '100%',
    borderRadius: Utils.scaleWithPixel(0),
    marginTop:Utils.scaleWithPixel(25),
  
  },
  content: {
    position: 'absolute',
    alignItems: 'flex-start',
    bottom: Utils.scaleWithPixel(0),
    padding: Utils.scaleWithPixel(10),
  },
});
