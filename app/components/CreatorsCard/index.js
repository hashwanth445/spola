import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Image, Icon, Text } from '@components';
import { Images, useTheme } from '@config';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import { BaseColor } from '../../config/theme';
;
// import { Icon } from 'react-native-elements/dist/icons/Icon';

export default function CreatorsCard(props) {
    const { colors } = useTheme();
    const { style, children, styleContent, image, onPress, time,creatorObject } = props;
    console.log("id",creatorObject);
    const [shouldShowFollow, setShouldShowFollow] = useState(true);
    const [selectedFallowing, setSelectedFollowing] = useState([]);
    const [loading, setLoading] = useState(false);

    const selectFollowers = async data => {
        console.log(data)
        let selectedFollowers = selectedFallowing;
        selectedFollowers.push({ "_id": creatorObject ? creatorObject : "" });
        setSelectedFollowing(selectedFollowers);
        setShouldShowFollow(!shouldShowFollow)
    };

    return (
        <View style={{ flexDirection: "column", height: Utils.scaleWithPixel(250) }}>

            <TouchableOpacity
                style={[styles.card, { borderColor: colors.border }, style]}
                onPress={onPress}
                activeOpacity={0.9}>
                <Neomorph
                    outer
                    style={{
                        shadowRadius: Utils.scaleWithPixel(7),
                        borderRadius: Utils.scaleWithPixel(20),
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(130),
                        height: Utils.scaleWithPixel(175),
                        justifyContent: 'center',
                        alignItems: 'center',
                        left: Utils.scaleWithPixel(0),
                        margin: Utils.scaleWithPixel(0),
                        marginBottom: Utils.scaleWithPixel(10)
                    }}
                >
                    {/* <Neomorph
                        inner
                        style={{
                            shadowRadius: 7,
                            borderRadius: 70,
                            backgroundColor: colors.neoThemebg,
                            width: 65,
                            height: 65,
                            justifyContent: 'center',
                            alignItems: 'center',
                            top: -30
                        }}
                    > */}
                    {/* <Image source={image} style={styles.imageBanner} /> */}
                    {/* </Neomorph> */}
                    <View style={{ top: 60 }}>
                        {shouldShowFollow ? (
                            <Neomorph
                                outer
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(20),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(115),
                                    height: Utils.scaleWithPixel(35),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    margin: Utils.scaleWithPixel(10),
                                    top: Utils.scaleWithPixel(10)
                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => setShouldShowFollow(!shouldShowFollow, children)}>
                                    <Text bold style={{ fontSize: 20, color: "#e3dce1" }}>Follow</Text>
                                </TouchableOpacity>
                            </Neomorph>) : (<Neomorph
                                inner
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(20),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(115),
                                    height: Utils.scaleWithPixel(35),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    margin: Utils.scaleWithPixel(10),
                                    top: Utils.scaleWithPixel(10)
                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => selectFollowers()}
                                >
                                    <View style={{flexDirection:"row"}}>
                                        <Text bold style={{ fontSize: 20, color: BaseColor.blueColor }}>Following </Text>
                                        <Icon name="check-circle-outline" size={20} color={BaseColor.blueColor} style={{alignSelf:"center",top:2}} />
                                    </View>
                                </TouchableOpacity>
                            </Neomorph>)}
                    </View>
                </Neomorph>

            </TouchableOpacity>

            <View style={[styles.content, styleContent]}>{children}</View>
        </View>
    );
}

CreatorsCard.propTypes = {
    image: PropTypes.node.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    styleContent: PropTypes.object,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element),
    ]),
    onPress: PropTypes.func,
};

CreatorsCard.defaultProps = {
    image: Images.profile2,
    style: {},
    styleContent: {},
    onPress: () => { },
};
