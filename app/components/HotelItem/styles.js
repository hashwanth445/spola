import {StyleSheet} from 'react-native';
import * as Utils from '@utils';
import {BaseColor} from '@config';

export default StyleSheet.create({
  //block css
  blockImage: {
    height: Utils.scaleWithPixel(200),
    width: '100%',
  },
  blockContentAddress: {
    flexDirection: 'row',
    marginTop: Utils.scaleWithPixel(3),
    alignItems: 'center',
  },
  blockContentDetail: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginTop: Utils.scaleWithPixel(10),
  },
  blockListContentIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: Utils.scaleWithPixel(50),
    width: '100%',
    marginTop: Utils.scaleWithPixel(4),
  },
  contentService: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Utils.scaleWithPixel(10),
  },
  serviceItemBlock: {
    alignItems: 'center',
    justifyContent: 'center',

    width: Utils.scaleWithPixel(60),
  },
  //list css
  listImage: {
    height: Utils.scaleWithPixel(140),
    width: Utils.scaleWithPixel(120),
    borderRadius: Utils.scaleWithPixel(8),
  },
  listContent: {
    flexDirection: 'row',
  },
  listContentRight: {
    paddingHorizontal: Utils.scaleWithPixel(10),
    paddingVertical: Utils.scaleWithPixel(2),
    flex: 1,
  },
  listContentRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: Utils.scaleWithPixel(5),
  },
  //gird css
  girdImage: {
    borderRadius: Utils.scaleWithPixel(8),
    height: Utils.scaleWithPixel(120),
    width: '100%',
  },
  girdContent: {
    flex: 1,
  },
  girdContentLocation: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: Utils.scaleWithPixel(5),
  },
  girdContentRate: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Utils.scaleWithPixel(10),
  },
});
