import {StyleSheet} from 'react-native';
import * as Utils from '@utils';


export default StyleSheet.create({
  contain: {
    flexDirection: 'row',
    borderColor:"#ffffff",
    borderWidth:Utils.scaleWithPixel(0.2),
    height:Utils.scaleWithPixel(58),
    width:Utils.scaleWithPixel(290),
    left:Utils.scaleWithPixel(45),
    borderRadius:Utils.scaleWithPixel(9)
  },
  contentLeft: {
    flex: 1,
    alignItems: "center",
    justifyContent: 'center',
    marginLeft: Utils.scaleWithPixel(20)
  },
  contentCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:Utils.scaleWithPixel(0)
  },
  contentRight: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginRight: Utils.scaleWithPixel(20)
  },
  itemInfor: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});
