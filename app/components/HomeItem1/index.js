import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text, Icon } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import * as Utils from '@utils';
import { BaseColor, useTheme } from '@config';
import { Neomorph } from 'react-native-neomorph-shadows';
import { useNavigation } from '@react-navigation/native';

export default function HomeItem1(props) {
  const { colors } = useTheme();
  const [liked, setLiked] = useState(liked);
  const [saved, setSaved] = useState(saved);
  const navigation = useNavigation();
  const { style, children, title, description, onPress, image, styleRight, time } = props;
  return (

    <View style={{ margin: Utils.scaleWithPixel(-30), left: Utils.scaleWithPixel(30), marginBottom: Utils.scaleWithPixel(-20) }}>
      {children}
      
      <TouchableOpacity onPress={onPress} activeOpacity={0.9}>
        <Image style={styles.imagePost} source={{ uri: image ? image : '' }} />
        {/* <Icon
            name="dots-horizontal"
            solid
            size={28}
            color={BaseColor.whiteColor}
            style={{ position: 'absolute', top: -40, right: 20 }}
          /> */}

      </TouchableOpacity>
      <View style={[styles.content, { borderBottomColor: colors.border }]}>
        {/* <Icon name="youtube" color="red" size={32} style={{ top: Utils.scaleWithPixel(-70), left: Utils.scaleWithPixel(160) }} /> */}
        <View style={{alignSelf:"flex-end",right:120, top: Utils.scaleWithPixel(-130), borderWidth: Utils.scaleWithPixel(1), borderColor: "#999999",width: Utils.scaleWithPixel(30), borderRadius: Utils.scaleWithPixel(5), backgroundColor: "#000000", }}>
          <Text body2 style={{ fontSize: 14,  fontFamily: "ProximaNova",alignSelf:"center" }}>
            {time}
          </Text>
        </View>
        <Text  style={{ left: Utils.scaleWithPixel(-10), top: Utils.scaleWithPixel(-50), fontWeight: "bold", fontFamily: "Proxima-Nova" }}>
          {title}
        </Text>
        <Text body2 style={{ left: Utils.scaleWithPixel(-10), top: Utils.scaleWithPixel(-40), fontFamily: "Proxima-Nova" }}>
          {description}
        </Text>
      </View>
    </View>
  );
}

HomeItem1.propTypes = {
  image: PropTypes.node.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  title: PropTypes.string,
  description: PropTypes.string,
  onPress: PropTypes.func,
};

HomeItem1.defaultProps = {
  image: '',
  title: '',
  description: '',
  style: {},
  onPress: () => { },
};
