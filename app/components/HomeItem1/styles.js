import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';
import { marginBottom } from 'styled-system';

export default StyleSheet.create({
  imagePost: {width: Utils.scaleWithPixel(310), height: Utils.scaleWithPixel(150),borderRadius:Utils.scaleWithPixel(10),margin:Utils.scaleWithPixel(10)},
  content: {
    // marginHorizontal: 20,
    // paddingVertical: 10,
    // borderBottomWidth: 1,
    margin:Utils.scaleWithPixel(-20),
    marginBottom:10
  },
  contentRight: {
    padding: Utils.scaleWithPixel(8),
    justifyContent: 'center',
    alignItems: 'flex-end',
    
  },
});
