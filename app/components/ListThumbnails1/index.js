import React, { useState } from 'react';
import { View, TouchableOpacity, ScrollView } from 'react-native';
import { Image, Text, Icon } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { useTheme } from '@config';
import * as Utils from '@utils';
import { Neomorph } from 'react-native-neomorph-shadows';
// import { Icon } from 'react-native-elements';

export default function ListThumbnails1(props) {
  const { colors } = useTheme();
  const [secure, setSecure] = useState(secure);
  const {
    style,
    imageStyle,
    image,
    txtLeftTitle,
    txtContent,
    txtRight,
    onPress,
    creatorsObj
  } = props;
  console.log("creatorsObj", creatorsObj);
  const [selectedUser, setSelectedUser] = useState([])
  const [state, setState] = useState({
    selectedIndex: [],
    index: 0,
  })
  const { selectedIndex, index } = state;

  const selectedItem = (id) => {

    if (selectedIndex == 0) {
      setState({ selectedIndex: [id], index: 1 });
      setSecure(true);

    } else {
      //alert(selectedIndex)
      if (selectedIndex.includes(id) === false) {
        setState({ selectedIndex: [...selectedIndex, id], index: index + 1 });
        setSecure(false);
      }
    }
  };

  // const selectUsers = async data => {
  //   let selectedUsers = selectedUser;
  //   const found = selectedUsers.find(element => element === data);
  //   const foundIndex = selectedUsers.findIndex(element => element === data);
  //   console.log('selectedUser', selectedUsers);

  //   if (found) {
  //     selectedUsers.splice(foundIndex, 1);
  //     setSelectedUser(selectedUsers);
  //     setSecure(true);
  //   } else {
  //     selectedUsers.push({ "_id": data });
  //     setSelectedUser(selectedUsers);
  //     setSecure(false);
  //   }

  // };
  return (
    <ScrollView>
      <TouchableOpacity
        style={[styles.item, style]}
        onPress={onPress}
        activeOpacity={0.9}>

        <View style={[styles.contain, { borderBottomColor: colors.border }]}>

          <Image source={{ uri: image ? image : '' }} style={[styles.thumb, imageStyle]} />
          <Icon name={secure ? "check-circle" : "plus-circle"}
            onPress={() => selectedItem()} size={20} color="white" style={{ color: secure ? 'dodgerblue' : 'white', left: Utils.scaleWithPixel(-20), margin: Utils.scaleWithPixel(-10), top: Utils.scaleWithPixel(10) }} />
        </View>

      </TouchableOpacity >
    </ScrollView>
  );
}

ListThumbnails1.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  imageStyle: PropTypes.object,
  image: PropTypes.node.isRequired,
  txtLeftTitle: PropTypes.string,
  txtContent: PropTypes.string,
  txtRight: PropTypes.string,
  onPress: PropTypes.func,
};

ListThumbnails1.defaultProps = {
  style: {},
  imageStyle: {},
  image: '',
  txtLeftTitle: '',
  txtContent: '',
  txtRight: '',
  onPress: () => { },
};
