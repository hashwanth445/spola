import React,{useState} from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text, Icon} from '@components';
import Icon1 from 'react-native-vector-icons/Ionicons';
import { useDispatch, useSelector } from 'react-redux';
import styles from './styles';
import PropTypes from 'prop-types';
import { useTheme } from '@config';
import * as Utils from '@utils';
import { Neomorph } from 'react-native-neomorph-shadows';
import { format, render, cancel, register } from 'timeago.js';

export default function ListThumbSquare(props) {
  const { colors } = useTheme();
  const {
    style,
    imageStyle,
    image,
    txtLeftTitle,
    txtContent,
    txtRight,
    onPress,
    messageObject
  } = props;
  console.log("messageObject",messageObject);
  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const messageToUser = messageObject?.messageToUser
  const messageFromUser = messageObject?.messageFromUser
  return (
    <TouchableOpacity
    style={[styles.item, style]}
    onPress={onPress}
    activeOpacity={0.9}>
    {messageToUser?.username == UserData?.user?.username ? (
      <Neomorph
        outer
        style={{
          shadowRadius: Utils.scaleWithPixel(7),
          borderRadius: Utils.scaleWithPixel(20),
          backgroundColor: colors.neoThemebg,
          width: Utils.scaleWithPixel(300),
          height: Utils.scaleWithPixel(60),
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: Utils.scaleWithPixel(-10),
          marginTop: Utils.scaleWithPixel(10),
          
        }}
      >
        <View style={[styles.contain, { borderBottomColor: colors.border }]}>
        {messageToUser?.username !== UserData?.user?.username ? (
          <Image source={{uri: image ? image : ''}} style={[styles.thumb, imageStyle]} />) : (
            <Image source={{uri: messageFromUser?.backBlazeImageUrl ? messageFromUser?.backBlazeImageUrl : ''}} style={[styles.thumb, imageStyle]} />
          )}
          <View style={styles.content}>
            <View style={styles.left}>
              {messageToUser?.username !== UserData?.user?.username ? (

              <Text bold style={{fontFamily:"ProximaNova"}}>
                {txtLeftTitle}
              </Text>
              ) : (
                <Text bold style={{fontFamily:"ProximaNova"}}>
                {messageFromUser?.username}
              </Text>
              )}
              <Text
                numberOfLines={1}
                footnote
                grayColor
                style={{
                  paddingTop: Utils.scaleWithPixel(5),
                  fontFamily:"ProximaNova"
                }}>
                {txtContent}
              </Text>
            </View>
            <View style={styles.right}>
              <Text caption2 grayColor numberOfLines={1} style={{fontFamily:"ProximaNova"}}>
              {format(txtRight)}
              </Text>
              {/* <Icon1 name="checkmark-done-outline" size={20} color="green" style={{top:15}} /> */}
            </View>
          </View>
        </View>
      </Neomorph>
    ) : (
      <Neomorph
        outer
        style={{
          shadowRadius: Utils.scaleWithPixel(7),
          borderRadius: Utils.scaleWithPixel(20),
          backgroundColor: colors.neoThemebg,
          width: Utils.scaleWithPixel(300),
          height: Utils.scaleWithPixel(60),
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: Utils.scaleWithPixel(-10),
          marginTop: Utils.scaleWithPixel(10),
          
        }}
      >
        <View style={[styles.contain, { borderBottomColor: colors.border }]}>
          <Image source={{uri: image ? image : ''}} style={[styles.thumb, imageStyle]} />
          <View style={styles.content}>
            <View style={styles.left}>
              <Text headline style={{fontFamily:"ProximaNova"}}>
                {txtLeftTitle}
              </Text>
              <Text
                numberOfLines={1}
                footnote
                grayColor
                style={{
                  paddingTop: Utils.scaleWithPixel(5),
                  fontFamily:"ProximaNova"
                }}>
                {txtContent}
              </Text>
            </View>
            <View style={styles.right}>
              <Text caption2 grayColor numberOfLines={1} style={{fontFamily:"ProximaNova"}}>
              {format(txtRight)}
              </Text>
              <Icon1 name="checkmark-done-outline" size={20} color="green" style={{top:15}} />
            </View>
          </View>
        </View>
      </Neomorph>
    )}
    </TouchableOpacity >
    
  );
}

ListThumbSquare.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  imageStyle: PropTypes.object,
  image: PropTypes.node.isRequired,
  txtLeftTitle: PropTypes.string,
  txtContent: PropTypes.string,
  txtRight: PropTypes.string,
  onPress: PropTypes.func,
};

ListThumbSquare.defaultProps = {
  style: {},
  imageStyle: {},
  image: '',
  txtLeftTitle: '',
  txtContent: '',
  txtRight: '',
  onPress: () => { },
};
