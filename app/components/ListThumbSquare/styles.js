import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  item: {
    paddingLeft: Utils.scaleWithPixel(20),
    paddingRight: Utils.scaleWithPixel(20),
    paddingTop: Utils.scaleWithPixel(5),
    paddingBottom: Utils.scaleWithPixel(5),
  },
  contain: {
    flexDirection: 'row',
    // borderBottomWidth: 1,
    paddingTop: Utils.scaleWithPixel(5),
    paddingBottom: Utils.scaleWithPixel(5),
  },
  thumb: {width: Utils.scaleWithPixel(48), height: Utils.scaleWithPixel(48), marginRight: Utils.scaleWithPixel(10), borderRadius:Utils.scaleWithPixel(35), marginLeft:Utils.scaleWithPixel(10)},
  content: {
    flex: 1,
    flexDirection: 'row',
  },
  left: {
    flex: 7.5,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  right: {
    flex: 2.5,
    alignItems: 'flex-end',
    // justifyContent: 'center',
    right:Utils.scaleWithPixel(10)
  },
});
