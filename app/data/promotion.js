import {Images} from '@config';
const PromotionData = [
  {
    id: '1',
    // image: Images.profile9,
    image: Images.video,
    title1: '200 Views',
    title2: 'Ali Latest',
    time:'3:20'
  },
  {
    id: '2',
    image: Images.photo1,
    title1: '5K Views',
    title2: 'Disha Patani',
    time:'3:20'
  },
  {
    id: '3',
    image: Images.photo7,
    title1: '500K Views',
    title2: 'Nature',
    time:'3:20'
  },
  {
    id: '4',
    image: Images.photo4,
    title1: '2K Views',
    title2: 'Sushanth',
    time:'3:20'
  },
  {
    id: '5',
    image: Images.photo5,
    title1: '10K Views',
    title2: 'Sidharth',
    time:'3:20'
  },
];

export {PromotionData};
