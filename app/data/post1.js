import {Images} from '@config';

const PostData1 = [
  {
    id: '1',
    image: Images.thumb8,
    title: 'Overseas Adventure Travel In Nepal',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo1,
    name: 'Rashmika Mandanna',
    detail: '5 hours ago | 100k views',
    date: 'Jun 2018',
    time: '4:20'
  },
  {
    id: '2',
    image: Images.thumb6,
    title: '50 Really Good Place For This Summer',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo4,
    name: 'Pavan Kalyan',
    detail: '4 hours 30 mins ago | 50 views',
    date: 'Jun 2018',
    time: '4:20'
  },
  {
    id: '3',
    image: Images.thumb5,
    title: 'Overseas Adventure Travel In Nepal',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo5,
    name: 'Ram',
    detail: '5 hours ago | 100k views',
    date: 'Jun 2018',
    time: '4:20'
  },
  {
    id: '4',
    image: Images.thumb7,
    title: '50 Really Good Place For This Summer',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo6,
    name: 'Vijay',
    detail: '4 hours 30 mins ago | 50 views',
    date: 'Jun 2018',
    time: '4:20'
  },
];

export {PostData1};
