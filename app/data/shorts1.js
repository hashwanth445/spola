import {Images} from '@config';
import {UserData} from './user';
const ShortsData1 = [
  {
    id: '1',
    // image: Images.profile9,
    image: Images.short1,
    title1: '200 Views',
    title2: '50 Really Good Place For This Summer',
    time:'4:30',
    name:'Superman | Hybrid Theory 20th tour'
  },
  {
    id: '2',
    image: Images.short2,
    title1: '5K Views',
    time:'4:30',
    title2: '50 Really Good Place For This Summer',
  },
  {
    id: '3',
    image: Images.short3,
    title1: '500K Views',
    time:'4:30',
    title2: '50 Really Good Place For This Summer',
  },
  {
    id: '4',
    image: Images.short4,
    title1: '2K Views',
    time:'4:30',
    title2: '50 Really Good Place For This Summer',
  },
  {
    id: '5',
    image: Images.short5,
    title1: '10K Views',
    time:'4:30',
    title2: '50 Really Good Place For This Summer',
  },
  {
    id: '5',
    image: Images.short6,
    title1: '110K Views',
    time:'4:30',
    title2: '50 Really Good Place For This Summer',
  },
];

export {ShortsData1};