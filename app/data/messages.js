import {Images} from '@config';

const MessagesData = [
  {
    id: '0',
    user: 'officialRajesh',
    message: 'mama get me lunch ...also get some fruites',
    image: Images.profile004,
    date: '2.30',
  },
  {
    id: '1',
    user: 'Dude',
    message: 'some changes in the plan',
    image: Images.profile007,
    date: '3.40',
  },
  {
    id: '1',
    user: 'Radhi',
    message: 'DO you have any pendrive that works with mac??',
    image: Images.profile001,
    date: '3.40',
  },
  {
    id: '2',
    user: 'SandeepK',
    message: 'get some juice while coming to office',
    image: Images.profile0010,
    date: '10.11',
  },
  {
    id: '3',
    user: 'Hashwanth M',
    message: 'Budget for this month discussions',
    image: Images.profile009,
    date: '23.11',
  },
  {
    id: '4',
    user: 'Bharath E',
    message: 'profile changes are reuired in the project. whom should I assign ',
    image: Images.profile0010,
    date: '2.54',
  },
  {
    id: '6',
    user: 'Naveen A',
    message: 'Get some milk while coming to room',
    image: Images.profile0011,
    date: 'A day ago',
  },
  
  
];

export {MessagesData};
