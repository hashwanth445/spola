import {Images} from '@config';
import {UserData} from './user';
const StatusData1 = [
  {
    id: '1',
    // image: Images.profile9,
    image: Images.profile001,
    title1: '200 Views',
    title2: 'Mounika',
  },
  {
    id: '2',
    image: Images.profile002,
    title1: '5K Views',
    title2: 'Amrutha',
  },
  {
    id: '3',
    image: Images.profile003,
    title1: '500K Views',
    title2: ' Sai Ram ',
  },
  {
    id: '4',
    image: Images.profile004,
    title1: '2K Views',
    title2: 'Sushanth',
  },
  {
    id: '5',
    image: Images.profile005,
    title1: '10K Views',
    title2: 'Sidharth',
  },
  {
    id: '6',
    image: Images.profile006,
    title1: '10K Views',
    title2: 'Akhila',
  },
];

export {StatusData1};