import {Images} from '@config';
import {UserData} from './user';
const StatusData = [
  {
    id: '1',
    // image: Images.profile9,
    image: Images.photo2,
    title1: '200 Views',
    title2: 'Mounika',
  },
  {
    id: '2',
    image: Images.photo3,
    title1: '5K Views',
    title2: 'Amrutha',
  },
  {
    id: '3',
    image: Images.photo5,
    title1: '500K Views',
    title2: ' Sai Ram ',
  },
  {
    id: '4',
    image: Images.photo6,
    title1: '2K Views',
    title2: 'Sushanth',
  },
  {
    id: '5',
    image: Images.photo7,
    title1: '10K Views',
    title2: 'Sidharth',
  },
  {
    id: '6',
    image: Images.photo3,
    title1: '10K Views',
    title2: 'Akhila',
  },
];

export {StatusData};