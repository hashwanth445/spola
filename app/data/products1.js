import {Images} from '@config';
import {UserData} from './user';
const ProductsData1 = [
  {
    id: '1',
    // image: Images.profile9,
    image: Images.proreward,
    title1: 'Extra 10% off on Digital Products',
    title2: '15 inch Tablet',
    price: '300'
  },
  {
    id: '2',
    image: Images.proreward1,
    title1: 'Extra 15% off on boAt Headsets',
    title2: 'boAt Bass Headset',
    price: '300'
  },
  {
    id: '3',
    image: Images.proreward2,
    title1: 'Extra 5% off on Nikon Cameras',
    title2: 'Nikon DSLR',
    price: '300'
  },
  {
    id: '4',
    image: Images.proreward3,
    title1: 'Extra 10% off on 1mg Medicines',
    title2: 'Medicines',
    price: '300'
  },
  {
    id: '5',
    image: Images.product2,
    title1: 'Extra 10% off on Digital Products',
    title2: 'Mobiles',
    price: '300'
  },
];

export {ProductsData1};