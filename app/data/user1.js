import {Images} from '@config';

const UserData1 = [
  {
    id: '1',
    image: Images.profile001,
    name: 'Amika Gosh',
    major: 'Travel Agency',
    email: 'lewis.victor@milford.tv',
    address: 'Hyderabad, Telangana',
    point: '3M Followers',
    id: '@amika.G',
    about:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    performance: [
      {value: '97', title: 'Following'},
      {value: '900k', title: 'Followers'},
      {value: '8.4M', title: 'Likes'},
    ],
  },
  {
    id: '2',
    image: Images.profile002,
    name: 'Bhanu Iyer',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '100k Followers',
    id: '@bhanu.iyer',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
  {
    id: '3',
    image: Images.profile003,
    name: 'Nayani Singh',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '9M Followers',
    id: '@Nayani.s',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
  {
    id: '4',
    image: Images.profile004,
    name: 'Prabhu Vittal',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '52k Followers',
    id: '@prabhu.v',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
];

export {UserData1};
