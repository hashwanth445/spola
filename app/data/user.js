import {Images} from '@config';

const userData = [
  {
    id: '1',
    image: Images.photo1,
    name: 'Meghana Pandey',
    major: 'Travel Agency',
    email: 'lewis.victor@milford.tv',
    address: 'Hyderabad, Telangana',
    point: '5M Followers',
    id: '@mounika.K',
    about:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    performance: [
      {value: '97', title: 'Following'},
      {value: '900k', title: 'Followers'},
      {value: '8000', title: 'Rewarded Points'},
    ],
  },
  {
    id: '2',
    image: Images.photo4,
    name: 'Athena alizabeth',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '90k Followers',
    id: '@steve.garrett',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
  {
    id: '3',
    image: Images.photo5,
    name: 'Alfred Richards',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '7M Followers',
    id: '@steve.garrett',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
  {
    id: '4',
    image: Images.photo6,
    name: 'Dung Joe Khim',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '950k Followers',
    id: '@steve.garrett',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
];

export {userData};
