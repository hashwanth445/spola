import {Images} from '@config';
import {UserData} from './user';
const ShortsData = [
  {
    id: '1',
    // image: Images.profile9,
    image: Images.thumb,
    title1: '200 Views',
    title2: '50 Really Good Place For This Summer',
    time:'4:30',
    name:'Superman | Hybrid Theory 20th tour'
  },
  {
    id: '2',
    image: Images.pubg,
    title1: '5K Views',
    time:'4:30',
    title2: '50 Really Good Place For This Summer',
  },
  {
    id: '3',
    image: Images.thumb8,
    title1: '500K Views',
    time:'4:30',
    title2: '50 Really Good Place For This Summer',
  },
  {
    id: '4',
    image: Images.thumb7,
    title1: '2K Views',
    time:'4:30',
    title2: '50 Really Good Place For This Summer',
  },
  {
    id: '5',
    image: Images.thumb4,
    title1: '10K Views',
    time:'4:30',
    title2: '50 Really Good Place For This Summer',
  },
];

export {ShortsData};