import {Images} from '@config';
import {UserData} from './user';
const ChannelsData = [
  {
    id: '1',
    // image: Images.profile9,
    image: Images.pubg,
    title1: '200 Views',
    title2: 'PUBG Official',
    point: '9M Followers',
    id: '@pubg.official',
  },
  {
    id: '2',
    image: Images.event6,
    title1: '5K Views',
    title2: 'Pop Concert',
    point: '9M Followers',
    id: '@pubg.official',
  },
  {
    id: '3',
    image: Images.event2,
    title1: '500K Views',
    title2: 'Car Fashion',
    point: '9M Followers',
    id: '@pubg.official',
  },
  {
    id: '4',
    image: Images.background4,
    title1: '2K Views',
    title2: 'Nature Photos',
    point: '9M Followers',
    id: '@pubg.official',
  },
 
];

export {ChannelsData};