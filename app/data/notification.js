import {Images} from '@config';

const NotificationData = [
  {
    id: '0',
    title: 'Obasey Chidy',
    description: 'Its time to build a difference ...',
    image: Images.photo1,
    date: 'Dec 11, 2018',
  },
  {
    id: '1',
    title: 'Steve Garrett',
    description: 'Its time to build a difference ...',
    image: Images.photo4,
    date: 'Dec 11, 2018',
  },
  {
    id: '2',
    title: 'Luvleen Lawrence',
    description: 'Its time to build a difference ...',
    image: Images.photo5,
    date: 'Dec 11, 2018',
  },
  {
    id: '3',
    title: 'Tom Hardy',
    description: 'Its time to build a difference ...',
    image: Images.photo6,
    date: 'Dec 11, 2019',
  },
  {
    id: '3',
    title: 'Tom Hardy',
    description: 'Its time to build a difference ...',
    image: Images.photo7,
    date: 'Dec 11, 2019',
  },
];

export {NotificationData};
