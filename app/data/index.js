import {BookingHistoryData} from './bookingHistory';
import {LanguageData} from './language';
import {CouponsData} from './/coupons';
import {CurrencyData} from './currency';
import {PromotionData} from './promotion';
import {TourData} from './tour';
import {TrendingData} from './trendingData';
import {HotelData} from './hotel';
import {userData} from './user';
import {MessagesData} from './messages';
import {NotificationData} from './notification';
import {PostData} from './post';
import {PackageData} from './package';
import {WorkProgressData} from './workprogress';
import {HelpBlockData} from './helpblock';
import {ReviewData} from './review';
import {CarData} from './car';
import {FlightBrandData} from './flightBrand';
import {FlightData} from './flight';
import {CruiseData} from './cruise';
import {BusData} from './bus';
import {EventListData} from './event';
import { PostData1 } from './post1';
import { ShortsData } from './shorts';
import { ProductsData } from './products';
import { StatusData } from './status';
import { RewardData } from './rewards';
import { StatusData1 } from './status1';
import { RewardData1 } from './rewards1';
import { CategoryData } from './categories';
import { UserData1 } from './user1';
import {ChannelsData} from './channels';
import { PostData2 } from './post2';
import { ShortsData1 } from './shorts1';
import { PostData3 } from './post3';
import { ProductsData1 } from './products1';
// Sample data for display on template
export {
  BusData,
  CruiseData,
  FlightData,
  BookingHistoryData,
  LanguageData,
  CouponsData,
  CurrencyData,
  PromotionData,
  TourData,
  TrendingData,
  HotelData,
  userData,
  MessagesData,
  NotificationData,
  PostData,
  PackageData,
  WorkProgressData,
  HelpBlockData,
  ReviewData,
  CarData,
  FlightBrandData,
  EventListData,
  PostData1,
  ShortsData,
  ProductsData,
  StatusData,
  RewardData,
  StatusData1,
  RewardData1,
  CategoryData,
  UserData1,
  ChannelsData,
  PostData2,
  ShortsData1,
  PostData3,
  ProductsData1,
};
