import {Images} from '@config';

const PostData3 = [
  {
    id: '1',
    image: Images.post,
    title: 'Masked Wolf Astronaut in the Ocean',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.profile008,
    name: 'Amanda Bailey',
    detail: 'TrapMusicHDTV - 5 hours ago | 100k views',
    date: 'Jun 2018',
    time:'5:10',
  },
  {
    id: '2',
    image: Images.post1,
    title: '50 Really Good Place For This Summer',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo4,
    name: 'Allu Arjun',
    detail: '4 hours 30 mins ago | 50 views',
    date: 'Jun 2018',
    time:'5:10',
  },
  {
    id: '3',
    image: Images.post2,
    title: 'Overseas Adventure Travel In Nepal',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo5,
    name: 'Ram Charan',
    detail: '5 hours ago | 100k views',
    date: 'Jun 2018',
    time:'5:10',
  },
  {
    id: '4',
    image: Images.post3,
    title: '50 Really Good Place For This Summer',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo6,
    name: 'Varun Tej',
    detail: '4 hours 30 mins ago | 50 views',
    date: 'Jun 2018',
    time:'5:10',
  },
];

export {PostData3};
