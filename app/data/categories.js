import {Images} from '@config';
import {UserData} from './user';
const CategoryData = [
  {
    id: '1',
    // image: Images.profile9,
    image: Images.stick,
    title1: '200 Views',
    title2: 'Beauty',
  },
  {
    id: '2',
    image: Images.photo3,
    title1: '5K Views',
    title2: 'Gaming',
  },
  {
    id: '3',
    image: Images.photo5,
    title1: '500K Views',
    title2: ' Sports ',
  },
  {
    id: '4',
    image: Images.photo6,
    title1: '2K Views',
    title2: 'Art',
  },
  {
    id: '5',
    image: Images.photo7,
    title1: '10K Views',
    title2: 'Food',
  },
  {
    id: '6',
    image: Images.photo9,
    title1: '10K Views',
    title2: 'DIY',
  },
  // {
  //   id: '7',
  //   image: Images.photo6,
  //   title1: '2K Views',
  //   title2: 'Sushanth',
  // },
  // {
  //   id: '8',
  //   image: Images.photo7,
  //   title1: '10K Views',
  //   title2: 'Sidharth',
  // },
  // {
  //   id: '9',
  //   image: Images.photo3,
  //   title1: '10K Views',
  //   title2: 'Akhila',
  // },
];

export {CategoryData};