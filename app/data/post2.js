import {Images} from '@config';

const PostData2 = [
  {
    id: '1',
    image: Images.thumb6,
    title: 'Masked Wolf Astronaut in the Ocean',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo1,
    name: 'Amanda Bailey',
    detail: 'TrapMusicHDTV - 5 hours ago | 100k views',
    date: 'Jun 2018',
    time:'5:10',
    video:Images.video1
  },
  {
    id: '2',
    image: Images.thumb5,
    title: '50 Really Good Place For This Summer',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo4,
    name: 'Allu Arjun',
    detail: '4 hours 30 mins ago | 50 views',
    date: 'Jun 2018',
    time:'5:10',
    video:Images.video2
  },
  {
    id: '3',
    image: Images.thumb7,
    title: 'Overseas Adventure Travel In Nepal',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo5,
    name: 'Ram Charan',
    detail: '5 hours ago | 100k views',
    date: 'Jun 2018',
    time:'5:10',
    video:Images.video1
  },
  {
    id: '4',
    image: Images.thumb8,
    title: '50 Really Good Place For This Summer',
    description:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    authorImage: Images.photo6,
    name: 'Varun Tej',
    detail: '4 hours 30 mins ago | 50 views',
    date: 'Jun 2018',
    time:'5:10',
    video:Images.video2
  },
];

export {PostData2};
