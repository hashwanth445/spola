import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Alert,
  Image,
  FlatList,
  Dimensions,
  StyleSheet,
} from 'react-native';
import { BaseStyle, BaseColor, useTheme } from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  BookingTime,
  TextInput,
} from '@components';
import * as Utils from '@utils';
import Modal from 'react-native-modal';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import Video from 'react-native-video';
import ImagePicker from 'react-native-image-crop-picker';
import { PermissionsAndroid } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import VideoPlayer from 'react-native-rn-videoplayer';
import { feedsPostRequest, feedsGetRequest } from '../../api/feeds';
import { uploadFilesPostRequest } from '../../api/uploadApi';
import { useDispatch, useSelector } from 'react-redux';
import { NeomorphBlur, Neomorph } from 'react-native-neomorph-shadows';

const IS_IOS = Platform.OS === 'ios';
const entryBorderRadius = 8;

export default function createFeed({ route, navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const [keyword, setKeyword] = useState('');

  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState(route?.params?.images ? route?.params?.images : []);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  // const { data } = route.params;
  const userData = useSelector(state => state.accessTokenReducer.userData);
  //   const devices = useCameraDevices('wide-angle-camera')
  // const device = devices.back


  // setImage(data)
  /**
   * call when action modal
   * @param {*} modal
   */
  //   const openModal = modal => {
  //     setModalVisible(modal);
  //   };

  /**
   * call when on change value
   * @param {*} mode
   * @param {*} value
   */


  const ViewsImage = () => {
    let options = {
      title: 'Video Picker',
      mediaType: 'image/video',
    };
    Alert.alert(
      'Select Your Video',
      'You want to select from you device?',
      [
        {
          text: 'Device',
          onPress: () =>
            launchImageLibrary(options, responce => {
              console.log('Response = ', responce.uri);
              if (responce.didCancel) {
                console.log('User cancelled image picker');
              } else if (responce.error) {
                console.log('ImagePicker Error: ', responce.errorMessage);
              } else {
                console.log(responce, "all 2 data how selected", responce.uri)
                let source = responce.uri;
                setVideoPlay(source)
                console.log(responce.uri, "uri video data")
              }
            }),
        },
        {
          text: 'Not Now',
          onPress: () => null,
        },
      ],
    );
    return true;
  };
  selectImage = () => {
    const options = {
      noData: true
    }
    ImagePicker.launchImageLibrary(options, response => {
      console.log(response.assets, "all response", response.assets.uri)
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        const source = { uri: response.uri }
        console.log(source)
        setImage(source)
      }
    })
  }
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write camera err', err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };


  const [selectedImage, setSelectedImage] = useState();
  console.log("selectedImage", selectedImage);
  const captureImage = async (type) => {
    // let options = {
    //   mediaType: type,
    //   maxWidth: 300,
    //   maxHeight: 550,
    //   quality: 1,
    //   videoQuality: 'low',
    //   durationLimit: 30, //Video max duration in seconds
    //   saveToPhotos: true,
    // };
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {

      ImagePicker.openPicker({
        multiple: true
      }).then(images => {
        // let imgd = image.concat(images)
        // setImage(imgd);
        toNextPage(images);
      });
    }
  };
  const toNextPage = async (images) => {
    // let formData = new FormData();
    // setLoading(true);
    // if (imageData.length > 0) {
    //   for (let i = 0; i < imageData.length; i++) {
    //     formData.append('files', { "name": imageData[i].modificationDate, "type": imageData[i].mime, "uri": imageData[i].path });
    //   };
    //   let imgApi = await uploadFilesPostRequest({ formData });
    //   setLoading(false);
    //   navigation.navigate('BasicInformation', { picdata: imageData, picselected: selectedItems, imgApi });
    // } else {
    //   let imgApi = [];
    //   setLoading(false);
    //   navigation.navigate('BasicInformation', { picdata: imageData, picselected: selectedItems, imgApi });
    // }
    console.log('imgApi2', images);
    let formData = new FormData();
    setLoading(true);

    console.log('lopala', images);

    formData.append('files', {
      name: images[0].modificationDate,
      type: images[0].mime,
      uri: images[0].path,
    });
    console.log(formData);

    let imgApi = await uploadFilesPostRequest(formData);
    console.log('imgApi', imgApi);
    setSelectedImage(imgApi[0]?.url);
    setLoading(false);

  }


  const ImgSelector = ({ item, index }) => (
    <Image source={{ uri: item }}
      style={{ width: 300, height: 400, margin: 8, alignItems: 'center' }}
      resizeMode="contain"
    />
  )

  const renderItem = ({ item, index }) => {
    return (
      <ImgSelector item={item} />
    )
  }

  const isCarousel = React.useRef(null)
  const SLIDER_WIDTH = Dimensions.get('window').width
  const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.95)
  const ITEM_HEIGHT = Math.round(SLIDER_WIDTH * 2);

  const CarouselCardItem = ({ item, index }) => {
    console.log("pathh", item)
    return (
      <View style={{
        backgroundColor: 'white',
        borderRadius: 8,
        width: ITEM_WIDTH,
        paddingBottom: 40,
        shadowColor: "#fff",
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
      }} key={index}>
        {item.mime === "video/mp4" ? (
          <VideoPlayer
            source={{ uri: item.path }}
            // poster={require('../../assets/images/avata.png')}
            // controls={true}
            style={{ height: 300, top: -15, borderWidth: 1.9, borderColor: "#2c3036", borderRadius: 20 }}
            // onEnd={onEnd}
            // playWhenInactive={true}
            // repeat={true}
            lockToPortrait={true}
            lockToLandscape={true}
            resizeMode='cover'
            lockControl={true}
            // tapAnywhereToPause={true}
            // rate={1.0}
            ignoreSilentSwitch={'obey'}
          />) : (
          <Image
            source={{ uri: item ? item : '' }}
            style={{
              width: ITEM_WIDTH,
              height: ITEM_HEIGHT, top: 30, marginBottom: 0
            }}
          />)}

        {/* <Text style={styles.header}>{item.title}</Text>
      <Text style={styles.body}>{item.body}</Text> */}
      </View>
    );
  }

  // post method 
  const saveFeed = async () => {
    // const selectedImg = selectedImage;
    console.log("coming to login")
    // debugger
    if (!title && !description && !selectedImage) {
      setSuccess({
        ...success,
        title: false,
        description: false,
        selectedImage: false,
      });
    } else {
        setLoading(true);
        await dispatch(feedsPostRequest(selectedImage, title, description, userData));
        navigation.navigate('Post')

        // setLoading(false);
      }
  };
  console.log("selectedImage", selectedImage);

  return (
    <ScrollView>
      <View style={{ flex: 1, marginTop: 20 }}>
        <Image
        source={{uri : selectedImage ? selectedImage : ''}}
        style={{height:ITEM_HEIGHT,width:ITEM_WIDTH,alignSelf:"center"}}
        />


        <View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 20 }}>
            <TouchableOpacity onPress={() => {
              navigation.navigate('Home')
            }}>
              <Image
                style={{ width: 15, height: 15, backgroundColor: 'white' }} resizeMode="contain"
                source={require('./cancel.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
              navigation.navigate('Home')
            }}>
              <Text style={{ fontFamily: "ProximaNova", fontSize: 15 }}>Edit</Text>
            </TouchableOpacity>
          </View>
          <View style={{ width: '100%', overflow: 'scroll' }}>
            {/* <FlatList
              contentContainerStyle={{ margin: 1, width: '100%' }}
              // numColumns={3}
              data={image}
              keyExtractor={(item, index) => item.path}
              renderItem={renderItem}
            /> */}

          </View>



        </View>
        <View style={{ marginTop: 10, alignItems: 'center' }}>
          <TouchableOpacity
            onPress={captureImage}
            style={{
            }}>
            <NeomorphBlur
              style={{
                shadowRadius: 3,
                borderRadius: 50,
                backgroundColor: colors.neoThemebg,
                width: 140,
                height: 56,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 10,
              }}>
              <Text bold style={{ fontFamily: "ProximaNova", fontSize: 22, color: BaseColor.grayColor }}>
                Add
              </Text>

            </NeomorphBlur>
          </TouchableOpacity>
          <Neomorph
            inner
            style={{
              shadowRadius: Utils.scaleWithPixel(7),
              borderRadius: Utils.scaleWithPixel(15),
              backgroundColor: colors.neoThemebg,
              width: Utils.scaleWithPixel(300),
              height: Utils.scaleWithPixel(50),
              justifyContent: 'center',
              alignItems: 'center',
              margin: 10,
              // top: 60
            }}>
            <TextInput
              placeholder='Enter title of the post'
              style={{ margin: 20 }}
              value={title}
              onChangeText={title => setTitle(title)}
            />
          </Neomorph>
          <Neomorph
            inner
            style={{
              shadowRadius: Utils.scaleWithPixel(7),
              borderRadius: Utils.scaleWithPixel(15),
              backgroundColor: colors.neoThemebg,
              width: Utils.scaleWithPixel(300),
              height: Utils.scaleWithPixel(100),
              // justifyContent: 'flex-start',
              // alignItems: 'flex-start',
              margin: 10,
              // top: 60
            }}>
            <TextInput
              placeholder='Enter description'
              // style={{textAlign:"center",alignSelf:"flex-start" }}
              multiline={true}
              numberOfLines={5}
              value={description}
              onChangeText={description => setDescription(description)}
            />
          </Neomorph>
          <View style={{ marginTop: 20, marginBottom: 20 }}>
            <TouchableOpacity status='success' style={{ marginBottom: 0, }} onPress={() => {
              saveFeed();
            }}>
              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 50,
                  backgroundColor: colors.neoThemebg,
                  width: 200,
                  height: 56,
                  justifyContent: 'center',
                  alignItems: 'center',

                }}>
                <Text bold style={{ fontFamily: "ProximaNova", fontSize: 22, color: BaseColor.grayColor }}>
                  Add post
                </Text>
              </NeomorphBlur>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

