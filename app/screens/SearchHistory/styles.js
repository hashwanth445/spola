import {StyleSheet} from 'react-native';
import {BaseColor, BaseStyle} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  btnClearSearch: {
    position: 'absolute',
    right: Utils.scaleWithPixel(0),
    alignItems: 'center',
    justifyContent: 'center',
    width: Utils.scaleWithPixel(30),
    height: '100%',
    right:Utils.scaleWithPixel(30)
  },
  rowTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemHistory: {
    marginTop: Utils.scaleWithPixel(5),
    padding: Utils.scaleWithPixel(5),
    marginRight: Utils.scaleWithPixel(10),
  },
  tabbar: {
    height: 40,
    
  },
  tab: {
    width: 120,
    paddingLeft:0,
  },
  indicator: {
    height: 1,
    width:45,
   marginLeft:50
  },
  label: {
    fontWeight: '400',
  },
});
