import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  RefreshControl,
} from 'react-native';
import {BaseStyle, BaseColor, Images, useTheme} from '@config';
import {
  Header,
  SafeAreaView,
  TextInput,
  Icon,
  Text,
  CategoryCard,
  PostItem,
  ProfileCard,
  ProfileGroup,
  CreatorsCard,
  ProfileAuthor2,
  HomeItem1,
} from '@components';
import styles from './styles';
import {useTranslation} from 'react-i18next';
import {Neomorph} from 'react-native-neomorph-shadows';
import {UserData1, UserData, CategoryData} from '@data';
import * as Utils from '@utils';
import {TabView, TabBar} from 'react-native-tab-view';
import {useSelector, useDispatch} from 'react-redux';
import {categoriesGetRequest, creatorsGetRequest} from '../../api/categories';
import {videosSearchRequest} from '../../api/videos';
import {ProfileAuthor, ProfileAuthor3, ProfileAuthor5} from '../../components';
export default function SearchHistory({navigation}) {
  const {colors} = useTheme();
  const {t} = useTranslation();
  const [index, setIndex] = useState(0);
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(creatorsGetRequest(userToken));
  }, [dispatch]);

  useEffect(() => {
    dispatch(categoriesGetRequest());
  }, [dispatch]);

  const [routes] = useState([
    {key: 'accounts', title: t('Accounts')},
    {key: 'videos', title: t('Videos')},
    {key: 'channels', title: t('Channels')},
    // { key: 'activity', title: t('setting'), icon: 'cog' },
  ]);

  // When tab is activated, set what's index value
  const handleIndexChange = index => setIndex(index);

  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, {backgroundColor: colors.text}]}
      style={[styles.tabbar, {backgroundColor: colors.neoThemeBg}]}
      tabStyle={styles.tab}
      // inactiveColor="black"
      // activeColor={colors.text}
      renderLabel={({route, focused, color}) => (
        <View style={{flex: 1, width: 350, alignItems: 'center', left: 20}}>
          {focused ? (
            <Text
              headline
              bold={focused}
              style={{fontFamily: 'ProximaNova', fontSize: 16, color: 'white'}}>
              {route.title}
            </Text>
          ) : (
            <Text
              headline
              bold
              style={{
                fontFamily: 'ProximaNova',
                fontSize: 16,
                color: BaseColor.grayColor,
              }}>
              {route.title}
            </Text>
          )}

          {/* <Icon name={route.icon} color="#DDDDDD" size={24} style={{ top: -22, left: -30 }} /> */}
        </View>
      )}
    />
  );

  const renderScene = ({route, jumpTo}) => {
    switch (route.key) {
      case 'accounts':
        return (
          <MessagesTab
            jumpTo={jumpTo}
            navigation={navigation}
            search={search}
          />
        );
      case 'videos':
        return (
          <AudioRoomTab
            jumpTo={jumpTo}
            navigation={navigation}
            search={search}
          />
        );
      case 'channels':
        return <ChannelsTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };

  const [posts] = useState(UserData1);
  const [category] = useState(CategoryData);
  const [user] = useState(UserData);
  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(false);
  const [searchHistory, setSearchHistory] = useState([
    {id: '1', keyword: 'Delux Room'},
    {id: '2', keyword: 'Tripple Room'},
    {id: '3', keyword: 'Single Room'},
    {id: '4', keyword: 'King Room'},
    {id: '5', keyword: 'Lux Room'},
  ]);
  const [discoverMore] = useState([
    {id: '1', keyword: 'Hotel California'},
    {id: '2', keyword: 'Top 10 Things Must To Do In This Autum'},
    {id: '3', keyword: 'Best Hotel Indonesia'},
  ]);
  const [recentlyView] = useState([
    {id: '1', name: 'France', image: Images.trip1},
    {id: '2', name: 'Dublin', image: Images.trip2},
    {id: '3', name: 'Houston', image: Images.trip3},
    {id: '4', name: 'Houston', image: Images.trip4},
  ]);

  /**
   * call when search data
   * @param {*} keyword
   */
  const onSearch = keyword => {
    const found = searchHistory.some(item => item.keyword == keyword);
    let searchData = [];

    if (found) {
      searchData = searchHistory.map(item => {
        return {
          ...item,
          checked: item.keyword == keyword,
        };
      });
    } else {
      searchData = searchHistory.concat({
        keyword: search,
      });
    }
    setSearchHistory(searchData);
    setLoading(true);
    setTimeout(() => navigation.goBack(), 1000);
  };

  return (
    <View style={{flex: 1}}>
      <View style={{flexDirection: 'row', marginTop: 20, marginLeft: 10}}>
        <Icon
          name="chevron-left"
          size={28}
          color={colors.text}
          style={{alignSelf: 'center'}}
          onPress={() => navigation.goBack()}
        />
        <Neomorph
          inner
          style={{
            shadowRadius: Utils.scaleWithPixel(7),
            borderRadius: Utils.scaleWithPixel(25),
            backgroundColor: colors.neoThemebg,
            width: Utils.scaleWithPixel(270),
            height: Utils.scaleWithPixel(40),
            justifyContent: 'center',
          }}>
          <View style={{flexDirection: 'row', marginLeft: -20}}>
            <TextInput
              onChangeText={text => setSearch(text)}
              placeholder={t('search')}
              style={{marginLeft: Utils.scaleWithPixel(25)}}
              value={search}
              onSubmitEditing={() => {
                onSearch(search);
              }}
              icon={
                <TouchableOpacity
                  onPress={() => {
                    setSearch('');
                  }}
                  style={styles.btnClearSearch}>
                  {/* <Icon name="close" size={18} color={BaseColor.grayColor} style={{ left: Utils.scaleWithPixel(15) }} /> */}
                  <View
                    style={{
                      backgroundColor: '#006699',
                      height: 40,
                      width: 40,
                      borderRadius: 20,
                      marginLeft: -20,
                    }}>
                    <Icon
                      name="magnify"
                      size={23}
                      color={BaseColor.whiteColor}
                      style={{alignSelf: 'center', marginTop: 8}}
                    />
                  </View>
                </TouchableOpacity>
              }
            />
          </View>
        </Neomorph>
      </View>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{flex: 1}}>
          <TabView
            lazy
            navigationState={{index, routes}}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            onIndexChange={handleIndexChange}
            style={{height: '100%', marginTop: 20, marginBottom: -30}}
          />
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
}

/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function MessagesTab({navigation, search}) {
  const {colors} = useTheme();
  const {t} = useTranslation();
  const [refreshing] = useState(false);
  const category = useSelector(state => state.CategoriesReducer.categoriesData);
  const creatorsData = useSelector(
    state => state.CategoriesReducer.creatorsData,
  );
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const shortsData = useSelector(state => state.ShortsReducer.shortsData);
  console.log('UserData', UserData);
  console.log('creatorsData', creatorsData);
  const [finaldata, setFinalData] = useState([]);
  const shorts = UserData?.user?.shorts;
  console.log('shorts', shorts);
  useEffect(() => {
    if (creatorsData) {
      const data = creatorsData;
      const filteredData = data.filter(element => {
        const searchtext = search.toLowerCase();
        const name = element.username.toLowerCase();
        if (name.includes(searchtext)) {
          return true;
        } else return false;
      });
      setFinalData(filteredData);
    }
  }, [search]);

  console.log('coming to BookingTab ------------------');
  return (
    <ScrollView
      contentContainerStyle={{
        paddingLeft: Utils.scaleWithPixel(25),
        paddingTop: Utils.scaleWithPixel(20),
      }}>
      <Neomorph
        inner
        style={{
          shadowRadius: Utils.scaleWithPixel(9),
          borderRadius: Utils.scaleWithPixel(15),
          backgroundColor: colors.neoThemebg,
          width: Utils.scaleWithPixel(320),
          height: Utils.scaleWithPixel(240),
          justifyContent: 'center',
          marginLeft: Utils.scaleWithPixel(-15),
          // marginTop: Utils.scaleWithPixel(35),
        }}>
        <Text
          bold
          style={{
            color: colors.text,
            fontSize: 20,
            top: Utils.scaleWithPixel(12),
            left: Utils.scaleWithPixel(14),
          }}>
          Trending Accounts
        </Text>

        <FlatList
          horizontal={true}
          contentContainerStyle={{
            alignSelf: 'flex-start',
            marginTop: 15,
            marginLeft: 10,
          }}
          // showsHorizontalScrollIndicator={false}
          data={finaldata}
          keyExtractor={(item, index) => item.id}
          renderItem={({item, index}) => (
            <CreatorsCard
              style={{
                margin: Utils.scaleWithPixel(10),
                top: Utils.scaleWithPixel(-10),
              }}
              onPress={() => commonUser()}
              // image={item.image}
              // name={item.name}
            >
              <Text
                style={{
                  top: Utils.scaleWithPixel(-15),
                  left: Utils.scaleWithPixel(5),
                  alignSelf: 'center',
                  color: '#909090',
                  fontFamily: 'ProximaNova-Medium',
                  fontSize: 14,
                }}>
                {item?.followersProfiles?.length
                  ? item?.followersProfiles?.length
                  : '0'}{' '}
                Followers
              </Text>
              <Text
                style={{
                  top: Utils.scaleWithPixel(-10),
                  left: Utils.scaleWithPixel(5),
                  alignSelf: 'center',
                  color: '#e3dce1',
                  fontWeight: 'bold',
                  fontFamily: 'ProximaNova-Bold',
                  fontSize: 14,
                }}>
                {item?.username}
              </Text>
              <Text
                style={{
                  top: Utils.scaleWithPixel(-8),
                  left: Utils.scaleWithPixel(5),
                  alignSelf: 'center',
                  color: '#909090',
                  fontFamily: 'ProximaNova-Medium',
                  fontSize: 14,
                }}>
                {item.email}
              </Text>

              <ProfileAuthor2
                style={{
                  left: Utils.scaleWithPixel(27),
                  top: Utils.scaleWithPixel(-145),
                  height: Utils.scaleWithPixel(100),
                  width: Utils.scaleWithPixel(100),
                }}
                image={
                  item?.backBlazeImageUrl
                    ? item?.backBlazeImageUrl
                    : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                }
                // name={item.name}
                // description={item.detail}
              />
            </CreatorsCard>
          )}
        />
      </Neomorph>
      <View style={{marginLeft: -20}}>
        <Text
          bold
          style={{
            color: colors.text,
            fontSize: 20,
            alignSelf: 'flex-start',
            marginTop: 20,
          }}>
          Browse Accounts through categories
        </Text>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={true}>
          <FlatList
            numColumns={5}
            //     columnWrapperStyle={{
            //         flex: 1,
            //         justifyContent: 'space-between',
            //    }}
            contentContainerStyle={{
              paddingTop: Utils.scaleWithPixel(80),
              margin: Utils.scaleWithPixel(-20),
            }}
            data={category}
            keyExtractor={(item, index) => item.id}
            renderItem={({item, index}) => (
              <CategoryCard
                onPress={() => selectCategories(item.id)}
                image={item?.backBlazeImageUrl}
                style={{
                  height: Utils.scaleWithPixel(230),
                  marginBottom: Utils.scaleWithPixel(-75),
                  marginRight: Utils.scaleWithPixel(10),
                }}>
                <TouchableOpacity>
                  <Text
                    title2
                    style={{
                      top: Utils.scaleWithPixel(-35),
                      alignSelf: 'center',
                      fontFamily: 'ProximaNova-Medium',
                      fontSize: 16,
                    }}>
                    {item?.name}
                  </Text>
                </TouchableOpacity>
              </CategoryCard>
            )}
          />
        </ScrollView>
      </View>
      {/* <View>
        <FlatList
          refreshControl={
            <RefreshControl
              colors={[colors.primary]}
              tintColor={colors.primary}
              refreshing={refreshing}
              onRefresh={() => { }}
            />
          }
          data={creatorsData}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) => (
            <PostItem
              // image={item?.image?.url ? `http://3.111.30.249:1337` + item?.image?.url : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
              title={item?.name}
              description={item?.feedDesc}
              feedDataObject={item}
            // onPress={() => success(item)}
            >
            <ProfileGroup
              name="Steve, Lincoln, Harry"
              detail="and 15 people like this"
              users={[
                {image: Images.profile1},
                {image: Images.profile3},
                {image: Images.profile4},
              ]}
            />
            </PostItem>
          )}
        />
      </View> */}
    </ScrollView>
  );
}

/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function AudioRoomTab({navigation, search}) {
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const shortsData = useSelector(state => state.ShortsReducer.shortsData);
  // const videosData = useSelector(state => state.VideosReducer.videosData);
  const videosData = useSelector(state =>
    state.VideosReducer.videosSearchData
      ? state.VideosReducer.videosSearchData
      : state.VideosReducer.videosData,
  );

  const [final, setFinal] = useState([]);
  console.log('UserData', UserData);
  const shorts = UserData?.user?.shorts;
  useEffect(() => {
    if (videosData) {
      dispatch(videosSearchRequest(search));
      
      const data = videosData;
      const filteredData = data.filter(element => {
        const searchtext = search.toLowerCase();
        const title = element.title.toLowerCase();
        const name = element.uploadedBy?.username?.toLowerCase();
        console.log('username', name);
        if (title.includes(searchtext) || name?.includes(searchtext)) {
          return true;
        } else return false;
      });
      setFinal(filteredData);
    }
  }, [search]);

  // useEffect(() => {
      
  //     const data = videosData;
  //     const filteredData = data.filter(element => {
  //       const searchtext = search.toLowerCase();
  //       const title = element.title.toLowerCase();
  //       const name = element.uploadedBy?.username?.toLowerCase();
  //       console.log('username', name);
  //       if (title.includes(searchtext) || name?.includes(searchtext)) {
  //         return true;
  //       } else return false;
  //     });
  //     setFinal(filteredData);
    
  // }, [videosData]);


  console.log('shorts', shorts);

  console.log('coming to BookingTab ------------------');
  return (
    <ScrollView>
      <FlatList
        data={final}
        // horizontal={true}
        keyExtractor={(item, index) => item.id}
        renderItem={({item, index}) => (
          <HomeItem1
            image={
              item?.backBlazeVideoUrl
                ? item?.backBlazeVideoUrl
                : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
            }
            // title={item.title}
            time={item?.Duration}
            // description={item.description}
            onPress={() => {
              success(item);
            }}>
            <ProfileAuthor
              image={
                item.uploadedBy?.backBlazeImageUrl
                  ? item.uploadedBy?.backBlazeImageUrl
                  : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png'
              }
              name={item?.uploadedBy?.username}
              title={item?.title}
              description={item?.viewCount ? item?.viewCount : '0'}
              dateAndTime={item?.createdAt ? item?.createdAt : '0'}
              style={{
                paddingHorizontal: Utils.scaleWithPixel(10),
                top: Utils.scaleWithPixel(210),
              }}
              onPress={() => {
                commonUser(item.uploadedBy);
              }}
            />
          </HomeItem1>
        )}
      />
    </ScrollView>
  );
}
/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function ChannelsTab({navigation}) {
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const shortsData = useSelector(state => state.ShortsReducer.shortsData);
  console.log('UserData', UserData);
  const shorts = UserData?.user?.shorts;
  console.log('shorts', shorts);

  console.log('coming to BookingTab ------------------');
  return (
    <ScrollView>
      <FlatList
        contentContainerStyle={{
          paddingLeft: 30,
          margin: 10,
          // paddingRight: 0,
          paddingTop: 20,
        }}
        numColumns={2}
        // vertical={true}
        // showsHorizontalScrollIndicator={false}
        data={shortsData}
        keyExtractor={(item, index) => item.id}
        renderItem={({item, index}) => (
          <ProfileCard
            grid
            image={
              item?.backBlazeSpolaUrl
                ? item?.backBlazeSpolaUrl
                : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
            }
            style={{margin: 10, left: -30, paddingTop: 20, top: -30}}
            // onPress={() => navigation.navigate('HotelDetail')}
          >
            <View>
              <Icon
                name="play-outline"
                color="#dddddd"
                size={22}
                style={{top: -20, left: -15}}
              />
              <Text
                subhead
                whiteColor
                style={{top: -40, left: 5, fontFamily: 'ProximaNova-Medium'}}>
                {item?.viewCount}
              </Text>
            </View>
          </ProfileCard>
        )}
      />
    </ScrollView>
  );
}
