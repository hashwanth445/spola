import React from 'react';
import { StyleSheet } from 'react-native';
import { BaseColor } from '@config';
import { fontWeight } from 'styled-system';

export default StyleSheet.create({
  contain: {
    alignItems: 'center',
    paddingHorizontal: 30,
    padding: 10,
    flex: 1,
  },
  textInput: {
    height: 46,
    backgroundColor: BaseColor.fieldColor,
    borderRadius: 5,
    marginTop: 10,
    padding: 10,
    width: '100%',
  },
  img: {
    height: 150, marginBottom: 30,
    width: 150
  },
  contain1: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 40,

    flex: 1,
  },
  Term: {
    fontSize:24,
    fontFamily:"ProximaNova",
    marginBottom:20
  },
  Term1: {
    fontSize:20,
    fontFamily:"ProximaNova",
    color:BaseColor.grayColor,
    textAlign:"center",
    letterSpacing:-0.5,
  },
  button: {
    height: 45,
    width: 220,
    borderRadius: 12,
    justifyContent: "center"
  },
  text: {
    fontFamily:"ProximaNova",
    fontSize:18,
    color:"white",
    alignSelf:"center",
    fontWeight:'700'
  }
});
