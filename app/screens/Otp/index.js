import React, { useState } from 'react';
import { View, KeyboardAvoidingView, Image, TouchableOpacity, Platform, ActivityIndicator } from 'react-native';
import { BaseStyle, useTheme, images,BaseColor } from '@config';
import { Header, SafeAreaView, Icon, Text, Button, TextInput } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { sendVerifyOtpRequest } from '../../api/signup';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import FIcon from "react-native-vector-icons/FontAwesome";
import LinearGradient from 'react-native-linear-gradient';
// import CheckBox from '@react-native-community/checkbox';
import CountDown from 'react-native-countdown-component';
import Toast from 'react-native-toast-message';


export default function Otp({ navigation, phonenumber }) {
  const userdata = useSelector(state => state.signupReducer.signupMessageData);
  console.log("userdata", userdata);

  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const signupSuccessOtpRequest = useSelector(state => state.signupReducer.signupSuccessOtpRequest);
  const otpVerifySuccess = useSelector(state => state.signupReducer.verifyOtpSuccess);

  // console.log("otp screen",userdata)

  // const signupSuccess = useSelector(state => state.signupReducer.signupSuccess)
  const [otp, setOtp] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [referralId, setReferral] = useState('');
  const [phone, setPhone] = useState("");
  const [loading, setLoading] = useState(false);
  const [secure, setSecure] = useState(true);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [success, setSuccess] = useState({
    name: true,
    email: true,
    address: true,
    referralId: true,
    phone: true,
    password: true
  });
  /**
   * call when action signup
   *
   */
   const showToast = () => {
    console.log("toassssst")
   Toast.show({
     type: 'success',
     text1: 'Hello',
     text2: 'OTP Verified Successfully 👋'
   });
 }

  const onSignUp = async () => {
    if (otp === '') {
      // setSuccess({
      //   ...success,
      //   otp: otp != '' ? true : false,
      // });
      // alert('Enter the OTP');
    } else {
      setLoading(true);
      const mobile = userdata.user.MobileNumber
      await dispatch(sendVerifyOtpRequest(mobile, otp));
      setLoading(false);
    }
  };
  if (otpVerifySuccess) {
    showToast()
    navigation.navigate('OnboardingScreenOneProfileDetails', userdata)
  };

  const MobileNumber = userdata?.user?.MobileNumber;

  // const arr = [2, 3, 4, 5, 6];

  // const replceWithAsterisk = (MobileNumber, indices) => {
  //   let res = '';
  //   res = indices.reduce((acc, val) => {
  //     acc[val] = '*';
  //     return acc;
  //   }, MobileNumber.split("")).join('');
  //   return res;
  // };
  // console.log(replceWithAsterisk(MobileNumber, arr));
  return (
    <SafeAreaView style={BaseStyle.safeAreaView} forceInset={{ top: 'always' }}>
      {/* <Header
        title={t('OTP')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.dgrblue}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}
      <ScrollView>
        {/* <KeyboardAvoidingView
        behavior={Platform.OS === 'android' ? 'height' : 'padding'}
        keyboardVerticalOffset={offsetKeyboard}
        style={{flex: 1}}> */}
        <View style={styles.contain}>
          <View style={{ alignItems: 'center', paddingBottom: 20 }}>
            <Image source={require("../../assets/images/logo.png")} style={styles.img} />
            <Text bold style={[styles.Term]}>Verify OTP</Text>
            <Text style={{fontSize:20,fontFamily:"ProximaNova",color:BaseColor.grayColor,textAlign:"center",}}>Enter the 4 digit OTP sent on {'\n'}{MobileNumber} to proceed</Text>
          </View>
          <Neomorph
            inner
            style={{
              shadowRadius: Utils.scaleWithPixel(7),
              borderRadius: Utils.scaleWithPixel(15),
              backgroundColor: colors.neoThemebg,
              width: Utils.scaleWithPixel(300),
              height: Utils.scaleWithPixel(50),
              justifyContent: 'center',
              alignItems: 'center',
              // left: 20

            }}
          >
            <TextInput
              onChangeText={(text) => setOtp(text)}
              placeholder={t('Enter OTP')}
              success={success.name}
              value={otp}
            />
          </Neomorph>
          {/* <View style={{
            marginTop: Utils.scaleWithPixel(-70),
            flexDirection: "row"
          }}>

            <Neomorph
              inner
              style={{
                shadowRadius: Utils.scaleWithPixel(7),
                borderRadius: Utils.scaleWithPixel(20),
                backgroundColor: colors.neoThemebg,
                width: Utils.scaleWithPixel(75),
                height: Utils.scaleWithPixel(75),
                justifyContent: 'center',
                alignItems: 'center',
                margin: Utils.scaleWithPixel(10)
              }}
            >
              <TextInput autoComplete="sms-otp" onChangeText={(text) => setenterOtp(text)} maxLength={4} keyboardType='phone-pad' style={{ color: "white", fontSize: 28, width: Utils.scaleWithPixel(70), height: Utils.scaleWithPixel(70), textAlign: "center" }} />
            </Neomorph>

            <Neomorph
              inner
              style={{
                shadowRadius: 7,
                borderRadius: 20,
                backgroundColor: colors.neoThemebg,
                width: 75,
                height: 75,
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10
              }}
            >
              <TextInput onChangeText={(text) => setenterOtp(text)} maxLength={1} keyboardType='phone-pad' style={{ color: "white", fontSize: 28, width: 70, height: 70, textAlign: "center" }} />
            </Neomorph>
            <Neomorph
              inner
              style={{
                shadowRadius: 7,
                borderRadius: 20,
                backgroundColor: colors.neoThemebg,
                width: 75,
                height: 75,
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10
              }}
            >
              <TextInput onChangeText={(text) => setenterOtp(text)} maxLength={1} keyboardType='phone-pad' style={{ color: "white", fontSize: 28, width: 70, height: 70, textAlign: "center" }} />
            </Neomorph>
            <Neomorph
              inner
              style={{
                shadowRadius: 7,
                borderRadius: 20,
                backgroundColor: colors.neoThemebg,
                width: 75,
                height: 75,
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10
              }}
            >
              <TextInput onChangeText={(text) => setenterOtp(text)} maxLength={1} keyboardType='phone-pad' style={{ color: "white", fontSize: 28, width: 70, height: 70, textAlign: "center" }} />
            </Neomorph>

          </View> */}

          {/* <TouchableOpacity onPress={() => onSignUp()}
            style={{ width: '100%', borderRadius: 12, }}
          > */}
          <View style={{ marginTop: 50 }}>
            <NeomorphBlur
              style={{
                shadowRadius: 3,
                borderRadius: 15,
                backgroundColor: colors.neoThemebg,
                width: 230,
                height: 56,
                justifyContent: 'center',
                alignItems: 'center',

              }}
            >
              {otp === '' ? (
                <Text style={{ color: '#3a404b', fontSize: 20, backgroundColor: colors.neoThemebg, fontFamily: "ProximaNova" }}>
                  Verify and proceed
                </Text>
              ) : (
                <TouchableOpacity onPress={() => {
                  onSignUp()
                }}
                >
                  <LinearGradient style={styles.button} colors={['#43D4FF', '#38ABFD', '#2974FA']} >
                    {loading ? <ActivityIndicator size="small" color="#0000ff" /> :
                    <Text semibold style={styles.text}>Verify and proceed</Text>}
                  </LinearGradient>
                </TouchableOpacity>
              )}

            </NeomorphBlur>
            <View style={{ marginTop: 40 }}>
              <CountDown
                until={60}
                size={20}
                digitStyle={{ backgroundColor: 'transparent' }}
                digitTxtStyle={{ color: 'white' }}
                timeToShow={['M', 'S']}
                timeLabels={{ m: null, s: null }}
                showSeparator
                separatorStyle={{ color: 'white' }}
              />
              <View style={{ flexDirection: "row", alignContent: "center" }}>
                <Text body1 style={{ alignSelf: "center", fontFamily: "ProximaNova", color: "#6a6c6e" }}>
                  Didn't receive the code?
                </Text>
                <Text body1 style={{ alignSelf: "center", fontFamily: "ProximaNova", color: "#0094ff" }}>
                  Request again
                </Text>
              </View>
            </View>
          </View>
          {/* </TouchableOpacity> */}
        </View>
        {/* </KeyboardAvoidingView> */}
      </ScrollView>
    </SafeAreaView>
  );
}
