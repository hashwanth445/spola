import React, { useState, useEffect } from 'react';
import { RefreshControl, FlatList, View, Image, ScrollView, TouchableOpacity, TextInput, Dimensions } from 'react-native';
import { BaseStyle, useTheme, Images, BaseColor } from '@config';
import { Header, SafeAreaView, ListThumbSquare, Icon, MessengerCircle, Text, ProfileGroup, AudioGroup, ListThumbnails1 } from '@components';
import styles from './styles';
import { MessagesData, StatusData1, PostData1, NotificationData } from '@data';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { StatusData } from '@data';
import { useDispatch, useSelector } from 'react-redux';
import { friendsGetRequest } from '../../api/friends';
// import { GiftedChat } from 'react-native-gifted-chat';
import { TabView, TabBar } from 'react-native-tab-view';
import Modal from 'react-native-modal';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import * as Utils from '@utils';
import { Input } from 'react-native-elements/dist/input/Input';
import { backgroundColor } from 'styled-system';
import { audioroomDeleteRequest, audioroomPostRequest } from '../../api/audioroom';
import { chatsGetRequest } from '../../api/chats';
import { creatorsGetRequest } from '../../api/categories';
import { audioroomGetRequest } from '../../api/audioroom';


export default function Messenger({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const accessToken = useSelector(state => state.accessTokenReducer.accessToken);
  const friendsData = useSelector(state => state.FriendsReducer.friendsData);
  const [index, setIndex] = useState(0);
  // const {accessToken} = route.params;

  // console.log('khilan', accessToken)
  useEffect(() => {
    dispatch(chatsGetRequest(accessToken));
  }, (dispatch))

  useEffect(() => {
    dispatch(creatorsGetRequest(accessToken));

  }, [dispatch])
  useEffect(() => {
    dispatch(audioroomGetRequest(accessToken));

  }, [dispatch])

  const [routes] = useState([
    { key: 'messages', title: t('Messages') },
    { key: 'audiorooms', title: t('Audio Rooms') },
    // { key: 'setting', title: t('COMMENTS') },
    // { key: 'activity', title: t('setting'), icon: 'cog' },
  ]);

  // When tab is activated, set what's index value
  const handleIndexChange = index => setIndex(index);

  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, { backgroundColor: colors.text }]}
      style={[styles.tabbar, { backgroundColor: colors.neoThemeBg, }]}
      tabStyle={styles.tab}
      inactiveColor={colors.text}
      activeColor={colors.text}
      renderLabel={({ route, focused, color }) => (
        <View style={{ flex: 1, width: 350, alignItems: 'center', left: 20 }}>
          <Text headline bold={focused} style={{ fontFamily: "ProximaNova", fontSize: 20 }}>
            {route.title}
          </Text>
          {/* <Icon name={route.icon} color="#DDDDDD" size={24} style={{ top: -22, left: -30 }} /> */}
        </View>
      )}
    />
  );

  const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'messages':
        return <MessagesTab jumpTo={jumpTo} navigation={navigation} />;
      case 'audiorooms':
        return <AudioRoomTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <Header
        title="Chat Room"
        renderLeft={() => {
          return (
            <Icon
              name="chevron-left"
              size={32}
              color={colors.text}
              enableRTL={true}
            />
          );
        }}
        renderRight={() => {
          return (
            <Icon
              name="dots-vertical"
              size={28}
              color={colors.text}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <TabView
        lazy
        navigationState={{ index, routes }}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={handleIndexChange}
        style={{ height: 1000, marginBottom: -30 }}
      />
    </View>
  );
}

/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
        * @date 2019-08-03
        * @class PreviewTab
        * @extends {Component}
        */
function MessagesTab({ navigation }) {
  // const [hotels] = useState(HotelData);
  // const [shorts] = useState(ShortsData1);
  // console.log(':::web',friendsData[0]?.requestFrom?.image?.url)
  const [status] = useState(StatusData);
  const [messenger] = useState(MessagesData);
  const { colors } = useTheme();
  const [loading, setLoading] = useState(false);
  const [refreshing] = useState(false);
  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const friendsData = useSelector(state => state.FriendsReducer.friendsData);
  const chats = useSelector(state => state.ChatsReducer.chatsData);
  // console.log("chats", chats);
  const [modalVisible, setModalVisible] = useState(false);
  const [shouldShowOpen, setShouldShowOpen] = useState(true);
  const [shouldShowSocial, setShouldShowSocial] = useState(true);
  const [shouldShowClosed, setShouldShowClosed] = useState(true);
  const [Input, setInput] = useState('');

  const creatorsData = useSelector(state => state.CategoriesReducer.creatorsData);

  const [status1] = useState(StatusData1);

  const [posts] = useState(PostData1);
  const [notify] = useState(NotificationData);
  const [RoomInput, setRoomInput] = useState('');
  const [Name, setName] = useState('');
  //  const [success, setSuccess] = useState({
  //    Name: true,
  //  });

  // const openModal = modal => {
  //   setModalVisible(modal);
  //   if (shouldShowOpen === false && shouldShowSocial === true && shouldShowClosed === true) {
  //     setRoomInput('Open');
  //   }
  //   if (shouldShowOpen === true && shouldShowSocial === false && shouldShowClosed === true) {
  //     setRoomInput('Social');
  //   }
  //   if (shouldShowOpen === true && shouldShowSocial === true && shouldShowClosed === false) {
  //     setRoomInput('Closed');
  //   }
  //   // audioroomPostRequest(Input, RoomInput);
  // };

  const success = async (messageUrlObject) => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('Messages', { messageUrlObject })
    setLoading(false);

  };

  const renderModal = () => {
    return (
      <View>
        <Modal
          isVisible={modalVisible === 'roomtype'}
          onSwipeComplete={() => setModalVisible(false)}
          swipeDirection={['down']}
          style={styles.bottomModal}>
          <View style={styles.contain, { backgroundColor: colors.neoThemebg }}>
            <View style={styles.contentSwipeDown}>
              <View style={styles.lineSwipeDown} />
            </View>
            <ScrollView>
              <Neomorph
                inner
                style={{
                  shadowRadius: 7,
                  borderRadius: 25,
                  backgroundColor: colors.neoThemebg,
                  width: 385,
                  height: 1000,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: 5,
                  marginTop: 40,

                }}
              >
                <FlatList
                  refreshControl={
                    <RefreshControl
                      colors={[colors.primary]}
                      tintColor={colors.primary}
                      refreshing={refreshing}
                      onRefresh={() => { }}
                    />
                  }
                  data={status1}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({ item, index }) => (
                    <View>

                      <ListThumbSquare
                        onPress={() => { navigation.navigate('Messages'); }}
                        image={item?.requestFrom?.image?.url ? `http://3.111.30.249:1337` + item?.requestFrom?.image?.url : ''}
                        txtLeftTitle={item?.requestFrom?.firstName}
                      // txtContent={item.message}
                      // txtRight={item.date}
                      />

                    </View>
                  )}
                />
              </Neomorph>
            </ScrollView>
          </View>
        </Modal>
      </View >
    )
  }

  return (

    <SafeAreaView
      style={BaseStyle.safeAreaView}
      edges={['right', 'left', 'bottom']}>
      {renderModal()}
      <Neomorph
        inner
        style={{
          shadowRadius: Utils.scaleWithPixel(7),
          borderRadius: Utils.scaleWithPixel(25),
          backgroundColor: colors.neoThemebg,
          width: Utils.scaleWithPixel(395),
          height: Utils.scaleWithPixel(125),
          justifyContent: 'center',
          marginLeft: Utils.scaleWithPixel(5),
          marginTop: Utils.scaleWithPixel(35),

        }}
      >
        <Text style={{ color: "#DDDDDD", left: Utils.scaleWithPixel(15), top: Utils.scaleWithPixel(10), fontSize: 18, fontWeight: 'bold', fontFamily: "ProximaNova" }}> Active now</Text>
        <ScrollView horizontal={true}>
          <View style={{
            flexDirection: "row",

            top: Utils.scaleWithPixel(0),
          }}>
            <FlatList
              horizontal={true}
              // contentContainerStyle={{ paddingHorizontal: 20, paddingVertical: 10 }}
              data={creatorsData}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <MessengerCircle
                  image={item?.backBlazeImageUrl ? item?.backBlazeImageUrl : ''}
                // style={{ marginBottom: Utils.scaleWithPixel(5) }} 
                >
                  <Text bold style={{ top: Utils.scaleWithPixel(20), alignSelf: "center", color: colors.text, fontSize: 16, fontFamily: "ProximaNova" }}>
                    {item?.username}
                  </Text>
                </MessengerCircle>

              )} />
          </View>
        </ScrollView>
      </Neomorph>

      <ScrollView>
        <Neomorph
          inner
          style={{
            shadowRadius: 7,
            borderRadius: 25,
            backgroundColor: colors.neoThemebg,
            width: 385,
            height: 1000,
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 5,
            marginTop: 40,

          }}
        >
          <FlatList
            refreshControl={
              <RefreshControl
                colors={[colors.primary]}
                tintColor={colors.primary}
                refreshing={refreshing}
                onRefresh={() => { }}
              />
            }
            data={chats}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item, index }) => (
              <View>
                <ListThumbSquare

                  image={item?.messageToUser?.backBlazeImageUrl}
                  txtLeftTitle={item?.messageToUser?.username}
                  txtContent={item?.message}
                  messageObject={item}
                  txtRight={item?.createdAt}
                  onPress={() => success(item)}
                />
              </View>
            )}
          />
        </Neomorph>
      </ScrollView>
      <View style={{ alignSelf: "flex-end" }}>
        <NeomorphBlur
          style={{
            shadowRadius: 3,
            borderRadius: 100,
            backgroundColor: colors.neoThemebg,
            width: 60,
            height: 56,

            justifyContent: 'center',
            alignItems: 'center',
            top: -50,
            left: -20
          }}
        >

          <TouchableOpacity style={{ borderRadius: 30, height: 55, width: 55, justifyContent: "center" }} onPress={() => openModal('roomtype')}>
            <Icon name="message-plus" size={28} color="#DDDDDD" style={{ left: 15 }} />
          </TouchableOpacity>

        </NeomorphBlur>
      </View>
    </SafeAreaView>


  );




}

/**
* @description Show when tab Profile activated
* @author Passion UI <passionui.com>
        * @date 2019-08-03
        * @class PreviewTab
        * @extends {Component}
        */
function AudioRoomTab({ navigation }) {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  // const [post3] = useState(PostData3);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const { colors } = useTheme();
  const [refreshing] = useState(false);
  const [messenger] = useState(MessagesData);
  const [status1] = useState(StatusData1);
  const [status] = useState(StatusData);
  const [posts] = useState(PostData1);
  const [notify] = useState(NotificationData);
  const userData = useSelector(state => state.accessTokenReducer.userData)
  const userId = userData?.user?.id
  const audioroomData = useSelector(state => state.AudioroomReducer.audioroomData);
  console.log("audioroomData", audioroomData);
  const [modalVisible, setModalVisible] = useState(false);
  const [shouldShowOpen, setShouldShowOpen] = useState(true);
  const [shouldShowSocial, setShouldShowSocial] = useState(true);
  const [shouldShowClosed, setShouldShowClosed] = useState(true);
  const dispatch = useDispatch();


  const success = async (audioroomUrlObject) => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('AudioRoom', { audioroomUrlObject })
    setLoading(false);

  };



  const [Input, setInput] = useState('');
  const [RoomInput, setRoomInput] = useState('');
  const [Name, setName] = useState('');
  // const [success, setSuccess] = useState({
  //   Name: true,
  // });
  const accessToken = useSelector(state => state.accessTokenReducer.accessToken);
  // console.log('pra.................................', accessToken);

  // useEffect(() => {
  //   dispatch(friendsGetRequest(accessToken));
  //   return () => {
  //     console.log('khilan');
  //   };
  // })

  useEffect(() => {
    dispatch(audioroomGetRequest(accessToken));

  }, [dispatch])

  const audioroompost = async () => {
    if (!Input) {
      alert('please enter your Room name')
    }

    else if (!shouldShowOpen && !shouldShowSocial && !shouldShowClosed) {
      alert('please select one');
    }

    else {
      let objectValue = {};
      objectValue.audioRoomName = Input;
      objectValue.audioRoomType = RoomInput;
      // objectValue.audioRoomAdmins = [userId];
      setLoading(true);
      await dispatch(audioroomPostRequest(objectValue));
      setLoading(false);

    }


  }

  const creatorsData = useSelector(state => state.CategoriesReducer.creatorsData)
  console.log("creatorsData", creatorsData);
  const friendsData = useSelector(state => state.FriendsReducer.friendsData);
  // console.log(' Prathamesh =========>>>', friendsData);

  const openModal = async (modal) => {
    // setModalVisible(modal);
    setModalVisible(modal);
    if (shouldShowOpen === false && shouldShowSocial === true && shouldShowClosed === true) {
      setRoomInput('Open');
    }
    if (shouldShowOpen === true && shouldShowSocial === false && shouldShowClosed === true) {
      setRoomInput('Social');
    }
    if (shouldShowOpen === true && shouldShowSocial === true && shouldShowClosed === false) {
      setRoomInput('Closed');
    }
  };

  const renderModal = () => {
    return (
      <View>
        <Modal
          isVisible={modalVisible === 'roomtype'}
          onSwipeComplete={() => setModalVisible(false)}
          swipeDirection={['down']}
          style={styles.bottomModal}>
          <View style={styles.contain, { backgroundColor: colors.neoThemebg }}>
            <View style={styles.contentSwipeDown}>
              <View style={styles.lineSwipeDown} />
            </View>
            <Text style={{ fontSize: 20, color: "#DDDDDD", padding: 30, left: 90, fontFamily: "ProximaNova" }}> Choose Room Type </Text>

            <Neomorph
              inner
              style={{
                shadowRadius: 7,
                borderRadius: 20,
                backgroundColor: colors.neoThemebg,
                width: 370,
                height: 70,
                // justifyContent: 'center',
                // alignItems: 'center',
                left: 15,
                top: 10,
                // margin: 10,
                marginBottom: 10
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <TextInput
                  style={{
                    marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(60),
                  }}
                  onChangeText={text => { setInput(text) }}
                  value={Input}
                  placeholder='Enter Your Room Name'
                />
              </View>
            </Neomorph>
            <View
              style={{
                marginTop: 20,
                flexDirection: "row",
                alignItems: "center",
                left: 10
              }}>
              {shouldShowOpen ? (
                <Neomorph
                  outer
                  style={{
                    shadowRadius: 7,
                    borderRadius: 20,
                    backgroundColor: colors.neoThemebg,
                    width: 105,
                    height: 105,
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      setShouldShowOpen(false),
                        setShouldShowSocial(true),
                        setShouldShowClosed(true)
                      setRoomInput('Open')
                    }}>
                    <IconSimple name="globe" size={40} color="#DDDDDD" style={{ marginTop: -2, margin: 5 }} />
                    <Text style={{ fontSize: 20, color: "#DDDDDD", marginLeft: 5, fontFamily: "ProximaNova" }}>Open </Text>
                  </TouchableOpacity>
                </Neomorph>)
                :
                (<Neomorph
                  inner
                  style={{
                    shadowRadius: 7,
                    borderRadius: 20,
                    backgroundColor: colors.neoThemebg,
                    width: 105,
                    height: 105,
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10
                  }}
                >
                  <TouchableOpacity
                    onPress={() => setShouldShowOpen(!shouldShowOpen)}>
                    <IconSimple name="globe" size={40} color="#008cff" style={{ marginTop: -2, margin: 5 }} />
                    <Text style={{ fontSize: 20, color: "#DDDDDD", marginLeft: 5, fontFamily: "ProximaNova" }}>Open </Text>
                  </TouchableOpacity>
                </Neomorph>)}
              {shouldShowSocial ? (
                <Neomorph
                  outer
                  style={{
                    shadowRadius: 7,
                    borderRadius: 20,
                    backgroundColor: colors.neoThemebg,
                    width: 105,
                    height: 105,
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10

                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      setShouldShowOpen(true),
                        setShouldShowSocial(false),
                        setShouldShowClosed(true)
                      setRoomInput('Social')
                    }}
                  >
                    <Icon name="account-group-outline" size={50} color="#DDDDDD" style={{ marginTop: -2, margin: 5, marginLeft: 10 }} />
                    <Text style={{ fontSize: 20, color: "#DDDDDD", marginLeft: 10, fontFamily: "ProximaNova" }}>Social</Text>
                  </TouchableOpacity>

                </Neomorph>
              ) : (<Neomorph
                inner
                style={{
                  shadowRadius: 7,
                  borderRadius: 20,
                  backgroundColor: colors.neoThemebg,
                  width: 105,
                  height: 105,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 10
                }}
              >
                <TouchableOpacity
                  onPress={() => setShouldShowSocial(!shouldShowSocial)}
                >
                  <Icon name="account-group-outline" size={50} color="#008cff" style={{ marginTop: -2, margin: 5, marginLeft: 10 }} />
                  <Text bold style={{ fontSize: 20, color: "#6a6c6e", marginLeft: 10, fontFamily: "ProximaNova" }}>Social</Text>
                </TouchableOpacity>
              </Neomorph>)}
              {shouldShowClosed ? (
                <Neomorph
                  outer
                  style={{
                    shadowRadius: 7,
                    borderRadius: 20,
                    backgroundColor: colors.neoThemebg,
                    width: 105,
                    height: 105,
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      setShouldShowOpen(true),
                        setShouldShowSocial(true),
                        setShouldShowClosed(false)
                      setRoomInput('Closed')
                    }}
                  >
                    <Icon name="shield-lock-outline" size={50} color="#DDDDDD" style={{ marginTop: -2, margin: 5, marginLeft: 10 }} />
                    <Text style={{ fontSize: 20, color: "#DDDDDD", marginLeft: 10, fontFamily: "ProximaNova" }}>Closed</Text>
                  </TouchableOpacity>
                </Neomorph>) : (<Neomorph
                  inner
                  style={{
                    shadowRadius: 7,
                    borderRadius: 20,
                    backgroundColor: colors.neoThemebg,
                    width: 105,
                    height: 105,
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10
                  }}
                >
                  <TouchableOpacity
                    onPress={() => setShouldShowClosed(!shouldShowClosed)}
                  >
                    <Icon name="shield-lock-outline" size={50} color="#008cff" style={{ marginTop: -2, margin: 5, marginLeft: 10 }} />
                    <Text style={{ fontSize: 20, color: "#DDDDDD", marginLeft: 10, fontFamily: "ProximaNova" }}>Closed</Text>
                  </TouchableOpacity>
                </Neomorph>)}
            </View>
            <Text bold style={{ fontSize: 19, color: "#DDDDDD", padding: 15, marginTop: 20, left: 50, fontFamily: "ProximaNova" }}> Create a room with people I follow </Text>
            <View style={{ marginTop: 50 }}>
              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 100,
                  backgroundColor: colors.neoThemebg,
                  width: 200,
                  height: 56,

                  justifyContent: 'center',
                  alignItems: 'center',
                  top: -30,
                  left: 100
                }}
              >
                <TouchableOpacity
                  onPress={() => audioroompost(openModal('roomwithfriends'))}
                >
                  <Text body1 bold style={{ fontSize: 20, color: "white", fontFamily: "ProximaNova" }}>
                    Start a Room
                  </Text>
                </TouchableOpacity>
              </NeomorphBlur>
            </View>
          </View>
        </Modal >
        <Modal
          isVisible={modalVisible === 'roomwithfriends'}
          onSwipeComplete={() => setModalVisible(false)}
          swipeDirection={['down']}
          style={styles.bottomModal1}>
          <View style={styles.contain1, { backgroundColor: colors.neoThemebg }}>
            <View style={{
              alignItems: "center",
              left: -80,
              height: 500
            }}
            >
              <View style={styles.contentSwipeDown, { left: 80, marginTop: 10 }}>
                <View style={styles.lineSwipeDown} />
              </View>
              <View style={{ flexDirection: "row" }}>
                <Icon name="chevron-left" size={32} color={colors.text} style={{ left: -20, top: 20 }} onPress={() => openModal('roomtype')} />
                <Text bold style={{ fontSize: 20, color: colors.text, left: 60, top: 20 }}>Start Room with</Text>
              </View>
              <View style={{
                // marginTop: -10,
                flexDirection: "row",
                alignItems: "center",
                left: 120,
                height: 500,
                paddingTop: 30
              }}>
                <View style={{
                  marginTop: 100,
                  margin: 60,
                  top: -120,
                }}>

                  <View style={{ left: -30, bottom: 10, top: 5, paddingTop: 20, }}>
                    <FlatList
                      // horizontal={true}
                      numColumns={4}
                      data={creatorsData}

                      keyExtractor={(item, index) => item.id}
                      renderItem={
                        ({ item, index }) => (
                          <View>
                            <ListThumbnails1
                              creatorsObj={[item]}
                              image={item?.backBlazeImageUrl}
                              style={{ paddingLeft: -10, left: 10 }}
                            />
                          </View>
                        )}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View
            // style={{ marginTop: -10, marginLeft: 75, marginBottom: 20 }}
            style={{
              marginTop: 0,
              flexDirection: "row",
              alignItems: "center",
              left: 70,
              height: 180,
              backgroundColor: colors.neoThemebg
            }}
          >
            <NeomorphBlur
              style={{
                shadowRadius: 3,
                borderRadius: 100,
                backgroundColor: colors.neoThemebg,
                width: 200,
                height: 56,
                justifyContent: 'center',
                alignItems: 'center',
                top: -30,
                left: 100
              }}
            >
              <TouchableOpacity
                onPress={() => success(objectValue)}
              >
                <Text body1 bold style={{ fontSize: 20, color: colors.text }}>
                  Get started
                </Text>
              </TouchableOpacity>
            </NeomorphBlur>
          </View>
        </Modal>
      </View >
    )
  }
  const { width, height } = Dimensions.get('window');

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        {renderModal()}
        <View style={{
          flexDirection: "row",
          top: 5,
          justifyContent: "space-evenly"
        }}>

        </View>
        <Neomorph
          inner
          style={{
            shadowRadius: 7,
            borderRadius: 25,
            backgroundColor: colors.neoThemebg,
            width: 395,
            height: 165,
            // justifyContent: 'center',
            marginLeft: 5,
            marginTop: 25,

          }}
        >
          <Text style={{ color: "#DDDDDD", left: 10, top: 0, fontSize: 20, fontWeight: "bold" }}> Active Rooms</Text>
          {/* <ScrollView horizontal={true}> */}
            <View style={{ flexDirection: "row", }}>
              <FlatList
                data={audioroomData}
                horizontal={true}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <ProfileGroup
                    style={{ margin: 15,left:0 }}
                    image={item?.audioRoomPeople?.backBlazeImageUrl}
                    detail={item?.audioRoomName}
                    audioroom={item}
                  />
                )}
              />
            </View>
          {/* </ScrollView> */}
        </Neomorph>

        <SafeAreaView
          style={BaseStyle.safeAreaView}
          edges={['right', 'left', 'bottom']}>
          <ScrollView vertical={true}>
            <Neomorph
              inner
              style={{
                shadowRadius: 7,
                borderRadius: 25,
                backgroundColor: colors.neoThemebg,
                width: 385,
                height: height * 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 5,
                marginTop: 30,

              }}
            >

              <View style={{ flex: 1, }}>
                <FlatList
                  data={audioroomData}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({ item, index }) => (
                    <AudioGroup
                      onPress={() => success(item)}
                      detail={item?.audioRoomName}
                      image={item?.audioRoomPeople?.backBlazeImageUrl}
                      audioroomObj={item}
                      name={item?.username}
                    />
                  )}
                />
              </View>
            </Neomorph>
          </ScrollView>

        </SafeAreaView>

        <NeomorphBlur
          style={{
            shadowRadius: 3,
            borderRadius: 100,
            backgroundColor: colors.neoThemebg,
            width: 60,
            height: 56,

            justifyContent: 'center',
            alignItems: 'center',
            top: -50,
            left: 320
          }}
        >
          <TouchableOpacity style={{ borderRadius: 30, height: 55, width: 55, justifyContent: "center" }} onPress={() => openModal('roomtype')}>
            <Icon name="account-multiple-plus-outline" size={28} color="#878b8e" style={{ left: 15 }} />
          </TouchableOpacity>
        </NeomorphBlur>
      </SafeAreaView>
    </View >
  );
}