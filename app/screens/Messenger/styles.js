import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import { fontSize } from 'styled-system';
import * as Utils from '@utils';

export default StyleSheet.create({
    image: {
        height: Utils.scaleWithPixel(70),
        width: Utils.scaleWithPixel(70),
        borderRadius: Utils.scaleWithPixel(70),
       
               
    },
    tabbar: {
        height: 40,
        MarginTop:15,
        // top:-5
        
      },
      tab: {
        width: 175,
        paddingLeft:0,
        color:BaseColor.blueColor
      },
      indicator: {
        height: 1,
        width:115,
       left:45,
      },
      label: {
        fontWeight: '400',
      },
      bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
      },
      contain: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        flex: 1,
        marginTop:200,
      },
      textInput: {
        height: 46,
        backgroundColor: BaseColor.backgroundColor,
        borderRadius: 5,
        marginTop: 14,
        marginLeft: 15,
        padding:10,
        width: 370,
        height: 48
      },
      Icon: {
        marginTop: -20,
        color: "#FFBF00"
      },
      Image: {
          height: 80,
          width: 80,
          borderRadius: 70,
          top:10  
      },
      contain1: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        flex: 1,
        marginTop:200
      },
      bottomModal1: {
        justifyContent: 'flex-end',
        margin: -70,
      },
      contentSwipeDown: {
        paddingTop: 10,
        alignItems: 'center',
      },
      lineSwipeDown: {
        width: 50,
        height: 2.5,
        backgroundColor: BaseColor.whiteColor,
      },
});
