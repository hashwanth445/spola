import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  textInput: {
    height: Utils.scaleWithPixel(46),
    backgroundColor: BaseColor.fieldColor,
    borderRadius: Utils.scaleWithPixel(5),
    marginTop: Utils.scaleWithPixel(65),
    padding: Utils.scaleWithPixel(10),
    width: '100%',
  },
});
