import React, { useState } from 'react';
import { View, KeyboardAvoidingView, Platform } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, TextInput, Button } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';

export default function ResetPassword({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('')
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState({ confirmPassword: true });

  /**
   * call when action reset pass
   */
  const onReset = () => {
    if (confirmPassword == '') {
      setSuccess({
        ...success,
        password: false,
        confirmPassword: false
      });
    } else {
      setLoading(true);
      setTimeout(() => {
        setLoading(false);
        navigation.navigate('SignIn');
      }, 500);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('reset_password')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              padding: Utils.scaleWithPixel(20),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>

              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(380),
                  height: Utils.scaleWithPixel(70),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-5), marginLeft: Utils.scaleWithPixel(-80), backgroundColor: colors.neoThemebg, width: Utils.scaleWithPixel(280),
                    height: Utils.scaleWithPixel(50),
                  }}
                  onChangeText={text => setPassword(text)}
                  onFocus={() => {
                    setSuccess({
                      ...success,
                      password: true,
                    });
                  }}
                  placeholder={t('input_password')}
                  success={success.password}
                  value={password}
                />
              </Neomorph>
            </View>
            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(380),
                  height: Utils.scaleWithPixel(70),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-5), marginLeft: Utils.scaleWithPixel(-80), backgroundColor: colors.neoThemebg, width: Utils.scaleWithPixel(280),
                    height: Utils.scaleWithPixel(50),
                  }}
                  onChangeText={text => setConfirmPassword(text)}
                  onFocus={() => {
                    setSuccess({
                      ...success,
                      confirmPassword: true,
                    });
                  }}
                  placeholder={t('password_confirm')}
                  success={success.confirmPassword}
                  value={confirmPassword}
                />
              </Neomorph>
            </View>
            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>
              <Neomorph
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(66),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Neomorph
                  style={{
                    width: Utils.scaleWithPixel(300),
                    height: Utils.scaleWithPixel(65),
                    borderRadius: Utils.scaleWithPixel(5),
                    // borderWidth: 2,
                    borderColor: colors.neoThemeBorderColor,
                    backgroundColor: colors.neoThemebg,
                    justifyContent: 'center',
                    alignItems: 'center',

                  }}
                >
                  <Button
                    style={{backgroundColor: colors.neoThemebg }}
                    full
                    onPress={() => {
                      onReset();
                    }}
                    loading={loading}>
                    {t('reset_password')}
                  </Button>

                </Neomorph>
              </Neomorph>
            </View>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View >
  );
}
