import React, { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  FlatList,
  Animated,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { BaseColor, Images, useTheme } from '@config';
import { useDispatch, useSelector } from 'react-redux';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  ProfileGroup,
  Tag,
  Image,
  Button,
  Card,
  Product,
  ListThumbSquare,
  TextInput
} from '@components';
import Modal from 'react-native-modal';
import { useTranslation } from 'react-i18next';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import * as Utils from '@utils';
import styles from './styles';
// import { ProductsData } from '@data';
import { NeomorphBlur, Neomorph } from 'react-native-neomorph-shadows';
import { productsGetRequest } from '../../api/products';
import { flexDirection } from 'styled-system';
import { ordersGetRequest, ordersPostRequest } from '../../api/orders';
import { cartsPostRequest } from '../../api/carts';
import {addressGetRequest} from '../../api/address';

var ID = require("nodejs-unique-numeric-id-generator")

export default function ProductDetail({ navigation, route }) {
  const deltaY = new Animated.Value(0);
  const heightImageBanner = Utils.scaleWithPixel(250, 1);
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [modalVisible, setModalVisible] = useState(false);
  const [address, setAddress] = useState("");
  console.log(ID.generate(new Date().toJSON()));
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);

  useEffect(() => {
    dispatch(ordersGetRequest(userToken));
  }, [dispatch]);
  useEffect(() => {
    dispatch(addressGetRequest(userId));
  }, [dispatch]);

  const UserData = useSelector(state => state.accessTokenReducer.userData)
  console.log("UserData", UserData);
  const userId = UserData?.user?.id;
  const orders = useSelector(state => state.OrdersReducer.ordersData);
  console.log("Orders", orders);

  const Address = useSelector(state => state.AddressReducer.addressData);
  console.log("address",Address);

  const carts = useSelector(state => state.cartsReducer.cartsDataForUser)
  console.log("carts", carts);

  const openModal = (modal) => {
    setModalVisible(modal);
  };

  const addressAdd = () => {
    alert("hello");
  }

  const [success, setSuccess] = useState({
    Address: true,
  });

  //   const productToCart = async (productObject) => {


  //     setLoading(true);
  //     // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
  //     navigation.navigate('Carts', { productObject })
  //     setLoading(false);

  // };

  const [add, setAdd] = useState(false)
  const cartPage = async () => {
    // console.log("commentPressed");
    // if (!order) {
    //   // alert('Order Placed')

    // }
    // else 
    {
      let objectValue = {};
      objectValue.productId = productId;
      objectValue.userId = userId
      setAdd(true);
      setLoading(true);
      // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
      await dispatch(cartsPostRequest(objectValue));
      setLoading(false);
      // navigation.navigate('Carts',{ productObject })

    }

  };

  const renderModal = () => {
    return (
      <View>
        <Modal
          isVisible={modalVisible === 'roomtype'}
          onSwipeComplete={() => setModalVisible(false)}
          swipeDirection={['down']}
          backdropColor="transparent"
          // transparent={true}
          onBackdropPress={() => setModalVisible(false)}
          style={styles.bottomModal}>
          <View style={styles.contain, { backgroundColor: colors.neoThemebg, borderTopLeftRadius: 30, borderTopRightRadius: 30,}}>
            <Neomorph
              outer
              style={{
                shadowRadius: 7,
                borderRadius: 20,
                backgroundColor: colors.neoThemeBorderColor,
                width: 390,
                height: 400,
                // justifyContent: 'center',
                borderTopLeftRadius: 80,
                borderTopRightRadius: 80,
                // overflow: 'hidden',
                // alignItems: 'center',

              }}>
              <View style={styles.contentSwipeDown}>
                <View style={styles.lineSwipeDown} />
              </View>
              <View style={{ left: 0 }}>
                <Text style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", alignSelf: "center", top: Utils.scaleWithPixel(10) }}>Quick Purchase</Text>
                <View>
                  <Text grayColor style={{ fontSize: 20, fontFamily: "ProximaNova", left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(22) }}>Ship To :</Text>

                  <Text bold style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(35) }}>{Address[0]?.address},{Address[0]?.pincode}</Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Address')} style={{alignItems:"center"}}>
                  <NeomorphBlur
                    style={{
                      shadowRadius: 3,
                      borderRadius: 100,
                      // left: Utils.scaleWithPixel(240),
                      top: Utils.scaleWithPixel(50),
                      backgroundColor: colors.neoThemeBorderColor,
                      width: 150,
                      height: 45,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                      <Text style={{ alignSelf: "center", fontSize: 20, fontFamily: "ProximaNova" }}>Add Address</Text>
                  </NeomorphBlur>
                </TouchableOpacity>
                
                {balance < 0 ? (
                  <View>
                    {/* <Text style={{textAlign: 'center', fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", fontWeight: "bold", top: Utils.scaleWithPixel(15) }}>{t('rewardscreen')}</Text> */}
                    <View style={{ marginTop: 40, marginBottom: 30 }}>
                      <Text grayColor bold style={{ fontSize: 20, fontFamily: "ProximaNova", left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(20) }}>Total</Text>
                      <Text bold style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(20) }}>Rs. {ProductPrice}</Text>
                    </View>
                  </View>
                ) : (
                  <View>
                    <View style={{ borderColor: colors.primaryDark, borderRadius: 20, borderWidth: 1, height: 60, width: 360, top: 70, marginBottom: 10, flexDirection: "row" }}>
                      <Text style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", fontWeight: "bold", left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(15) }}>Using {ProductRequiredRewardedPoints} Spola coins (Balance = <Text bold style={{ color: BaseColor.greenColor, fontFamily: "ProximaNova" }}>{balance}</Text>)</Text>
                      <Icon name="close" size={20} color="white" style={{ alignSelf: "center", marginLeft: 60 }} />
                    </View>
                    <View style={{ marginTop: 40, marginBottom: 30 }}>
                      <Text grayColor bold style={{ fontSize: 20, fontFamily: "ProximaNova", left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(20) }}>Total</Text>

                      <Text semibold style={{ fontSize: 20, color: "white", fontFamily: "ProximaNova", left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(20) }}>Rs. {Total}</Text>
                      <Text style={{ fontSize: 20, color: "white", fontFamily: "ProximaNova", textDecorationLine: 'line-through', left: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(20) }}>Rs. {ProductPrice}</Text>
                    </View>
                  </View>
                )}
              </View>
              {add ?
                (<View style={{alignItems:"center"}}>
                  <TouchableOpacity onPress={() => navigation.navigate('Carts')}>
                    <NeomorphBlur
                      style={{
                        shadowRadius: 3,
                        borderRadius: 100,
                        backgroundColor: colors.neoThemeBorderColor,
                        width: 170,
                        height: 65,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    > 
                    <Text bold style={{ fontSize: 20, color: "white", fontFamily: "ProximaNova", }}>View Cart</Text>
                    </NeomorphBlur>
                  </TouchableOpacity>
                </View>
                ) :
                <TouchableOpacity onPress={() => cartPage()} style={{ alignSelf: "center" }}>
                  <NeomorphBlur
                    style={{
                      shadowRadius: 3,
                      borderRadius: 100,
                      backgroundColor: colors.neoThemeBorderColor,
                      width: 170,
                      height: 65,
                      position: 'relative',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    {loading ? 
                    <ActivityIndicator size="small" color="#0000ff" /> :
                    <Text style={{ fontSize: 24, fontFamily: "ProximaNova", }}>Add to cart</Text>}
                  </NeomorphBlur>
                </TouchableOpacity>
              }
            </Neomorph>
          </View>
        </Modal >
      </View >

    )
  }
  const dispatch = useDispatch();


  useEffect(() => {
    dispatch(productsGetRequest());

  }, [dispatch])

  const productsData = useSelector(state => state.ProductsReducer.productsData)
  const [loading, setLoading] = useState(false)
  const product = async (productObject) => {


    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('ProductDetail', { productObject })
    setLoading(false);

  };

  const { productObject } = route.params;
  const productId = productObject?.id
  console.log("productObject", productId);
  const productImage = productObject?.backBlazeProductUrl;
  const ProductName = productObject?.productName;
  const ProductDesc = productObject?.productDescription;
  const ProductPrice = Number(productObject?.productPrice);
  const userRewardedPoints = Number(UserData?.user?.rewardedPoints);
  const ProductRequiredRewardedPoints = Number(productObject?.productRequiredRewardedPoints);
  const balance = userRewardedPoints - ProductRequiredRewardedPoints

  const Total = ProductPrice - ProductRequiredRewardedPoints;


  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const [region] = useState({
    latitude: 1.352083,
    longitude: 103.819839,
    latitudeDelta: 0.009,
    longitudeDelta: 0.004,
  });
  const [facilities] = useState([
    { id: '1', icon: 'wifi', name: 'Free Wifi', checked: true },
    { id: '2', icon: 'shower', name: 'Shower' },
    { id: '3', icon: 'paw', name: 'Pet Allowed' },
    { id: '4', icon: 'bus', name: 'Shuttle Bus' },
    { id: '5', icon: 'cart-plus', name: 'Supper Market' },
    { id: '6', icon: 'clock', name: 'Open 24/7' },
  ]);
  const [relate] = useState([
    {
      id: '0',
      image: Images.event4,
      title: 'BBC Music Introducing',
      time: 'Thu, Oct 31, 9:00am',
      location: 'Tobacco Dock, London',
    },
    {
      id: '1',
      image: Images.event5,
      title: 'Bearded Theory Spring Gathering',
      time: 'Thu, Oct 31, 9:00am',
      location: 'Tobacco Dock, London',
    },
  ]);

  return (
    <View style={{ flex: 1 }}>
      {renderModal()}
      <Header
        title=""
        renderLeft={() => {
          return (
            <Icon
              name="chevron-left"
              size={35}
              style={{ color: colors.text }}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />

      <Neomorph
        inner
        style={{
          shadowRadius: 7,
          borderRadius: 10,
          backgroundColor: colors.neoThemebg,
          width: 350,
          height: 220,
          marginTop: 0,
          marginLeft: 20,
          justifyContent: 'center',
        }}>

        <Image source={{ uri: productImage ? productImage : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg' }} style={{ borderRadius: 10, height: 190, width: 330, alignSelf: "center" }} />
      </Neomorph>
      <Animated.View
        style={{
          position: 'absolute',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'flex-end',
          paddingHorizontal: Utils.scaleWithPixel(20),
          width: '100%',
          bottom: Utils.scaleWithPixel(15),
          opacity: deltaY.interpolate({
            inputRange: [
              0,
              Utils.scaleWithPixel(140),
              Utils.scaleWithPixel(140),
            ],
            outputRange: [1, 0, 0],
          }),
        }}>
        <View style={styles.rowBanner}>
          {/* <Image source={Images.profile2} style={styles.userIcon} /> */}
          <View style={{ alignItems: 'flex-start' }}>
            {/* <Text title1 bold whiteColor style={{ fontSize: 24 }}>
                {ProductName}
              </Text> */}
            {/* <Text footnote whiteColor>
                5 {t('hour_ago')} | 100k {t('views')}
              </Text> */}
          </View>
        </View>
        {/* <Tag rateSmall>{t('sold_out')}</Tag> */}
      </Animated.View>
      {/* </Animated.View> */}
      {/* Header */}

      <SafeAreaView style={{ flex: 1 }} edges={['right', 'left', 'bottom']}>
        <ScrollView
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: { y: deltaY },
              },
            },
          ])}
          onContentSizeChange={() => {
            setHeightHeader(Utils.heightHeader());
          }}
          scrollEventThrottle={8}>
          <View style={{ height: Utils.scaleWithPixel(95) - heightHeader }} />
          <View
            style={{
              paddingHorizontal: Utils.scaleWithPixel(35),
              marginBottom: Utils.scaleWithPixel(20),
              top: -60,
              left: -10,
              flexDirection: "row"
            }}>
            <View style={{ flexDirection: "column" }}>

              <Text style={{ fontFamily: "ProximaNova", fontSize: 15 }}>
                Reward Points Required : {ProductRequiredRewardedPoints}
              </Text>
              <Text title3 bold style={{ fontFamily: "ProximaNova", marginTop: Utils.scaleWithPixel(10), marginBottom: Utils.scaleWithPixel(5), }}>
                {t('description')}
              </Text>
              <Text grayColor lineHeight={20} style={{ fontFamily: "ProximaNova", fontSize: 18 }}>
                {ProductDesc}
              </Text>
            </View>
            {/* <View style={{ alignContent: "flex-end", marginLeft: -40 }}>
              <Text bold style={{ fontSize: 20, color: "#DDDDDD", fontFamily: "ProximaNova" }}>Rs. {Total}</Text>
              <Text style={{ fontSize: 20, color: "#DDDDDD", fontFamily: "ProximaNova", textDecorationLine: 'line-through', }}>Rs. {ProductPrice}</Text>
            </View> */}

          </View>
          <Text
            title3
            bold
            style={{
              marginTop: Utils.scaleWithPixel(-30),
              marginLeft: 10,
              marginBottom: Utils.scaleWithPixel(-50),
              fontFamily: "ProximaNova"
            }}>
            {t('you_may_also_like')}
          </Text>
          <FlatList
            contentContainerStyle={{
              // paddingLeft: 5,
              // paddingRight: 20,
              marginTop: Utils.scaleWithPixel(60),
            }}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={productsData}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item, index }) => (
              <Product
                prouctsObject={item}
                image={item?.backBlazeProductUrl ? item?.backBlazeProductUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                // style={{ margin: Utils.scaleWithPixel(0), height: Utils.scaleWithPixel(180), width: Utils.scaleWithPixel(120), left: Utils.scaleWithPixel(0), marginRight: 50 }}
                onPress={() => {
                  product(item)
                }}>
                <View style={{ marginRight: 20, top: -10 }}>
                  <Text bold grayColor style={{ fontFamily: "ProximaNova", fontSize: 18, }}>
                    {item.productName}
                  </Text>
                  <Text body1 style={{ fontFamily: "ProximaNova", fontSize: 20 }}>
                    Extra {item?.productId}% Off {'\n'}on {item.productName}
                  </Text>
                </View>

                <View style={styles.contentCartPromotion}>
                </View>
              </Product>
            )}
          />
        </ScrollView>
        {/* Pricing & Booking Process */}
        <View
          style={[styles.contentButtonBottom, { borderTopColor: colors.border }]}>
          <View>
            <Text headline semibold grayColor style={{ fontFamily: "ProximaNova" }}>
              {t('Price')}
            </Text>
            <Text title3 whiteColor bold>
              Rs. {ProductPrice}
            </Text>
          </View>
          <NeomorphBlur
            style={{
              shadowRadius: 3,
              borderRadius: 100,
              backgroundColor: colors.neoThemebg,
              width: 135,
              height: 55,

              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <TouchableOpacity onPress={() => openModal('roomtype')}>
              <Text style={{ fontFamily: "ProximaNova", fontSize: 18 }}>
                {t('book_now')}
              </Text>
            </TouchableOpacity>
          </NeomorphBlur>
        </View>
      </SafeAreaView>
    </View>
  );
}
