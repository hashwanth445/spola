import {StyleSheet} from 'react-native';
import * as Utils from '@utils';
import {BaseColor} from '@config';

export default StyleSheet.create({
  imgBanner: {
    width: '100%',
    height: Utils.scaleWithPixel(250),
    position: 'absolute',
  },
  rowBanner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userIcon: {
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    borderWidth: Utils.scaleWithPixel(1),
    borderColor: 'white',
    marginRight: Utils.scaleWithPixel(5),
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  contentButtonBottom: {
    borderTopWidth: Utils.scaleWithPixel(1),
    paddingVertical: Utils.scaleWithPixel(10),
    paddingHorizontal: Utils.scaleWithPixel(20),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemPrice: {
    borderBottomWidth: Utils.scaleWithPixel(1),
    paddingVertical: Utils.scaleWithPixel(10),
    alignItems: 'flex-start',
  },
  linePrice: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  iconRight: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapContent: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    borderBottomWidth: Utils.scaleWithPixel(1),
    paddingBottom: Utils.scaleWithPixel(20),
  },
  contentCartPromotion: {
    marginTop: Utils.scaleWithPixel(10),
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin:0,
    
  },
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    flex: 1,
    // marginTop:150,
  },
  contain1: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    flex: 1,
    marginTop:0,
  },
  contentSwipeDown: {
    paddingTop: 10,
    alignItems: 'center',
  },
  lineSwipeDown: {
    width: 50,
    height: 2.5,
    backgroundColor: BaseColor.whiteColor,
  },
});
