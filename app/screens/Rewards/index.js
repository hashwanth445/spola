import React, { useState, useEffect } from 'react'
import { Neomorph } from 'react-native-neomorph-shadows';
import {
    View,
    ScrollView,
    Animated,
    TouchableOpacity,
    FlatList,
    RefreshControl,
    Platform,
    Dimensions
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import {
    Image,
    Header,
    HeaderHome,
    Text,
    Icon,
    HotelItem,
    Card,
    Button,
    SafeAreaView,
    EventCard,
    EventItem,
    FilterSort,
    CardRewards,
    Product1
} from '@components';
import { BaseStyle, Images, BaseColor, useTheme } from '@config';
import styles from './styles';
import { RewardData, TourData, TrendingData, HotelData, RewardData1 } from '@data';
import { useTranslation } from 'react-i18next';
import * as Utils from '@utils';
import { productsGetRequest } from '../../api/products';
// import { CardRewards } from '../../components';


export default function Rewards({ navigation, route }) {
    const { t } = useTranslation();
    const { colors } = useTheme();
    const [loading, setLoading] = useState(false);
    const [reward] = useState(RewardData);
    const [reward1] = useState(RewardData1);
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(productsGetRequest());

    }, [dispatch])
    const productsData = useSelector(state => state.ProductsReducer.productsData)
    const UserData = useSelector(state => state.accessTokenReducer.userData)

    const product = async (productObject) => {


        setLoading(true);
        // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
        navigation.navigate('ProductDetail', { productObject })
        setLoading(false);

    };
    const { profileDataObject } = route.params
    const rewardPoints = profileDataObject[2]?.value;
    console.log(profileDataObject);
    return (
        <SafeAreaView style={{ flex: 2 }} edges={['right', 'left', 'bottom']}>

            {/* <View style={{ flexDirection: "row", backgroundColor: "#131617", height: 130 }}>
                <Text semibold style={{ left: 20, fontSize: 22, top: 85, margin: 10 }}>Rewards</Text>

            </View> */}
            <ScrollView showsVerticalScrollIndicator={true}>
                <View style={{ top: Utils.scaleWithPixel(-40), left: Utils.scaleWithPixel(10), }}>
                    <View style={{ flexDirection: "row", left: Utils.scaleWithPixel(10), justifyContent: "space-around" }}>
                        <Text semibold style={{ left: Utils.scaleWithPixel(0), fontSize: 18, top: Utils.scaleWithPixel(90), fontFamily: "ProximaNova" }}>Rewarded Points : </Text>
                        <Neomorph
                            inner
                            style={{
                                shadowRadius: Utils.scaleWithPixel(7),
                                borderRadius: Utils.scaleWithPixel(15),
                                backgroundColor: colors.neoThemebg,
                                width: Utils.scaleWithPixel(105),
                                height: Utils.scaleWithPixel(45),
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: Utils.scaleWithPixel(80),
                                marginBottom: Utils.scaleWithPixel(30),
                            }}
                        >
                            <Icon name="currency-inr" size={20} color="white" style={{ left: Utils.scaleWithPixel(-35), top: Utils.scaleWithPixel(9) }} />
                            <Text semibold style={{ left: Utils.scaleWithPixel(0), fontSize: 18, top: Utils.scaleWithPixel(-10), fontFamily: "ProximaNova" }}>{rewardPoints}</Text>

                        </Neomorph>
                    </View>

                    <Neomorph
                        inner
                        style={{
                            padding: Utils.scaleWithPixel(20),
                            shadowRadius: Utils.scaleWithPixel(7),
                            borderRadius: Utils.scaleWithPixel(20),
                            backgroundColor: colors.neoThemebg,
                            width: Utils.scaleWithPixel(280),
                            height: Utils.scaleWithPixel(330),
                            justifyContent: 'center',
                            alignItems: 'center',
                            left: Utils.scaleWithPixel(20)
                        }}
                    >

                        <Image source={{ uri: productsData[2]?.backBlazeProductUrl ? productsData[2]?.backBlazeProductUrl : '' }} style={styles.promotionBanner} >
                            <Text bold grayColor style={{fontFamily:"ProximaNova",alignSelf:"flex-start",fontSize:20, top:-30,left:10}}>{productsData[2]?.productName}</Text>
                            <Text semibold grayColor style={{fontFamily:"ProximaNova",alignSelf:"flex-start",fontSize:18, top:-30,left:10}}> Reward Points Required : {productsData[2]?.productRequiredRewardedPoints}</Text>
                        </Image>
                        <View style={[styles.line, { backgroundColor: colors.border }]} />
                        {/* </View> */}
                    </Neomorph>
                    {/* <Neomorph
                        inner
                        style={{
                            padding: 20,
                            paddingLeft:20,
                            shadowRadius: 7,
                            borderRadius: 0,
                            backgroundColor: colors.neoThemebg,
                            width: Utils.scaleWithPixel(380),
                            height: Utils.scaleWithPixel(250),
                            justifyContent: 'center',
                            alignItems: 'center',
                            top: 30,
                            // marginLeft:20
                        }}
                    >
                        <Text>Rewards</Text>

                        <FlatList

                            // horizontal={true}
                            
                            showsHorizontalScrollIndicator={false}
                            data={promotion}
                            keyExtractor={(item, index) => item.id}
                            renderItem={({ item, index }) => (

                                <Card
                                    image={item.image}
                                    style={{height:180,width:150}}
                                    onPress={() => navigation.navigate('HotelDetail')}>

                                </Card>
                            )}
                        />
                    </Neomorph> */}

                    <Text style={styles.text}>New jackpots everyday at 7PM</Text>

                    <Neomorph
                        inner
                        style={{
                            padding: Utils.scaleWithPixel(20),
                            shadowRadius: Utils.scaleWithPixel(7),
                            borderRadius: Utils.scaleWithPixel(30),
                            backgroundColor: colors.neoThemebg,
                            width: Utils.scaleWithPixel(315),
                            height: Utils.scaleWithPixel(1315),
                            
                            top: Utils.scaleWithPixel(50)
                        }}
                    >
                        <View style={{ flexDirection: "row" }}>
                            <ScrollView vertical>
                                <FlatList
                                    grid
                                    numColumns={2}
                                    // vertical={true}
                                    // showsHorizontalScrollIndicator={false}
                                    data={productsData}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={({ item, index }) => (

                                        <Product1
                                            prouctsObject={item}
                                            image={item?.backBlazeProductUrl ? item?.backBlazeProductUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                            style={{ height: Utils.scaleWithPixel(180), width: Utils.scaleWithPixel(120), }}
                                            onPress={() => {
                                                product(item)
                                            }}
                                        >
                                            <View style={{ justifyContent: "center", }}>
                                                <Text title2 bold whiteColor style={{ top: -20, fontFamily: "ProximaNova", fontSize: 18 }}>
                                                    {item.productName}
                                                </Text>
                                                <Text footnote bold style={{ alignSelf: "center", top: -20, fontFamily: "ProximaNova" }}>
                                                    Reward Points Required : {item.productRequiredRewardedPoints}
                                                </Text>
                                            </View>

                                        </Product1>
                                    )}
                                />
                            </ScrollView>
                            {/* <FlatList

                                // horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={reward}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (

                                    <CardRewards
                                        image={item.image}
                                    // onPress={() => navigation.navigate('HotelDetail')}
                                    >

                                    </CardRewards>
                                )}
                            /> */}
                            {/* <FlatList

                                // horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={reward1}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (

                                    <CardRewards
                                        image={item.image}
                                    // onPress={() => navigation.navigate('HotelDetail')}
                                    >

                                    </CardRewards>
                                )}
                            /> */}
                        </View>
                    </Neomorph>

                    {/* <Text style={styles.text1}>Upcoming jackpots</Text> */}
                    {/* <Neomorph
                        inner
                        style={{
                            padding: Utils.scaleWithPixel(20),
                            shadowRadius: Utils.scaleWithPixel(7),
                            borderRadius: Utils.scaleWithPixel(30),
                            backgroundColor: colors.neoThemebg,
                            width: Utils.scaleWithPixel(315),
                            height: Utils.scaleWithPixel(710),
                            justifyContent: 'center',
                            alignItems: 'center',
                            top: Utils.scaleWithPixel(100),
                            marginBottom: Utils.scaleWithPixel(100)
                        }}
                    > */}
                    <View style={{ top: Utils.scaleWithPixel(0), left: Utils.scaleWithPixel(-5), flexDirection: "row" }}>
                        {/* <FlatList

                                // horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={reward}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (

                                    <CardRewards
                                        image={item.image}
                                    // onPress={() => navigation.navigate('HotelDetail')}
                                    >

                                    </CardRewards>
                                )}
                            /> */}
                        {/* <FlatList

                                // horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={reward1}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (

                                    <CardRewards
                                        image={item.image}
                                    // onPress={() => navigation.navigate('HotelDetail')}
                                    >

                                    </CardRewards>
                                )}
                            /> */}
                    </View>
                    {/* </Neomorph> */}




                </View>
            </ScrollView>
        </SafeAreaView>

    )
}
