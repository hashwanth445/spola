import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  imageBackground: {
    height: Utils.scaleWithPixel(140),
    width: '100%',
    position: 'absolute',
  },
  searchForm: {
    padding: Utils.scaleWithPixel(10),
    borderRadius: Utils.scaleWithPixel(10),
    borderWidth: Utils.scaleWithPixel(0.5),
    width: '100%',
    shadowColor: 'black',
    shadowOffset: {width: Utils.scaleWithPixel(1.5), height: Utils.scaleWithPixel(1.5)},
    shadowOpacity: Utils.scaleWithPixel(0.3),
    shadowRadius: Utils.scaleWithPixel(1),
    elevation: Utils.scaleWithPixel(1),
  },
  contentServiceIcon: {
    marginTop: Utils.scaleWithPixel(10),
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  contentCartPromotion: {
    marginTop: Utils.scaleWithPixel(10),
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  btnPromotion: {
    height: Utils.scaleWithPixel(25),
    borderRadius: Utils.scaleWithPixel(3),
    paddingHorizontal: Utils.scaleWithPixel(10),
    paddingVertical: Utils.scaleWithPixel(5),
  },
  contentHiking: {
    marginTop: Utils.scaleWithPixel(20),
    marginLeft: Utils.scaleWithPixel(20),
    marginBottom: Utils.scaleWithPixel(10),
  },
  promotionBanner: {
    
    height: Utils.scaleWithPixel(310),
    width: Utils.scaleWithPixel(260),
    marginTop: Utils.scaleWithPixel(25),
    borderRadius:Utils.scaleWithPixel(20),
    justifyContent:"flex-end"
  },
  line: {
    height: Utils.scaleWithPixel(1),
    marginTop: Utils.scaleWithPixel(10),
    marginBottom: Utils.scaleWithPixel(15),
  },
  iconContent: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Utils.scaleWithPixel(36),
    height: Utils.scaleWithPixel(36),
    borderRadius: Utils.scaleWithPixel(18),
  },
  itemService: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    paddingTop: Utils.scaleWithPixel(10),
  },
  promotionItem: {
    width: Utils.scaleWithPixel(100),
    height: Utils.scaleWithPixel(100),
    padding: Utils.scaleWithPixel(1),
                 
  },
  tourItem: {
    width: Utils.scaleWithPixel(135),
    height: Utils.scaleWithPixel(160),
  },
  titleView: {
    paddingHorizontal: Utils.scaleWithPixel(20),
    paddingTop: Utils.scaleWithPixel(30),
    paddingBottom: Utils.scaleWithPixel(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnClearSearch: {
    position: 'absolute',
    right: Utils.scaleWithPixel(0),
    alignItems: 'center',
    justifyContent: 'center',
    width: Utils.scaleWithPixel(30),
    height: '100%',
  },
  serviceItem: {
    marginTop:Utils.scaleWithPixel(10),
    paddingHorizontal: Utils.scaleWithPixel(15),
    justifyContent: 'center',
    alignItems: 'center',
  },
  serviceCircleIcon: {
    width: Utils.scaleWithPixel(36),
    height: Utils.scaleWithPixel(36),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Utils.scaleWithPixel(18),
  },
  verticleLine: {
    height: Utils.scaleWithPixel(30),
    width: Utils.scaleWithPixel(3.5),
    left: Utils.scaleWithPixel(10),
    top: Utils.scaleWithPixel(0),
    backgroundColor: '#292e35',
    borderRadius: Utils.scaleWithPixel(120)
  },
  text:{
    fontFamily:"ProximaNova",
    top:Utils.scaleWithPixel(30),
    left:Utils.scaleWithPixel(10),
    fontWeight:"bold",
    fontSize:17
  },
  text1:{
    fontFamily:"ProximaNova",
    top:Utils.scaleWithPixel(80),
    left:Utils.scaleWithPixel(10),
    fontWeight:"bold",
    fontSize:17
  }
});
