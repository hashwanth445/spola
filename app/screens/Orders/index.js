import React, { useState, useRef, useCallback, useEffect } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Animated,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import _ from 'lodash';
import { BaseStyle, Images, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Image, Text, TextInput, OrderComp } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
// import { GiftedChat } from 'react-native-gifted-chat'
import { useDispatch, useSelector } from 'react-redux';
import Icon1 from "react-native-vector-icons/SimpleLineIcons";
import Modal from 'react-native-modal';
import { ordersGetRequestForUser, ordersPostRequest } from '../../api/orders';
var ID = require("nodejs-unique-numeric-id-generator")

export default function Orders({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(ordersGetRequestForUser(userId));

  }, [dispatch])
  const orderId = ID.generate(new Date().toJSON());

  const [isVisible, setIsVisible] = useState(false);

  const [input, setInput] = useState(false);
  const [Loading, setLoading] = useState();


  const UserData = useSelector(state => state.accessTokenReducer.userData)
  console.log("UserData", UserData);
  const userId = UserData?.user?.id;
  const orders = useSelector(state => state.OrdersReducer.ordersDataForUser)
console.log("orders",orders);

  const userToken = useSelector(state => state.accessTokenReducer.accessToken)

  // const [commentId, setCommentId] = useState("")
  // useEffect(() => {
  //   const commented = _.find(commentDataFromUser, function (o) { return o.commentFeed?.id === feedDataObject.id; });
  //   if (commented) {
  //     setCommentId(commented.id);
  //     setInput(true);
  //   }
  // }, []);

  // useEffect(() => {
  //   const commented = _.find(commentDataFromUser, function (o) { return o.commentFeed?.id === feedDataObject.id; });
  //   if (commented) {
  //     setCommentId(commented.id);
  //     setInput(true);
  //   }
  // }, [commentDataFromUser]);
  // const saveComment = async () => {
  //   console.log("commentPressed");
  //   if (!input) {
  //     alert('please enter Comment')
  //     await dispatch(commentsGetRequest())
  //     await dispatch(commentsGetRequestForFeed(feedId));
  //   }
  //   else {
  //     let objectValue = {};
  //     objectValue.commentFeed = feedId;
  //     objectValue.commentedBy = userId;
  //     objectValue.message = input;
  //     objectValue.postType = "feed";
  //     setLoading(true);
  //     // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
  //     await dispatch(commentsPostRequest(objectValue));

  //     await dispatch(commentsGetRequestForFeed(feedId))
  //   }

  // };

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={{ margin: 10 }}>
          <Header
            title={t('Orders')}
            renderLeft={() => {
              return <Icon name="chevron-left" size={25} color={colors.text} />;
            }}
            onPressLeft={() => {
              navigation.goBack();
            }}
          />
          <View>
            <FlatList
              data={orders}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <View>
                  <OrderComp
                    image={item?.products?.backBlazeProductUrl}
                    detail={item?.orderId}
                    // txtContent={item?.message}
                    orderObj={item}
                    name={item?.totalCost}
                    onPress={() => success(item)}
                  />
                </View>
              )}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
