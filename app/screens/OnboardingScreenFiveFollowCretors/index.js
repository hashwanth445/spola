import React, { useState,useEffect } from 'react';
import { View, KeyboardAvoidingView, Platform, TouchableOpacity, Image, FlatList } from 'react-native';
import { BaseStyle, useTheme, Images } from '@config';
import { Header, SafeAreaView, Icon, Button, Text, CreatorsCard, ProfileAuthor2 } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { NeomorphBlur } from 'react-native-neomorph-shadows';
import { BaseColor } from '../../config/theme';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Dots from 'react-native-dots-pagination';
// import { UserData, UserData1 } from '@data';
import * as Utils from '@utils';
import { useDispatch, useSelector } from 'react-redux'
import { creatorsGetRequest } from '../../api/categories';
import LinearGradient from 'react-native-linear-gradient';

export default function OnboardingScreenFiveFollowCretors({ navigation,route }) {
    const { colors } = useTheme();
    const { t } = useTranslation();
    const userToken =  useSelector(state => state.accessTokenReducer.accessToken)
    const userdata = useSelector(state => state.signupReducer.signupMessageData);

    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });
    const creatorsData = useSelector(state => state.CategoriesReducer.creatorsData);
    const [creatorUsersData, setCreatorUsersData] = useState([])
    // const [user] = useState(UserData);
    // const [user1] = useState(UserData1);

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(creatorsGetRequest(userToken));

    }, [dispatch])

    useEffect(() => {
        console.log(creatorsData,userdata,"sami lanag")
            const found = creatorsData.find(element => element._id === userdata?.user?._id);
            const foundIndex = creatorsData.findIndex(element => element._id === userdata?.user?._id);

            if (found) {
                creatorsData.splice(foundIndex, 1);
                setCreatorUsersData(creatorsData);
            
            }
    }, [creatorsData])

    const  selectedCat  = route.params.NewArray;
    const [selectedFollowing, setSelectedFollowing] = useState([]);
    const [loading, setLoading] = useState(false);

    const selectFollowers = async data => {
        let selectedFollowers = selectedFollowing;
        selectedFollowers.push({ "_id": data });
        setSelectedFollowing(selectedFollowers);


    };
    const success = async () => {
console.log(selectedCat,"select categoriessss")
        // setLoading(true);
        // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
        navigation.navigate('OnboardingScreenSixFollowChannels', {selectedCat,selectedFollowing} );
        // setLoading(false);



    };
    return (
        <View style={{ flex: 1, top: Utils.scaleWithPixel(40) }}>
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>

                    <View style={styles.contain}>
                        <Text bold style={{ fontSize: 20, color: "#DDDDDD" }}>Creators you might like</Text>
                        <Text bold style={{ fontSize: 15, color: "#6a6c6e", marginTop: Utils.scaleWithPixel(10) }}>Follow to stay updated with fresh content</Text>
                        <Text bold style={{ fontSize: 18, color: "#008cff", top: Utils.scaleWithPixel(15), }}>Follow upto 3 Creators</Text>
                        <ScrollView style={{ top: Utils.scaleWithPixel(30) }} vertical={true} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}>

                            <FlatList
                                numColumns={2}
                                contentContainerStyle={{ alignSelf: 'flex-start', marginTop: Utils.scaleWithPixel(-5) }}
                                showsHorizontalScrollIndicator={false}
                                data={creatorUsersData}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (

                                    <CreatorsCard
                                        style={{ margin: Utils.scaleWithPixel(10) }}
                                        creatorObject={item?._id}
                                    // image={item.image}
                                    // name={item.name}
                                    >
                                        {/* <Text style={{ top: Utils.scaleWithPixel(-10), left: Utils.scaleWithPixel(25), color: "#909090", fontFamily: "ProximaNova-Medium", fontSize: 14 }}>
                                            {item.point}
                                        </Text> */}
                                        <Text style={{ top: Utils.scaleWithPixel(-25), alignSelf:"center", color: "#e3dce1", fontWeight: "bold", fontFamily: "ProximaNova-Bold", fontSize: 14 }}>
                                            {item?.username}
                                        </Text>
                                        {/* <Text style={{ top: Utils.scaleWithPixel(0),alignSelf:"center", color: "#909090", fontFamily: "ProximaNova-Medium", fontSize: 14 }}>
                                            {item.email}
                                        </Text> */}

                                        <ProfileAuthor2
                                            style={{ left: Utils.scaleWithPixel(25), top: Utils.scaleWithPixel(-135), height: Utils.scaleWithPixel(100), width: Utils.scaleWithPixel(100) }}
                                            image={item?.backBlazeImageUrl ? item?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                        // name={item.name}
                                        // description={item.detail}

                                        />
                                    </CreatorsCard>
                                )}

                            />
                           
                        </ScrollView>

                        <View style={{ marginTop: Utils.scaleWithPixel(65), top: Utils.scaleWithPixel(-30), left: Utils.scaleWithPixel(0) }}>
                            <NeomorphBlur
                                style={{
                                    shadowRadius: 3,
                                    borderRadius: 15,
                                    backgroundColor: colors.neoThemebg,
                                    width: 230,
                                    height: 56,
                                    marginBottom: 0,
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                
                                <TouchableOpacity
                                    onPress={() => success()}
                                >
                                    <LinearGradient style={styles.button} colors={['#43D4FF', '#38ABFD', '#2974FA']} >
                                        <Text bold style={styles.text}>Next</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            
                            </NeomorphBlur>
                        </View>

                    </View>
                    <View style={{ top: Utils.scaleWithPixel(-50), }}>
                        <Dots length={4} activeColor="#0081ff" active={2} />
                    </View>


                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    );
}