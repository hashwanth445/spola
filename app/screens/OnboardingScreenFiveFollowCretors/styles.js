import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: Utils.scaleWithPixel(10),
    flex: 1,
    top:Utils.scaleWithPixel(-20)
  },
  Icon: {
    marginTop: Utils.scaleWithPixel(-20),
    color: "#FFBF00"
  },
  image: {
      height: Utils.scaleWithPixel(80),
      width: Utils.scaleWithPixel(80),
      borderRadius: Utils.scaleWithPixel(70),
      borderWidth:Utils.scaleWithPixel(0.5),
      borderColor:"#DDDDDD"

  },
  button: {
    height: 45,
    width: 220,
    borderRadius: 12,
    justifyContent: "center"
  },
  text: {
    fontFamily:"ProximaNova",
    fontSize:20,
    color:"white",
    alignSelf:"center",
    fontWeight:'700'
  }
});
