import React, {useState, useEffect, useRef} from 'react';
import {
  RefreshControl,
  FlatList,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import Modal from 'react-native-modal';
import {TabView, TabBar} from 'react-native-tab-view';
import {useTranslation} from 'react-i18next';
import {NeomorphBlur, Neomorph} from 'react-native-neomorph-shadows';
import {
  Text,
  TextInput,
  Icon,
  ProfileCard,
  HomeItem1,
  ChannelsCard,
  ProfileAuthor1,
  SpolaItem
} from '@components';
import {useDispatch, useSelector} from 'react-redux';
import Carousel from 'react-native-snap-carousel';
import * as Utils from '@utils';
import _ from 'lodash';
import styles from './styles';
import {shortsGetRequest} from '../../api/shorts';
import {videosGetRequest} from '../../api/videos';
import {channelGetRequestForUser} from '../../api/channel';
import Video from 'react-native-video';
// import Icon from '@components'
import Icon2 from 'react-native-vector-icons/EvilIcons';
import Icon3 from 'react-native-vector-icons/Feather';
import {Avatar} from 'react-native-paper';
// import {data1} from './videodata';
import {BaseColor, Images, useTheme, useFont, BaseStyle} from '@config';
import {stripepaymentsettingUpdateCheckSuccess} from '../../actions';
import {SwiperFlatList} from 'react-native-swiper-flatlist';;

// import videoData from '../../assets/videos';
const {width, height} = Dimensions.get('window');
export default function SpolaScreen({navigation, route}) {
  const {colors} = useTheme();
  const {t} = useTranslation();
  const [currindex, setIndex] = useState(0);
  const [shouldShowFollow, setShouldShowFollow] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const likesdataFromUser = useSelector(state => state.LikesReducer.likesDataForShort)
  const userId = UserData?.user?.id;
  const dispatch = useDispatch();
  const videoRef = useRef(null);
  // useEffect(() => {
  //   dispatch(videosGetRequest(userToken));
  // }, [dispatch]);

  // useEffect(() => {
  //   dispatch(shortsGetRequest());
  // }, [dispatch]);

  // useEffect(() => {
  //   dispatch(channelGetRequestForUser(userId));
  // }, [dispatch]);
  useEffect(() => {
    if (!!videoRef.current) {
      videoRef.current.seek(0);
    }
  }, [currindex]);
 
  const OnChangeIndex = ({index}) => {
    console.log("indexxxzxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",index)
    setIndex(index);
  };

  // const {spolaUrlObject} = route.params;
  // console.log(spolaUrlObject);  

  const [follow, setFollow] = useState();
  const [loading, setLoading] = useState(false);
  const [lasttap, setLastTap] = useState();
  const shortsData = useSelector(state => state.ShortsReducer.shortsData);
  console.log('shorts', shortsData);
  

  const options = {
    
    title: 'Title',
    message: 'Message',
    // urls: reviewShare.state.blobArr[0],
  };
  const share = async (customOptions = options) => {
    try {
      await Share.open(customOptions);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View>
      <SwiperFlatList
        // ref={c => {
        //   this._carousel = c;
        // }}
        data={shortsData}
        vertical={true}
        disableGesture={false}
        onChangeIndex={OnChangeIndex}
        keyExtractor={(item, index) => item.id}
        renderItem={({ item, index }) => (
          <SpolaItem
            image={item?.backBlazeSpolaUrl ? item?.backBlazeSpolaUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
            spolaUrlObject={item}
            currindex={currindex}
            index={index}
          // onPress={() => success(item)}
          >
          </SpolaItem>
        )}
        
      />
    </View>
  );
}
