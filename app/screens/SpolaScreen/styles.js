import {StyleSheet, Dimensions} from 'react-native';
import * as Utils from '@utils';
import {BaseColor} from '@config';
import { justifyContent } from 'styled-system';
const {width,height}=Dimensions.get('window')
export default StyleSheet.create({
  backgroundVideo: {
    top:0,
    bottom:0,
    left:0,
    right:0,
    height: height,
    width:width,
    position: "absolute",
    backgroundColor:'black'
  },
  bottomModal: {
    justifyContent: 'flex-end',
   margin:0,
   
  },
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
    // marginTop:200,

  },
  contentSwipeDown: {
    paddingTop: 10,
    alignItems: 'center',
  },
  lineSwipeDown: {
    width: 50,
    height: 2.5,
    backgroundColor: BaseColor.whiteColor,
  },
  tabbar: {
    height: 40,
    marginTop:15,
    
  },
  tab: {
    width: 120,
    marginLeft:0,
    justifyContent:"center"
    
  },
  indicator: {
    height: 1,
    width:45,
   left:45
  },
  label: {
    fontWeight: '400',
  },
});
