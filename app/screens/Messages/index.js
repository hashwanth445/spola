import React, { useState, useRef,  useCallback, useEffect  } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { BaseStyle, Images, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Image, Text, TextInput } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Neomorph } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
// import { GiftedChat } from 'react-native-gifted-chat'
import { useDispatch, useSelector } from 'react-redux';

export default function Messages({ navigation,route }) {
  const [messages, setMessages] = useState([]);
  const accessToken = useSelector(state => state.accessTokenReducer.accessToken);
  const { messageUrlObject } = route.params;
  console.log("messageUrlObject",messageUrlObject);
  const friendsData = useSelector(state => state.FriendsReducer.friendsData);
 
  useEffect(() => {
    setMessages([
      {
        _id: 1,
        text: messageUrlObject?.message,
        createdAt: new Date(),
        user: {
          _id: 2,
          name: messageUrlObject?.messageFromUser?.username,
          avatar: messageUrlObject?.messageFromUser?.backBlazeImageUrl,
        },
        image: friendsData?.requestFrom?.image?.url ? `http://3.111.30.249:1337` + friendsData?.requestFrom?.image?.url : '', 
      },
    ])
  }, [])
 
  const onSend = useCallback((messages = []) => {
    // setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
  }, [])
 
  return (
    // <GiftedChat
    //   messages={messages}
    //   onSend={messages => onSend(messages)}
    //   user={{
    //     _id: 1,
    //   }}
    // />
    <View></View>
  )
}


// export default function Messages({ navigation }) {
//   const { t } = useTranslation();
//   const { colors } = useTheme();
//   const offsetKeyboard = Platform.select({
//     ios: 0,
//     android: 20,
//   });
//   const refFlatList = useRef(null);
//   const [input, setInput] = useState('');
//   const [messages, setMessages] = useState([
//     {
//       id: 1,
//       message: 'Hello',
//       created: '11:03 PM',
//       user: {
//         id: 1,
//         name: 'Mounika K',
//         // avatar: Images.profile,
//       },
//     },
//     {
//       id: 2,
//       message: 'New movie is released today, shall we go?',
//       created: '11:03 PM',
//       user: {
//         id: 1,
//         name: 'Mounika K',
//         //  avatar: Images.profile2,
//       },
//     },
//     {
//       id: 3,
//       message: "Hi, sure",
//       created: '11:15 PM',
//     },
//   ]);

//   const renderItem = item => {
//     if (item.user) {
//       return (
//         <View style={styles.userContent}>

//           <Image
//             source={Images.photo4}
//             style={[styles.avatar, { borderColor: colors.border }]}
//           />

//           <View style={{ paddingHorizontal: 8, flex: 7 }}>
//             <Text caption1 style={{fontFamily:"ProximaNova",left:-10}}>{item.user.name}</Text>
//             <Neomorph
//               outer
//               style={{
//                 shadowRadius: Utils.scaleWithPixel(7),
//                 borderRadius: Utils.scaleWithPixel(20),
//                 backgroundColor: colors.neoThemebg,
//                 width: Utils.scaleWithPixel(245),
//                 height: Utils.scaleWithPixel(50),
//                 justifyContent: 'center',
//                 // alignItems: 'center',
//                 left: Utils.scaleWithPixel(-50),
//                 top: Utils.scaleWithPixel(16),
//               }}
//             >
//               <View
//                 style={[
//                   styles.userContentMessage,
//                   // { backgroundColor: colors.primaryLight },
//                 ]}>
//                 <Text body2 whiteColor style={{fontFamily:"ProximaNova",fontSize:16}}>
//                   {item.message}
//                 </Text>
//               </View>
//             </Neomorph>
//           </View>

//           <View style={styles.userContentDate}>
//             <Text footnote numberOfLines={1}style={{fontFamily:"ProximaNova"}}>
//               {item.created}
//             </Text>
//           </View>

//         </View >
//       );
//     }

//     return (

//       <View style={styles.meContent}>
//         <View style={styles.meContentDate}>
//           <Text footnote numberOfLines={1} style={{fontFamily:"ProximaNova"}}>
//             {item.created}
//           </Text>
//         </View>
//         <Neomorph
//           outer
//           style={{
//             shadowRadius: Utils.scaleWithPixel(7),
//             borderRadius: Utils.scaleWithPixel(20),
//             backgroundColor: colors.neoThemebg,
//             width: Utils.scaleWithPixel(185),
//             height: Utils.scaleWithPixel(50),
//             justifyContent: 'center',
//             salignItems: 'center',
//             left: Utils.scaleWithPixel(10),
//             top: Utils.scaleWithPixel(5),
//           }}
//         >
//           <View style={{ paddingLeft: Utils.scaleWithPixel(10), flex: 7 }}>
//             <View
//               style={[styles.meContentMessage]}>
//               <Text body2 style={{fontFamily:"ProximaNova",fontSize:16}}>{item.message}</Text>
//             </View>
//           </View>
//         </Neomorph>
//       </View>

//     );
//   };

//   const sendMessage = () => {
//     if (input != '') {
//       messages.push({
//         id: Math.random().toString(),
//         message: input,
//         created: '08:45 AM',
//       });
//       setInput('');
//       setMessages(messages);
//       if (refFlatList.current) {
//         setTimeout(() => {
//           refFlatList.current.scrollToEnd({ animated: false });
//         }, 500);
//       }
//     }
//   };

//   return (
//     <View style={{ flex: 1 }}>
//       <Header
//         title={t('messages')}
//         renderLeft={() => {
//           return (
//             <Icon
//               name="chevron-left"
//               size={30}
//               color={colors.text}
//               enableRTL={true}
//             />
//           );
//         }}
//         onPressLeft={() => {
//           navigation.goBack();
//         }}
//       />
//       <SafeAreaView
//         style={BaseStyle.safeAreaView}
//         edges={['right', 'left', 'bottom']}>
//         <KeyboardAvoidingView
//           style={{ flex: 1, justifyContent: 'flex-end' }}
//           behavior={Platform.OS === 'android' ? 'height' : 'padding'}
//           keyboardVerticalOffset={offsetKeyboard}
//           enabled>
//           <View style={{ flex: 1 }}>

//             <FlatList
//               ref={refFlatList}
//               data={messages}
//               keyExtractor={(item, index) => item.id.toString()}
//               renderItem={({ item }) => renderItem(item)}
//             />

//           </View>
//           <View style={{ flexDirection: "row" }} >
//             <View style={styles.inputContent}>
//               <View >
//                 <Neomorph
//                   outer
//                   style={{
//                     shadowRadius: Utils.scaleWithPixel(7),
//                     borderRadius: 20,
//                     backgroundColor: colors.neoThemebg,
//                     width: Utils.scaleWithPixel(260),
//                     height: Utils.scaleWithPixel(45),
//                     justifyContent: 'center',
//                     alignItems: 'center',
//                     marginLeft: Utils.scaleWithPixel(-20),
//                   }}
//                 >
//                   <TextInput
//                     onChangeText={text => setInput(text)}
//                     onSubmitEditing={() => sendMessage()}
//                     placeholder={t('type_message')}
//                     value={input}
//                   />
//                 </Neomorph>
//               </View>

//             </View>
//             <TouchableOpacity
//               style={[styles.sendIcon, { backgroundColor: colors.primary }]}
//               onPress={sendMessage}>
//               {/* <Icon
//                 name="telegram"
//                 size={24}
//                 color="white"
//                 enableRTL={true}
//               /> */}
//               <Image source={require('../../assets/images2/rocketshare_icn.png')} style={{height:Utils.scaleWithPixel(20),width:Utils.scaleWithPixel(20)}} />
//             </TouchableOpacity>
//           </View>
//         </KeyboardAvoidingView>
//       </SafeAreaView>
//     </View>
//   );
// }
