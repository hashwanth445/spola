import React, { useState, useEffect } from 'react';
import { RefreshControl, FlatList, View,ScrollView } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header,Text, SafeAreaView, PostItem, ListThumbCircle, ProfileAuthor6 } from '@components';
import styles from './styles';
import { TabView, TabBar } from 'react-native-tab-view';
import { useTranslation } from 'react-i18next';
import * as Utils from '@utils';
import { useDispatch, useSelector } from 'react-redux';
import { feedsGetRequest } from '../../api/feeds';
import { NotificationData } from '@data';
import { Neomorph } from 'react-native-neomorph-shadows';
import { bookmarksGetRequestForFeed } from '../../api/bookmarks';
import likesGetRequestForFeed from '../../api/likes';


export default function Post({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [index, setIndex] = useState(0);
  const [Loading, setLoading] = useState();
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const userId = UserData?.user?.id;
  const dispatch = useDispatch();



  
  const feedData = useSelector(state => state.FeedsReducer.feedsData)
  console.log("feedData", feedData);
  // console.log("feed images",feedData[0]?.feedPostedusername);



  const [routes] = useState([
    { key: 'feed', title: t('Feed') },
    { key: 'notification', title: t('You') },
    // { key: 'setting', title: t('COMMENTS') },
    // { key: 'activity', title: t('setting'), icon: 'cog' },
  ]);

  // When tab is activated, set what's index value
  const handleIndexChange = index => setIndex(index);

  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, { backgroundColor: colors.text }]}
      style={[styles.tabbar, { backgroundColor: colors.neoThemeBg, }]}
      tabStyle={styles.tab}
      inactiveColor={colors.text}
      activeColor={colors.text}
      renderLabel={({ route, focused, color }) => (
        <View style={{ flex: 1, width: 350, alignItems: 'center', left: 20 }}>
          <Text headline bold={focused} style={{ fontFamily: "ProximaNova", fontSize: 20 }}>
            {route.title}
          </Text>
          {/* <Icon name={route.icon} color="#DDDDDD" size={24} style={{ top: -22, left: -30 }} /> */}
        </View>
      )}
    />
  );

  const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'feed':
        return <FeedTab jumpTo={jumpTo} navigation={navigation} />;
      case 'notification':
        return <NotificationTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };




  return (
    <View style={{ flex: 1 }}>
      <Header title={t('Activity')} />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <TabView
          lazy
          navigationState={{ index, routes }}
          renderScene={renderScene}
          renderTabBar={renderTabBar}
          onIndexChange={handleIndexChange}
          style={{ height:'100%'}}
        />
      </SafeAreaView>
    </View>
  );
}


/**
 * @description Show when tab Profile activated
 * @author Passion UI <passionui.com>
          * @date 2019-08-03
          * @class PreviewTab
          * @extends {Component}
          */
function FeedTab({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [refreshing, setRefreshing] = React.useState(false);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  const dispatch = useDispatch();

  useEffect(() => {
    loadUserData()

  }, [])
  const [loading, setLoading] = useState(false);
  const success = async (postUrlObject) => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('PostDetail', { postUrlObject })
    setLoading(false);
  };

  const [userData, setUserData] = useState([])

  const loadUserData =() => {
    dispatch(feedsGetRequest(userToken))
    .then((response) => response.json())
    .then((responseJson) => {
      setRefreshing(false);
      var newdata = userData.concat(responseJson.results);
      setUserData(newdata);
    })
    .catch((error) => {
      console.error(error);
    });
  };

  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const feedData = useSelector(state => state.FeedsReducer.feedsData)
  console.log("feedData", feedData);

  return (
    <View style={{marginTop:20, height: "100%" }}>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <FlatList
          refreshControl={
            <RefreshControl
              colors={[colors.primary]}
              enabled={true}
              tintColor={colors.primary}
              refreshing={refreshing}
              onRefresh={loadUserData}
            />
          }
          data={feedData}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) => (
            <PostItem
              image={item?.backBlazeImageUrl ? item?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
              title={item?.name}
              description={item?.feedDesc}
              feedDataObject={item}
            // onPress={() => success(item)}
            >
              <ProfileAuthor6
                image={item?.feedPostedBy?.backBlazeImageUrl ? item?.feedPostedBy?.backBlazeImageUrl: 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                title={item?.feedPostedBy?.username}
                style={{ paddingHorizontal: Utils.scaleWithPixel(15),top:5 }}
              />
            </PostItem>
          )}
        />
      </SafeAreaView>
    </View>
  );
}


/**
 * @description Show when tab Profile activated
 * @author Passion UI <passionui.com>
          * @date 2019-08-03
          * @class PreviewTab
          * @extends {Component}
          */
function NotificationTab({ navigation }) {
  const { colors } = useTheme();
  const [refreshing] = useState(false);
  const { t } = useTranslation();

  const [loading, setLoading] = useState(false);
  const [notification] = useState(NotificationData);

  return (
    <View style={{ marginTop: 20, height: "100%" }}>
      <ScrollView>
        <Text style={{ fontSize: 23, color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(30), fontWeight: "bold" }}>Today</Text>
        <FlatList
          contentContainerStyle={{ paddingHorizontal: Utils.scaleWithPixel(20), paddingVertical: Utils.scaleWithPixel(10) }}
          refreshControl={
            <RefreshControl
              colors={[colors.primary]}
              tintColor={colors.primary}
              refreshing={refreshing}
              onRefresh={() => { }}
            />
          }
          data={notification}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) => (
            <Neomorph
              outer
              style={{
                shadowRadius: Utils.scaleWithPixel(7),
                borderRadius: Utils.scaleWithPixel(20),
                backgroundColor: colors.neoThemebg,
                width: Utils.scaleWithPixel(310),
                height: Utils.scaleWithPixel(60),
                justifyContent: 'center',
                alignItems: 'center',
                margin: Utils.scaleWithPixel(8),
                marginLeft: Utils.scaleWithPixel(-10)
              }}
            >
              <ListThumbCircle
                image={item.image}
                txtLeftTitle={item.title}
                // txtContent={item.description}
                txtRight={item.date}
                style={{ marginBottom: Utils.scaleWithPixel(5) }}
              />

            </Neomorph>
          )}
        />
        {/* <View style={{ top: Utils.scaleWithPixel(0) }}>
          <Text style={{ fontSize: 23, color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(30), fontWeight: "bold", margin: Utils.scaleWithPixel(10) }}>Yesterday</Text>
          <FlatList
            contentContainerStyle={{ paddingHorizontal: Utils.scaleWithPixel(20), paddingVertical: Utils.scaleWithPixel(10) }}
            refreshControl={
              <RefreshControl
                colors={[colors.primary]}
                tintColor={colors.primary}
                refreshing={refreshing}
                onRefresh={() => { }}
              />
            }
            data={notification}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item, index }) => (
              <Neomorph
                outer
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(20),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(310),
                  height: Utils.scaleWithPixel(60),
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: Utils.scaleWithPixel(8),
                  marginLeft: Utils.scaleWithPixel(-10)
                }}
              >

                <ProfileAuthor2
                     image={item.uploadedBy?.image?.url ? 'http://3.111.30.249:1337' + item.uploadedBy?.image?.url : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                    name={item.title}
                    description={item.description}
                    // textRight={item.date}
                    style={{ paddingHorizontal: Utils.scaleWithPixel(20) }}
                  />

              </Neomorph>
            )}
          />
        </View> */}
      </ScrollView>
    </View>
  );
}