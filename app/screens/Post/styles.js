import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  footerContainer: {
    marginTop: Utils.scaleWithPixel(5),
    marginLeft: Utils.scaleWithPixel(10),
    marginRight: Utils.scaleWithPixel(10),
    marginBottom: Utils.scaleWithPixel(10),
  },
  footerText: {
    fontSize: 14,
    color: '#aaa',
  },
  tabbar: {
    height: 40,
    MarginTop:15,
    // top:-5
    
  },
  tab: {
    width: 175,
    paddingLeft:0,
    color:BaseColor.blueColor
  },
  indicator: {
    height: 1,
    width:115,
   left:45,
  },
  label: {
    fontWeight: '400',
  },
});
