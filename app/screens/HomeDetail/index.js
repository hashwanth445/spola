import React, { useState, useRef, useEffect } from 'react';
import {
    View,
    ScrollView,
    FlatList,
    Animated,
    TouchableOpacity,
    Dimensions,
    RefreshControl,
} from 'react-native';
import { BaseColor, Images, useTheme } from '@config';
import { useSelector, useDispatch } from 'react-redux';
import { AuthActions } from '@actions';
import {
    Header,
    SafeAreaView,
    Icon,
    Text,
    ProfileGroup,
    TextInput,
    Image,
    Button,
    ProfileAuthor2,
    ProfileAuthor,
    ProfileAuthor1,
    CreatorsCard,
    HomeItem1,
    PostItem,
    ProfilePerformance1,
    Card,
    HomeLatestVideosComponent,
    SpolasCard,
    ProductsCard,
    ProfileAuthor3,
    ProfileAuthor4,
    ProfileAuthor5,

} from '@components';
import _ from 'lodash';
import { Accordion, NativeBaseProvider, Center, Box } from 'native-base';
import { videosGetRequest } from '../../api/videos'
import { commentsGetRequest } from '../../api/comments';
import { commentsPostRequest } from '../../api/comments';
import { creatorsGetRequest } from '../../api/categories';
import { likesGetRequestByUser } from '../../api/likes';
import { productsGetRequest } from '../../api/products';
import { likesPostRequest, likesGetRequest, likesDeleteRequest } from '../../api/likes';
import { dislikeGetRequest, dislikePostRequest, dislikeDeleteRequest, dislikeGetRequestByUser } from '../../api/dislike';
import { useTranslation } from 'react-i18next';
import { TabView, TabBar } from 'react-native-tab-view';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import * as Utils from '@utils';
import Modal from 'react-native-modal';
import styles from './styles';
import { List } from 'react-native-paper';
import { UserData, MessagesData, PostData, ShortsData1, ShortsData, PostData3, PostData1, ProductsData1 } from "@data";
import Video from 'react-native-video';
import VideoPlayer from 'react-native-rn-videoplayer';
import { YoutubePlayer, FacebookPlayer } from 'react-native-video-extension';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { bookmarksGetRequestByUser, bookmarksGetRequest, bookmarksPostRequest, bookmarksDeleteRequest } from '../../api/bookmarks';
import { format, render, cancel, register } from 'timeago.js';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

export default function HomeDetail({ navigation, route }) {
    const deltaY = new Animated.Value(0);
    const heightImageBanner = Utils.scaleWithPixel(250, 1);
    const { colors } = useTheme();
    const { t } = useTranslation();
    const [expanded, setExpanded] = React.useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const openModal = (modal) => {
        setModalVisible(modal);
    };

    const { width, height } = Dimensions.get('window');
   

    const renderModal = () => {
        return (
            <View>
                <Modal
                    isVisible={modalVisible === 'description'}
                    onSwipeComplete={() => setModalVisible(false)}
                    swipeDirection={['down']}
                    backdropColor="transparent"
                    onBackdropPress={() => setModalVisible(false)}
                    style={styles.bottomModal}>
                    <View style={styles.contain, { backgroundColor: colors.neoThemebg, height: 600 }}>
                        {/* <View style={{flexDirection: "row", justifyContent: "space-between" }}> */}
                        <TouchableOpacity onPress={() => setModalVisible(false)} style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <Text title2 bold style={{ left: 10, top: 10, fontFamily: "ProximaNova" }}>
                                {/* Imran Khan - Satisfya (Official Music Video) */}
                                Description
                            </Text>
                            <Icon name='close' size={28} color={colors.text} style={{ top: 7 }} />
                        </TouchableOpacity>
                        {/* </View> */}
                        <View style={{ height: 0.6, width: 380, left: 5, marginTop: 15, backgroundColor: colors.text }}></View>
                        <View style={{ left: 10, flexDirection: "column" }}>
                            <Text bold style={{ marginBottom: 10, marginLeft: 5, marginTop: 20, fontSize: 18, fontFamily: "ProximaNova" }}>
                                {/* Imran Khan - Satisfya (Official Music Video) */}
                                {description} | {uploadedUser}
                            </Text>
                            <ProfileAuthor5
                                image={videoUrlObject?.uploadedBy?.backBlazeImageUrl ? videoUrlObject?.uploadedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                title={uploadedUser}
                                // name={subscribers}
                                style={{ top: 0 }}
                            />

                        </View>
                        <View style={styles.contentInfor}>
                            <ProfilePerformance1
                                data={performance}
                                flexDirection="row"
                                videoDataObject={performance}
                            />
                        </View>
                        <View style={{ height: 0.6, width: 380, left: 5, marginTop: 25, backgroundColor: colors.text }}></View>
                        <View>
                            {/* <Text title3 style={{fontFamily:"ProximaNova",marginTop:10,marginLeft:5}}>{description}</Text> */}
                            <Text title3 style={{ fontFamily: "ProximaNova", marginTop: 10, marginLeft: 5, color: BaseColor.blueColor }}>{tags}</Text>
                        </View>
                    </View>
                </Modal>
            </View>

        )
    }

    const handlePress = () => setExpanded(expanded);

    const [input, setInput] = useState('');
    const [index, setIndex] = useState(0);
    const [Loading, setLoading] = useState();
    const userToken = useSelector(state => state.accessTokenReducer.accessToken)
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(videosGetRequest(userToken));

    }, [dispatch])
    useEffect(() => {
        dispatch(likesGetRequestByUser(userId, videoId));

    }, [dispatch])
    useEffect(() => {
        dispatch(commentsGetRequest(userToken));

    }, [dispatch])

    useEffect(() => {
        dispatch(creatorsGetRequest(userToken));

    }, [dispatch])
    useEffect(() => {
        dispatch(dislikeGetRequestByUser(userId, videoId));

    }, [dispatch])
    useEffect(() => {
        dispatch(bookmarksGetRequestByUser(userId, videoId));

    }, [dispatch])
    useEffect(() => {
        dispatch(productsGetRequest());

    }, [dispatch])
    const creatorsData = useSelector(state => state.CategoriesReducer.creatorsData);
    const likesdataFromUser = useSelector(state => state.LikesReducer.likesDataByUser)
    const dislikedataFromUser = useSelector(state => state.DislikeReducer.dislikeDataByUser)
    const bookmarkdataFromUser = useSelector(state => state.BookmarksReducer.bookmarksDataByUser)
    const productsData = useSelector(state => state.ProductsReducer.productsData)
    // const commentData = useSelector(state => state.CommentsReducer.commentsData)
    const videosData = useSelector(state => state.VideosReducer.videosData);
    // const commentedUser = commentData[0]?.commentedBy?.username
    // const comment = commentData[0]?.
    console.log("productsData", productsData);

    // console.log("likesdataFromUser", likesdataFromUser, likesdataFromUser[0]?.like, likesdataFromUser[0]?.id);
    // console.log("dislikesdataFromUser", dislikedataFromUser, likesdataFromUser[0]?.disLike, dislikedataFromUser[0]?.id);
    // console.log("bookmarkdataFromUser", bookmarkdataFromUser, bookmarkdataFromUser[0]?.addToBookmark, bookmarkdataFromUser[0]?.id);
    const [routes] = useState([
        { key: 'booking', title: t('UP NEXT VIDEOS') },
        { key: 'profile', title: t('ABOUT') },
        { key: 'setting', title: t('COMMENTS') },
        // { key: 'activity', title: t('setting'), icon: 'cog' },
    ]);

    const success = async (videoUrlObject) => {


        setLoading(true);
        // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
        navigation.navigate('HomeDetail', { videoUrlObject })
        setLoading(false);

    };

    // const video = require('../../assets/images/videoplayback.mp4');


    // const { t } = useTranslation();
    const [refreshing] = useState(false);
    const [user] = useState(UserData)
    const [posts] = useState(PostData);
    const [post1] = useState(PostData1)
    const [shorts1] = useState(ShortsData1);
    const [shorts] = useState(ShortsData);
    const [posts3] = useState(PostData3);
    const [product1] = useState(ProductsData1);
    const [messenger] = useState(MessagesData);
    const [commentMessage, setCommentMessage] = useState();

    const videoPlayer = useRef(null);

    const UserData = useSelector(state => state.accessTokenReducer.userData)
    const userId = UserData?.user?.id;
    const { videoUrlObject } = route.params;
    console.log("videoUrlObject", videoUrlObject);
    const videourl = videoUrlObject?.backBlazeVideoUrl;
    const videotitle = videoUrlObject?.title;
    const description = videoUrlObject?.description;
    const tags = videoUrlObject?.tags;
    const commentData = videoUrlObject?.comment;
    const commentData1 = videoUrlObject?.comment.slice(1);
    const likesd = videoUrlObject?.likes
    console.log("likesd", likesd);
    const time = videoUrlObject?.createdAt
    const year = new Date(time).getFullYear();
    const date = new Date(time).getDate()
    const month = new Date(time).getMonth()
    const [performance, setPerformance] = useState([
        { value: likesd?.length ? likesd?.length : '0', title: 'Likes' },
        { value: videoUrlObject?.channelVideo?.channel?.subscribers.length ? videoUrlObject?.channelVideo?.channel?.subscribers.length : '0', title: 'Subscribers' },
        { value: date, title: year },
    ])


    const viewcount = videoUrlObject?.viewCount;
    const commentcount = videoUrlObject?.comment?.length;
    const commentTime = videoUrlObject?.comment?.dateAndTime;
    const uploadedUser = videoUrlObject?.channelVideo?.channel?.channelName;
    const subscribers = videoUrlObject?.channelVideo?.channel?.subscribers.length;
    const [likecount, setLikeCount] = useState(videoUrlObject?.likes?.length);
    const [dislikecount, setDislikeCount] = useState(videoUrlObject?.dislikes?.length);
    const videoId = videoUrlObject?.id;
    const [shouldShowFollow, setShouldShowFollow] = useState(true);
    const [likes, setLiked] = useState(false);
    const [dislikes, setDisliked] = useState(false);
    const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
    const [region] = useState({
        latitude: 1.352083,
        longitude: 103.819839,
        latitudeDelta: 0.009,
        longitudeDelta: 0.004,
    });
    const [facilities] = useState([
        { id: '1', icon: 'wifi', name: 'Free Wifi', checked: true },
        { id: '2', icon: 'shower', name: 'Shower' },
        { id: '3', icon: 'paw', name: 'Pet Allowed' },
        { id: '4', icon: 'bus', name: 'Shuttle Bus' },
        { id: '5', icon: 'cart-plus', name: 'Supper Market' },
        { id: '6', icon: 'clock', name: 'Open 24/7' },
    ]);
    const [relate] = useState([
        {
            id: '0',
            image: Images.event4,
            title: 'BBC Music Introducing',
            time: 'Thu, Oct 31, 9:00am',
            location: 'Tobacco Dock, London',
        },
        {
            id: '1',
            image: Images.event5,
            title: 'Bearded Theory Spring Gathering',
            time: 'Thu, Oct 31, 9:00am',
            location: 'Tobacco Dock, London',
        },
    ]);
    const saveComment = async () => {
        console.log("commentPressed");
        if (!input) {
            alert('please enter Comment')
        }
        else {
            const postType = "video"
            setLoading(true);
            // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
            await dispatch(commentsPostRequest(input, videoId, userId, postType));

        }

    };


    const [likelistId, setLikelistId] = useState("")
    useEffect(() => {
        const shorted = _.find(likesdataFromUser, function (o) { return o.likedUploadVideo?.id === videoUrlObject.id; });
        if (shorted) {
            setLikelistId(shorted.id);
            setLiked(true)
        }
    }, []);

    useEffect(() => {
        const shorted = _.find(likesdataFromUser, function (o) { return o.likedUploadVideo?.id === videoUrlObject.id; });
        if (shorted) {
            setLikelistId(shorted.id);
            setLiked(true)
        }
    }, [likesdataFromUser]);
    const saveLike = async () => {
        if (likes) {
            console.log("likePressed update");
            // const likeId = likesdataFromUser[0]?.id;
            setLiked(false);
            setLikeCount(likecount - 1)
            setLoading(true);
            // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
            await dispatch(likesDeleteRequest(likelistId));
            await dispatch(likesGetRequest());
        } else {
            setLiked(true)
            let objectValue = {};
            objectValue.likedUploadVideo = videoId;
            objectValue.likedBy = userId;
            objectValue.postType = 'video'

            console.log("likePressed post", objectValue);
            setLoading(true);
            // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
            await dispatch(likesPostRequest(objectValue));
            await dispatch(likesGetRequestByUser(userId, videoId));
            setLikeCount(likecount + 1);
        }

    };

    const [dislikelistId, setDislikelistId] = useState("")
    useEffect(() => {
        const dislike = _.find(dislikedataFromUser, function (o) { return o.dislikeUploadVideo?.id === videoUrlObject.id; });
        if (dislike) {
            setDislikelistId(dislike.id);
            setDisliked(true)
        }
    }, []);

    useEffect(() => {
        const dislike = _.find(dislikedataFromUser, function (o) { return o.dislikeUploadVideo?.id === videoUrlObject.id; });
        if (dislike) {
            setDislikelistId(dislike.id);
            setDisliked(true)
        }
    }, [dislikedataFromUser]);
    const saveDislike = async () => {
        if (dislikes) {
            // const dislikeId = dislikedataFromUser?.id;
            setDisliked(false);
            setDislikeCount(dislikecount - 1);
            setLoading(true);
            await dispatch(dislikeDeleteRequest(dislikelistId));
            await dispatch(dislikeGetRequest());
            await dispatch(dislikeGetRequestByUser(userId, videoId));
        }
        else {
            setDisliked(true)
            let objectValue = {};
            objectValue.dislikeUploadVideo = videoId;
            objectValue.disLikedBy = userId;
            setLoading(true);
            await dispatch(dislikePostRequest(objectValue));
            await dispatch(dislikeGetRequestByUser(userId, videoId));
            setDislikeCount(dislikecount + 1);
        }
        console.log("dislike");
    };


    const [bookmarkId, setBookmarkId] = useState("")
    const [bookmarked, setBookmarked] = useState(false);
    useEffect(() => {
        const bookmark = _.find(bookmarkdataFromUser, function (o) { return o.savedVideo?.id === videoUrlObject.id; });
        if (bookmark) {
            setBookmarkId(bookmark.id);
            setBookmarked(true)
        }
    }, []);

    useEffect(() => {
        const bookmark = _.find(bookmarkdataFromUser, function (o) { return o.savedVideo?.id === videoUrlObject.id; });
        if (bookmark) {
            setBookmarkId(bookmark.id);
            setBookmarked(true)
        }
    }, [bookmarkdataFromUser]);
    const saveBookmark = async () => {
        if (bookmarked) {
            setBookmarked(false);
            setLoading(true);
            await dispatch(bookmarksDeleteRequest(bookmarkId));
            await dispatch(bookmarksGetRequest());
            await dispatch(bookmarksGetRequestByUser(userId, videoId));
        }
        else {
            setBookmarked(true);
            let objectValue = {};
            objectValue.savedVideo = videoId;
            objectValue.bookmarkedBy = userId;
            objectValue.postType = "video"
            console.log("bookmark post hit", objectValue);
            setLoading(true);

            await dispatch(bookmarksPostRequest(objectValue));
            await dispatch(bookmarksGetRequestByUser(userId, videoId));
        }

    };
    const [lasttap, setLastTap] = useState();
    const [paused, setPaused] = useState(false);

    // const handleDoubleTap = () => {
    //     const now = Date.now();
    //     const DOUBLE_PRESS_DELAY = 300;
    //     if (lasttap && now - lasttap < DOUBLE_PRESS_DELAY) {
    //         // setLiked(!liked);
    //         setPaused(false);
    //     } else {
    //         setLastTap(now);
    //         paused ? setPaused(false) : setPaused(true);
    //     }
    // };

    function AccordionComponent() {
        return (
            <Box m={3}>
                <Accordion index={[0, 1]} style={{ width: 380, top: 40, marginBottom: 50, borderWidth: 0 }}>
                    <Accordion.Item>
                        <Accordion.Summary style={{ top: 5, backgroundColor: colors.neoThemeBg }}>
                            <Text body1 bold style={{ marginBottom: 10, left: 0, top: 0, fontFamily: "ProximaNova" }}>Comments - </Text>
                            <Text grayColor style={{ fontFamily: "ProximaNova", left: -110, top: -5, fontSize: 16 }}>{commentcount}</Text>
                            <Accordion.Icon color={colors.text} />
                        </Accordion.Summary>
                        <ProfileAuthor1
                            image={commentData[0]?.commentedBy?.backBlazeImageUrl ? commentData[0]?.commentedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                            name={commentData[0]?.commentedBy?.username}
                            description={commentData[0]?.message}
                            time={commentData[0]?.createdAt}
                            style={{ left: 10 }}
                        />
                        <Accordion.Details>
                            <FlatList
                                data={commentData1}
                                vertical={true}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (
                                    <ProfileAuthor1
                                        image={item?.commentedBy?.backBlazeImageUrl ? item?.commentedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                        name={item?.commentedBy?.username}
                                        description={item?.message}
                                        time={item?.createdAt}
                                        style={{ marginTop: -10 }}
                                    />
                                )}
                            />
                        </Accordion.Details>
                    </Accordion.Item>
                </Accordion>
            </Box>
        )
    }


    return (

        <View>
            <YoutubePlayer
                source={{ uri: videourl}}
                // // poster={require('../../assets/images/avata.png')}
                // controls={true}
                
                // pictureInPicture={PictureInPicture.start()}
                mode="auto-fit"
                aspecRatio="landscape"
                fullscreenOrientation='landscape'
                fullscreenAutorotate={true}
                fullscreen={true}
            />
            {renderModal()}
            <ScrollView style={{ marginBottom: 10 }}>
                <View style={{ top: 0, left: 10 }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <View>
                            <Text title2 bold style={{ marginBottom: 10, left: 10, top: 10, fontFamily: "ProximaNova" }}>
                                {/* Imran Khan - Satisfya (Official Music Video) */}
                                {videotitle}
                            </Text>
                            <Text body1 grayColor style={{ fontFamily: "ProximaNova", top: 5, left: 10 }}>
                                {videoUrlObject?.channelVideo?.channel?.channelName} - {viewcount} Views | {format(videoUrlObject?.createdAt)}
                            </Text>
                        </View>
                        <TouchableOpacity onPress={() => openModal('description')}>
                            <IconSimple name="arrow-down" size={20} color={colors.text} style={{ top: 20, left: -20 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: 2.5, width: 360, left: 10, top: 30, backgroundColor: colors.card }}></View>
                    <View style={{ flexDirection: "row", top: 20, left: 40 }}>
                        <View style={{ flexDirection: "column", top: 10, justifyContent: "center", alignContent: "center" }}>
                            <TouchableOpacity onPress={() => saveLike()}>
                                <IconSimple name="like"
                                    size={25} solid={true} style={{ color: likes ? "red" : "#808080", top: 5, alignSelf: "center" }} />
                            </TouchableOpacity>
                            <Text body1 bold grayColor style={{ fontFamily: "ProximaNova", top: 7, alignSelf: "center" }}>
                                {likecount}
                            </Text>
                        </View>
                        <View style={{ flexDirection: "column", top: 10, justifyContent: "flex-end", alignContent: "center", left: 50 }}>
                            <TouchableOpacity
                                onPress={() => saveDislike()}
                            >
                                <IconSimple name="dislike" size={25} style={{ color: dislikes ? "blue" : "#808080", alignSelf: "center", top: 5 }} />
                            </TouchableOpacity>
                            <Text body1 bold grayColor style={{ fontFamily: "ProximaNova", top: 7, alignSelf: "center" }}>
                                {dislikecount}
                            </Text>
                        </View>

                        <View style={{ flexDirection: "column", left: 105, top: 10 }}>
                            <IconSimple name="paper-plane" size={25} style={{ color: "#808080", left: 8, top: 10 }} />
                            <Text body1 bold grayColor style={{ fontFamily: "ProximaNova", top: 7, left: 5 }}>
                                Share
                            </Text>
                        </View>

                        <View style={{ flexDirection: "column", left: 150, top: 10 }}>
                            <TouchableOpacity onPress={() => saveBookmark()}>
                                {bookmarked ?
                                    (<Icon name="bookmark" size={29} style={{ color: "#808080", left: 10, top: 8 }} />) :
                                    (<Icon name="bookmark-outline" size={29} style={{ color: "#808080", left: 10, top: 8 }} />)}

                            </TouchableOpacity>
                            <Text body1 bold grayColor style={{ fontFamily: "ProximaNova", top: 3, left: 10 }}>
                                Save
                            </Text>
                        </View>
                        {/* </NeomorphBlur> */}
                    </View>
                    <View style={{ height: 2.5, width: 360, left: 10, top: 40, backgroundColor: colors.card }}></View>
                    <View style={styles.rowBanner}>
                        <ProfileAuthor4
                            image={videoUrlObject.channelVideo?.channel?.backBlazeCoverImg ? videoUrlObject.channelVideo?.channel?.backBlazeCoverImg : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                            title={uploadedUser}
                            name={subscribers}
                            style={{ top: 0 }}
                        />
                    </View>
                    {shouldShowFollow ? (
                        <NeomorphBlur
                            outer
                            style={{
                                shadowRadius: Utils.scaleWithPixel(3),
                                borderRadius: Utils.scaleWithPixel(100),
                                backgroundColor: colors.neoThemebg,
                                width: Utils.scaleWithPixel(115),
                                height: Utils.scaleWithPixel(35),
                                justifyContent: 'center',
                                alignItems: 'center',
                                // margin: Utils.scaleWithPixel(10),
                                top: Utils.scaleWithPixel(-5),
                                left: Utils.scaleWithPixel(190)
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => setShouldShowFollow(!shouldShowFollow)}>
                                <Text bold style={{ fontSize: 20, color: "#e3dce1", fontFamily: "ProximaNova" }}>Follow</Text>
                            </TouchableOpacity>
                        </NeomorphBlur>) : (<NeomorphBlur
                            outer
                            style={{
                                shadowRadius: Utils.scaleWithPixel(3),
                                borderRadius: Utils.scaleWithPixel(100),
                                backgroundColor: colors.neoThemebg,
                                width: Utils.scaleWithPixel(115),
                                height: Utils.scaleWithPixel(35),
                                justifyContent: 'center',
                                alignItems: 'center',
                                // margin: Utils.scaleWithPixel(10),
                                top: Utils.scaleWithPixel(-5),
                                left: Utils.scaleWithPixel(190)
                            }}
                        >
                            <TouchableOpacity style={{ backgroundColor: BaseColor.blueColor, width: 120, height: 35, borderRadius: 20 }}
                                onPress={() => setShouldShowFollow(!shouldShowFollow)}>
                                <Text bold style={{ fontSize: 20, fontFamily: "ProximaNova", left: 35, top: 5 }}>Following</Text>
                            </TouchableOpacity>
                        </NeomorphBlur>)}
                    {/* <Image source={require('../../assets/images/subscribe.png')} style={{ height: 50, width: 100, left: 270, top: -25 }} /> */}
                    <View style={{ height: 2.5, width: 360, left: 10, top: 5, backgroundColor: colors.card }}></View>
                </View>
                {/* </Neomorph> */}

                <Neomorph
                    inner
                    style={{
                        shadowRadius: 7,
                        borderRadius: 20,
                        backgroundColor: colors.neoThemebg,
                        width: 370,
                        height: 70,
                        // justifyContent: 'center',
                        // alignItems: 'center',
                        left: 15,
                        top: 40,
                        // margin: 10,
                        marginBottom: 10
                    }}
                >
                    <View style={{ flexDirection: "row" }}>
                        <TextInput
                            onChangeText={text => setInput(text)}
                            style={{ top: 15, left: 10 }}
                            // onSubmitEditing={() => sendMessage()}

                            placeholder={t('Add a new comment')}
                            value={input}
                        />
                        <NeomorphBlur
                            style={{
                                shadowRadius: 3,
                                borderRadius: 20,
                                backgroundColor: colors.neoThemebg,
                                width: 60,
                                height: 56,

                                justifyContent: 'center',
                                alignItems: 'center',
                                top: 8,
                                left: -70
                            }}
                        >

                            <TouchableOpacity onPress={() => saveComment()} style={{ borderRadius: 15, height: 54, width: 58, justifyContent: "center", backgroundColor: colors.neoThemeMainButtonBorderColor }}>
                                <Icon name="message-plus" size={28} color="#DDDDDD" style={{ left: 15 }} />
                            </TouchableOpacity>

                        </NeomorphBlur>
                    </View>
                </Neomorph>

                {/* <View style={{ marginTop: 30, flexDirection: "row" }}>
                    <Text body1 bold style={{ marginBottom: 10, left: 20, top: 10, fontFamily: "ProximaNova" }}>Comments - </Text>
                    <Text grayColor style={{ fontFamily: "ProximaNova", left: 20, top: 12, fontSize: 16 }}>{commentcount}</Text>
                    <Icon name="chevron-down" color={colors.text} size={30} style={{ left: 250, top: 10 }} />
                </View>
                <View style={{ left: 10 }}>
                    <ProfileAuthor1
                        image={commentData[0]?.commentedBy?.image?.formats?.thumbnail?.url ? 'http://3.111.30.249:1337' + commentData[0]?.commentedBy?.image?.formats?.thumbnail?.url : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                        name={commentedUser}
                        description={comment}
                        time={commentTime}
                        style={{ top: 0 }}
                    />

                </View> */}

                <NativeBaseProvider>
                    <Center flex={1}>
                        <ScrollView>
                            <AccordionComponent />
                        </ScrollView>
                    </Center>
                </NativeBaseProvider>

                <SafeAreaView style={{ flex: 1, top: 40, marginBottom: 80 }} edges={['right', 'left', 'bottom']}>

                    {/* <TabView
                    lazy
                    navigationState={{ index, routes }}
                    renderScene={renderScene}
                    renderTabBar={renderTabBar}
                    onIndexChange={handleIndexChange}
                    style={{ height: 1000, marginBottom: -30 }}
                    tabBarShowLabel
                    tabBarIcon
                /> */}


                    <Neomorph
                        inner
                        style={{
                            // padding: 20,
                            shadowRadius: Utils.scaleWithPixel(7),
                            borderRadius: Utils.scaleWithPixel(20),
                            backgroundColor: colors.neoThemebg,
                            width: Utils.scaleWithPixel(325),
                            height: height*8,
                            flex: 1,
                            // justifyContent: 'center',
                            // alignItems: 'center',
                            // marginTop: Utils.scaleWithPixel(-30),
                            left: Utils.scaleWithPixel(5),
                            top: Utils.scaleWithPixel(15)
                        }}
                    >
                        <ScrollView horizontal>
                            <View style={{ flexDirection: "row", paddingLeft: 20, top: 20, width: 1000 }}>
                                <TouchableOpacity style={{ backgroundColor: BaseColor.blueColor, height: 40, borderRadius: 10 }}>
                                    <View style={{ borderRadius: 10, borderColor: "white", borderWidth: 1, height: 40, width: 70 }}>
                                        <Text style={{ color: colors.text, fontSize: 20, fontFamily: "ProximaNova", left: 25, top: 5 }}>All</Text>
                                    </View>
                                </TouchableOpacity>
                                <View style={{ borderRadius: 10, borderColor: "white", borderWidth: 1, height: 40, width: 100, left: 20 }}>
                                    <Text style={{ color: colors.text, fontSize: 20, fontFamily: "ProximaNova", left: 20, top: 5 }}>Related</Text>
                                </View>
                                <View style={{ borderRadius: 10, borderColor: "white", borderWidth: 1, height: 40, width: 100, left: 40 }}>
                                    <Text style={{ color: colors.text, fontSize: 20, fontFamily: "ProximaNova", left: 15, top: 5 }}>Trending</Text>
                                </View>
                                <View style={{ borderRadius: 10, borderColor: "white", borderWidth: 1, height: 40, width: 140, left: 60 }}>
                                    <Text style={{ color: colors.text, fontSize: 20, fontFamily: "ProximaNova", left: 15, top: 5 }}>Recommended</Text>
                                </View>
                            </View>
                        </ScrollView>
                        <View style={{ left: Utils.scaleWithPixel(0), top: Utils.scaleWithPixel(0), marginTop: -50 }}>
                            <FlatList
                                data={videosData}
                                horizontal={true}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (
                                    <HomeLatestVideosComponent
                                        image={item?.backBlazeVideoUrl ? item?.backBlazeVideoUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                        time={item?.Duration}
                                        onPress={() => {
                                            success(item)
                                        }}>
                                        {/* <ProfileAuthor
                                            image={item.uploadedBy?.backBlazeImageUrl ? item.uploadedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                            name={item?.channelVideo?.channel?.channelName}
                                            title={item?.title}
                                            description={item?.viewCount ? item?.viewCount : "0"}
                                            dateAndTime={item?.createdAt ? item?.createdAt : "0"}
                                            style={{ paddingHorizontal: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(210) }}
                                        /> */}
                                    </HomeLatestVideosComponent>
                                )}
                            />
                        </View>


                        <View style={{ paddingLeft: Utils.scaleWithPixel(5), top: Utils.scaleWithPixel(0) }}>
                            <Neomorph
                                inner
                                style={{
                                    padding: Utils.scaleWithPixel(20),
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(25),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(350),
                                    height: Utils.scaleWithPixel(315),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    left: 5
                                }}
                            >
                                <View style={{ flexDirection: "row", alignItems: "flex-start" }}>
                                    {/* <Icon name="youtube" size={22} color="red" style={{ left: -168, top: -8 }} /> */}
                                    <Text bold style={styles.text, { left: -115, top: -10, fontSize: 18, fontFamily: "ProximaNova" }}>Spolas watch and earn</Text>
                                    {/* <Image source={require('../../assets/images2/right_icn.png')} style={{ height: Utils.scaleWithPixel(13), width: Utils.scaleWithPixel(13), left: Utils.scaleWithPixel(50), top: -5 }} /> */}
                                </View>
                                <FlatList

                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                    data={videosData}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={({ item, index }) => (

                                        <Card
                                            image={item?.backBlazeVideoUrl ? item?.backBlazeVideoUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                            style={{ margin: Utils.scaleWithPixel(5), left: Utils.scaleWithPixel(-10) }}
                                            onPress={() => navigation.navigate('HotelDetail')}>
                                            <Text title2 style={{ top: Utils.scaleWithPixel(45), left: Utils.scaleWithPixel(-10), fontFamily: "ProximaNova-Medium", fontSize: 18 }}>
                                                {item?.title}
                                            </Text>
                                            <Text subhead style={{ top: Utils.scaleWithPixel(50), left: Utils.scaleWithPixel(-10), fontFamily: "ProximaNova-Medium", color: "#909090" }}>
                                                {item?.viewCount}
                                            </Text>
                                            <ProfileAuthor5
                                                image={item.uploadedBy?.backBlazeImageUrl ? item.uploadedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                                description={item?.viewCount ? item?.viewCount : "0"}
                                                style={{ top: Utils.scaleWithPixel(-170), left: Utils.scaleWithPixel(-10) }}
                                            />
                                        </Card>
                                    )}
                                />
                            </Neomorph>
                        </View>
                        <View style={{ left: Utils.scaleWithPixel(0), top: Utils.scaleWithPixel(-60) }}>
                            <FlatList
                                data={videosData}
                                horizontal={true}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (
                                    <HomeLatestVideosComponent
                                        image={item?.backBlazeVideoUrl ? item?.backBlazeVideoUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                        time={item?.Duration}
                                        onPress={() => {
                                            success(item)
                                        }}>
                                        {/* <ProfileAuthor
                                            image={item.uploadedBy?.backBlazeImageUrl ? item.uploadedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                            name={item?.uploadedBy?.username}
                                            title={item?.title}
                                            description={item?.viewCount ? item?.viewCount : "0"}
                                            dateAndTime={item?.createdAt ? item?.createdAt : "0"}
                                            style={{ paddingHorizontal: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(210) }}
                                        /> */}
                                    </HomeLatestVideosComponent>
                                )}
                            />
                        </View>
                        <View style={{ top: Utils.scaleWithPixel(-90) }}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={videosData}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (

                                    <SpolasCard
                                        image={item?.backBlazeVideoUrl ? item?.backBlazeVideoUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                        style={{ margin: Utils.scaleWithPixel(10), left: Utils.scaleWithPixel(-10), height: Utils.scaleWithPixel(400), width: Utils.scaleWithPixel(310) }}
                                        onPress={() => navigation.navigate('HotelDetail')}>
                                        <View style={styles.time}>
                                            <Text subhead style={{ fontFamily: "ProximaNova", fontSize: 18, color: "#ffffff", alignSelf: "flex-start" }}> {item?.Duration}</Text>
                                        </View>
                                        <Text subhead style={{ top: Utils.scaleWithPixel(-20), left: Utils.scaleWithPixel(5), fontFamily: "ProximaNova", color: "#909090" }}>
                                            {/* {item.title1} */}
                                        </Text>
                                        {/* <Text title2 style={{ top: Utils.scaleWithPixel(-20), fontFamily: "ProximaNova", fontWeight: "bold", fontSize: 20 }}>
                                            {item.title2}
                                        </Text> */}
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name="youtube" size={22} color="red" style={{ left: Utils.scaleWithPixel(-1), top: Utils.scaleWithPixel(-347) }} />
                                            <Text style={{ alignSelf: "flex-end", top: Utils.scaleWithPixel(-350), fontFamily: "ProximaNova-Semibold" }}>Spolas</Text>
                                        </View>
                                        <ProfileAuthor
                                            image={item.uploadedBy?.backBlazeImageUrl ? item.uploadedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                            name={item?.uploadedBy?.username}
                                            title={item?.title}
                                            description={item?.likesCount}
                                            dateAndTime={item?.createdAt}
                                            style={{ width: Utils.scaleWithPixel(240), top: Utils.scaleWithPixel(-35) }}
                                        />
                                    </SpolasCard>
                                )}
                            />
                        </View>
                        <View style={{ flexDirection: "row", top: Utils.scaleWithPixel(-100) }}>
                            <Text style={{ color: "#ffffff", fontSize: 20, top: Utils.scaleWithPixel(-5), left: Utils.scaleWithPixel(15), fontFamily: "ProximaNova", fontWeight: "bold" }}>Get inspired our influencers</Text>
                            {/* <Icon name="chevron-right" color="white" size={28} style={{ left: 250, top: 15 }} /> */}
                        </View>
                        <View style={{ top: Utils.scaleWithPixel(-90) }}>
                            <FlatList
                                horizontal={true}
                                // contentContainerStyle={{ alignSelf: 'flex-start', marginTop: -15 }}
                                // showsHorizontalScrollIndicator={false}
                                data={creatorsData}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (

                                    <CreatorsCard
                                        style={{ margin: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(-10) }}
                                    // image={item.image}
                                    // name={item.name}
                                    >
                                        <Text style={{ top: Utils.scaleWithPixel(-15), left: Utils.scaleWithPixel(5), color: "#909090", fontFamily: "ProximaNova-Medium", fontSize: 14, alignSelf: "center", }}>
                                            {item?.followersProfiles?.length ? item?.followersProfiles?.length : '0'} Followers
                                        </Text>
                                        <Text style={{ top: Utils.scaleWithPixel(-10), left: Utils.scaleWithPixel(5), alignSelf: "center", color: "#e3dce1", fontWeight: "bold", fontFamily: "ProximaNova-Bold", fontSize: 14 }}>
                                            {item?.username}
                                        </Text>
                                        <Text style={{ top: Utils.scaleWithPixel(-8), left: Utils.scaleWithPixel(5), alignSelf: "center", color: "#909090", fontFamily: "ProximaNova-Medium", fontSize: 14 }}>
                                            {item.email}
                                        </Text>

                                        <ProfileAuthor2
                                            style={{ left: Utils.scaleWithPixel(27), top: Utils.scaleWithPixel(-145), height: Utils.scaleWithPixel(100), width: Utils.scaleWithPixel(100) }}
                                            image={item?.backBlazeImageUrl ? item?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                        // name={item.name}
                                        // description={item.detail}

                                        />
                                    </CreatorsCard>
                                )}

                            />
                        </View>
                        {/* <View style={{ paddingLeft: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(-140) }}>
                            <Neomorph
                                inner
                                style={{
                                    padding: Utils.scaleWithPixel(0),
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(20),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(500),
                                    height: Utils.scaleWithPixel(310),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <View style={{ flexDirection: "row", alignItems: "flex-start" }}>
                                    
                                    <Text bold style={styles.text, { fontSize: 17, left: Utils.scaleWithPixel(-165), top: Utils.scaleWithPixel(20), fontFamily: "ProximaNova" }}>Popular in Rewards</Text>
                                    <Image source={require('../../assets/images2/right_icn.png')} style={{ height: Utils.scaleWithPixel(13), width: Utils.scaleWithPixel(13), left: Utils.scaleWithPixel(0), top: Utils.scaleWithPixel(20), margin: Utils.scaleWithPixel(5) }} />
                                </View>
                                <FlatList

                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                    data={productsData}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={({ item, index }) => (

                                        <ProductsCard
                                            // image={item.image}
                                            // price={item.price}
                                            // prouctsObject={item}
                                            style={{ margin: Utils.scaleWithPixel(25), height: Utils.scaleWithPixel(180), width: Utils.scaleWithPixel(120), left: Utils.scaleWithPixel(-20) }}
                                            onPress={() => navigation.navigate('ProductDetail')}>
                                            <Text body1 semibold grayColor style={{ top: Utils.scaleWithPixel(55), fontFamily: "ProximaNova" }}>
                                            {item.productName} 
                                            </Text>
                                            <Text title3 style={{ top: Utils.scaleWithPixel(60), fontFamily: "ProximaNova" }}>
                                            Extra {item?.productRequiredRewardedPoints}% Off on {item.productName}
                                            </Text>
                                            <ProfileAuthor3
                                                style={{ left: Utils.scaleWithPixel(27), top: Utils.scaleWithPixel(-145), height: Utils.scaleWithPixel(100), width: Utils.scaleWithPixel(100) }}
                                                image={item?.backBlazeProductUrl ? item?.backBlazeProductUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                            // name={item.name}
                                            // description={item.detail}

                                            />
                                        </ProductsCard>
                                    )}
                                />
                            </Neomorph>
                        </View> */}
                        <View style={{ top: -170 }}>
                            <FlatList
                                data={videosData}
                                // horizontal={true}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item, index }) => (
                                    <HomeItem1
                                        image={item?.backBlazeVideoUrl ? item?.backBlazeVideoUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                        // title={item.title}
                                        time={item?.Duration}
                                        // description={item.description}
                                        onPress={() => {
                                            success(item)
                                        }}>
                                        <ProfileAuthor
                                            image={item.uploadedBy?.backBlazeImageUrl ? item.uploadedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                                            name={item?.uploadedBy?.username}
                                            title={item?.title}
                                            description={item?.viewCount ? item?.viewCount : "0"}
                                            dateAndTime={item?.createdAt ? item?.createdAt : "0"}
                                            style={{ paddingHorizontal: Utils.scaleWithPixel(10), top: Utils.scaleWithPixel(210) }}
                                        />
                                    </HomeItem1>
                                )}
                            /></View>
                    </Neomorph>


                    {/* Pricing & Booking Process */}
                </SafeAreaView>
            </ScrollView>
        </View >
    );

};
