import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  imageBackground: {
    height: Utils.scaleWithPixel(140),
    width: '100%',
    position: 'absolute',
  },
  searchForm: {
    padding: Utils.scaleWithPixel(10),
    borderRadius: Utils.scaleWithPixel(10),
    borderWidth: Utils.scaleWithPixel(0.5),
    width: '100%',
    shadowColor: 'black',
    shadowOffset: {width: Utils.scaleWithPixel(1.5), height: Utils.scaleWithPixel(1.5)},
    shadowOpacity: Utils.scaleWithPixel(0.3),
    shadowRadius: Utils.scaleWithPixel(1),
    elevation: Utils.scaleWithPixel(1),
  },
  contentServiceIcon: {
    marginTop: Utils.scaleWithPixel(10),
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  contentCartPromotion: {
    marginTop: Utils.scaleWithPixel(10),
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  btnPromotion: {
    height: Utils.scaleWithPixel(25),
    borderRadius: Utils.scaleWithPixel(3),
    paddingHorizontal: Utils.scaleWithPixel(10),
    paddingVertical: Utils.scaleWithPixel(5),
  },
  contentHiking: {
    marginTop: Utils.scaleWithPixel(20),
    marginLeft: Utils.scaleWithPixel(20),
    marginBottom: Utils.scaleWithPixel(10),
  },
  promotionBanner: {
    height: Utils.scaleWithPixel(100),
    width: '100%',
    marginTop: Utils.scaleWithPixel(10),
  },
  line: {
    height: Utils.scaleWithPixel(1),
    marginTop: Utils.scaleWithPixel(10),
    marginBottom: Utils.scaleWithPixel(15),
  },
  iconContent: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Utils.scaleWithPixel(36),
    height: Utils.scaleWithPixel(36),
    borderRadius: Utils.scaleWithPixel(18),
  },
  itemService: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    paddingTop: Utils.scaleWithPixel(10),
  },
  promotionItem: {
    width: Utils.scaleWithPixel(100),
    height: Utils.scaleWithPixel(100),
    padding: Utils.scaleWithPixel(1),
                 
  },
  tourItem: {
    width: Utils.scaleWithPixel(135),
    height: Utils.scaleWithPixel(160),
  },
  titleView: {
    paddingHorizontal: Utils.scaleWithPixel(100),
    paddingTop: Utils.scaleWithPixel(10),
    paddingBottom: Utils.scaleWithPixel(15),
    // justifyContent: 'center',
    alignSelf: 'center',
    fontSize:23,
    fontFamily:"ProximaNova"
  },
  btnClearSearch: {
    position: 'absolute',
    right: Utils.scaleWithPixel(0),
    alignItems: 'center',
    justifyContent: 'center',
    width: Utils.scaleWithPixel(30),
    height: '100%',
  },
  serviceItem: {
    marginTop:Utils.scaleWithPixel(10),
    paddingHorizontal: Utils.scaleWithPixel(15),
    justifyContent: 'center',
    alignItems: 'center',
  },
  serviceCircleIcon: {
    width: Utils.scaleWithPixel(36),
    height: Utils.scaleWithPixel(36),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Utils.scaleWithPixel(18),
  },
  icon: {
    left:Utils.scaleWithPixel(-140),
    top:Utils.scaleWithPixel(12)

  },
  text:{
    top:Utils.scaleWithPixel(-10),
    left:Utils.scaleWithPixel(-8),
    alignSelf:"center",
    fontSize:Utils.scaleWithPixel(14),
    fontFamily:"ProximaNova",
    // fontWeight:'semibold',
    color:"#ffffff"
  },
  image: {
    height: Utils.scaleWithPixel(80),
    width: Utils.scaleWithPixel(80),
    borderRadius: Utils.scaleWithPixel(70)
},
time:{
  top: Utils.scaleWithPixel(-325),
  left: Utils.scaleWithPixel(240), 
  borderWidth:Utils.scaleWithPixel(1),
  borderColor:"#999999",
  borderRadius:Utils.scaleWithPixel(5),
  width:Utils.scaleWithPixel(30) ,
  backgroundColor:"#000000",
  paddingLeft:Utils.scaleWithPixel(2),
  alignContent:"center"
}
});
