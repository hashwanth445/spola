import React, { useState, useEffect,useRef } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  Platform,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import io from 'socket.io-client';
import { useDispatch, useSelector } from 'react-redux';
import {
  Image,
  Header,
  HeaderHome,
  Text,
  Icon,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  EventCard,
  EventItem,
  FilterSort,
  Search,
  ProfileAuthor,
  ProfileAuthor2,
  ProfileAuthor4,
  CreatorsCard,
  HomeLatestVideosComponent,
  HomeItem1,
  Product,
  SpolasCard,
} from '@components';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import Icon1 from 'react-native-vector-icons/Feather';

import { BaseStyle, Images, BaseColor, useTheme } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { EventListData } from '@data';
import { Shadow } from 'react-native-neomorph-shadows';
import { launchImageLibrary } from 'react-native-image-picker';
// import { VESDK } from 'react-native-videoeditorsdk';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { Menu, MenuItem, MenuDivider } from 'react-native-material-menu';
import { fontSize } from 'styled-system';
import { audioroomGetRequest } from '../../api/audioroom';
import { moviesGetRequest } from '../../api/movies';
import { productsGetRequest } from '../../api/products';
import { rewardedvideosGetRequest } from '../../api/rewardedvideos';
import { shortsGetRequest } from '../../api/shorts';
import { videosGetRequest, videosPostRequest } from '../../api/videos';
import { creatorsGetRequest } from '../../api/categories';
import { ProfileAuthor1, ProfileAuthor3, ProfileAuthor5 } from '../../components';
import Livedata from './Livedata';

export default function Home({ navigation, route }) {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [visible, setVisible] = useState(false);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);

  const hideMenu = () => setVisible(false);
  const [shouldShowFollow, setShouldShowFollow] = useState(true);

  const showMenu = () => setVisible(true);
  // const socketRef = useRef();

  const socketRef = io("http://192.168.0.103:1337");


  useEffect(() => {
    console.log('-----------------------------------messageeee=======')
   
  //  socketRef.current = io('http://localhost:1337');

   socketRef.emit('drawing')

   socketRef.on("connect", () => {
    console.log(socketRef.connected,"-----------------------------------==========================",socketRef.id); // x8WIv7-mJelg7on_ALbx
  });
   socketRef.on('drawing', message => {
     console.log('messageeee=======1111')
  });

  // socketRef.current.on('roomData', ({ users }) => {
  //   setUsers(users);
  // });
  }, []);

  const dispatch = useDispatch();
  useEffect(() => {
  //   socketRef = io("http://localhost:1337");

  //   socketRef.emit('drawing', () => {});
 
  //   socketRef.on('message', message => {
  //     console.log('messageeee=======1111')
  //  });
    dispatch(audioroomGetRequest());
    dispatch(moviesGetRequest(userToken));
    dispatch(shortsGetRequest());
    dispatch(videosGetRequest(userToken));
    dispatch(rewardedvideosGetRequest());
    dispatch(productsGetRequest());
    dispatch(creatorsGetRequest(userToken));

  }, [dispatch]);

  const creatorsData = useSelector(state => state.CategoriesReducer.creatorsData,);
  const audioroomData = useSelector(state => state.AudioroomReducer.audioroomData,);
  const moviesData = useSelector(state => state.MoviesReducer.moviesData);
  const shortsData = useSelector(state => state.ShortsReducer.shortsData);
  const videosData = useSelector(state => state.VideosReducer.videosData);
  const productsData = useSelector(state => state.ProductsReducer.productsData);
  const rewardedvideos = useSelector(state => state.RewardedvideosReducer.rewardedvideosData,);

  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const userid = UserData?.user?._id;

  const Rusers = [];
  
  creatorsData?.forEach((i) => {
    if(i?._id === userid) {
    // Rusers.push(i);
    console.log("---1-----",i,UserData?.user?.id);
    }
    else {
      console.log("---2-----",i,userid);
      Rusers.push(i)
    }
  })
console.log("Users",Rusers);

  console.log('shortsData', shortsData);
  console.log('videosData', videosData);
  console.log('productsData', productsData);

  const [count, setCount] = useState(true);

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const deltaY = new Animated.Value(0);
  const [loading, setLoading] = useState(false);

  const scrollAnim = new Animated.Value(0);
  const offsetAnim = new Animated.Value(0);
  const clampedScroll = Animated.diffClamp(
    Animated.add(
      scrollAnim.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
        extrapolateLeft: 'clamp',
      }),
      offsetAnim,
    ),
    0,
    40,
  );

  const success = async videoUrlObject => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('HomeDetail', { videoUrlObject });
    setLoading(false);
  };

  const rewarded = async videoUrlObject => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('RewardedVideoPlayer', { videoUrlObject });
    setLoading(false);
  };

  const spolas = async spolaUrlObject => {
    setLoading(true);

    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('SpolaScreen', { spolaUrlObject });
    setLoading(false);
  };

  const commonUser = async userObjectUrl => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('CommonProfile', { userObjectUrl });
    setLoading(false);
  };
  const product = async productObject => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('ProductDetail', { productObject });
    setLoading(false);
  };

  const [refreshing] = useState(false);
  const [modeView, setModeView] = useState('block');
  const [list] = useState(EventListData);
  /**
   * @description Show icon services on form searching
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   * @returns
   */
  // const renderIconService = () => {
  //   return (
  //     <FlatList
  //       data={icons}
  //       numColumns={4}
  //       keyExtractor={(item, index) => index.toString()}
  //       renderItem={({ item }) => {
  //         return (
  //           <TouchableOpacity
  //             style={styles.itemService}
  //             activeOpacity={0.9}
  //             onPress={() => {
  //               navigation.navigate(item.route);
  //             }}>
  //             <View
  //               style={[styles.iconContent, { backgroundColor: colors.card }]}>
  //               <Icon name={item.icon} size={18} color={colors.primary} solid />
  //             </View>
  //             <Text footnote grayColor numberOfLines={1}>
  //               {t(item.name)}
  //             </Text>
  //           </TouchableOpacity>
  //         );
  //       }}
  //     />
  //   );
  // };

  const heightImageBanner = Utils.scaleWithPixel(140);
  const marginTopBanner = heightImageBanner - heightHeader;
  const navbarTranslate = clampedScroll.interpolate({
    inputRange: [0, 40],
    outputRange: [0, -40],
    extrapolate: 'clamp',
  });

  // const imagePicker = () => {
  //   let options = {
  //     mediaType: 'any',
  //     durationLimit: 600,
  //     saveToPhotos: true,
  //   };

  //   launchImageLibrary(options, response => {
  //     if (response.didCancel) {
  //       alert('You did not select any video');
  //     } else if (response.error) {
  //       console.log('ImagePicker Error: ', response.error);
  //     } else if (response.customButton) {
  //       console.log('User tapped custom button: ', response.customButton);
  //       // } else {
  //       //   VESDK.openEditor(response.assets[0].uri).then(res =>
  //       //     console.log('Done editing video', res),
  //       //   );
  //     }
  //   });
  // };

  
  return (
    <View style={{ flex: 1 }}>
      <HeaderHome
        // title={t('profile')}
        renderLeft={() => {
          return <IconSimple name="bell" size={22} color="#808080" />;
        }}
        onPressLeft={() => {
          userToken == null
            ? navigation.navigate('Post')
            : navigation.navigate('SignIn');
         
        }}
        renderRight={() => {
          return <Icon1 name="message-square" size={24} color="#949494" />;
        }}
        onPressRight={() => {
          userToken == null
            ? navigation.navigate('Post')
            : navigation.navigate('Messenger');
        }}
        renderRightSecond={() => {
          return <Icon1 name="search" size={24} color="#949494" />;
        }}
        onPressRightSecond={() => {
          navigation.navigate('SearchHistory');
        }}
      />

      <SafeAreaView style={{ flex: 1 }} edges={['right', 'left', 'bottom']}>
        <ScrollView
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: { y: deltaY },
              },
            },
          ])}
          onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
          scrollEventThrottle={8}>
          <View>
            <View
              style={{
                flex: 3,
                flexDirection: 'row',
                left: Utils.scaleWithPixel(0),
                justifyContent: 'center',
              }}>
              <Text style={styles.titleView}>{t('Discover')}</Text>
              <Icon1
                name="chevron-down"
                size={28}
                color="#DDDDDD"
                style={{ alignSelf: 'center', right: Utils.scaleWithPixel(90) }}
              />
            </View>
           

            <View
              style={{
                paddingLeft: Utils.scaleWithPixel(5),
                top: Utils.scaleWithPixel(0),
              }}>
              <Neomorph
                inner
                style={{
                  padding: Utils.scaleWithPixel(20),
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(20),
                  backgroundColor: colors.neoThemebg,
                  width: wp('110%'),
                  height: hp('34%'),
                  // justifyContent: 'center',
                  // alignItems: 'center',
                }}>
                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                  <Icon
                    name="youtube"
                    size={24}
                    color="red"
                    style={{
                      left: Utils.scaleWithPixel(-10),
                      top: Utils.scaleWithPixel(-7),
                    }}
                  />
                  <Text style={styles.text}>Live</Text>
                  <Icon
                    name="moon-full"
                    size={4}
                    style={{
                      color: 'red',
                      top: Utils.scaleWithPixel(-4),
                      right: Utils.scaleWithPixel(8),
                      borderRadius: Utils.scaleWithPixel(20),
                    }}
                  />
                  <Icon
                    name="chevron-right"
                    color="white"
                    size={30}
                    style={{ left: 275, top: -10 }}
                  />
                  {/* <Image source={require('../../assets/images2/right_icn.png')} style={{ height: Utils.scaleWithPixel(13), width: Utils.scaleWithPixel(13), left: Utils.scaleWithPixel(240), top: Utils.scaleWithPixel(-5) }} /> */}
                </View>

                <Livedata />
              </Neomorph>
            </View>
          </View>
          {/* Hiking */}
          <View
            style={{
              top: Utils.scaleWithPixel(-5),
              left: Utils.scaleWithPixel(0),
            }}>
            <FlatList
              data={videosData}
              horizontal={true}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <HomeLatestVideosComponent
                  image={
                    item?.backBlazeVideoUrl
                      ? item?.backBlazeVideoUrl
                      : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                  }
                  time={item?.Duration}
                  
                  onPress={() => {
                    success(item);
                  }}
                  channelImage={
                    item.channelVideo?.channel?.backBlazeCoverImg
                      ? item.channelVideo?.channel?.backBlazeCoverImg
                      : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                  }
                  name={item?.channelVideo?.channel?.channelName}
                  title={item?.title}
                  description={item?.viewCount ? item?.viewCount : '0'}
                  dateAndTime={item?.createdAt ? item?.createdAt : '0'}>
                  {/* <ProfileAuthor
                    image={
                      item.channelVideo?.channel?.backBlazeCoverImg
                        ? item.channelVideo?.channel?.backBlazeCoverImg
                        : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                    }
                    name={item?.channelVideo?.channel?.channelName}
                    title={item?.title}
                    description={item?.viewCount ? item?.viewCount : '0'}
                    dateAndTime={item?.createdAt ? item?.createdAt : '0'}
                    style={{
                      paddingHorizontal: Utils.scaleWithPixel(10),
                      top: Utils.scaleWithPixel(220),
                    }}
                  /> */}
                </HomeLatestVideosComponent>
              )}
            />
          </View>
          {/* spolas Latest start */}
          <View
            style={{
              paddingLeft: Utils.scaleWithPixel(5),
              top: Utils.scaleWithPixel(-60),
            }}>
            <Neomorph
              inner
              style={{
                padding: Utils.scaleWithPixel(20),
                shadowRadius: Utils.scaleWithPixel(7),
                borderRadius: Utils.scaleWithPixel(25),
                backgroundColor: colors.neoThemebg,
                width: Utils.scaleWithPixel(350),
                height: Utils.scaleWithPixel(300),
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                {/* <Icon name="youtube" size={22} color="red" style={{ left: -168, top: -8 }} /> */}
                <View style={{ flexDirection: 'row', justifyContent: "space-around" }}>
                  <Text
                    semibold
                    style={
                      (styles.text,
                        { top: -5, fontSize: 18, fontFamily: 'ProximaNova' })
                    }>
                    Spolas
                  </Text>
                  <Image source={require('../../assets/logo/Spola_Logo_25x25.png')} style={{ height: Utils.scaleWithPixel(15), width: Utils.scaleWithPixel(15), left: 10, top: -3 }} />
                </View>
                <Icon
                  name="chevron-right"
                  color="white"
                  size={30}
                  style={{ left: 265, top: -10 }}
                />
              </View>
              <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={shortsData}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <Card
                    image={
                      item?.backBlazeSpolaUrl
                        ? item?.backBlazeSpolaUrl
                        : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                    }
                    style={{
                      margin: Utils.scaleWithPixel(5),
                      left: Utils.scaleWithPixel(-10),
                    }}
                    onPress={() => spolas(item)}>
                    <Text title1 numberOfLines={2} bold style={{ top: Utils.scaleWithPixel(40), left: Utils.scaleWithPixel(-15), fontFamily: "ProximaNova", color: '#e2e2e2', fontSize: 14 }}>
                      {item?.description}
                    </Text>
                    <Text subhead bold style={{ top: Utils.scaleWithPixel(45), left: Utils.scaleWithPixel(-10), fontFamily: "ProximaNova", fontSize: 12, color: '#B3B6B7' }}>
                      {item?.viewCount} views
                    </Text>
                    <ProfileAuthor5
                      image={
                        item.uploadedBy?.backBlazeImageUrl
                          ? item.uploadedBy?.backBlazeImageUrl
                          : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png'
                      }
                      // name={item.name}
                      description={item.detail}
                      style={{
                        top: Utils.scaleWithPixel(-180),
                        left: Utils.scaleWithPixel(-10),
                      }}
                    />
                  </Card>
                )}
              />
            </Neomorph>
          </View>
          {/* spolas Latest End */}
          {/* ?rewarded  Videos Start*/}
          <Neomorph
            inner
            style={{
              // padding: 20,
              shadowRadius: Utils.scaleWithPixel(7),
              borderRadius: Utils.scaleWithPixel(20),
              backgroundColor: colors.neoThemebg,
              width: Utils.scaleWithPixel(370),
              height: Utils.scaleWithPixel(260),
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: Utils.scaleWithPixel(-30),
              left: Utils.scaleWithPixel(5),
              top: Utils.scaleWithPixel(-5),
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginTop: Utils.scaleWithPixel(10),
                alignItems: 'flex-start',
                marginLeft: 50,
              }}>
              <Icon
                name="youtube"
                size={28}
                color="red"
                style={{
                  left: Utils.scaleWithPixel(-128),
                  top: Utils.scaleWithPixel(21),
                }}
              />
              <Text

                style={{
                  left: Utils.scaleWithPixel(-125),
                  top: Utils.scaleWithPixel(25),
                  fontFamily: 'ProximaNova',
                  fontSize: 17
                }}>
                Rewarded Videos
              </Text>
              <Icon
                name="chevron-right"
                color="white"
                size={30}
                style={{ left: 60, top: 20 }}
              />
              {/* <Image source={require('../../assets/images2/right_icn.png')} style={{ height: Utils.scaleWithPixel(13), width: Utils.scaleWithPixel(-5), left: Utils.scaleWithPixel(100), top: Utils.scaleWithPixel(2) }} /> */}
            </View>
            <View
              style={{
                left: Utils.scaleWithPixel(0),
                marginTop: Utils.scaleWithPixel(-35),
              }}>
              <FlatList
                data={videosData}
                horizontal={true}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  // <HomeLatestVideosComponent
                  //   image={
                  //     item?.backBlazeVideoUrl
                  //       ? item?.backBlazeVideoUrl
                  //       : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                  //   }
                  //   time={item?.Duration}
                  //   onPress={() => {
                  //     rewarded(item);
                  //   }}>
                  //   <ProfileAuthor
                  //     image={
                  //       item.uploadedBy?.backBlazeImageUrl
                  //         ? item.uploadedBy?.backBlazeImageUrl
                  //         : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png'
                  //     }
                  //     name={item?.uploadedBy?.username}
                  //     title={item?.title}
                  //     description={item?.viewCount}
                  //     dateAndTime={item?.createdAt ? item?.createdAt : '0'}
                  //     onPress={() => {
                  //       commonUser(item?.uploadedBy);
                  //     }}
                  //     style={{
                  //       paddingHorizontal: Utils.scaleWithPixel(10),
                  //       top: Utils.scaleWithPixel(215),
                  //       left: 10,
                  //     }}
                  //   />
                  // </HomeLatestVideosComponent>
                  <HomeLatestVideosComponent
                  image={
                    item?.backBlazeVideoUrl
                      ? item?.backBlazeVideoUrl
                      : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                  }
                  time={item?.Duration}
                  
                  onPress={() => {
                    success(item);
                  }}
                  channelImage={
                    item.channelVideo?.channel?.backBlazeCoverImg
                      ? item.channelVideo?.channel?.backBlazeCoverImg
                      : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                  }
                  name={item?.channelVideo?.channel?.channelName}
                  title={item?.title}
                  description={item?.viewCount ? item?.viewCount : '0'}
                  dateAndTime={item?.createdAt ? item?.createdAt : '0'}>
                  {/* <ProfileAuthor
                    image={
                      item.channelVideo?.channel?.backBlazeCoverImg
                        ? item.channelVideo?.channel?.backBlazeCoverImg
                        : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                    }
                    name={item?.channelVideo?.channel?.channelName}
                    title={item?.title}
                    description={item?.viewCount ? item?.viewCount : '0'}
                    dateAndTime={item?.createdAt ? item?.createdAt : '0'}
                    style={{
                      paddingHorizontal: Utils.scaleWithPixel(10),
                      top: Utils.scaleWithPixel(220),
                    }}
                  /> */}
                </HomeLatestVideosComponent>
                )}
              />
            </View>
          </Neomorph>
          {/* Rewarded Videos End */}
          {/* Trending Videos Start */}
          <View
            style={{
              top: Utils.scaleWithPixel(-55),
              left: Utils.scaleWithPixel(0),
            }}>
            <FlatList
              data={videosData}
              horizontal={true}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <HomeLatestVideosComponent
                  image={
                    item?.backBlazeVideoUrl
                      ? item?.backBlazeVideoUrl
                      : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                  }
                  time={item?.Duration}
                  
                  onPress={() => {
                    success(item);
                  }}
                  channelImage={
                    item.channelVideo?.channel?.backBlazeCoverImg
                      ? item.channelVideo?.channel?.backBlazeCoverImg
                      : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                  }
                  name={item?.channelVideo?.channel?.channelName}
                  title={item?.title}
                  description={item?.viewCount ? item?.viewCount : '0'}
                  dateAndTime={item?.createdAt ? item?.createdAt : '0'}>
                  {/* <ProfileAuthor
                    image={
                      item.channelVideo?.channel?.backBlazeCoverImg
                        ? item.channelVideo?.channel?.backBlazeCoverImg
                        : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                    }
                    name={item?.channelVideo?.channel?.channelName}
                    title={item?.title}
                    description={item?.viewCount ? item?.viewCount : '0'}
                    dateAndTime={item?.createdAt ? item?.createdAt : '0'}
                    style={{
                      paddingHorizontal: Utils.scaleWithPixel(10),
                      top: Utils.scaleWithPixel(220),
                    }}
                  /> */}
                </HomeLatestVideosComponent>
                // <HomeLatestVideosComponent
                //   image={
                //     item?.backBlazeVideoUrl
                //       ? item?.backBlazeVideoUrl
                //       : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                //   }
                //   time={item?.Duration}
                //   onPress={() => {
                //     success(item);
                //   }}>
                //   <ProfileAuthor
                //     image={
                //       item.uploadedBy?.backBlazeImageUrl
                //         ? item.uploadedBy?.backBlazeImageUrl
                //         : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png'
                //     }
                //     name={item?.uploadedBy?.username}
                //     title={item?.title}
                //     description={item?.viewCount ? item?.viewCount : '0'}
                //     dateAndTime={item?.createdAt ? item?.createdAt : '0'}
                //     style={{
                //       paddingHorizontal: Utils.scaleWithPixel(10),
                //       top: Utils.scaleWithPixel(210),
                //     }}
                //     onPress={() => {
                //       commonUser(item?.uploadedBy);
                //     }}
                //   />
                // </HomeLatestVideosComponent>
              )}
            />
          </View>
          {/* Trending Videos End */}
          {/* Spolas Trending Start */}
          <View style={{ top: Utils.scaleWithPixel(-80) }}>
            <FlatList
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={shortsData}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <SpolasCard
                  image={
                    item?.backBlazeSpolaUrl
                      ? item?.backBlazeSpolaUrl
                      : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                  }
                  style={{
                    margin: Utils.scaleWithPixel(10),
                    left: Utils.scaleWithPixel(-10),
                    height: Utils.scaleWithPixel(400),
                    width: Utils.scaleWithPixel(310),
                  }}
                  onPress={() => spolas(item)}>
                  <View style={styles.time}>
                    <Text style={{ fontFamily: "ProximaNova", fontSize: 15, color: "#ffffff", alignSelf: "flex-start" }}> {item?.duration}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignContent: "center", top: Utils.scaleWithPixel(-345), borderWidth: Utils.scaleWithPixel(1), borderColor: "#999999", width: Utils.scaleWithPixel(55),height:23, borderRadius: Utils.scaleWithPixel(5), backgroundColor: "#000000" }}>
                    <Image source={require('../../assets/images2/reels_icn.png')} tintColor="white" style={{ height: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(10),marginLeft:5,top:3 }} />
                    <Text
                      style={{
                        color: 'white',
                        // top: Utils.scaleWithPixel(-350),
                        fontFamily: 'ProximaNova',
                        marginLeft: 3,
                        top:1,
                        fontSize: 15
                      }}>
                      Spolas
                    </Text>
                  </View>
                  <ProfileAuthor
                    image={
                      item.uploadedBy?.backBlazeImageUrl
                        ? item.uploadedBy?.backBlazeImageUrl
                        : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png'
                    }
                    name={item?.uploadedBy?.username}
                    title={item?.title}
                    description={item?.viewCount}
                    dateAndTime={item?.createdAt}
                    onPress={() => {
                      commonUser(item.uploadedBy);
                    }}
                    style={{
                      width: Utils.scaleWithPixel(240),
                      top: Utils.scaleWithPixel(-35),
                    }}
                  />
                </SpolasCard>
              )}
            />
          </View>
          {/* Spolas Trending End */}
          {/* <Neomorph
            inner
            style={{
              shadowRadius: 7,
              borderRadius: 25,
              backgroundColor: colors.neoThemebg,
              width: 425,
              height: 280,
              justifyContent: 'center',
              marginLeft: 5,
              marginTop: 25,

            }}
          > */}
          {/* Influencers Start */}
          <View style={{ flexDirection: "row", top: Utils.scaleWithPixel(-80) }}>
            <Text style={{ color: "#ffffff", fontSize: 16, top: Utils.scaleWithPixel(-5), left: Utils.scaleWithPixel(15), fontFamily: "ProximaNova", fontWeight: "bold" }}>Get inspired our influencers</Text>

          </View>
           {/* Influenceres End */}
          <View style={{ top: Utils.scaleWithPixel(-80) }}>
            <FlatList
              horizontal={true}
              // contentContainerStyle={{ alignSelf: 'flex-start', marginTop: -15 }}
              // showsHorizontalScrollIndicator={false}
              data={Rusers}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <CreatorsCard
                  style={{
                    margin: Utils.scaleWithPixel(10),
                    top: Utils.scaleWithPixel(-10),
                  }}
                  onPress={() => commonUser(item)}
                // image={item.image}
                // name={item.name}
                >
                  <Text style={{ top: Utils.scaleWithPixel(-15), left: Utils.scaleWithPixel(5), alignSelf: "center", color: "#909090", fontFamily: "ProximaNova-Medium", fontSize: 12 }}>
                    {item?.followersProfiles?.length ? item?.followersProfiles?.length : '0'} Followers
                  </Text>
                  <Text
                    style={{
                      top: Utils.scaleWithPixel(-10),
                      left: Utils.scaleWithPixel(5),
                      alignSelf: 'center',
                      color: '#e3dce1',
                      fontWeight: 'bold',
                      fontFamily: 'ProximaNova-Bold',
                      fontSize: 14,
                    }}>
                    {item?.username}
                  </Text>
                  <Text style={{ top: Utils.scaleWithPixel(-8), left: Utils.scaleWithPixel(5), alignSelf: "center", color: "#909090", fontFamily: "ProximaNova-Medium", fontSize: 12 }}>
                    {item.email}
                  </Text>

                  <ProfileAuthor2
                    style={{
                      left: Utils.scaleWithPixel(27),
                      top: Utils.scaleWithPixel(-145),
                      height: Utils.scaleWithPixel(100),
                      width: Utils.scaleWithPixel(100),
                    }}
                    image={
                      item?.backBlazeImageUrl
                        ? item?.backBlazeImageUrl
                        : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png'
                    }
                  // name={item.name}
                  // description={item.detail}
                  />
                </CreatorsCard>
              )}
            />
          </View>
          {/* </Neomorph> */}
          <View></View>
          {/* Promotion */}
          {/* Popular In Rewards Start */}
          <View
            style={{
              paddingLeft: Utils.scaleWithPixel(10),
              top: Utils.scaleWithPixel(-110),
            }}>
            <Neomorph
              inner
              style={{
                padding: Utils.scaleWithPixel(20),
                shadowRadius: Utils.scaleWithPixel(7),
                borderRadius: Utils.scaleWithPixel(20),
                backgroundColor: colors.neoThemebg,
                width: Utils.scaleWithPixel(340),
                height: Utils.scaleWithPixel(260),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                {/* <Icon name="youtube" size={26} color="red" style={{ left: -170, top: -15 }} /> */}
                <Text
                  bold
                  style={
                    (styles.text,
                    {
                      fontSize: 17,
                      left: Utils.scaleWithPixel(-85),
                      top: Utils.scaleWithPixel(-4),
                      fontFamily: 'ProximaNova',
                    })
                  }>
                  Popular in rewards
                </Text>
                <Icon
                  name="chevron-right"
                  color="white"
                  size={28}
                  style={{ left: 105, top: -10 }}
                />
                {/* <Image source={require('../../assets/images2/right_icn.png')} style={{ height: Utils.scaleWithPixel(13), width: Utils.scaleWithPixel(13), left: Utils.scaleWithPixel(90), top: Utils.scaleWithPixel(-5), margin: Utils.scaleWithPixel(5) }} /> */}
              </View>
              <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={productsData}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <Product
                    prouctsObject={item}
                    image={
                      item?.backBlazeProductUrl
                        ? item?.backBlazeProductUrl
                        : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                    }
                    // style={{ margin: Utils.scaleWithPixel(-10), height: Utils.scaleWithPixel(100), width: Utils.scaleWithPixel(120),alignSelf:"center",right:5,top:-50 }}
                    onPress={() => {
                      product(item);
                    }}>
                    <View style={{ marginRight: 20, top: -30 }}>
                      <Text
                        bold
                        grayColor
                        style={{ fontFamily: 'ProximaNova', fontSize: 18 }}>
                        {item.productName}
                      </Text>
                      <Text
                        body1
                        style={{ fontFamily: 'ProximaNova', fontSize: 20 }}>
                        Extra {item?.productId}% Off {'\n'}on {item.productName}
                      </Text>
                    </View>
                  </Product>
                )}
              />
            </Neomorph>
          </View>
          {/* Popular In Rewards End */}
          {/* Event*/}
          {/* Category Selected Based Random Videos Start */}
          <View style={{ top: -130 }}>
            <FlatList
              data={videosData}
              // horizontal={true}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <HomeItem1
                  image={
                    item?.backBlazeVideoUrl
                      ? item?.backBlazeVideoUrl
                      : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'
                  }
                  // title={item.title}
                  time={item?.Duration}
                  // description={item.description}
                  onPress={() => {
                    success(item);
                  }}>
                  <ProfileAuthor
                    image={
                      item.uploadedBy?.backBlazeImageUrl
                        ? item.uploadedBy?.backBlazeImageUrl
                        : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png'
                    }
                    name={item?.uploadedBy?.username}
                    title={item?.title}
                    description={item?.viewCount ? item?.viewCount : '0'}
                    dateAndTime={item?.createdAt ? item?.createdAt : '0'}
                    style={{
                      paddingHorizontal: Utils.scaleWithPixel(10),
                      top: Utils.scaleWithPixel(210),
                    }}
                    onPress={() => {
                      commonUser(item.uploadedBy);
                    }}
                  />
                </HomeItem1>
              )}
            />
          </View>
          {/* Category Selected Based Random Videos End */}
          </ScrollView>
      </SafeAreaView>
    </View>
  );
}
