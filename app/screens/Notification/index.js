import React, { useState } from 'react';
import { RefreshControl, FlatList, View, Text } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { useTranslation } from 'react-i18next';
import { Header, SafeAreaView, Icon, ListThumbCircle, ProfileAuthor2, PostItem } from '@components';
import styles from './styles';
import { NotificationData } from '@data';
import { PostData1 } from '@data';

import { Neomorph } from 'react-native-neomorph-shadows';
import { ScrollView } from 'react-native-gesture-handler';
import Messages from '../Messages';
import { MessagesData } from '@data';
import * as Utils from '@utils';

export default function Notification({ navigation }) {
  const { t } = useTranslation();
  const { colors } = useTheme();

  const [refreshing] = useState(false);
  const [posts] = useState(PostData1);
  const [notification] = useState(NotificationData);
  const [messages] = useState(MessagesData);

  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('notification')}
        renderLeft={() => {
          return (
            <Icon
              name="chevron-left"
              size={32}
              color={colors.text}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <ScrollView>
          <Text style={{ fontSize: 23, color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(30), fontWeight: "bold" }}>Today</Text>
          <FlatList
            contentContainerStyle={{ paddingHorizontal: Utils.scaleWithPixel(20), paddingVertical: Utils.scaleWithPixel(10) }}
            refreshControl={
              <RefreshControl
                colors={[colors.primary]}
                tintColor={colors.primary}
                refreshing={refreshing}
                onRefresh={() => { }}
              />
            }
            data={notification}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item, index }) => (
              <Neomorph
                outer
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(20),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(310),
                  height: Utils.scaleWithPixel(60),
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: Utils.scaleWithPixel(8),
                  marginLeft: Utils.scaleWithPixel(-10)
                }}
              >
                <ListThumbCircle
                  image={item.image}
                  txtLeftTitle={item.title}
                  // txtContent={item.description}
                  txtRight={item.date}
                  style={{ marginBottom: Utils.scaleWithPixel(5) }}
                />

              </Neomorph>
            )}
          />
          <View style={{ top: Utils.scaleWithPixel(0) }}>
            <Text style={{ fontSize: 23, color: "#DDDDDD", marginLeft: Utils.scaleWithPixel(30), fontWeight: "bold", margin: Utils.scaleWithPixel(10) }}>Yesterday</Text>
            <FlatList
              contentContainerStyle={{ paddingHorizontal: Utils.scaleWithPixel(20), paddingVertical: Utils.scaleWithPixel(10) }}
              refreshControl={
                <RefreshControl
                  colors={[colors.primary]}
                  tintColor={colors.primary}
                  refreshing={refreshing}
                  onRefresh={() => { }}
                />
              }
              data={notification}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <Neomorph
                  outer
                  style={{
                    shadowRadius: Utils.scaleWithPixel(7),
                    borderRadius: Utils.scaleWithPixel(20),
                    backgroundColor: colors.neoThemebg,
                    width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(60),
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: Utils.scaleWithPixel(8),
                    marginLeft: Utils.scaleWithPixel(-10)
                  }}
                >

                  {/* <ProfileAuthor2
                     image={item.uploadedBy?.image?.url ? 'http://3.111.30.249:1337' + item.uploadedBy?.image?.url : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                    name={item.title}
                    description={item.description}
                    // textRight={item.date}
                    style={{ paddingHorizontal: Utils.scaleWithPixel(20) }}
                  /> */}

                </Neomorph>
              )}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
