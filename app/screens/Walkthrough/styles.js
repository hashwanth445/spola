import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    paddingHorizontal: Utils.scaleWithPixel(20),
    paddingVertical: Utils.scaleWithPixel(20),
  },
  wrapper: {
    width: '100%',
    height: Utils.scaleWithPixel(350),
  },
  contentPage: {
    bottom: Utils.scaleWithPixel(0),
  },
  contentActionBottom: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Utils.scaleWithPixel(25),
  },
  img: {
    width: Utils.scaleWithPixel(200),
    height: Utils.scaleWithPixel(200),
    borderRadius: Utils.scaleWithPixel(200) / 2,
  },
  slide: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    fontFamily:"ProximaNova"
  },
  textSlide: {
    marginTop: Utils.scaleWithPixel(30),
    fontSize:18,
    fontFamily:"ProximaNova-Semibold"
  },
});
