import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {AuthActions} from '@actions';
import {View, TouchableOpacity, ScrollView} from 'react-native';
import {SafeAreaView, Text, Button, Image} from '@components';
import styles from './styles';
import Swiper from 'react-native-swiper';
import {BaseColor, BaseStyle, Images, useTheme} from '@config';
import * as Utils from '@utils';
import {useTranslation} from 'react-i18next';

export default function Walkthrough({navigation}) {
  const [loading, setLoading] = useState(false);
  const [scrollEnabled, setScrollEnabled] = useState(true);
  const [slide] = useState([
    {key: 1, image: Images.slide1,title:"Chat Room"},
    {key: 2, image: Images.slide2,title:"Audio Rooms"},
    {key: 3, image: Images.slide3,title:"Spolas TV"},
    {key: 4, image: Images.slide4,title:"Audio Room"},
  ]);
  const {colors} = useTheme();
  const dispatch = useDispatch();
  const {t} = useTranslation();
  /**
   * @description Simple authentication without call any APIs
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   */
  const authentication = () => {
    setLoading(true);
    dispatch(AuthActions.authentication(true, response => {}));
  };

  return (
    <SafeAreaView
      style={BaseStyle.safeAreaView}
      edges={['right', 'left', 'bottom']}>
      <ScrollView
        contentContainerStyle={styles.contain}
        scrollEnabled={scrollEnabled}
        onContentSizeChange={(contentWidth, contentHeight) =>
          setScrollEnabled(Utils.scrollEnabled(contentWidth, contentHeight))
        }>
        <View style={styles.wrapper}>
          {/* Images Swiper */}
          <Swiper
            dotStyle={{
              backgroundColor: BaseColor.dividerColor,
            }}
            activeDotColor={colors.primary}
            paginationStyle={styles.contentPage}
            removeClippedSubviews={false}>
            {slide.map((item, index) => {
              return (
                <View style={styles.slide} key={item.key}>
                  <Image source={item.image} style={styles.img} />
                  <Text style={styles.textSlide}>
                    {item.title}
                  </Text>
                </View>
              );
            })}
          </Swiper>
        </View>
        <View style={{marginTop:Utils.scaleWithPixel(100),width: '100%'}}>
          {/* <Button
            full
            style={{
              backgroundColor: BaseColor.navyBlue,
              marginTop: 20,
            }}
            onPress={() => {
              authentication();
            }}>
            {t('login_facebook')}
          </Button> */}
          <Button
            full
            style={{marginTop: Utils.scaleWithPixel(20)}}
            loading={loading}
            
             onPress={() => 
               authentication()
              //navigation.navigate('SignIn')
             }>
            {t('sign_in')}
          </Button>
          <View style={styles.contentActionBottom}>
           
              <Text body1 grayColor style={{fontFamily:"ProximaNova"}}>
                {t('not_have_account')}
              </Text>
            
            <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
              <Text body1 primaryColor style={{fontFamily:"ProximaNova"}}>
                {t('join_now')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
