import React, { useState, useRef, useCallback, useEffect } from 'react';
import {
    View,
    FlatList,
    TouchableOpacity,
    TouchableHighlight,
    ScrollView,
    Animated,
    KeyboardAvoidingView,
    Platform,
} from 'react-native';
import _ from 'lodash';
import { BaseStyle, Images, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Image, Text, TextInput, Cart } from '@components';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import { useDispatch, useSelector } from 'react-redux';
import { addressPostRequest } from '../../api/address';


export default function Address({ navigation, route }) {
    const { colors } = useTheme();
    const { t } = useTranslation();
    const dispatch = useDispatch();
    // useEffect(() => {
    //     dispatch(cartsGetRequestForUser());

    // }, [dispatch])
    const [input, setInput] = useState(false);
    const [Loading, setLoading] = useState();


    const UserData = useSelector(state => state.accessTokenReducer.userData)
    console.log("UserData", UserData);
    const userId = UserData?.user?.id;

    const add = async () => {
        {
            let objectValue = {};
            objectValue.address = address;
            objectValue.pincode = code;
            objectValue.userId = userId;
            setLoading(true);
            setAddr(true);
            // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
            await dispatch(addressPostRequest(objectValue));
           navigation.navigate('Home');

        }

    };

    const [address, setAddress] = useState('');
    const [code, setCode] = useState('');
    const userToken = useSelector(state => state.accessTokenReducer.accessToken)
const [addr, setAddr] = useState(false);

    return (
        <SafeAreaView>
            <View>
                <ScrollView>
                    <View style={{ margin: 10 }}>
                        <Header
                            title={t('Address')}
                            renderLeft={() => {
                                return <Icon name="chevron-left" size={25} color={colors.text} />;
                            }}
                            onPressLeft={() => {
                                navigation.goBack();
                            }}
                        />
                        <View style={{ flexDirection: "column" }}>
                            <Neomorph
                                inner
                                style={{
                                    shadowRadius: 7,
                                    borderRadius: 20,
                                    backgroundColor: colors.neoThemebg,
                                    width: 370,
                                    height: 100,
                                    justifyContent: "center"
                                }}
                            >
                                <View style={{ flexDirection: "row" }}>
                                    <TextInput
                                        onChangeText={address => setAddress(address)}
                                        // style={{ top: 15, left: 0 }}
                                        // onSubmitEditing={() => sendMessage()}

                                        placeholder={t('Address')}
                                        value={address}
                                    />
                                </View>
                            </Neomorph>
                            <Neomorph
                                inner
                                style={{
                                    shadowRadius: 7,
                                    borderRadius: 20,
                                    backgroundColor: colors.neoThemebg,
                                    width: 370,
                                    height: 70,
                                    marginTop: 10,
                                    justifyContent: "center"
                                }}
                            >
                                <View style={{ flexDirection: "row" }}>
                                    <TextInput
                                        onChangeText={code => setCode(code)}
                                        // style={{ top: 15, left: 0 }}
                                        // onSubmitEditing={() => sendMessage()}

                                        placeholder={t('Pincode')}
                                        value={code}
                                    />
                                </View>
                            </Neomorph>
                        </View>
                    </View>
                </ScrollView>
                <View style={{ flex: 1, alignItems: "center" }}>
                {addr ? (
                       <NeomorphBlur
                       style={{
                           shadowRadius: 3,
                           borderRadius: 20,
                           backgroundColor: colors.neoThemebg,
                           width: 200,
                           height: 56,
                           justifyContent: 'center',
                           alignItems: 'center',
                           top: 8,
                           left: 0
                       }}
                   >
                      
                       <Text style={{ fontFamily: "ProximaNova", fontSize: 16, color: "white" }}>Address added successfully</Text>
                   </NeomorphBlur> 
                    ):
                   ( <TouchableOpacity
                     onPress={() => add()}
                    >
                        <NeomorphBlur
                            style={{
                                shadowRadius: 3,
                                borderRadius: 20,
                                backgroundColor: colors.neoThemebg,
                                width: 150,
                                height: 56,
                                justifyContent: 'center',
                                alignItems: 'center',
                                top: 8,
                                left: 0
                            }}
                        >
                           
                            <Text style={{ fontFamily: "ProximaNova", fontSize: 16, color: "white" }}>Add</Text>
                        </NeomorphBlur>
                    </TouchableOpacity>)}
                </View>
            </View>
        </SafeAreaView>
    )
}
