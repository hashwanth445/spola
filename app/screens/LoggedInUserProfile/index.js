import React, { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  FlatList,
  Switch,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  RefreshControl,
} from 'react-native';
import { setAccessToken, setUserData } from "../../actions";
import { useSelector, useDispatch } from 'react-redux';
import { AuthActions } from '@actions';
import { BaseStyle, BaseColor, Images, useTheme } from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Text,
  Icon,
  Button,
  ProfileCard,
  ProfileAuthor,
  ProfilePerformance,
  HomeItem1,
  PostItem,
  ChannelsCard1,
  ProfileAuthor6,
} from '@components';
import styles from './styles';
import { TabView, TabBar } from 'react-native-tab-view';
import { userData, HotelData, ShortsData1, PostData2, PostData3 } from '@data';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import { cartsGetRequestForUser } from '../../api/carts';
import { videosGetRequest } from '../../api/videos';
import { shortsGetRequest } from '../../api/shorts';
import { channelGetRequestForUser } from '../../api/channel';
import logout from '../../actions';



export default function LoggedInUserProfile({ navigation }) {

  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const [index, setIndex] = useState(0);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const [routes] = useState([
    { key: 'Shorts', title: t('Shorts'), icon: 'video-wireless' },
    { key: 'Videos', title: t('Videos'), icon: 'video' },
    { key: 'Channels', title: t('Channels'), icon: 'youtube' },
    { key: 'Settings', title: t('settings'), icon: 'cog' },
  ]);

  const UserData = useSelector(state => state.accessTokenReducer.userData)
  console.log("UserData", UserData);
  const userId = UserData?.user?.id;
  // console.log("users",UserData?.user?.followersProfiles?.length);
  useEffect(() => {
    dispatch(cartsGetRequestForUser(userId));

  }, [dispatch])

  const [user] = useState(userData[0]);
  // useEffect(() => {
  //   dispatch(videosGetRequest(userToken));

  // }, [dispatch])

  // useEffect(() => {
  //   dispatch(shortsGetRequest());

  // }, [dispatch])

  useEffect(() => {
    dispatch(channelGetRequestForUser(userId));

  }, [dispatch])

  // When tab is activated, set what's index value
  const handleIndexChange = index => setIndex(index);

  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, { backgroundColor: colors.text }]}
      style={[styles.tabbar, { backgroundColor: colors.neoThemeBg }]}
      tabStyle={styles.tab}
      getLabelText={({ route }) => route.title}
      inactiveColor={BaseColor.grayColor}
      activeColor={colors.text}
      renderLabel={({ route, focused, color }) => (
        <View style={{ flex: 1, width: Utils.scaleWithPixel(350), alignItems: 'center', left: Utils.scaleWithPixel(15), }}>
          {focused ? (<View>
            <Text headline bold={focused} style={{ fontFamily: "ProximaNova" }}>
              {route.title}
            </Text>
            <Icon name={route.icon} color="#DDDDDD" size={24} style={{ top: -22, left: -30 }} />
          </View>
          ) : (
            <Icon name={route.icon} color="#909090" size={24} style={{ top: 0, left: -15 }} />
          )}

        </View>
      )}
    />
  );


  // Render correct screen container when tab is activated
  const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'Shorts':
        return <ShortsTab jumpTo={jumpTo} navigation={navigation} />;
      case 'Videos':
        return <VideosTab jumpTo={jumpTo} navigation={navigation} />;
      case 'Channels':
        return <ChannelsTab jumpTo={jumpTo} navigation={navigation} data={UserData?.user?.channels} />;
      case 'Settings':
        return <SettingsTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };



  const [performance, setPerformance] = useState([
    { value: UserData?.user?.followingProfiles?.length ? UserData?.user?.followingProfiles?.length : '0', title: 'Following' },
    { value: UserData?.user?.followersProfiles?.length ? UserData?.user?.followersProfiles?.length : '0', title: 'Followers' },
    { value: UserData?.user?.rewardedPoints ? UserData?.user?.rewardedPoints : '0', title: 'Rewarded Points' },
  ])
  // console.log("performance",performance);
  return (
    <View style={{ flex: 1 }}>
      <ImageBackground source={require('../../assets/images/photo-1.jpg')} resizeMode="cover" style={styles.image} imageStyle={{ borderRadius: Utils.scaleWithPixel(25) }}>
        <Header
          title="Profile"
          onPressLeft={() => {
            navigation.goBack();
          }}
        />
        <SafeAreaView
          style={BaseStyle.safeAreaView}
          edges={['right', 'left', 'bottom']}>
          <Image
            style={{ width: 100, height: 100, borderRadius: 50, alignSelf: "center", borderColor: "#00656565", borderWidth: 3 }}
            source={{ uri: UserData?.user?.backBlazeImageUrl ? UserData?.user?.backBlazeImageUrl : '' }}
          />

          <View style={{ padding: 20, left: -25, top: -10, }}>
            <View style={{ flexDirection: "row", alignSelf: "center", left: 20 }}>
              <Text title1 bold style={{ fontFamily: "ProximaNova-Bold", fontSize: 18, alignSelf: "center" }} >
                {UserData?.user?.username}
              </Text>
              {UserData?.user?.followersProfiles?.length > 5 ? (
                <Image source={Images.group} style={{ height: 40, width: 35, top: -5 }} />
              ) : null}

              {/* <Icon name="star-circle-outline" color="#DDDDDD" size={18} style={{ left: 135, top: 4, backgroundColor: "#5ebaf4", borderRadius: 10, height: 16 }} /> */}
            </View>
            {/* <Text subhead grayColor>
              {userData.major}
            </Text> */}
            {/* <View style={styles.location}>
              <Icon name="map-marker" size={10} color={colors.primary} />
              <Text
                caption1
                primaryColor
                style={{
                  marginHorizontal: 5,
                }}>
                {userData.address}
              </Text>
            </View> */}
            <View style={styles.contentInfor}>
              <ProfilePerformance
                data={performance}
                flexDirection="row"
                profileDataObject={performance}
              />
            </View>

            {/* <Text headline semibold style={{ marginBottom:10}}>
              {t('about_me')}
            </Text>
            <Text body2 numberOfLines={5}>
              {userData.about}
            </Text> */}
          </View>
          {/* <ScrollView> */}
          <TabView
            lazy
            navigationState={{ index, routes }}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            onIndexChange={handleIndexChange}
            style={{ height: '100%' }}
            tabBarShowLabel
            tabBarIcon
          />
          {/* </ScrollView> */}
        </SafeAreaView>
      </ImageBackground>
    </View>
  );
}

/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
        * @date 2019-08-03
        * @class PreviewTab
        * @extends {Component}
        */
function ShortsTab({ navigation }) {

  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const shortsData = useSelector(state => state.ShortsReducer.shortsData);
  // console.log("UserData", UserData);
  const shorts = UserData?.user?.shorts
  // console.log("shorts", shorts);

  // console.log("coming to BookingTab ------------------")
  return (
    <ScrollView>
      <FlatList
        contentContainerStyle={{
          paddingLeft: 30,
          margin: 10,
          // paddingRight: 0,
          paddingTop: 20,
        }}
        numColumns={2}
        // vertical={true}
        // showsHorizontalScrollIndicator={false}
        data={shorts}
        keyExtractor={(item, index) => item.id}
        renderItem={({ item, index }) => (

          <ProfileCard
            grid
            image={item?.backBlazeSpolaUrl ? item?.backBlazeSpolaUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
            style={{ margin: 10, left: -30, paddingTop: 20, top: -30 }}
          // onPress={() => navigation.navigate('HotelDetail')}
          >
            <View>
              <Icon name="play-outline" color="#dddddd" size={22} style={{ top: -20, left: -15 }} />
              <Text subhead whiteColor style={{ top: -40, left: 5, fontFamily: "ProximaNova-Medium", }}>
                {item?.viewCount}
              </Text>
            </View>

          </ProfileCard>
        )}
      />
    </ScrollView>
  );
}

/**
 * @description Show when tab Profile activated
 * @author Passion UI <passionui.com>
          * @date 2019-08-03
          * @class PreviewTab
          * @extends {Component}
          */
function VideosTab({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();

  const [loading, setLoading] = useState(false);
  const success = async (videoUrlObject) => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('HomeDetail', { videoUrlObject })
    setLoading(false);

  };
  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const videos = UserData?.user?.videos
  // const [post3] = useState(PostData3);

  return (
    <View style={{ marginTop: -5, height: "100%" }}>
      <FlatList
        data={videos}
        contentContainerStyle={{
          paddingTop: 50,
        }}
        keyExtractor={(item, index) => item.id}
        renderItem={({ item, index }) => (
          <HomeItem1
            image={item?.backBlazeVideoUrl ? item?.backBlazeVideoUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
            // title={item.title}
            time={item?.Duration}

            // description={item.description}
            onPress={() => success()}>
          </HomeItem1>
        )}
      />
    </View>
  );
}

/**
 * @description Show when tab Setting activated
 * @author Passion UI <passionui.com>
            * @date 2019-08-03
            * @class PreviewTab
            * @extends {Component}
            */
function ChannelsTab({ navigation, data }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [refreshing] = useState(false)
  const { width, height } = Dimensions.get('window');
  const channels = data;
  const channeldata = useSelector(state => state.ChannelReducer.channelDataForUser)
  console.log("channel", channeldata);
  return (
    <View style={{top:20}}>
      <View style={{ alignItems: "flex-end", top: 0 }}>
        <NeomorphBlur
          style={{
            shadowRadius: 3,
            borderRadius: 100,
            backgroundColor: colors.neoThemebg,
            width: 150,
            height: 56,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate('CreateChannel')}>
            <Text style={{ fontSize: 20, fontFamily: "ProximaNova" }}> + Channel</Text>
          </TouchableOpacity>
        </NeomorphBlur>
      </View>
      <ScrollView>
        <FlatList
          numColumns={2}
          contentContainerStyle={{ alignSelf: 'flex-start', marginTop: Utils.scaleWithPixel(-5) }}
          showsHorizontalScrollIndicator={false}
          data={channels}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) => (

            <ChannelsCard1
              style={{ margin: Utils.scaleWithPixel(10), left: 15 }}
              image={item?.backBlazeCoverImg ? item?.backBlazeCoverImg : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
            // name={item.name}
            >
              <Text bold style={{ top: Utils.scaleWithPixel(20), left: Utils.scaleWithPixel(5), alignSelf: "center", color: "#909090", fontFamily: "ProximaNova", fontSize: 14 }}>
                {item.channelName}
              </Text>
              <Text style={{ top: Utils.scaleWithPixel(25), left: Utils.scaleWithPixel(5), alignSelf: "center", color: "#e3dce1", fontWeight: "bold", fontFamily: "ProximaNova", fontSize: 16 }}>
                {item?.subscribers?.length ? item?.subscribers?.length : '0'} Followers
              </Text>
              <Text style={{ top: Utils.scaleWithPixel(30), left: Utils.scaleWithPixel(5), alignSelf: "center", color: "#909090", fontFamily: "ProximaNova-Medium", fontSize: 14 }}>
                {item.channelDesc}
              </Text>

              <ProfileAuthor6
                style={{ left: Utils.scaleWithPixel(40), top: Utils.scaleWithPixel(-95), height: Utils.scaleWithPixel(100), width: Utils.scaleWithPixel(100) }}
                image={item?.backBlazeCoverImg ? item?.backBlazeCoverImg : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
              // name={item.name}
              // description={item.detail}

              />
            </ChannelsCard1>
          )}

        />
      </ScrollView>
    </View>

  );
}

/**
 * @description Show when tab Activity activated
 * @author Passion UI <passionui.com>
              * @date 2019-08-03
              * @class PreviewTab
              * @extends {Component}
              */
function SettingsTab({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const forceDark = useSelector(state => state.application.force_dark);
  const font = useSelector(state => state.application.font);
  const orders = useSelector(state => state.OrdersReducer.ordersDataForUser)
  const carts = useSelector(state => state.cartsReducer.cartsDataForUser)

  const [loading, setLoading] = useState(false);

  const onLogOut = () => {
    setLoading(true);
    dispatch(AuthActions.authentication(false, response => {}));
    navigation.navigate('SignIn')
  };


  const darkOption = forceDark
    ? t('always_on')
    : forceDark != null
      ? t('always_off')
      : t('dynamic_system');
  return (
    <ScrollView>
      <View style={{ padding: 20, top: -20 }}>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => {
            navigation.navigate('SelectDarkOption');
          }}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>{t('dark_theme')}</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text body1 grayColor style={{ fontFamily: "ProximaNova" }}>
              {darkOption}
            </Text>
            <Icon
              name="angle-right"
              size={18}
              color={colors.primary}
              style={{ marginLeft: 5 }}
              enableRTL={true}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => {
            navigation.navigate('OnboardingScreenOneProfileDetails');
          }}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>{t('edit_profile')}</Text>
          <Icon
            name="angle-right"
            size={18}
            color={colors.primary}
            style={{ marginLeft: 5 }}
            enableRTL={true}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => {
            navigation.navigate('ChangePassword');
          }}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>{t('change_password')}</Text>
          <Icon
            name="angle-right"
            size={18}
            color={colors.primary}
            style={{ marginLeft: 5 }}
            enableRTL={true}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => {
            navigation.navigate('Orders');
          }}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>Orders</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
              {orders?.length>1 ? (
                <Text body1 grayColor style={{ fontFamily: "ProximaNova" }}>
                {orders?.length} orders
                </Text>
              ) : (
                <Text body1 grayColor style={{ fontFamily: "ProximaNova" }}>
                {orders?.length} order
                </Text>
              ) }
           
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => navigation.navigate('Carts')}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>View Cart</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text body1 grayColor style={{ fontFamily: "ProximaNova" }}>
              {carts?.length}
            </Text>
          </View>
        </TouchableOpacity>
        <View style={{ paddingHorizontal: 20, paddingVertical: 15, left: 40 }}>
          <NeomorphBlur
            style={{
              shadowRadius: 3,
              borderRadius: 100,
              backgroundColor: colors.neoThemebg,
              width: 230,
              height: 56,

              justifyContent: 'center',
              alignItems: 'center',

            }}
          >
            <Button full loading={loading} onPress={() => onLogOut()} style={{ backgroundColor: colors.neoThemebg, width: 215, height: 45, borderRadius: 20 }}>
              {t('sign_out')}
            </Button>
          </NeomorphBlur>
        </View>
      </View>
    </ScrollView>
  )
}
