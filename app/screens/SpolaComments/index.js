import React, { useState } from 'react'
import { View, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import styles from './styles'
import Video from 'react-native-video';
import { data1 } from '../SpolaScreen/videodata';
import {useDispatch, useSelector} from 'react-redux';
import {
    Header,
    SafeAreaView,
    Icon,
    Text,
    RateDetail,
    CommentItem,
    TextInput,
    ProfileAuthor1
} from '@components';
import { BaseColor, useTheme, useFont } from '@config';
import Icon2 from 'react-native-vector-icons/Feather';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { useTranslation } from 'react-i18next';
import * as Utils from '@utils';
import commentsPostRequest from '../../api/comments';

export default function SpolaComments({ route, navigation }) {
    const { colors } = useTheme();
    const { spolaUrlObject } = route.params;
    console.log("comments", spolaUrlObject);
    const UserData = useSelector(state => state.accessTokenReducer.userData);
    const userId = UserData?.user?.id
    const video = spolaUrlObject?.backBlazeSpolaUrl
    const feedComments = spolaUrlObject?.comments
    const shortId = spolaUrlObject?.id
    const { t } = useTranslation();
    const [input, setInput] = useState('')
    const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
    const saveComment = async () => {
        console.log("commentPressed");
        if (!input) {
          alert('please enter Comment')
        }
        else {
          const postType = "feed"
          setLoading(true);
          // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
          await dispatch(commentsPostRequest(input, shortId, userId, postType));
    
        }
    
      };
    // const commentData = useSelector(state => state.CommentsReducer.commentsData)
    return (
        <View style={styles.container}>
            <Video source={{uri : video ? video : '' }} style={styles.Video} resizeMode="cover" />
            <View style={styles.commentsmain}>
                <View style={styles.comments}>
                    <Icon
                        name="arrow-left"
                        size={25}
                        color="white"
                        enableRTL={true}
                        onPress={() => navigation.goBack()}

                    />
                    <Text style={{ fontSize: 25, fontFamily: "ProximaNova", alignSelf: "center" }}>Comments</Text>
                </View>
                <Icon
                    name="share-outline"
                    size={25}
                    color="white"
                    enableRTL={true}


                />
            </View>
            <ScrollView vertical={true}>
                <FlatList
                    data={feedComments}
                    style={{ margin: 10 }}
                    vertical={true}
                    keyExtractor={(item, index) => item.id}
                    renderItem={({ item, index }) => (
                        <ProfileAuthor1
                            image={item?.commentedBy?.backBlazeImageUrl ? item?.commentedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                            name={item?.commentedBy?.username}
                            description={item?.message}
                            time={item?.dateAndTime}
                            style={{ top: 0 }}
                        />
                    )}
                />
            </ScrollView>
            <View style={{ justifyContent: 'flex-end',marginTop:20 }}>
                <Neomorph
                    inner
                    style={{
                        shadowRadius: 7,
                        borderRadius: 20,
                        backgroundColor: colors.neoThemebg,
                        width: Utils.scaleWithPixel(260),
                        height: 70,
                        justifyContent: 'center',
                        // alignItems: 'center',
                        // marginTop: Utils.scaleWithPixel(0),

                        // margin: 10,
                        marginBottom: 10
                    }}
                >

                    <View style={styles.addcomment}>
                        <TextInput
                            onChangeText={text => setInput(text)}
                            style={{ marginTop: 5 }}
                            // onSubmitEditing={() => sendMessage()}

                            placeholder={t('Add a new comment')}
                            value={input}

                        />
                        <NeomorphBlur
                            style={{
                                shadowRadius: 3,
                                borderRadius: 20,
                                backgroundColor: colors.neoThemebg,
                                width: 60,
                                height: 56,
                                right: 80,
                                justifyContent: 'center',
                                alignItems: 'center',

                            }}
                        >

                            <TouchableOpacity onPress={() => saveComment()} style={{ justifyContent: "center", backgroundColor: colors.neoThemeMainButtonBorderColor }}>
                                <Icon name="message-plus" size={28} color="#DDDDDD" style={{}} />
                            </TouchableOpacity>

                        </NeomorphBlur>
                    </View>
                </Neomorph>
            </View>
        </View>

    )
}