import { StyleSheet } from 'react-native';
import * as Utils from '@utils';
import BaseColor from '@config';

export default StyleSheet.create({
  imgBanner: {
    width: '100%',
    height: Utils.scaleWithPixel(250),
    position: 'absolute',
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
  },
  overlayset: {
    // flex: 1,
    flexDirection:"row",
  },
  image: {
    flex: 1,
    // justifyContent: "center",
    height: 300,
  },
  rowBanner: {
    flexDirection: 'row',
    alignItems: 'center',
    top: Utils.scaleWithPixel(40),
    left: Utils.scaleWithPixel(10)
  },
  userIcon: {
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    borderWidth: Utils.scaleWithPixel(1),
    borderColor: 'white',
    marginRight: Utils.scaleWithPixel(5),

  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  contentButtonBottom: {
    borderTopWidth: Utils.scaleWithPixel(1),
    paddingVertical: Utils.scaleWithPixel(10),
    paddingHorizontal: Utils.scaleWithPixel(20),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemPrice: {
    borderBottomWidth: Utils.scaleWithPixel(1),
    paddingVertical: Utils.scaleWithPixel(10),
    alignItems: 'flex-start',
  },
  linePrice: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  iconRight: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapContent: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    borderBottomWidth: Utils.scaleWithPixel(1),
    paddingBottom: Utils.scaleWithPixel(20),
  },
  tabbar: {
    height: 40,
    MarginTop: 15,
    top: -5

  },
  tab: {
    width: 125,
    paddingLeft: 0

  },
  indicator: {
    height: 1,
    width: 105,
    left: 25
  },
  label: {
    fontWeight: '400',
  },
  profileItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
  },
  time:{
    top: Utils.scaleWithPixel(-305),
    left: Utils.scaleWithPixel(240), 
    borderWidth:Utils.scaleWithPixel(1),
    borderColor:"#999999",
    borderRadius:Utils.scaleWithPixel(5),
    width:Utils.scaleWithPixel(42) ,
    height:28,
    backgroundColor:"#000000",
    paddingLeft:Utils.scaleWithPixel(3),
    alignContent:"center"
  },
  text: {
    top: Utils.scaleWithPixel(-9),
    left: Utils.scaleWithPixel(-150),
    fontSize: Utils.scaleWithPixel(14),
    fontFamily: "ProximaNova",
    fontWeight: 'semibold',
    color: "#ffffff"
  },
  contentCartPromotion: {
    marginTop: Utils.scaleWithPixel(10),
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,


  },
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    flex: 1,
    marginTop: 100,

  },
  contentSwipeDown: {
    paddingTop: 10,
    alignItems: 'center',
  },
  contentInfor: { paddingTop: 30, top: -20, right: 30, marginBottom: -30 },
  // lineSwipeDown: {
  //   width: 50,
  //   height: 2.5,
  //   backgroundColor: BaseColor.whiteColor,
  // },
});
