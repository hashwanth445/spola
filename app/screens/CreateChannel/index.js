import React, { useState } from 'react';
import { View, ScrollView, KeyboardAvoidingView, Platform, PermissionsAndroid } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import ImagePicker from 'react-native-image-crop-picker';
import {
    Image,
    Header,
    SafeAreaView,
    Icon,
    Text,
    Button,
    TextInput,
} from '@components';
import styles from './styles';
import { channelPostRequest } from '../../api/channel';
// import { UserData } from '@data';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { uploadFilesPostRequest } from '../../api/uploadApi';


export default function CreateChannel({ navigation, route }) {
    const { colors } = useTheme();
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });

    //   const { MobileNumber, password } = route.params

    const channelPostCheckSuccess = useSelector(state => state.ChannelReducer.channelPostCheckSuccess)

    const UserData = useSelector(state => state.accessTokenReducer.userData)
    const [channelName, setChannelName] = useState('');
    const [channelDesc, setChannelDesc] = useState('');

    const [imageData, setImageData] = useState([]);
    const userId = UserData?.user?.id;
    const userToken = useSelector(state => state.accessTokenReducer.accessToken)
    // const [image] = useState(UserData[0].image);
    const [loading, setLoading] = useState(false);

    const success = async () => {
        console.log("success", success);
        if (!channelName) {
            alert('please enter Channel Name')
        }
        else if (!channelDesc) {
            alert('please enter Channel Description')
        }

        else {
            setLoading(true);
            // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
            await dispatch(channelPostRequest(channelName, channelDesc, image, userId));
            navigation.navigate('Profile2')
        }

    };

    // let ImageUrl = imageData ? { imageData } : require('../../assets/images/avata-01.jpeg')


    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'Camera Permission',
                        message: 'App needs camera permission',
                    },
                );
                // If CAMERA Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                return false;
            }
        } else return true;
    };

    const requestExternalWritePermission = async () => {
        console.log('requestExternalWritePermission');
    
        if (Platform.OS === 'android') {
          try {
            console.log('requestExternalWritePermission');
    
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: 'External Storage Write Permission',
                message: 'App needs write permission',
              },
            );
            // If WRITE_EXTERNAL_STORAGE Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
            console.log(PermissionsAndroid.RESULTS.GRANTED);
    
          } catch (err) {
            console.warn(err);
            alert('Write permission err', err);
          }
          return false;
        } else return true;
      };

    const [image, setImage] = useState();
    console.log("image",image);

    const captureImage = async type => {
        console.log('outsideee');

        // let options = {
        //   storageOptions: {
        //     skipBackup: true,
        //     path: 'images',
        //   },
        // };
        let isCameraPermitted = await requestCameraPermission();
        let isStoragePermitted = await requestExternalWritePermission();
        console.log('outsideee', isCameraPermitted, isStoragePermitted);

        if (isCameraPermitted && isStoragePermitted) {
            console.log('insideeee');

            ImagePicker.openPicker({
                multiple: true,
            }).then(images => {
                console.log('imgApi');
                toNextPage(images);

            });
        }
    };
    const toNextPage = async images => {
        setLoading(true)

        let formData = new FormData();
        setLoading(true);
        formData.append('files', {
            name: images[0].modificationDate,
            type: images[0].mime,
            uri: images[0].path,
        });
        let imgApi = await uploadFilesPostRequest(formData);
        setLoading(false)
        console.log('image api', imgApi);

        setImage(imgApi[0]?.url);
        console.log('url', imgApi[0].url);

    };




    return (
        <View style={{ flex: 1 }}>
            <Header
                title={t('Create a channel')}
                renderLeft={() => {
                    return (
                        <Icon
                            name="chevron-left"
                            size={32}
                            color={colors.primary}
                            enableRTL={true}
                        />
                    );
                }}
                onPressLeft={() => {
                    navigation.goBack();
                }}
                onPressRight={() => { }}
            />
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>
                    <ScrollView contentContainerStyle={styles.contain}>
                        <View style={{
                            flexDirection: "column",
                            alignItems: "center"
                        }}>
                            <Neomorph
                                Outer
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(20),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(235),
                                    height: Utils.scaleWithPixel(155),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    margin: Utils.scaleWithPixel(10)
                                }}
                            >
                                <View>
                                    <Image source={{ uri: image ? image : '' }} style={{height:175,width:260,borderRadius:20,alignSelf:"center"}} />
                                </View>
                            </Neomorph>
                            <TouchableOpacity onPress={() => captureImage()} style={{ flexDirection: "row" }}>
                                <Text semibold style={{ fontSize: 20, fontFamily: "ProximaNova", textDecorationLine: "underline" }}>Cover Image for Channel</Text>
                                <Icon name="pencil" size={20} style={styles.Icon} />
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            marginTop: Utils.scaleWithPixel(40),
                        }}>
                            <Neomorph
                                inner
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(15),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(300),
                                    height: Utils.scaleWithPixel(50),
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                <TextInput
                                    style={{
                                        marginTop: Utils.scaleWithPixel(-5), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                                        height: Utils.scaleWithPixel(40), borderRadius: Utils.scaleWithPixel(10)
                                    }}
                                    onChangeText={text => setChannelName(text)}
                                    placeholder={t('Channel Name')}
                                    value={channelName}
                                />
                            </Neomorph>
                        </View>
                        <View style={{
                            marginTop: Utils.scaleWithPixel(30),
                        }}>
                            <Neomorph
                                inner
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(15),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(300),
                                    height: Utils.scaleWithPixel(50),
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                <TextInput
                                    style={{
                                        marginTop: Utils.scaleWithPixel(-2), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                                        height: Utils.scaleWithPixel(40), borderRadius: Utils.scaleWithPixel(10)
                                    }}
                                    onChangeText={text => setChannelDesc(text)}
                                    placeholder={t('Channel Description')}
                                    value={channelDesc}
                                />
                            </Neomorph>
                        </View>
                        {/* <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-2), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(50),
                  }}
                  onChangeText={text => setEmail(text)}
                  placeholder={t('email')}
                  value={email}
                />
              </Neomorph>
            </View>

            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-2), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(40), borderRadius: Utils.scaleWithPixel(10)
                  }}
                  onChangeText={text => setusername(text)}
                  placeholder={'username'}
                  value={username}
                />
              </Neomorph>
            </View> */}




                        <TouchableOpacity onPress={() => success()} style={{ width: 200, alignItems: "center" }}>
                            <NeomorphBlur
                                style={{
                                    shadowRadius: 3,
                                    borderRadius: 50,
                                    backgroundColor: colors.neoThemebg,
                                    width: 190,
                                    height: 56,
                                    top: 50,
                                    marginBottom: 70,
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                <Text grayColor semibold style={{ fontSize: 20, fontFamily: "ProximaNova" }}>Create</Text>
                            </NeomorphBlur>
                        </TouchableOpacity>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View >
    );
}
