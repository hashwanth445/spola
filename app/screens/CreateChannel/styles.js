import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contentTitle: {
    alignItems: 'flex-start',
    width: '100%',
    height: Utils.scaleWithPixel(32),
    justifyContent: 'center',
  },
  contain: {
    alignItems: 'center',
    padding: Utils.scaleWithPixel(30),
  },
  textInput: {
    height: Utils.scaleWithPixel(46),
    backgroundColor: BaseColor.fieldColor,
    borderRadius: Utils.scaleWithPixel(5),
    padding: Utils.scaleWithPixel(10),
    width: '100%',
    color: BaseColor.grayColor,
  },
  thumb: {
    width: Utils.scaleWithPixel(100),
    height: Utils.scaleWithPixel(100),
    borderRadius: Utils.scaleWithPixel(50),
    marginBottom: Utils.scaleWithPixel(20),
  },
  Icon: {
    alignSelf:"center",
    borderRadius: Utils.scaleWithPixel(50),
    color: "#008cff"
  },
});
