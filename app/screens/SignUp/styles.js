import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import { ThemeSupport } from '../../config/theme';
import * as Utils from '@utils';


export default StyleSheet.create({
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    // padding: Utils.scaleWithPixel(20),
    flex: 3,
  },
  button: {
    height: 45,
    width: 220,
    borderRadius: 12,
    justifyContent: "center"
  },
  text: {
    fontFamily:"ProximaNova",
    fontSize:18,
    color:"white",
    alignSelf:"center",
    fontWeight:'700'
  },
  lottie: {
    width: 100,
    height: 100
  }

 
});
