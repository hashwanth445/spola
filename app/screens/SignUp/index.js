import React, { useState, useEffect } from 'react';
import { View, KeyboardAvoidingView, Platform, TouchableOpacity, Keyboard,ActivityIndicator } from 'react-native';
import { BaseStyle, useTheme, Images } from '@config';
import { Header, SafeAreaView, Icon, Button, TextInput, Image, Text } from '@components';
import styles from './styles';
import { signupRequest } from '../../api/signup'
import { sendOtpRequest } from '../../api/signup';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { BaseColor } from '../../config/theme';
import * as Utils from '@utils';
import { ScrollView } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import { usersGetRequest } from '../../api/users';

export default function SignUp({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const signLoading = useSelector(state => state.signupReducer.signupLoading);
  const signupSuccess = useSelector(state => state.signupReducer.signupSuccess)
  const userdata = useSelector(state => state.signupReducer.signupMessageData);

  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  useEffect(() => {
    dispatch(usersGetRequest());
  }, [dispatch]);
  const userData = useSelector(state => state.usersReducer.usersOneData)
  console.log("User", userData);

  const signupSuccessOtpRequest = useSelector(state => state.signupReducer.signupSuccessOtpRequest);
  const [name, setName] = useState('');
  // const [isAdmin, setIsAdmin] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // const [referralId, setReferral] = useState('');
  const [phone, setPhone] = useState('');
  const [loading, setLoading] = useState(false);
  const [secure, setSecure] = useState(true);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [suggestionsList, setSuggestionsList] = useState(null)

  const [success, setSuccess] = useState({
    name: true,
    email: true,
    // address: true,
    // referralId: true,
    phone: true,
    password: true
  });
  /**
   * call when action signup
   *
   */

  //  const getSuggestions = useCallback(async (q) => {

  //   if (typeof q !== "string" || q.length < 3) {
  //     setSuggestionsList(null)
  //     return
  //   }
  //   setLoading(true)
  //   var myHeaders = new Headers();
  //   myHeaders.append("Authorization", `Bearer ${accessToken}`);
  //   myHeaders.append("Content-Type", "application/json");
  //   const response = await fetch(`http://52.91.136.119:1337/users?_where[0][MyReferralCode]=${q}`, { headers: myHeaders })
  //   const items = await response.json()
  //   console.log("response------------", response.body, items)

  //   setSuggestionsList(response)
  //   setLoading(false)
  // }, [])

  const onSignUp = async () => {
    console.log("signup button press");
    //  navigation.navigate('Otp');
    if (!name && !email && !phone && !password) {
      setSuccess({
        ...success,
        phone: false,
        password: false,
      });
    }
    else {
      setLoading(true);
      await dispatch(signupRequest(name, email, phone, password));
      console.log("screen signup function call");
    }
    setLoading(false);


  };
  if (signupSuccess) {
    console.log("signupSuccesssignupSuccess 123----------------------", userdata)
    // navigation.navigate('Otp')
    navigation.navigate('OnboardingScreenOneProfileDetails', userdata)

  };

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <Image source={Images.logo} style={{ height: Utils.scaleWithPixel(150), width: Utils.scaleWithPixel(150), alignSelf: "center", marginBottom: -30 }} />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView>
            <View style={styles.contain}>
              <View style={{
                top: Utils.scaleWithPixel(70),
                marginBottom: 170,
                // alignItems: "flex-start"
              }}>
                <Text style={{ fontSize: 23, color: "#DDDDDD", fontFamily: "ProximaNova", fontWeight: '700', alignSelf: "center", top: Utils.scaleWithPixel(-20) }}>Sign up</Text>
                <Neomorph
                  inner
                  style={{
                    shadowRadius: Utils.scaleWithPixel(7),
                    borderRadius: Utils.scaleWithPixel(15),
                    backgroundColor: colors.neoThemebg,
                    width: Utils.scaleWithPixel(300),
                    height: Utils.scaleWithPixel(50),
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10
                    // left: 20

                  }}
                >
                  <TextInput
                    style={{
                      marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                      height: Utils.scaleWithPixel(60),
                    }}
                    onChangeText={(text) => setName(text)}
                    placeholder={t('Full Name')}
                    success={success.name}
                    value={name}
                  />

                </Neomorph>
                {userData.map((item, index) => {
                  return (
                    name == item?.username ?
                      <View style={{ alignItems: "center", justifyContent: "flex-start", left: 20, flexDirection: "row" }}>
                        <Icon name="alert-circle" size={15} color="red" />
                        <Text style={{ fontFamily: "ProximaNova", fontSize: 18, color: "red" }}>Username already exists</Text>
                      </View> : null
                  )
                })}
                <Neomorph
                  inner
                  style={{
                    shadowRadius: Utils.scaleWithPixel(7),
                    borderRadius: Utils.scaleWithPixel(15),
                    backgroundColor: colors.neoThemebg,
                    width: Utils.scaleWithPixel(300),
                    height: Utils.scaleWithPixel(50),
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10
                    // left:20,
                    // top: 20

                  }}
                >
                  <TextInput
                    style={{
                      marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                      height: Utils.scaleWithPixel(60),
                    }}
                    onChangeText={(text) => setEmail(text)}
                    placeholder={t('Email ID')}
                    keyboardType="email-address"
                    success={success.email}
                    value={email}
                  />

                </Neomorph>
                {userData.map((item, index) => {
                  return (
                    email == item?.email ?
                      <View style={{ alignItems: "center", justifyContent: "flex-start", left: 20, flexDirection: "row" }}>
                        <Icon name="alert-circle" size={15} color="red" />
                        <Text style={{ fontFamily: "ProximaNova", fontSize: 18, color: "red" }}>Email already exists</Text>
                      </View> : null
                  )
                })}
                <Neomorph
                  inner
                  style={{
                    shadowRadius: Utils.scaleWithPixel(7),
                    borderRadius: Utils.scaleWithPixel(15),
                    backgroundColor: colors.neoThemebg,
                    width: Utils.scaleWithPixel(300),
                    height: Utils.scaleWithPixel(50),
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10
                    // top: 40

                  }}
                >

                  <TextInput
                    style={{
                      marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                      height: Utils.scaleWithPixel(60),
                    }}
                    onChangeText={(text) => setPhone(text)}
                    placeholder={t('Mobile number')}
                    success={success.phone}
                    keyboardType="phone-pad"
                    value={phone}
                    editable={false}
                  />
                </Neomorph>
                {userData.map((item, index) => {
                  return (
                    phone == item?.MobileNumber ?
                      <View style={{ alignItems: "center", justifyContent: "flex-start", left: 20, flexDirection: "row" }}>
                        <Icon name="alert-circle" size={15} color="red" />
                        <Text style={{ fontFamily: "ProximaNova", fontSize: 18, color: "red" }}>Mobile Number already exists</Text>
                      </View> : null
                  )
                })}
                <Neomorph
                  inner
                  style={{
                    shadowRadius: Utils.scaleWithPixel(7),
                    borderRadius: Utils.scaleWithPixel(15),
                    backgroundColor: colors.neoThemebg,
                    width: Utils.scaleWithPixel(300),
                    height: Utils.scaleWithPixel(50),
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 10
                    // top: 60

                  }}
                >
                  <TextInput
                    style={{
                      marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                      height: Utils.scaleWithPixel(60), top: Utils.scaleWithPixel(10),
                    }}
                    onChangeText={text => setPassword(text)}
                    onFocus={() => {
                      setSuccess({
                        ...success,
                        password: true,
                      });
                    }}
                    placeholder={t('password')}
                    secureTextEntry={secure}
                    // keyboardType="name-pad"
                    success={success.password}
                    value={password}
                    onSubmitEditing={Keyboard.dismiss}
                  />
                  <Icon
                    name={secure ? "eye-off-outline" : "eye-outline"}
                    onPress={() => setSecure(!secure)}
                    solid color="white" size={24} style={{ marginLeft: Utils.scaleWithPixel(260), top: Utils.scaleWithPixel(-30), right: Utils.scaleWithPixel(10) }} />
                  {/* <Image source={ secure ? Images.eye : Images.eye1} onPress={() => setSecure(!secure)} style={{height:20,width:20,top:-30,left:140}} /> */}
                </Neomorph>
              </View>
              <View style={{
                top: Utils.scaleWithPixel(0),
              }}>
                <NeomorphBlur
                  style={{
                    shadowRadius: 3,
                    borderRadius: 15,
                    backgroundColor: colors.neoThemebg,
                    width: 230,
                    height: 56,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  {name.length < 2 || !email.includes('@') || !email.includes('.com') || phone.length < 10 || password.length < 6 ? (

                    <Button
                      style={{ backgroundColor: colors.neoThemebg }}
                      full
                      loading={loading}
                    >
                      Continue
                    </Button>
                  ) : (

                    <TouchableOpacity onPress={() => {
                      onSignUp()
                    }}
                    >
                      <LinearGradient style={styles.button} colors={['#43D4FF', '#38ABFD', '#2974FA']} >
                        {loading ? <ActivityIndicator size="small" color="#0000ff" /> :                        
                        <Text semibold style={styles.text}>Continue</Text>   
                        }
                      </LinearGradient>
                    </TouchableOpacity>
                  )}
                </NeomorphBlur>
                <TouchableOpacity
                  style={{ alignItems: "center", top: Utils.scaleWithPixel(30), marginBottom: 40 }}
                  onPress={() => navigation.navigate('SignIn')}>
                  <View style={{ flexDirection: "row", alignContent: "center" }}>
                    <Text body1 style={{ alignSelf: "center", fontFamily: "ProximaNova", color: "#6a6c6e" }}>
                      Existing user?
                    </Text>
                    <Text body1 style={{ alignSelf: "center", fontFamily: "ProximaNova", color: "#0094ff" }}>
                      {t('Sign In')}
                    </Text>
                  </View>
                </TouchableOpacity>

              </View>

            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
}
