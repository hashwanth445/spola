import React, { useState } from 'react';
import { View, KeyboardAvoidingView, Platform, FlatList, TouchableOpacity, Image, RefreshControl } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Button, Text, ListThumbnails1 } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { MessagesData } from '@data';
import { Neomorph } from 'react-native-neomorph-shadows';
import { BaseColor } from '../../config/theme';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import * as Utils from '@utils';

export default function RoomWithFriends({ navigation }) {
    const { colors } = useTheme();
    const { t } = useTranslation();
    const [refreshing] = useState(false);
    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });
    // const [shouldShowFollow, setShouldShowFollow] = useState(true);
    const [messenger] = useState(MessagesData);
    const [shouldShowLeave, setShouldShowLeave] = useState(true);

    return (
        <View style={{ flex: 1 }}>
            <Header
                title="Start Room with"
                renderLeft={() => {
                    return (
                        <Icon
                            name="arrow-left"
                            size={20}
                            color={colors.primary}
                            enableRTL={true}
                        />
                    );
                }}
                onPressLeft={() => {
                    navigation.goBack();
                }}
            />
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>
                    <ScrollView>
                        <View style={styles.contain}>
                           <Text bold style={{ fontSize: 20, color: "#DDDDDD",marginTop:Utils.scaleWithPixel(200) }}>Start Room with</Text>
                            {/*  <Text bold style={{ fontSize: 15, color: "#6a6c6e", marginTop: 10 }}>Follow to stay updated with fresh content</Text> */}
                            <View style={{
                                marginTop: Utils.scaleWithPixel(50),

                            }}>

                                <FlatList
                                    refreshControl={
                                        <RefreshControl
                                            colors={[colors.primary]}
                                            tintColor={colors.primary}
                                            refreshing={refreshing}
                                            onRefresh={() => { }}
                                        />
                                    }
                                    data={messenger}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={({ item, index }) => (
                                        <View>
                                            <ListThumbnails1
                                                image={item.image}
                                            />
                                        </View>
                                    )}
                                />
                                <View style={{ flexDirection: "row", left: Utils.scaleWithPixel(25) }}>
                                </View>


                            </View>

                        </View>
                        <View style={{ marginTop: Utils.scaleWithPixel(-10), marginLeft:Utils.scaleWithPixel(75), marginBottom:Utils.scaleWithPixel(20) }}>
                            
                            <Neomorph
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(3),
                                    borderRadius: Utils.scaleWithPixel(5),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(250),
                                    height: Utils.scaleWithPixel(62),
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                <Neomorph
                                    style={{
                                        width: Utils.scaleWithPixel(250),
                                        height: Utils.scaleWithPixel(61),
                                        shadowRadius: Utils.scaleWithPixel(7),
                                        borderRadius: Utils.scaleWithPixel(15),
                                        // borderWidth: 2,
                                        // borderColor: "#292e35",
                                        backgroundColor: colors.neoThemebg,
                                        justifyContent: 'center',
                                        alignItems: 'center',

                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('AudioRoom')}
                                      >
                                        <Text body1 bold style={{ fontSize: 20, color:"white" }}>
                                            Get started
                                        </Text>
                                    </TouchableOpacity>

                                </Neomorph>

                            </Neomorph>
                            </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View >
    );
}