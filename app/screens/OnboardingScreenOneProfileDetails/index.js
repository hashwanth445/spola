import React, { useState } from 'react';
import { View, ScrollView, KeyboardAvoidingView, Platform, TouchableOpacity,ActivityIndicator } from 'react-native';
import { signupRequest } from '../../api/signup'
import { BaseStyle, useTheme } from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  TextInput,
} from '@components';
import styles from './styles';
// import { UserData } from '@data';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import LinearGradient from 'react-native-linear-gradient';


export default function OnboardingScreenOneProfileDetails({ navigation, route }) {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });


  const { MobileNumber, password } = route.params


  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const [firstName, setFirstName] = useState(UserData?.user?.firstName);
  const [lastName, setlastName] = useState(UserData?.user?.lastName);
  const [email, setEmail] = useState(UserData?.user?.email);
  const [username, setusername] = useState(UserData?.user?.username);


  const userId = UserData?.user?.id;
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  console.log(UserData,userToken,userId,"OnboardingScreenOneProfileDetails")
  // const [image] = useState(UserData[0].image);
  const [loading, setLoading] = useState(false);

  const success = async () => {
    if (!firstName) {
      alert('please enter firstName')
    }
    else if (!lastName) {
      alert('please enter lastName')
    }

    else {
      setLoading(true);
      // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
      navigation.navigate('OnboardingScreenTwoGender', { firstName, lastName, userId, userToken })
      setLoading(false);

    }

  };




  return (
    <View style={{ flex: 1 }}>
      {/* <Header
        title={t('edit_profile')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
        onPressRight={() => { }}
      /> */}
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <Icon onPress={() => navigation.goBack()}
            name="chevron-left" size={28} color="white" style={{ top: Utils.scaleWithPixel(20), left: Utils.scaleWithPixel(10) }} />
          <ScrollView contentContainerStyle={styles.contain}>
            <View style={{
              marginTop: Utils.scaleWithPixel(110),
            }}>
              <Text style={{ fontSize: 22, color: "#B3B6B7", fontFamily: "ProximaNova", fontWeight: "bold", top: Utils.scaleWithPixel(-90), alignSelf:"center" }}> Profile details</Text>
              <Text bold style={{ fontSize: 16, top: Utils.scaleWithPixel(-80), color: "#6a6c6e", alignSelf:"center" }}>Let's get to know about you</Text>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-5), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(40), borderRadius: Utils.scaleWithPixel(10)
                  }}
                  onChangeText={text => setFirstName(text)}
                  placeholder={t('f_name')}
                  value={firstName}
                />
              </Neomorph>
            </View>
            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-2), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(40), borderRadius: Utils.scaleWithPixel(10)
                  }}
                  onChangeText={text => setlastName(text)}
                  placeholder={t('l_name')}
                  value={lastName}
                />
              </Neomorph>
            </View>
            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-2), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(50),
                  }}
                  onChangeText={text => setEmail(text)}
                  placeholder={t('email')}
                  value={email}
                />
              </Neomorph>
            </View>

            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-2), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(40), borderRadius: Utils.scaleWithPixel(10)
                  }}
                  onChangeText={text => setusername(text)}
                  placeholder={'username'}
                  value={username}
                />
              </Neomorph>
            </View>

              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 50,
                  backgroundColor: colors.neoThemebg,
                  width: 230,
                  height: 56,
                  top: 50,
                  marginBottom: 50,
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                {!firstName && !lastName ? (
                <Text
                  style={{ fontFamily:"ProximaNova",fontSize:22,color:"#3a404b", backgroundColor: colors.neoThemebg, }}
                >
                  {t('confirm')}
                </Text>
                ) : (
                <TouchableOpacity onPress={() => {
                 success()
                }}
                >
                  <LinearGradient style={styles.button} colors={['#43D4FF', '#38ABFD', '#2974FA']} >
                    {loading ? <ActivityIndicator size="small" color="#0000ff" />: 
                    <Text semibold style={styles.text}> Save and Next</Text>}
                  </LinearGradient>
                </TouchableOpacity>
                )}
              </NeomorphBlur>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View >
  );
}
