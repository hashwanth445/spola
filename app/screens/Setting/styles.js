import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import styles from './styles';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    paddingHorizontal: Utils.scaleWithPixel(20),
    paddingVertical: Utils.scaleWithPixel(15),
  },
  profileItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: Utils.scaleWithPixel(20),
  },
  themeIcon: {
    width: Utils.scaleWithPixel(16),
    height: Utils.scaleWithPixel(16),
  },
});
