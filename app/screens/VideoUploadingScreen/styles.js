import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
    container: {
        flex: 1,
        // alignSelf:"flex-start",
      },
      checkboxContainer: {
        flexDirection: "row",
        marginBottom: 20,
        
      },
      checkbox: {
        alignSelf: "center",
      },
      label: {
        margin: 8,
        fontFamily:"ProximaNova"
      },
});