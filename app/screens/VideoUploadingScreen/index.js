import React, { useState, useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Alert,
  FlatList,
  Dimensions,
  StyleSheet,
  ActivityIndicator
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { BaseStyle, BaseColor, useTheme, Images } from '@config';
import { YoutubePlayer, FacebookPlayer } from 'react-native-video-extension';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  BookingTime,
  TextInput,
  Image,
} from '@components';
import Modal from 'react-native-modal';
import * as Utils from '@utils';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Picker } from '@react-native-picker/picker';
import DropDownPicker from 'react-native-dropdown-picker';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import Video from 'react-native-video';
import ImagePicker from 'react-native-image-crop-picker';
import { PermissionsAndroid } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import VideoPlayer from 'react-native-rn-videoplayer';
import { videosPostRequest, feedsGetRequest } from '../../api/videos';
import { useDispatch, useSelector } from 'react-redux';
import { categoriesGetRequest } from '../../api/categories';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { uploadFilesPostRequest } from '../../api/uploadApi';
import { channelGetRequestForUser } from '../../api/channel';
import { channelVideoPostRequest, channelVideoPostRequestForReward } from '../../api/channelVideos';
import { rewardedvideosPostRequest } from '../../api';
import {
  VESDK,
  VideoEditorModal,
  Configuration,
} from 'react-native-videoeditorsdk';

import { result } from 'lodash';

const IS_IOS = Platform.OS === 'ios';
const entryBorderRadius = 8;

export default function VideoUploadingScreen({ route, navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [selectedValue, setSelectedValue] = useState();
  console.log("selectedValue", selectedValue);
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  // const {result} = route.params;
  // console.log('werwer=======dddddddd==============dddd====', result);
  const [keyword, setKeyword] = useState('');

  useEffect(() => {
    dispatch(categoriesGetRequest(userToken));
  }, [dispatch]);
  useEffect(() => {
    dispatch(channelGetRequestForUser(userId));
  }, [dispatch]);

  const category = useSelector(state => state.CategoriesReducer.categoriesData);
  // const catName = category[0]?.name;
  console.log('category', category);

  const channeldata = useSelector(state => state.ChannelReducer.channelDataForUser)
  console.log("channelsData", channeldata);

  const renderCatList = () => {
    return channels.map(channels => {
      return <Picker.Item label={channels?.channelName} value={channels?.id} />;
    });
  };

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState(category);

  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState(
    route?.params?.images ? route?.params?.images : [],
  );
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  // const { data } = route.params;
  const userData = useSelector(state => state.accessTokenReducer.userData);
  console.log("userData", userData);
  const userId = userData?.user?.id;
  const channels = userData?.user?.channels
  // setImage(data)
  /**
   * call when action modal
   * @param {*} modal
   */
  //   const openModal = modal => {
  //     setModalVisible(modal);
  //   };

  /**
   * call when on change value
   * @param {*} mode
   * @param {*} value
   */

  const ViewsImage = () => {
    let options = {
      title: 'Video Picker',
      mediaType: 'image/video',
    };
    Alert.alert('Select Your Video', 'You want to select from you device?', [
      {
        text: 'Device',
        onPress: () =>
          launchImageLibrary(options, responce => {
            console.log('Response = ', responce.uri);
            if (responce.didCancel) {
              console.log('User cancelled image picker');
            } else if (responce.error) {
              console.log('ImagePicker Error: ', responce.errorMessage);
            } else {
              console.log(responce, 'all 2 data how selected', responce.uri);
              let source = responce.uri;
              setVideoPlay(source);
              console.log(responce.uri, 'uri video data');
            }
          }),
      },
      {
        text: 'Not Now',
        onPress: () => null,
      },
    ]);
    return true;
  };
  selectImage = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, response => {
      console.log(response.assets, 'all response', response.assets.uri);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        console.log(source);
        setImage(source);
      }
    });
  };
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write camera err', err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const captureImage = async type => {
    // let options = {
    //   mediaType: type,
    //   maxWidth: 300,
    //   maxHeight: 550,
    //   quality: 1,
    //   videoQuality: 'low',
    //   durationLimit: 30, //Video max duration in seconds
    //   saveToPhotos: true,
    // };
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      ImagePicker.openPicker({
        multiple: true,
      }).then(images => {
        console.log("images", images);
        // let imgd = image.concat(images);
        // setImage(imgd);
        const url = images[0]?.path;
        const time = images[0]?.duration;
        openEditor(url, time);
      });
    }
  };
  // const toNextPage = async images => {
  //   console.log('imgApi2', images);
  //   let formData = new FormData();
  //   setLoading(true);

  //   console.log('lopala', images);

  //   formData.append('files', {
  //     name: images[0].modificationDate,
  //     type: images[0].mime,
  //     uri: images[0].path,
  //   });
  //   console.log(formData);

  //   let imgApi = await uploadFilesPostRequest(formData);
  //   console.log('imgApi', imgApi);
  //   const url = imgApi[0]?.url;
  //   setLoading(false);
  //   openEditor(url);
  // };
  const [video, setVideo] = useState();
  const [duration, setDuration] = useState('');
  const openEditor = (url, time) => {
    // this.props.navigation.navigate('VideoUploadingScreen', {url});
    VESDK.openEditor(url).then(
      result => {
        setVideo(result?.video);
        console.log('result', result);
      },
      error => {
        console.log(error);
      },
    );
    var milliseconds = time % 1000;
    var seconds = Math.floor((time / 1000) % 60);
    var minutes = Math.floor((time / (60 * 1000)) % 60);
    const t = minutes + ":" + seconds
    setDuration(t);
    console.log("time", t);
  };

  const ImgSelector = ({ item, index }) => (
    <Image
      source={{ uri: item.path }}
      style={{ width: 300, height: 400, margin: 8, alignItems: 'center' }}
      resizeMode="contain"
    />
  );

  const renderItem = ({ item, index }) => {
    return <ImgSelector item={item} />;
  };

  const isCarousel = React.useRef(null);
  const SLIDER_WIDTH = Dimensions.get('window').width + 80;
  const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);

  const CarouselCardItem = ({ item, index }) => {
    console.log('path', item);
    return (
      <View
        style={{
          backgroundColor: 'white',
          borderRadius: 8,
          width: ITEM_WIDTH,
          paddingBottom: 40,
          shadowColor: '#fff',
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: 0.29,
          shadowRadius: 4.65,
          elevation: 7,
        }}
        key={index}>
        {item.mime === 'video/mp4' ? (
          <VideoPlayer
            source={{ uri: item.path }}
            // poster={require('../../assets/images/avata.png')}
            // controls={true}
            style={{
              height: 300,
              top: -15,
              borderWidth: 1.9,
              borderColor: '#2c3036',
              borderRadius: 20,
            }}
            // onEnd={onEnd}
            // playWhenInactive={true}
            // repeat={true}
            lockToPortrait={true}
            lockToLandscape={true}
            resizeMode="cover"
            lockControl={true}
            // tapAnywhereToPause={true}
            // rate={1.0}
            ignoreSilentSwitch={'obey'}
          />
        ) : (
          <Image
            source={{ uri: item.path }}
            style={{
              width: ITEM_WIDTH,
              height: 300,
            }}
          />
        )}

        {/* <Text style={styles.header}>{item.title}</Text>
      <Text style={styles.body}>{item.body}</Text> */}
      </View>
    );
  };

  const saveRewardedVideo = async () => {
    // debugger
    if (!title && !description) {
      setSuccess({
        ...success,
        title: false,
        description: false,
      });
    } else {
      let formData = new FormData();

      formData.append('files', {
        name: title,
        type: 'mime/mp4',
        uri: video,
      });

      let videoApi = await uploadFilesPostRequest(formData);
      console.log('videoApi', videoApi);
      if (videoApi) {
        setLoading(true);
        console.log("userdata", userData)
        let response = await dispatch(
          rewardedvideosPostRequest(videoApi, title, description, userData),
        );
        console.log('response', response);
        const postType = 'rewardedVideo'
        await dispatch(channelVideoPostRequestForReward(selectedValue, videoApi, postType));
        setLoading(false);
        navigation.navigate("Home")
      }
    }
  };

  // post method
  const saveVideo = async () => {
    // debugger
    if (!title && !description) {
      setSuccess({
        ...success,
        title: false,
        description: false,
      });
    } else {
      let formData = new FormData();

      formData.append('files', {
        name: title,
        type: 'mime/mp4',
        uri: video,
      });

      let videoApi = await uploadFilesPostRequest(formData);
      console.log('videoApi', videoApi);

      if (videoApi) {
        setLoading(true);
        console.log("userdata", userData)
        let response = await dispatch(
          videosPostRequest(videoApi, title, description, userData),
        );
        console.log('response', response);
        const postType = 'uploadedVideo'
        await dispatch(channelVideoPostRequest(selectedValue, videoApi, postType));
        setLoading(false);
        navigation.navigate("Home")
      }
    }
  };
  return (
    <ScrollView>
      <View style={{ flex: 1, marginTop: 20 }}>
        {/* <Carousel
        layout={'stack'}
        layoutCardOffset={19}
        ref={isCarousel}
        data={route.params.image}
        renderItem={CarouselCardItem}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
        inactiveSlideShift={0}
        useScrollView={true}
      /> */}
        <ScrollView style={{ flex: 1 }}>
          <View>
            <YoutubePlayer
              source={{ uri: video ? video : 'video' }}
              // // poster={require('../../assets/images/avata.png')}
              // controls={true}

              // pictureInPicture={PictureInPicture.start()}
              mode="auto-fit"
              aspecRatio="landscape"
              fullscreenOrientation='landscape'
              fullscreenAutorotate={true}
              fullscreen={true}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                margin: 20,
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Home');
                }}>
                <Image
                  style={{ width: 15, height: 15, backgroundColor: 'white' }}
                  resizeMode="contain"
                  source={require('./cancel.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Home');
                }}>
                <Text style={{ fontFamily: 'ProximaNova', fontSize: 16 }}>
                  Edit
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ width: '100%', overflow: 'scroll' }}>
              {/* <FlatList
            contentContainerStyle={{ margin: 1, width: '100%' }}
            // numColumns={3}
            data={image}
            keyExtractor={(item, index) => item.path}
            renderItem={renderItem}
          /> */}
            </View>
          </View>
          <View style={{ marginTop: 10, alignItems: 'center' }}>
            <NeomorphBlur
              style={{
                shadowRadius: 3,
                borderRadius: 50,
                backgroundColor: colors.neoThemebg,
                width: 170,
                height: 56,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 10,
              }}>
              <Button onPress={captureImage} style={{}}>
                Add Videos
              </Button>
            </NeomorphBlur>

            <Neomorph
              inner
              style={{
                shadowRadius: Utils.scaleWithPixel(7),
                borderRadius: Utils.scaleWithPixel(15),
                backgroundColor: colors.neoThemebg,
                width: Utils.scaleWithPixel(300),
                height: Utils.scaleWithPixel(50),
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
                // top: 60
              }}>
              <TextInput
                placeholder="Enter title of the post"
                style={{ margin: 20 }}
                value={title}
                onChangeText={title => setTitle(title)}
              />
            </Neomorph>

            <Neomorph
              inner
              style={{
                shadowRadius: Utils.scaleWithPixel(7),
                borderRadius: Utils.scaleWithPixel(15),
                backgroundColor: colors.neoThemebg,
                width: Utils.scaleWithPixel(300),
                height: Utils.scaleWithPixel(50),
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
                // top: 60
              }}>
              <TextInput
                placeholder="Enter description"
                style={{ margin: 20 }}
                value={description}
                onChangeText={description => setDescription(description)}
              />
            </Neomorph>
            <View style={styles.container}>
              <View style={styles.checkboxContainer}>
                <CheckBox
                  disabled={false}
                  value={toggleCheckBox}
                  tintColor="red"
                  boxType="circle"
                  onValueChange={newValue => setToggleCheckBox(newValue)}
                  style={styles.checkbox}
                />
                <Text style={styles.label}>Rewarded Video</Text>
              </View>
            </View>

            <View style={{ flex: 2, flexDirection: 'row', alignItems: 'center', marginBottom: 20 }}>
              <Text bold style={{ fontFamily: 'ProximaNova', fontSize: 16 }}>
                Select a channel  :
              </Text>
              <View style={{ justifyContent: "center", height: 40, width: 170, borderColor: "white", borderWidth: 1, marginLeft: 10 }}>
                <Picker
                  mode="dropdown"
                  dropdownIconColor="#fff"
                  selectedValue={selectedValue}
                  style={{ height: 40, width: 160, margin: 10, color: '#fff' }}
                  onValueChange={(itemValue, itemIndex) => {
                    setSelectedValue(itemValue);
                  }}
                  Icon={
                    <Icon
                      name="arrow-dropdown-circle"
                      style={{ color: 'white', fontSize: 20 }}
                    />
                  }>
                  <Picker.Item key={'unselectable'} label='--Please Select--' />
                  {renderCatList()}
                </Picker>
              </View>
            </View>
            {toggleCheckBox ?
              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 50,
                  backgroundColor: colors.neoThemebg,
                  width: 200,
                  height: 56,
                  marginBottom: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {loading ? <ActivityIndicator size="small" color="#0000ff" /> :
                  <Button
                    status="success"
                    onPress={() => {
                      saveRewardedVideo();
                    }}>
                    Next
                  </Button>}
              </NeomorphBlur> :
              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 50,
                  backgroundColor: colors.neoThemebg,
                  width: 200,
                  height: 56,
                  marginBottom: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {loading ? <ActivityIndicator size="small" color="#0000ff" /> :
                  <Button
                    status="success"
                    onPress={() => {
                      saveVideo();
                    }}>
                    Upload Video
                  </Button>}
              </NeomorphBlur>}
          </View>
        </ScrollView>
      </View>
    </ScrollView>
  );
}
