import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: Utils.scaleWithPixel(10),
    flex: 1,
  },
  Icon: {
    marginTop: Utils.scaleWithPixel(-85),
    marginLeft: Utils.scaleWithPixel(60),
    paddingTop: Utils.scaleWithPixel(10),
    borderRadius: Utils.scaleWithPixel(50),
    color: "#008cff"
  },
  image: {
    height: Utils.scaleWithPixel(70),
    width: Utils.scaleWithPixel(70),
    borderRadius: Utils.scaleWithPixel(70),
    marginTop:Utils.scaleWithPixel(-35) ,
    alignSelf:"center"

},
button: {
  height: 45,
  width: 220,
  borderRadius: 12,
  justifyContent: "center"
},
text: {
  fontFamily:"ProximaNova",
  fontSize:22,
  color:"white",
  alignSelf:"center",
  fontWeight:'700'
}
});
