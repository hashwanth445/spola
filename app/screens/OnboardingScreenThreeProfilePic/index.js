import React, { useState, useEffect } from 'react';
import {
  View,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  Image,
  PermissionsAndroid,
  ActivityIndicator
} from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Button, Text } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import ImagePicker from 'react-native-image-crop-picker';
import { BaseColor } from '../../config/theme';
import { TextInput } from 'react-native-gesture-handler';
import * as Utils from '@utils';
import Dots from 'react-native-dots-pagination';
// import { VESDK } from 'react-native-videoeditorsdk';
import { uploadFilesPostRequest } from '../../api/uploadApi';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import { signupRequestPut } from '../../api/signup';

export default function OnboardingScreenThreeProfilePic({ navigation,route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const [loading, setLoading] = useState(false);
  const [buttonEnabled, setButtonEnabled] = useState(false);

  const { firstName, lastName,gender, userId, userToken } = route.params;
  const signupPutSuccess = useSelector(state => state.signupReducer.signupPutSuccess)

  const [image, setImage] = useState(
    'https://www.pngall.com/wp-content/uploads/5/Profile.png',
  );

  const success = async () => {
    //url change code has to be written here for CDN
      setLoading(true);
      await dispatch(signupRequestPut(firstName, lastName,gender, userId, userToken,image));
      // navigation.navigate('Categories',{image})
      setLoading(false);

  };

  if (signupPutSuccess) {
    console.log("signupSuccesssignupSuccess 123----------------------")
    navigation.navigate('OnboardingScreenFourSelectCategories')
  
  };

  const [imageString, setImageString] = useState('');
  const [imageData, setImageData] = useState();
  const dispatch = useDispatch();
  let UserData = useSelector(state => state.accessTokenReducer.userData);

  const [username, setusername] = useState(UserData?.user?.username);

  // useEffect(() => {
  //   setImage(imageData);
  // }, [imageData]);

  const requestCameraPermission = async () => {
    console.log('requestCameraPermission');

    if (Platform.OS === 'android') {
      try {
        console.log('requestCameraPermission');

        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
        console.log(PermissionsAndroid.RESULTS.GRANTED);

      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    console.log('requestExternalWritePermission');

    if (Platform.OS === 'android') {
      try {
        console.log('requestExternalWritePermission');

        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
        console.log(PermissionsAndroid.RESULTS.GRANTED);

      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const captureImage = async type => {
    console.log('outsideee');

    // let options = {
    //   storageOptions: {
    //     skipBackup: true,
    //     path: 'images',
    //   },
    // };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    console.log('outsideee', isCameraPermitted, isStoragePermitted);

    if (isCameraPermitted && isStoragePermitted) {
      console.log('insideeee');

      ImagePicker.openPicker({
        multiple: true,
      }).then(images => {
        console.log('imgApi');
        toNextPage(images);

      });
    }
  };

 
  const toNextPage = async images => {
    setLoading(true)

    let formData = new FormData();
    setLoading(true);
    formData.append('files', {
      name: images[0].modificationDate,
      type: images[0].mime,
      uri: images[0].path,
    });

    let imgApi = await uploadFilesPostRequest(formData);
    setLoading(false)
    console.log('image api', imgApi);
    setButtonEnabled(true)
    setImage(imgApi[0].url);
    console.log('url', imgApi[0].url);

  };

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <View style={styles.contain}>
            <View
              style={{
                marginTop: Utils.scaleWithPixel(630),
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <Neomorph
                Outer
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(70),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(85),
                  height: Utils.scaleWithPixel(85),
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: Utils.scaleWithPixel(0),
                }}>
                <View>
                  <Image source={{ uri: image ? image : ''}} style={styles.image} />

                  <Icon
                    onPress={() => captureImage()}
                    name="image-plus"
                    size={26}
                    style={styles.Icon}
                  />
                </View>
              </Neomorph>

              <View style={{ alignItems: 'center' }}>
                <Text
                  bold
                  style={{
                    fontSize: 20,
                    color: '#DDDDDD',
                    marginTop: Utils.scaleWithPixel(50),
                    fontFamily: 'Metropolis-Semibold',
                  }}>
                  Welcome to Spola
                </Text>
                <Text
                  bold
                  style={{
                    fontSize: 30,
                    color: '#008cff',
                    marginTop: Utils.scaleWithPixel(20),
                  }}>
                  {username} !
                </Text>
                <Text
                  numberOfLines={1}
                  bold
                  style={{
                    fontSize: 16,
                    color: '#B3B6B7',
                    marginTop: Utils.scaleWithPixel(30),
                    marginLeft: Utils.scaleWithPixel(10),
                    fontFamily: 'ProximaNova',
                  }}>
                  Your answers to the next few questions will
                </Text>
                <Text
                  numberOfLines={1}
                  bold
                  style={{
                    fontSize: 16,
                    color: '#B3B6B7',
                    top: 5,
                    marginLeft: Utils.scaleWithPixel(10),
                    fontFamily: 'ProximaNova',
                  }}>
                  help us find the right ideas for you
                </Text>
              </View>
            </View>
            <View style={{ top: Utils.scaleWithPixel(160) }}>
              {loading ? (
                <NeomorphBlur
                  style={{
                    shadowRadius: 3,
                    borderRadius: 15,
                    backgroundColor: colors.neoThemebg,
                    width: 230,
                    height: 56,
                    marginBottom: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {/* <TouchableOpacity
                onPress={() => navigation.navigate('Categories')}>
                <Text bold style={{ fontSize: 25, fontFamily: "ProximaNova", color: "#3a404b" }}>
                  Next
                </Text>
              </TouchableOpacity> */}

                  <TouchableOpacity
                  >
                    <LinearGradient
                      style={styles.button}
                      colors={['#43D4FF', '#38ABFD', '#2974FA']}>
                     <ActivityIndicator loading={loading} size="small" color="#0000ff" />
                    </LinearGradient>
                  </TouchableOpacity>
                </NeomorphBlur>
              ) : (
                <NeomorphBlur
                  style={{
                    shadowRadius: 3,
                    borderRadius: 15,
                    backgroundColor: colors.neoThemebg,
                    width: 230,
                    height: 56,
                    marginBottom: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {/* <TouchableOpacity
                    onPress={() => navigation.navigate('Categories')}>
                    <Text bold style={{ fontSize: 25, fontFamily: "ProximaNova", color: "#3a404b" }}>
                      Next
                    </Text>
                  </TouchableOpacity> */}
                  {buttonEnabled ? (
                  <TouchableOpacity
                  onPress={() => success()}>
                  <LinearGradient
                    style={styles.button}
                    colors={['#43D4FF', '#38ABFD', '#2974FA']}>
                    <Text loading={loading} bold style={styles.text}>
                      Next
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
                  ):(
                    <Button
                    style={{ backgroundColor: colors.neoThemebg }}
                    full
                    loading={loading}
                  >
                    Continue
                  </Button>
                   
                  )}
                  
                </NeomorphBlur>
              )}

              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 15,
                  backgroundColor: colors.neoThemebg,
                  width: 230,
                  height: 56,
                  marginBottom: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {/* <TouchableOpacity
                    onPress={() => navigation.navigate('Categories')}>
                    <Text bold style={{ fontSize: 25, fontFamily: "ProximaNova", color: "#3a404b" }}>
                      Next
                    </Text>
                  </TouchableOpacity> */}

                <TouchableOpacity
                  onPress={() => navigation.navigate('OnboardingScreenFourSelectCategories', { image })}>
                  <LinearGradient
                    style={styles.button}
                    colors={['#43D4FF', '#38ABFD', '#2974FA']}>
                    <Text loading={loading} bold style={styles.text}>
                      Skip
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </NeomorphBlur>

              <View style={{ top: Utils.scaleWithPixel(0), left: 10 }}>
                <Dots length={4} activeColor="#0081ff" active={0} />
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
}
