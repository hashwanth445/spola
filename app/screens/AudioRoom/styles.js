import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: Utils.scaleWithPixel(10),
    flex: 1,
   
  },
  Icon: {
    marginTop: Utils.scaleWithPixel(-20),
    color: "#FFBF00"
  },
  image: {
      height: Utils.scaleWithPixel(80),
      width: Utils.scaleWithPixel(80),
      borderRadius: Utils.scaleWithPixel(70),
      top:Utils.scaleWithPixel(10)
      
  }
});
