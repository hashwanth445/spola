import React, { useEffect, useState } from 'react';
import { View, KeyboardAvoidingView, Platform, FlatList, TouchableOpacity, Image, RefreshControl } from 'react-native';
import { BaseStyle, useTheme, Images } from '@config';
import { Header, SafeAreaView, Icon, Text, ListThumbnails, ProfileGroup1 } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { MessagesData } from '@data';
import { Neomorph } from 'react-native-neomorph-shadows';
import { BaseColor } from '../../config/theme';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import * as Utils from '@utils';
import {
    Menu,
    Button,
    VStack,
    Select,
    CheckIcon,
    IconButton,
    Center,
    NativeBaseProvider,
} from "native-base";
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import { MaterialIcons } from 'react-native-vector-icons'
import { Buffer } from 'buffer';
import Permissions from 'react-native-permissions';
import { AudioPlayer } from 'react-native-audio-player-recorder';
import LiveAudioStream from 'react-native-live-audio-stream';
import { useDispatch } from 'react-redux';

export default function AudioRoom({ navigation, route }) {
    const { colors } = useTheme();
    const dispatch = useDispatch();

    const sound = null
    const [audioFile,setAudioFile] = useState("");
    const [recording,setRecording] = useState(false);
    const [loaded,setLoaded] = useState(false);
    const [paused,setPaused] = useState(true);
    const { t } = useTranslation();
    const [refreshing] = useState(false);
    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });
    // const [shouldShowFollow, setShouldShowFollow] = useState(true);
    const [messenger] = useState(MessagesData);
    const [shouldOverlapWithTrigger] = useState(false);
    const [position, setPosition] = useState("auto");
    const [shouldShowLeave, setShouldShowLeave] = useState(true);
    const [shouldShowMute, setShouldShowMute] = useState(true);
    const { audioroomUrlObject } = route.params;
    console.log("audioroomUrlObject", audioroomUrlObject);
    const options = {
        sampleRate: 32000,  // default is 44100 but 32000 is adequate for accurate voice recognition
        channels: 1,        // 1 or 2, default 1
        bitsPerSample: 16,  // 8 or 16, default 16
        audioSource: 7,     // android only (see below)
        bufferSize: 4096 ,  // default is 2048
        wavFile: 'test.wav',
      };
      useEffect(() => {
        checkPermission() //have to implement permissions just like in live note by ashok
         initAudioPlayer()
        LiveAudioStream.init(options);
        // LiveAudioStream.on('data', data => {
        //     const chunk = Buffer.from(data, 'base64');
        //     console.log('chunk size rrrr',data, chunk);
        //     // do something with audio chunk
        //     // base64-encoded audio data chunks
        //   });
          
       
    }, [])
    
    const checkPermission = async () => {
        const p = await Permissions.check('microphone');
        console.log('permission check', p);
        if (p === 'authorized') return;
        return requestPermission();
      };
    
     const requestPermission = async () => {
        const p = await Permissions.request('microphone');
        console.log('permission request', p);
      };
     
    //   LiveAudioStream.start();
    //   LiveAudioStream.stop();

    const initAudioPlayer = () => {
        AudioPlayer.onFinished = () => {
          console.log('finished playback');
          setPaused(true)
          setLoaded(false)
        //   this.setState({ paused: true, loaded: false });
        };
        AudioPlayer.setFinishedSubscription();
    
        AudioPlayer.onProgress = data => {
          console.log('progress', data);
        };
        AudioPlayer.setProgressSubscription();
      };

      const start = () => {
        console.log('start record -----------------------');

        setShouldShowMute(!shouldShowMute)
        console.log('start record');
        setRecording(true)
          setLoaded(false)
          setAudioFile('')
        // this.setState({ audioFile: '', recording: true, loaded: false });
        LiveAudioStream.start();
      };
    
      const stop = async () => {
        console.log('stop record -----------------------',recording);

        setShouldShowMute(!shouldShowMute)
        if (!recording) return;
        console.log('stop recorddingggg====================');
        let audioFiles = await LiveAudioStream.stop();
        console.log('audioFile=================================', audioFiles);
        setRecording(false)
          setAudioFile(audioFiles)
        // this.setState({ audioFile, recording: false });
      };
    
      const play = () => {
        if (loaded) {
          AudioPlayer.unpause();
          setPaused(false)
        //   this.setState({ paused: false });
        } else {
          AudioPlayer.play(this.state.audioFile);
          setPaused(false)
          setLoaded(true)
        //   this.setState({ paused: false, loaded: true });
        }
      };
    
      const pause = () => {
        AudioPlayer.pause();
        setPaused(true)
        // this.setState({ paused: true });
      };

    return (
        <View style={{ flex: 1 }}>
            <Header
                title="Audio Room"
                renderLeft={() => {
                    return (
                        <Icon
                            name="chevron-left"
                            size={32}
                            color={colors.text}
                            enableRTL={true}
                        />
                    );
                }}
                renderRightSecond={() => {
                    return (
                        <Icon
                            name="share-outline"
                            size={28}
                            color={colors.text}
                            enableRTL={true}
                        />
                    );
                }}
                renderRight={() => {
                    return (
                        <Icon
                            name="dots-vertical"
                            size={28}
                            color={colors.text}
                            enableRTL={true}
                        />
                    );
                }}
                onPressLeft={() => {
                    navigation.goBack();
                }}
            />
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>
                    <ScrollView>
                        <View style={styles.contain}>

                            {/* <Text bold style={{ fontSize: 15, color: "#6a6c6e", marginTop: 10 }}>Follow to stay updated with fresh content</Text> */}
                            <View style={{
                                marginTop: Utils.scaleWithPixel(10),

                            }}>
                                <Neomorph
                                    outer
                                    style={{
                                        shadowRadius: Utils.scaleWithPixel(7),
                                        borderRadius: Utils.scaleWithPixel(50),
                                        borderBottomRightRadius: Utils.scaleWithPixel(0),
                                        backgroundColor: colors.neoThemebg,
                                        width: Utils.scaleWithPixel(350),
                                        height: Utils.scaleWithPixel(505),
                                        // justifyContent: 'center',
                                        // alignItems: 'center',
                                        top: Utils.scaleWithPixel(5),
                                        // left: -30,
                                        margin: Utils.scaleWithPixel(10)
                                    }}
                                >
                                    <Text bold style={{ fontSize: 18, color: "#DDDDDD", top: Utils.scaleWithPixel(10),alignSelf:"center", margin: Utils.scaleWithPixel(10) }}>{audioroomUrlObject?.audioRoomDesc}</Text>

                                    <View>
                                        <ProfileGroup1
                                            // onPress={() => { navigation.navigate('Messages'); }}
                                            style={{ margin: Utils.scaleWithPixel(30) }}
                                            
                                            // image={audioroomUrlObject?.audioRoomPeople?.backBlazeImageUrl}
                                            audioObj={audioroomUrlObject}
                                        />
                                    </View>
                                </Neomorph>
                                <View style={{ flexDirection: "row", left: Utils.scaleWithPixel(50), top: 10 }}>

                                    <Neomorph
                                        outer
                                        style={{
                                            shadowRadius: Utils.scaleWithPixel(7),
                                            borderRadius: Utils.scaleWithPixel(20),
                                            backgroundColor: colors.neoThemebg,
                                            width: Utils.scaleWithPixel(125),
                                            height: Utils.scaleWithPixel(45),
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            top: Utils.scaleWithPixel(5),
                                            left: Utils.scaleWithPixel(-30),
                                            margin: Utils.scaleWithPixel(10)
                                        }}
                                    >
                                        <TouchableOpacity onPress={() => {
                                            navigation.navigate('Messenger')
                                        }}>
                                            <Text bold style={{ fontSize: 20, color: "#e3dce1" }}>✌️ Leave quietly</Text>
                                        </TouchableOpacity>
                                    </Neomorph>


                                    <Neomorph
                                        outer
                                        style={{
                                            shadowRadius: Utils.scaleWithPixel(7),
                                            borderRadius: Utils.scaleWithPixel(30),
                                            backgroundColor: colors.neoThemebg,
                                            width: Utils.scaleWithPixel(50),
                                            height: Utils.scaleWithPixel(50),
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            margin: Utils.scaleWithPixel(10),
                                            top: Utils.scaleWithPixel(5),
                                            left: -45
                                        }}
                                    >
                                        {/* <TouchableOpacity>
                                        <Icon name="attachment" size={30} color="#5a5b5d" style={{ margin: Utils.scaleWithPixel(5) }} />
                                        </TouchableOpacity> */}
                                        <NativeBaseProvider >
                                            <Center flex={1} px="3">
                                                <VStack space={6} alignSelf="flex-start" w="100%">
                                                    <Menu
                                                        style={{
                                                            padding: 0,
                                                            backgroundColor: 'transparent',
                                                            blurRadius: 90,
                                                            blurType: "light",
                                                            blurAmount: 50,
                                                            alignItems: 'center',
                                                            borderWidth: 0,
                                                            borderRadius: 10,
                                                            width: 120,
                                                            top: 0,
                                                            left: -28,
                                                        }}
                                                        shouldOverlapWithTrigger={shouldOverlapWithTrigger} // @ts-ignore
                                                        placement={position == "auto" ? undefined : position}
                                                        trigger={(triggerProps) => {
                                                            return (
                                                                <Button style={{
                                                                    backgroundColor: 'transparent',
                                                                }} {...triggerProps}>
                                                                    <Neomorph

                                                                        style={{
                                                                            width: Utils.scaleWithPixel(45),
                                                                            height: Utils.scaleWithPixel(45),
                                                                            borderRadius: Utils.scaleWithPixel(60),
                                                                            // borderWidth: Utils.scaleWithPixel(2),
                                                                            // borderColor: colors.neoThemeBorderColor,
                                                                            backgroundColor: colors.neoThemebg,
                                                                            justifyContent: 'center',
                                                                            alignItems: 'center',
                                                                        }}
                                                                    >

                                                                        <Neomorph
                                                                            // inner={true}
                                                                            // darkShadowColor="#1d2024" // <- set this
                                                                            // lightShadowColor="#2d4660"
                                                                            // darkShadowColor="#2aa9f9" // <- set this
                                                                            // lightShadowColor="#83cfff"
                                                                            swapShadows="false"
                                                                            style={{
                                                                                shadowRadius: 10,
                                                                                borderRadius: 100,
                                                                                // backgroundColor: "#2aa9f9",
                                                                                width: 51,
                                                                                height: 51,
                                                                                borderWidth: 0,
                                                                                // borderColor: "#b1d9f2",
                                                                                shadowOpacity: 0.2,
                                                                                justifyContent: 'center',
                                                                                alignItems: 'center',

                                                                            }}
                                                                        >
                                                                            {/* <Icon solid color="#e2e2e2" name="plus-thick" size={20} solid /> */}
                                                                            {/* <Image source={require('../assets/images2/videocam_icn.png')} style={{ height: 20, width: 24 }} /> */}
                                                                            <Icon color="#5a5b5d" name="attachment" size={28} />

                                                                        </Neomorph>
                                                                    </Neomorph>

                                                                </Button>
                                                            )
                                                        }}
                                                    >
                                                        <View style={{ flexDirection: "row" }}>
                                                            <View style={{ flexDirection: "column" }}>

                                                                <Menu.Item>
                                                                    <View style={{ flexDirection: "row", justifyContent: 'flex-start', padding: 6, backgroundColor: BaseColor.greenColor, borderRadius: 20, width: 40, height: 40 }}>
                                                                        <Icon color="#fff" name="map-marker" size={24} style={{ left: 2, top: 2 }} />

                                                                    </View>
                                                                </Menu.Item>

                                                                {/* <View style={{ height: 1, width: 85, left: 3, backgroundColor: "rgb(192,192,192)" }}></View> */}

                                                                <Menu.Item>
                                                                    <View style={{ flexDirection: "row", justifyContent: 'flex-start', padding: 6, backgroundColor: BaseColor.blueColor, borderRadius: 20, width: 40, height: 40 }}>

                                                                        <Icon color="#fff" name="image" size={24} style={{ left: 2, top: 2 }} />
                                                                    </View>
                                                                </Menu.Item>

                                                                {/* <View style={{ height: 1, width: 85, left: 3, backgroundColor: "rgb(192,192,192)" }}></View> */}

                                                                <Menu.Item>
                                                                    <View style={{ flexDirection: "row", justifyContent: 'flex-start', padding: 6, backgroundColor: BaseColor.navyBlue, borderRadius: 20, width: 40, height: 40 }}>

                                                                        <Icon color="#fff" name="account" size={24} style={{ left: 2, top: 2 }} />
                                                                    </View>
                                                                </Menu.Item>

                                                                {/* <View style={{ height: 1, width: 85, left: 3, backgroundColor: "rgb(192,192,192)" }}></View> */}

                                                                <Menu.Item>
                                                                    <View style={{ flexDirection: "row", justifyContent: 'flex-start', padding: 6, backgroundColor: BaseColor.yellowColor, borderRadius: 20, width: 40, height: 40 }}>

                                                                        <Icon color="#fff" name="headphones" size={24} style={{ left: 2, top: 2 }} />
                                                                    </View>
                                                                </Menu.Item>
                                                            </View>
                                                            <View style={{ flexDirection: "column" }}>

                                                                <Menu.Item>
                                                                    <View style={{ flexDirection: "row", justifyContent: 'flex-start', padding: 6, backgroundColor: "pink", borderRadius: 20, width: 40, height: 40 }}>
                                                                        <Icon color="#fff" name="video" size={24} style={{ left: 2, top: 2 }} />

                                                                    </View>
                                                                </Menu.Item>

                                                                {/* <View style={{ height: 1, width: 85, left: 3, backgroundColor: "rgb(192,192,192)" }}></View> */}

                                                                <Menu.Item>
                                                                    <View style={{ flexDirection: "row", justifyContent: 'flex-start', padding: 6, backgroundColor: "brown", borderRadius: 20, width: 40, height: 40 }}>

                                                                        <Icon color="#fff" name="camera" size={24} style={{ left: 2, top: 2 }} />
                                                                    </View>
                                                                </Menu.Item>

                                                                {/* <View style={{ height: 1, width: 85, left: 3, backgroundColor: "rgb(192,192,192)" }}></View> */}

                                                                <Menu.Item>
                                                                    <View style={{ flexDirection: "row", justifyContent: 'flex-start', padding: 6, backgroundColor: BaseColor.pinkColor, borderRadius: 20, width: 40, height: 40 }}>

                                                                        <Icon color="#fff" name="file" size={24} style={{ left: 2, top: 2 }} />
                                                                    </View>
                                                                </Menu.Item>

                                                                {/* <View style={{ height: 1, width: 85, left: 3, backgroundColor: "rgb(192,192,192)" }}></View> */}

                                                                {/* <Menu.Item>
                                                            <View style={{ flexDirection: "row", justifyContent: 'flex-start', padding: 6,backgroundColor:BaseColor.yellowColor,borderRadius:20,width:40,height:40 }}>

                                                                <Icon color="#fff" name="headphones" size={24} style={{ left: 2, top: 2 }} />
                                                            </View>
                                                        </Menu.Item> */}
                                                            </View>
                                                        </View>
                                                    </Menu>
                                                </VStack></Center></NativeBaseProvider>
                                    </Neomorph>
                                    {shouldShowMute ? (
                                        <Neomorph
                                            outer
                                            style={{
                                                shadowRadius: Utils.scaleWithPixel(7),
                                                borderRadius: Utils.scaleWithPixel(30),
                                                backgroundColor: colors.neoThemebg,
                                                width: Utils.scaleWithPixel(50),
                                                height: Utils.scaleWithPixel(50),
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: Utils.scaleWithPixel(10),
                                                top: Utils.scaleWithPixel(5),
                                                left: -55
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => 
                                                    start()
                                                    }>
                                                <Icon name="microphone-off" size={30} color="#5a5b5d" style={{ margin: Utils.scaleWithPixel(5) }} />
                                            </TouchableOpacity>
                                        </Neomorph>) : (
                                        <Neomorph
                                            inner
                                            style={{
                                                shadowRadius: Utils.scaleWithPixel(7),
                                                borderRadius: Utils.scaleWithPixel(30),
                                                backgroundColor: colors.neoThemebg,
                                                width: Utils.scaleWithPixel(50),
                                                height: Utils.scaleWithPixel(50),
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: Utils.scaleWithPixel(10),
                                                top: Utils.scaleWithPixel(5),
                                                left: -55

                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => 
                                                    stop()
                                                    }>
                                                <Icon name="microphone" size={30} color="#DDDDDD" style={{ margin:5 }} />
                                            </TouchableOpacity>
                                        </Neomorph>
                                    )}
                                    {shouldShowLeave ? (
                                        <Neomorph
                                            outer
                                            style={{
                                                shadowRadius: Utils.scaleWithPixel(7),
                                                borderRadius: Utils.scaleWithPixel(30),
                                                backgroundColor: colors.neoThemebg,
                                                width: Utils.scaleWithPixel(50),
                                                height: Utils.scaleWithPixel(50),
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: Utils.scaleWithPixel(10),
                                                top: Utils.scaleWithPixel(5),
                                                left: -65
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => setShouldShowLeave(!shouldShowLeave)}>
                                                <Icon name="hand-right" size={30} color="#5a5b5d" style={{ margin: Utils.scaleWithPixel(5) }} />
                                            </TouchableOpacity>
                                        </Neomorph>) : (
                                        <Neomorph
                                            inner
                                            style={{
                                                shadowRadius: Utils.scaleWithPixel(7),
                                                borderRadius: Utils.scaleWithPixel(30),
                                                backgroundColor: colors.neoThemebg,
                                                width: Utils.scaleWithPixel(50),
                                                height: Utils.scaleWithPixel(50),
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: Utils.scaleWithPixel(10),
                                                top: Utils.scaleWithPixel(5),
                                                left: -65

                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => setShouldShowLeave(!shouldShowLeave)}>
                                                <Icon name="hand-right" size={30} color="#DDDDDD" style={{ margin:5 }} />
                                            </TouchableOpacity>
                                        </Neomorph>
                                    )}
                                </View>


                            </View>

                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View >
    );
}