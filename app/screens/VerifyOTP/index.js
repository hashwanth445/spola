import React, { useState } from 'react';
import { View, KeyboardAvoidingView, Platform, TouchableOpacity } from 'react-native';
import { BaseStyle, useTheme, Images } from '@config';
import { sendVerifyOtpRequest } from '../../api/signup'
import { useDispatch, useSelector } from 'react-redux';
import { Header, SafeAreaView, Icon, Button, Text, Image } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
// import { BaseColor } from '../../config/theme';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { TextInput } from 'react-native-gesture-handler';
import { number } from 'prop-types';
import { verifyOtpSucess } from '../../actions/signup';
import * as Utils from '@utils';

export default function VerifyOTP({ navigation, route }) {

    //const signloading = useSelector(state => state.signupReducer.signupLoading);
    // const verifyOtpSucess = useSelector(state => state.signupReducer.verifyOtpSucess)
    const { colors } = useTheme();
    const [enterOtp, setenterOtp] = useState('')
    const dispatch = useDispatch();
    const { t } = useTranslation();

    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });

    const { MobileNumber, password } = route.params

    const [loading, setLoading] = useState(false);

    const onSignUp = async () => {
        if (MobileNumber == '' || password == '') {
            setSuccess({
                ...success,
                MobileNumber: MobileNumber != '' ? true : false,
                password: password != '' ? true : false,
                // address: address != '' ? true : false,
            });
        } else {
            setLoading(true);
            await dispatch(sendOtpRequest(MobileNumber));
            navigation.navigate('VerifyOTP', { MobileNumber, password });
            // navigation.navigate('VerifyOTP', { param : [MobileNumber , password ] } );

            setLoading(false);
        }
    };



    const onVerifyOTP = async () => {
        if (!enterOtp) {

            alert('Enter the OTP');
        }

        else {
            console.log('otp------------------', enterOtp);
            setLoading(true);
            await dispatch(sendVerifyOtpRequest(MobileNumber, enterOtp));
            // navigation.navigate('OnboardingScreenOneProfileDetails')
            setLoading(false);
        }
        if (verifyOtpSucess(true)) {
            alert('verifyOtpSucess-------------');
            navigation.navigate('OnboardingScreenOneProfileDetails', { MobileNumber, password })
        }


    };


    return (
        <View style={{ flex: 1 }}>
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>
                    <Icon onPress={() => navigation.goBack()}
                        name="chevron-left" size={28} color="white" style={{ top: Utils.scaleWithPixel(20), left: Utils.scaleWithPixel(10) }} />
                    <Image source={Images.logo} style={{ height: Utils.scaleWithPixel(160), width: Utils.scaleWithPixel(160), left: Utils.scaleWithPixel(80), top: Utils.scaleWithPixel(30) }} />
                    <View style={styles.contain}>
                        <Text style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", fontWeight: "bold", top: Utils.scaleWithPixel(-110) }}> Verify OTP</Text>
                        <Text bold style={{ fontSize: 20, top: Utils.scaleWithPixel(-90), color: "#6a6c6e" }}>Enter the 4 digit OTP sent on  MobileNumber to proceed</Text>
                        <View style={{
                            marginTop: Utils.scaleWithPixel(-70),
                            flexDirection: "row"
                        }}>

                            <Neomorph
                                inner
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(20),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(150),
                                    height: Utils.scaleWithPixel(55),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    margin: Utils.scaleWithPixel(10)
                                }}
                            >
                                <TextInput autoComplete="sms-otp" onChangeText={(text) => setenterOtp(text)} maxLength={4} keyboardType='phone-pad' style={{ color: "white", fontSize: 28, width: Utils.scaleWithPixel(70), height: Utils.scaleWithPixel(70), textAlign: "center" }} />
                            </Neomorph>

                            {/*  <Neomorph
                                inner
                                style={{
                                    shadowRadius: 7,
                                    borderRadius: 20,
                                    backgroundColor: colors.neoThemebg,
                                    width: 75,
                                    height: 75,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    margin: 10
                                }}
                            >
                                 <TextInput onChangeText={(text) => setenterOtp(text)} maxLength={1} keyboardType='phone-pad' style={{color:"white", fontSize: 28, width: 70, height: 70, textAlign: "center"}} />
                            </Neomorph>
                            <Neomorph
                                inner
                                style={{
                                    shadowRadius: 7,
                                    borderRadius: 20,
                                    backgroundColor: colors.neoThemebg,
                                    width: 75,
                                    height: 75,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    margin: 10
                                }}
                            >
                                <TextInput onChangeText={(text) => setenterOtp(text)} maxLength={1} keyboardType='phone-pad' style={{color:"white", fontSize: 28, width: 70, height: 70, textAlign: "center"}} />
                            </Neomorph>
                            <Neomorph
                                inner
                                style={{
                                    shadowRadius: 7,
                                    borderRadius: 20,
                                    backgroundColor: colors.neoThemebg,
                                    width: 75,
                                    height: 75,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    margin: 10
                                }}
                            >
                                <TextInput onChangeText={(text) => setenterOtp(text)} maxLength={1} keyboardType='phone-pad' style={{color:"white", fontSize: 28, width: 70, height: 70, textAlign: "center"}} />
                            </Neomorph>
                            */}
                        </View>
                        <View style={{ top: Utils.scaleWithPixel(60) }}>
                            <NeomorphBlur
                                style={{
                                    shadowRadius: 3,
                                    borderRadius: 50,
                                    backgroundColor: colors.neoThemebg,
                                    width: 230,
                                    height: 56,

                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => {
                                        onVerifyOTP()
                                    }}>
                                    {/* onPress={() => navigation.navigate('OnboardingScreenOneProfileDetails')}> */}
                                    <Text body1 style={{ fontSize: 20, fontFamily: "Metropolis-Semibold", color: "#3a404b" }}>
                                        Verify and Proceed
                                    </Text>
                                </TouchableOpacity>
                            </NeomorphBlur>
                            <TouchableOpacity
                                // onPress={() => navigation.navigate('SignUp')}
                                onPress={() => {
                                    onSignUp()
                                }}
                            >
                                <View style={{ flexDirection: "row", left: Utils.scaleWithPixel(-30) }}>
                                    <Text body1 style={{ top: Utils.scaleWithPixel(100), left: Utils.scaleWithPixel(30), fontFamily: "Metropolis-Semibold", color: "#6a6c6e" }}>
                                        Didn't receive the code?
                                    </Text>
                                    <Text body1 style={{ top: Utils.scaleWithPixel(100), left: Utils.scaleWithPixel(32), fontFamily: "Metropolis-Semibold", color: "#0094ff" }}>
                                        Request again
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    );
}