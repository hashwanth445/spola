import React,{useState} from 'react';
import {View,StyleSheet, Alert,Text } from 'react-native'
import Video from 'react-native-video'
import AsyncStorage from '@react-native-async-storage/async-storage'
import CameraRoll from '@react-native-community/cameraroll'
function Playvideo({route}) {
  //  const {data}=route.params; 
const [video,setVideo]=useState([])
  // AsyncStorage.getItem('video').then(data=>setVideo(data))
  CameraRoll.getPhotos({first:1,assetType:'Videos'}).then(data=>setVideo(data.edges))
    return (
        <View>
            <Video
            source={{uri: video[0].node.video.uri}}
            style={styles.backgroundVideo}
            resizeMode={"cover"}
          />
          <Text>Hi</Text>
        </View>
    );
}
const styles=StyleSheet.create({
  container:{
    flex:1
  },
  backgroundVideo:{
    top:0,
    bottom:0,
    left:0,
    right:0,
    position:'absolute'
  }
})

export default Playvideo;