/* eslint-disable no-console */
import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Slider,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  Modal,
  TextInput,
  FlatList,
  ToastAndroid,
} from 'react-native';

// eslint-disable-next-line import/no-unresolved
import {RNCamera} from 'react-native-camera';
import TrackPlayer, {Capability, Event} from 'react-native-track-player';
import Icon from 'react-native-vector-icons/AntDesign';
import Sound from 'react-native-sound';
import Video from 'react-native-video';
import {
  VESDK,
  VideoEditorModal,
  Configuration,
} from 'react-native-videoeditorsdk';
import DocumentPicker from 'react-native-document-picker';
import CameraRoll from '@react-native-community/cameraroll';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CountDown from 'react-native-countdown-component';
import {uploadFilesPostRequest} from '../../api/uploadApi';
// import { ProcessingManager } from 'react-native-video-processing';
import Bgms from '../../components/Bgms';
import {bgmsPostRequest, bgmsGetRequest} from '../../api/bgms';
import * as _ from 'lodash';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const flashModeOrder = {
  off: 'on',
  on: 'auto',
  auto: 'torch',
  torch: 'off',
};

const wbOrder = {
  auto: 'sunny',
  sunny: 'cloudy',
  cloudy: 'shadow',
  shadow: 'fluorescent',
  fluorescent: 'incandescent',
  incandescent: 'auto',
};

const landmarkSize = 2;

export default class CreateSpola extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flash: 'off',
      zoom: 0,
      autoFocus: 'on',
      modalvisible: false,
      bgmName: 'Sooong',
      musicid: null,
      tempmusicid: null,
      textmodal: false,
      playpause: false,
      playing: false,
      duration: 15,
      timerstart: false,
      rstop: false,
      timer: null,
      nameflag: false,
      warning: false,
      rstarting: 0,
      bgms: [],
      bgmscurr: [],
      timediff: 0,
      autoFocusPoint: {
        normalized: {x: 0.5, y: 0.5}, // normalized values required for autoFocusPointOfInterest
        drawRectPosition: {
          x: Dimensions.get('window').width * 0.5 - 32,
          y: Dimensions.get('window').height * 0.5 - 32,
        },
      },
      depth: 0,
      type: 'back',
      whiteBalance: 'auto',
      ratio: '16:9',
      recordOptions: {
        mute: false,
        maxDuration: 60,
        quality: RNCamera.Constants.VideoQuality['1080p'],
      },
      isRecording: false,
      canDetectFaces: false,
      canDetectText: false,
      canDetectBarcode: false,
      faces: [],
      textBlocks: [],
      barcodes: [],
    };
  }
  componentDidMount() {
    bgmsGetRequest().then(response => {
      console.log(response);
      this.setState({bgms: response});
    });
  }
  // componentDidUpdate(){
  //   bgmsGetRequest().then(response=>{
  //     console.log(response);
  //   this.setState({bgmscurr:response})});
  //   const {bgms}=this.state;
  //   const {bgmscurr}=this.state;
  //   (bgms!==bgmscurr)?this.setState({bgms:bgmscurr}):null;
  // }
  componentDidUpdate(prevProps, prevState) {
    const {bgms} = this.state;

    if (!_.isEqual(bgms, prevState.bgms)) {
      console.log('hiii');
      console.log('bgms', bgms);
      console.log('prevstate', prevState.bgms);
      bgmsGetRequest().then(response => {
        this.setState({bgms: response});
      });
    }
  }
  setupTrackPlayer() {
    TrackPlayer.setupPlayer();
    TrackPlayer.add(this.state.bgms.bgmBackblazeUrl);
  }

  toggleFacing() {
    this.setState({
      type: this.state.type === 'back' ? 'front' : 'back',
    });
  }

  toggleFlash() {
    this.setState({
      flash: flashModeOrder[this.state.flash],
    });
  }

  toggleWB() {
    this.setState({
      whiteBalance: wbOrder[this.state.whiteBalance],
    });
  }
  duration_handle() {
    const {duration} = this.state;
    if (duration === 15) {
      this.setState({duration: 30});
      
      ToastAndroid.show('30 seconds', 100);
    } else if (duration === 30) {
      this.setState({duration: 60});
      

      ToastAndroid.show('60 seconds', 100);
    } else {
      this.setState({duration: 15});
      

      ToastAndroid.show('15 seconds', 100);
    }
  }
  timer_handle() {
    const {timer} = this.state;
    if (timer === null) {
      this.setState({
        timer: 3
      });
    } else if (timer === 3) {
      this.setState({
        timer: 5
      });
    } else if (timer === 5) {
      this.setState({
        timer: 10
      });
    } else {
      this.setState({
        timer: null,
      });
    }
  }
  selectbgm = index => {
    this.setState({
      musicid: index,
      playing: true,
      tempmusicid: index,
    });
    this.setState({modalvisible: false});
  };

  addbgm = () => {
    DocumentPicker.pick({
      type: [DocumentPicker.types.audio],
    })
      .then(async res => {
        this.setState({textmodal: true});
        if (this.state.nameflag === true) this.toNextPage(res);
        console.log('local file', res);
        this.setState({nameflag: false});
      })
      .catch(err => console.log(err));
  };

  toNextPage = async audio => {
    let formData = new FormData();
    // setLoading(true);
    if (audio.length > 0) {
      for (let i = 0; i < audio.length; i++) {
        formData.append('files', {
          name: audio[i].name,
          type: audio[i].type,
          uri: audio[i].uri,
        });
      }
      let not = await uploadFilesPostRequest(formData);
      let url = not[0].url;
      console.log('believe', not);
      let {bgmName} = this.state;
      let bgmobj = await bgmsPostRequest(url, bgmName);
      let bgmuri = bgmobj.bgmBackblazeUrl;
      console.log('achieve', bgmuri);

      console.log('bgmss state', this.state.bgms);
      // setLoading(false);
    }
  };
  toggleFocus() {
    this.setState({
      autoFocus: this.state.autoFocus === 'on' ? 'off' : 'on',
    });
  }

  touchToFocus(event) {
    const {pageX, pageY} = event.nativeEvent;

    const isPortrait = screenHeight > screenWidth;

    let x = pageX / screenWidth;
    let y = pageY / screenHeight;
    // Coordinate transform for portrait. See autoFocusPointOfInterest in docs for more info
    if (isPortrait) {
      x = pageY / screenHeight;
      y = -(pageX / screenWidth) + 1;
    }

    this.setState({
      autoFocusPoint: {
        normalized: {x, y},
        drawRectPosition: {x: pageX, y: pageY},
      },
    });
  }

  zoomOut() {
    this.setState({
      zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1,
    });
  }

  zoomIn() {
    this.setState({
      zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1,
    });
  }

  setFocusDepth(depth) {
    this.setState({
      depth,
    });
  }
  compressVideo() {
    this.videoPlayerRef
      .compress(options)
      .then(newSource => console.log(newSource))
      .catch(console.warn);
  }
 

  openEditor = url => {
    // this.props.navigation.navigate('VideoUploadingScreen', {url});
    VESDK.openEditor(url).then(
      result => {
        this.props.navigation.navigate('VideoUploading', {result});
        console.log('result', result);
      },
      error => {
        console.log(error);
      },
    );
  };

  takeVideo = async () => {
    const {isRecording} = this.state;
    const {timer} = this.state;
    const {duration} = this.state;
    if (this.camera && !isRecording) {
      try {
        const promise = this.camera.recordAsync(this.state.recordOptions);
        

        if (promise) {
          if (timer!==null) {
            this.setState({timerstart: true});
            setTimeout(async () => {
              this.setState({isRecording: true});
              setTimeout(async () => {
                this.setState({isRecording: false});
                this.stopVideo();
                const data = await promise;
              const source = data.uri;
              this.openEditor(source);
              }, duration * 1000);
              
            }, timer * 1000);
            
          } else {
            this.setState({isRecording: true});
            setTimeout(async () => {
              this.setState({isRecording:false})
              this.stopVideo();
              const data = await promise;
              const source = data.uri;
              this.openEditor(source);
            }, duration * 1000);
          }
       
        }
      } catch (e) {
        console.error(e);
      }
    }
  };

  toggle = value => () =>
    this.setState(prevState => ({[value]: !prevState[value]}));

  facesDetected = ({faces}) => this.setState({faces});

  renderFace = ({bounds, faceID, rollAngle, yawAngle}) => (
    <View
      key={faceID}
      transform={[
        {perspective: 600},
        {rotateZ: `${rollAngle.toFixed(0)}deg`},
        {rotateY: `${yawAngle.toFixed(0)}deg`},
      ]}
      style={[
        styles.face,
        {
          ...bounds.size,
          left: bounds.origin.x,
          top: bounds.origin.y,
        },
      ]}>
      <Text style={styles.faceText}>ID: {faceID}</Text>
      <Text style={styles.faceText}>rollAngle: {rollAngle.toFixed(0)}</Text>
      <Text style={styles.faceText}>yawAngle: {yawAngle.toFixed(0)}</Text>
    </View>
  );
  toggletype = value => {
    value === 'back'
      ? this.setState({type: 'front'})
      : this.setState({type: 'back'});
  };
  toggleflash = value => {
    value === 'off'
      ? this.setState({flash: 'torch'})
      : this.setState({flash: 'off'});
  };
  renderLandmarksOfFace(face) {
    const renderLandmark = position =>
      position && (
        <View
          style={[
            styles.landmark,
            {
              left: position.x - landmarkSize / 2,
              top: position.y - landmarkSize / 2,
            },
          ]}
        />
      );
    return (
      <View key={`landmarks-${face.faceID}`}>
        {renderLandmark(face.leftEyePosition)}
        {renderLandmark(face.rightEyePosition)}
        {renderLandmark(face.leftEarPosition)}
        {renderLandmark(face.rightEarPosition)}
        {renderLandmark(face.leftCheekPosition)}
        {renderLandmark(face.rightCheekPosition)}
        {renderLandmark(face.leftMouthPosition)}
        {renderLandmark(face.mouthPosition)}
        {renderLandmark(face.rightMouthPosition)}
        {renderLandmark(face.noseBasePosition)}
        {renderLandmark(face.bottomMouthPosition)}
      </View>
    );
  }

  renderFaces = () => (
    <View style={styles.facesContainer} pointerEvents="none">
      {this.state.faces.map(this.renderFace)}
    </View>
  );

  renderLandmarks = () => (
    <View style={styles.facesContainer} pointerEvents="none">
      {this.state.faces.map(this.renderLandmarksOfFace)}
    </View>
  );

  renderTextBlock = ({bounds, value}) => (
    <React.Fragment key={value + bounds.origin.x}>
      <Text
        style={[
          styles.textBlock,
          {left: bounds.origin.x, top: bounds.origin.y},
        ]}>
        {value}
      </Text>
      <View
        style={[
          styles.text,
          {
            ...bounds.size,
            left: bounds.origin.x,
            top: bounds.origin.y,
          },
        ]}
      />
    </React.Fragment>
  );

  barcodeRecognized = ({barcodes}) => this.setState({barcodes});
  // handletimer(){
  //   this.setState({rstarting:moment().format('mm:ss')})
  // }
  renderRecording = () => {
    const {isRecording} = this.state;
    const {musicid}=this.state;
    const backgroundColor = '#FC1B2B';
    const action =()=>{ 
      musicid?( isRecording   ? this.stopVideo() : this.takeVideo() ):this.setState({modalvisible:true})
      // isRecording? this.stopVideo: this.takeVideo();
    }
  
    isRecording   ? this.start() : TrackPlayer.stop();

    return (
      <View>
        <TouchableOpacity
          style={styles.round}
          onPress={() => {
            action();
          }}>
          <View
            style={[
              {backgroundColor},
              isRecording ? styles.stopButton : styles.flipButton,
            ]}
          />
        </TouchableOpacity>
      </View>
    );
  };
  start = async( ) => {
    // Set up the player

    let track =await this.state.bgms.find(index => index._id === this.state.musicid);
    TrackPlayer.setupPlayer();
    console.log("ggggg",track.bgmBackblazeUrl)
    // Add a track to the queue
    TrackPlayer.add({
      id: 'trackId',
      url: track.bgmBackblazeUrl,
      title: 'Track Title',
    });

    // Start playing it
    TrackPlayer.play();
  };

  stopVideo = async () => {
    this.camera.stopRecording();
    this.setState({isRecording: false, rstop: true});
  };
  playsong = (index,playpause) => {
    console.log("indexxxx",index)
    // this.setState({musicid: index,playing:true});
    // playpause?TrackPlayer.pause: this.start()
  };
  renderCamera() {
    const {canDetectFaces, canDetectText, canDetectBarcode} = this.state;

  
   
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
          padding: '8%',
        }}
        type={this.state.type}
        flashMode={this.state.flash}
        autoFocus={this.state.autoFocus}
        autoFocusPointOfInterest={this.state.autoFocusPoint.normalized}
        zoom={this.state.zoom}
        whiteBalance={this.state.whiteBalance}
        ratio={this.state.ratio}
        focusDepth={this.state.depth}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        faceDetectionLandmarks={
          RNCamera.Constants.FaceDetection.Landmarks
            ? RNCamera.Constants.FaceDetection.Landmarks.all
            : undefined
        }
        onFacesDetected={canDetectFaces ? this.facesDetected : null}
        onTextRecognized={canDetectText ? this.textRecognized : null}
        onGoogleVisionBarcodesDetected={
          canDetectBarcode ? this.barcodeRecognized : null
        }>
        <View>
          <TouchableOpacity onPress={() => this.toggleflash(this.state.flash)}>
            <Image
              source={
                this.state.flash === 'off'
                  ? require('../../assets/camera/noflash.png')
                  : require('../../assets/camera/flash.png')
                
              }
              style={{width: 30, height: 30, alignSelf: 'center'}}
            />
          </TouchableOpacity>
          <View style={{top: '30%'}}>
            <TouchableWithoutFeedback
              onPress={() => this.setState({modalvisible: true})}>
              <Image
                source={require('../../assets/music/music.png')}
                style={{
                  width: 25,
                  height: 25,
                  alignSelf: 'flex-start',
                  marginVertical: '4%',
                }}
              />
            </TouchableWithoutFeedback>
            <TouchableOpacity onPress={() => this.duration_handle()}>
              <Image
                source={
                  this.state.duration === 15
                    ? require('../../assets/camera/15.png')
                    : this.state.duration === 30
                    ? require('../../assets/camera/30.png')
                    : require('../../assets/camera/60.png')
                }
                style={{width: 30, height: 30, marginVertical: '4%'}}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.timer_handle()}>
              <Image
                source={
                  this.state.timer === null
                    ? require('../../assets/camera/notime.png')
                    : this.state.timer === 3
                    ? require('../../assets/camera/3seconds.png')
                    : this.state.timer === 5
                    ? require('../../assets/camera/5seconds.png')
                    : require('../../assets/camera/10seconds.png')
                }
                style={{width: 30, height: 30, marginVertical: '2%'}}
              />
            </TouchableOpacity>
          </View>

          {this.state.timer && this.state.timerstart && (
            <View>
              <CountDown
                until={this.state.timer}
                size={30}
                onFinish={() => this.setState({timerstart: false})}
                digitStyle={{backgroundColor: 'transparent'}}
                digitTxtStyle={{color: 'white'}}
                timeToShow={['S']}
                timeLabels={'null'}
              />
            </View>
          )}
          <View>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.modalvisible}>
              <View style={styles.modalView}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View>
                    <TextInput style={styles.search}></TextInput>
                    <Icon
                      name="search1"
                      size={22}
                      style={{bottom: 35, left: 10, color: 'white'}}
                    />
                  </View>
                  <Icon
                    name="close"
                    size={22}
                    style={{color: 'white'}}
                    onPress={() => this.setState({modalvisible: false})}
                  />
                </View>
                {this.state.bgms.map(index => (
                  <View>
                    <Bgms
                      index={index}
                      fun={this.selectbgm}
                      fun2={this.playsong}
                    />
                  </View>
                ))}
                {this.state.textmodal && (
                  <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.textmodal}>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: '100%',
                        alignItems: 'center',
                        justifyContent: 'space-evenly',
                      }}>
                      <TextInput
                        style={styles.addbgm}
                        onChangeText={text =>
                          this.setState({bgmName: text})
                        }></TextInput>
                      <TouchableOpacity
                        onPress={() => {
                          this.state.bgmName === ''
                            ? this.setState({warning: true})
                            : this.setState({nameflag: true, textmodal: false});
                        }}>
                        <Image
                          source={require('../../assets/music/check.png')}
                          style={{width: 25, height: 25}}
                        />
                      </TouchableOpacity>
                    </View>
                  </Modal>
                )}
                <View
                  style={{
                    flexDirection: 'row',
                    top: 20,
                    justifyContent: 'space-between',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.addbgm();
                    }}>
                    <Image
                      source={require('../../assets/camera/add.png')}
                      style={{width: 50, height: 50}}
                    />
                  </TouchableOpacity>
                  {this.state.warning &&
                    ToastAndroid.show('enter a title for the bgm', 100)}
                </View>
              </View>
            </Modal>
          </View>

          <View
            style={{
              justifyContent: 'flex-end',
              marginTop: this.state.timerstart ? '170%' : '147%',
            }}>
            <TouchableOpacity onPress={() => this.toggletype(this.state.type)}>
              <Image
                source={require('../../assets/camera/switchcamera.png')}
                style={{
                  width: 45,
                  height: 45,
                  alignSelf: 'flex-start',
                  top: 50,
                  left: 20,
                }}
              />
            </TouchableOpacity>

            {this.renderRecording()}

            <View
              style={{
                bottom: 60,
                backgroundColor: 'transparent',
                flexDirection: 'row',
                alignSelf: 'flex-end',
                justifyContent: 'space-between',
                width: '25%',
              }}>
              <TouchableOpacity onPress={this.zoomIn.bind(this)}>
                <Image
                  source={require('../../assets/camera/zoomin.png')}
                  style={{width: 40, height: 40}}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={this.zoomOut.bind(this)}>
                <Image
                  source={require('../../assets/camera/zoomout.png')}
                  style={{width: 40, height: 40}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {!!canDetectFaces && this.renderFaces()}
        {!!canDetectFaces && this.renderLandmarks()}
        {!!canDetectText && this.renderTextBlocks()}
        {!!canDetectBarcode && this.renderBarcodes()}
      </RNCamera>
    );
  }

  render() {
    return <View style={styles.container}>{this.renderCamera()}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: '#000',
  },
  flipButton: {
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  autoFocusBox: {
    position: 'absolute',
    height: 64,
    width: 64,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: 'white',
    opacity: 0.4,
  },
  flipText: {
    color: 'white',
    fontSize: 15,
  },
  zoomText: {
    position: 'absolute',
    bottom: 70,
    zIndex: 2,
    left: 2,
  },
  picButton: {
    backgroundColor: 'darkseagreen',
  },
  facesContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
  },
  face: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#FFD700',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  landmark: {
    width: landmarkSize,
    height: landmarkSize,
    position: 'absolute',
    backgroundColor: 'red',
  },
  faceText: {
    color: '#FFD700',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
    backgroundColor: 'transparent',
  },
  text: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#F00',
    justifyContent: 'center',
  },
  textBlock: {
    color: '#F00',
    position: 'absolute',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  stopButton: {
    width: 25,
    height: 25,
    borderRadius: 7,

    backgroundColor: '#FC1B2B',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addbgm: {
    width: (screenWidth * 70) / 100,
    height: 50,
    backgroundColor: 'white',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    color: 'black',
    padding: '2%',
    fontSize: 20,
  },
  round: {
    borderColor: 'white',
    borderWidth: 2,
    width: 65,
    height: 65,
    backgroundColor: 'transparent',
    borderRadius: 65,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderRadius: 20,
    padding: '6%',
    height: '100%',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
  search: {
    width: (screenWidth * 75) / 100,
    height: (screenHeight * 6) / 100,
    borderRadius: 20,
    backgroundColor: 'rgba(255,255,255, 0.3)',
    paddingHorizontal: '10%',
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
