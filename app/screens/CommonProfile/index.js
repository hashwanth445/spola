import React, { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  FlatList,
  Switch,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  RefreshControl,
} from 'react-native';
import { setAccessToken, setUserData } from "../../actions";
import { useSelector, useDispatch } from 'react-redux';
import { AuthActions } from '@actions';
import { BaseStyle, BaseColor, Images, useTheme } from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Text,
  Icon,
  Button,
  ProfileCard,
  ProfileAuthor5,
  ProfilePerformance,
  HomeItem1,
  PostItem,
  CommonUserProfilePerformance
} from '@components';
import styles from './styles';
import { TabView, TabBar } from 'react-native-tab-view';
import { UserData, HotelData, ShortsData1, PostData2, PostData3 } from '@data';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import { videosGetRequest } from '../../api/videos';
import { shortsGetRequest } from '../../api/shorts';
import { feedsGetRequest } from '../../api/feeds';



export default function CommonProfile({ navigation, route }) {

  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const [index, setIndex] = useState(0);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const [routes] = useState([
    { key: 'booking', title: t('Shorts'), icon: 'video-wireless' },
    { key: 'profile', title: t('Videos'), icon: 'video' },
    { key: 'setting', title: t('Channels'), icon: 'youtube' },
    { key: 'activity', title: t('Subscribers'), icon: 'cog' },
  ]);

  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const [userData] = useState(UserData);
 


  const { userObjectUrl } = route.params;
  console.log("userObjectUrl", userObjectUrl);

  // When tab is activated, set what's index value
  const handleIndexChange = index => setIndex(index);

  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, { backgroundColor: colors.text }]}
      style={[styles.tabbar, { backgroundColor: colors.neoThemeBg }]}
      tabStyle={styles.tab}
      inactiveColor={BaseColor.grayColor}
      activeColor={colors.text}
      renderLabel={({ route, focused, color }) => (
        <View style={{ flex: 1, width: Utils.scaleWithPixel(350), alignItems: 'center', left: Utils.scaleWithPixel(20), }}>
          {focused ? (<View>
            <Text headline bold={focused} style={{ fontFamily: "ProximaNova" }}>
              {route.title}
            </Text>
            <Icon name={route.icon} color="#DDDDDD" size={24} style={{ top: -22, left: -30 }} />
          </View>
          ) : (
            <Icon name={route.icon} color="#909090" size={24} style={{ top: 0, left: -15 }} />
          )}
        </View>
      )}
    />
  );


  // Render correct screen container when tab is activated
  const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'booking':
        return <BookingTab jumpTo={jumpTo} navigation={navigation} data={userObjectUrl?.shorts} />;
      case 'profile':
        return <ProfileTab jumpTo={jumpTo} navigation={navigation} data={userObjectUrl?.videos} />;
      case 'setting':
        return <SettingTab jumpTo={jumpTo} navigation={navigation} />;
      case 'activity':
        return <ActivityTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };

  const [performance, setPerformance] = useState([
    {value: userObjectUrl?.followingProfiles?.length ? userObjectUrl?.followingProfiles?.length : '0', title: 'Following'},
    {value: userObjectUrl?.followersProfiles?.length ? userObjectUrl?.followersProfiles?.length : '0', title: 'Followers'},
    {value:userObjectUrl?.rewardedPoints ? userObjectUrl?.rewardedPoints : '0', title: 'Rewarded Points'},
])

  return (
    <View style={{ flex: 1 }}>
      <ImageBackground source={require('../../assets/images/photo-1.jpg')} resizeMode="cover" style={styles.image} imageStyle={{ borderRadius: Utils.scaleWithPixel(30) }}>
        <Header
          title="Profile"
          renderLeft={() => {
            return <Icon name="chevron-left" size={28} color={colors.text} />;
          }}
          onPressLeft={() => {
            navigation.goBack();
          }}
        />
        <SafeAreaView
          style={BaseStyle.safeAreaView}
          edges={['right', 'left', 'bottom']}>
          <Image
            style={{ width: 100, height: 100, borderRadius: 50, marginLeft: 140, borderColor: "#00656565", borderWidth: 3 }}
            // source={userObjectUrl.uploadedBy?.image?.url ? 'http://3.111.30.249:1337' + userObjectUrl.uploadedBy?.image?.url : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
            source={{ uri: userObjectUrl?.backBlazeImageUrl ? userObjectUrl?.backBlazeImageUrl : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png' }}
          />

          <View style={{ padding: 20, left: -25, top: -10 }}>
            <View style={{ flexDirection: "row",alignSelf: "center", left: 20  }}>
              <Text title2 bold style={{ alignSelf:"center" , fontFamily: "ProximaNova-Bold", fontSize: 18 }} >
                {userObjectUrl?.username}
              </Text>
              <Image source={Images.group} style={{ height: 40, width: 35,top:5 }} />
              {/* <Icon name="star-circle-outline" color="#DDDDDD" size={17} style={{ left: 135, top: 4, backgroundColor: "#5ebaf4", borderRadius: 10, height: 16 }} /> */}
            </View>
            {/* <Text subhead grayColor>
              {userData.major}
            </Text> */}
            {/* <View style={styles.location}>
              <Icon name="map-marker" size={10} color={colors.primary} />
              <Text
                caption1
                primaryColor
                style={{
                  marginHorizontal: 5,
                }}>
                {userData.address}
              </Text>
            </View> */}
            <View style={styles.contentInfor}>
              <CommonUserProfilePerformance
                data={performance}
                flexDirection="row"
                userObject={userObjectUrl}
              />
            </View>

            {/* <Text headline semibold style={{ marginBottom: 10 }}>
              {t('about_me')}
            </Text>
            <Text body2 numberOfLines={5}>
              {userData.about}
            </Text> */}
          </View>
          {/* <ScrollView> */}
          <TabView
            lazy
            navigationState={{ index, routes }}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            onIndexChange={handleIndexChange}
            style={{ height: '100%' }}
            tabBarShowLabel
            tabBarIcon
          />
          {/* </ScrollView> */}
        </SafeAreaView>
      </ImageBackground>
    </View>
  );
}

/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
        * @date 2019-08-03
        * @class PreviewTab
        * @extends {Component}
        */
function BookingTab({ navigation,data }) {
  const [hotels] = useState(HotelData);
  // const [shorts] = useState(ShortsData1);
  const shorts = data;
  // const shortsData = useSelector(state => state.ShortsReducer.shortsData);
console.log("shorts",shorts);
  return (
    <ScrollView>
      <FlatList
        contentContainerStyle={{
          paddingLeft: 25,

          // paddingRight: 0,
          paddingTop: 30,
        }}
        numColumns={2}
        // vertical={true}
        // showsHorizontalScrollIndicator={false}
        data={shorts}
        keyExtractor={(item, index) => item.id}
        renderItem={({ item, index }) => (

          <ProfileCard
            grid
            image={item?.backBlazeSpolaUrl ? item?.backBlazeSpolaUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
            style={{ margin: 10, left: -15, paddingTop: 20, top: -30 }}
          // onPress={() => navigation.navigate('HotelDetail')}
          >
            {/* <Image source={Images.path} style={{ height: 13, width: 11, left: -10, top: -25, }} /> */}
            <Icon name="play-outline" color="#dddddd" size={22} style={{ left: -13, top: -20, }} />
            <Text subhead whiteColor style={{ top: -40, left: 5, fontFamily: "ProximaNova-Medium", }}>
              {item?.likesCount} likes
            </Text>
            {/* <Text title2 style={{ top: 30, left: -10, fontFamily: "ProximaNova-Medium", fontSize: 18 }}>
              {item.title2}
            </Text> */}
            {/* <ProfileAuthor
              image={item.image}
              // name={item.name}
              description={item.detail}
              style={{ top: -195, left: -15 }}
            /> */}
          </ProfileCard>
        )}
      />
    </ScrollView>
  );
}

/**
 * @description Show when tab Profile activated
 * @author Passion UI <passionui.com>
          * @date 2019-08-03
          * @class PreviewTab
          * @extends {Component}
          */
function ProfileTab({ navigation,data }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
const videos = data;
  const videosData = useSelector(state => state.VideosReducer.videosData);

  // const [post3] = useState(PostData3);

  return (
    <View style={{ left: 5, marginTop: -20, height: "100%" }}>
      <FlatList
        data={videos}
        // horizontal={true}
        contentContainerStyle={{
          // paddingLeft: 25,

          // paddingRight: 0,
          paddingTop: 50,
        }}
        keyExtractor={(item, index) => item.id}
        renderItem={({ item, index }) => (
          <HomeItem1
            image={item?.backBlazeVideoUrl ? item?.backBlazeVideoUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
            // title={item.title}
            time={item?.Duration}

            // description={item.description}
            onPress={() => navigation.navigate('HomeDetail')}>
            {/* <ProfileAuthor
          image={item.authorImage}
          name={item.name}
          description={item.detail}
          style={{ paddingHorizontal: 10, top: 250 }}
        /> */}
          </HomeItem1>
        )}
      />
    </View>
  );
}

/**
 * @description Show when tab Setting activated
 * @author Passion UI <passionui.com>
            * @date 2019-08-03
            * @class PreviewTab
            * @extends {Component}
            */
function SettingTab({ navigation }) {
  const { colors } = useTheme();
  const [refreshing] = useState(false)
  const feedData = useSelector(state => state.FeedsReducer.feedsData)
  const [posts] = useState(PostData2);
  return (
    <FlatList
      refreshControl={
        <RefreshControl
          colors={[colors.primary]}
          tintColor={colors.primary}
          refreshing={refreshing}
          onRefresh={() => { }}
        />
      }
      data={feedData}
      keyExtractor={(item, index) => item.id}
      renderItem={({ item, index }) => (
        <PostItem
          image={item?.backBlazeImageUrl? item?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
          title={item?.name}
          description={item?.feedDesc}
          feedDataObject={item}>
          <ProfileAuthor5
            image={item?.feedPostedBy?.image?.url ? 'http://3.111.30.249:1337' + item?.feedPostedBy?.image?.url : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
            title={item?.feedPostedBy?.username}
            style={{ paddingHorizontal: Utils.scaleWithPixel(20) }}
          />
        </PostItem>
      )}
    />
  );
}

/**
 * @description Show when tab Activity activated
 * @author Passion UI <passionui.com>
              * @date 2019-08-03
              * @class PreviewTab
              * @extends {Component}
              */
function ActivityTab({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const forceDark = useSelector(state => state.application.force_dark);
  const font = useSelector(state => state.application.font);

  const [loading, setLoading] = useState(false);

  const onLogOut = () => {
    setLoading(true);
    dispatch(setAccessToken(null));
    dispatch(AuthActions.authentication(false, response => { }));
  };


  const darkOption = forceDark
    ? t('always_on')
    : forceDark != null
      ? t('always_off')
      : t('dynamic_system');
  return (
    <ScrollView>
      <View style={{ padding: 20, top: -20 }}>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => {
            navigation.navigate('SelectDarkOption');
          }}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>{t('dark_theme')}</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text body1 grayColor style={{ fontFamily: "ProximaNova" }}>
              {darkOption}
            </Text>
            <Icon
              name="angle-right"
              size={18}
              color={colors.primary}
              style={{ marginLeft: 5 }}
              enableRTL={true}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => {
            navigation.navigate('OnboardingScreenOneProfileDetails');
          }}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>{t('edit_profile')}</Text>
          <Icon
            name="angle-right"
            size={18}
            color={colors.primary}
            style={{ marginLeft: 5 }}
            enableRTL={true}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => {
            navigation.navigate('ChangePassword');
          }}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>{t('change_password')}</Text>
          <Icon
            name="angle-right"
            size={18}
            color={colors.primary}
            style={{ marginLeft: 5 }}
            enableRTL={true}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => {
            navigation.navigate('ChangeLanguage');
          }}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>{t('change_language')}</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text body1 grayColor style={{ fontFamily: "ProximaNova" }}>
              English
            </Text>
            <Icon
              name="angle-right"
              size={18}
              color={colors.primary}
              style={{ marginLeft: 5 }}
              enableRTL={true}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.profileItem,
            { borderBottomColor: colors.border, borderBottomWidth: 1 },
          ]}
          onPress={() => navigation.navigate('SelectFontOption')}>
          <Text body1 style={{ fontFamily: "ProximaNova" }}>{t('font')}</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text body1 grayColor style={{ fontFamily: "ProximaNova" }}>
              {font ?? t('default')}
            </Text>
            <Icon
              name="angle-right"
              size={18}
              color={colors.primary}
              style={{ marginLeft: 5 }}
              enableRTL={true}
            />
          </View>
        </TouchableOpacity>
        <View style={{ paddingHorizontal: 20, paddingVertical: 15, left: 40 }}>
          <NeomorphBlur
            style={{
              shadowRadius: 3,
              borderRadius: 100,
              backgroundColor: colors.neoThemebg,
              width: 230,
              height: 56,

              justifyContent: 'center',
              alignItems: 'center',

            }}
          >
            <Button full loading={loading} style={{ backgroundColor: colors.neoThemebg, width: 215, height: 45, borderRadius: 20 }} onPress={() => onLogOut()}>
              {t('sign_out')}
            </Button>
          </NeomorphBlur>
        </View>
      </View>
    </ScrollView>
  )
}
