import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import { alignItems } from 'styled-system';
import * as Utils from '@utils';

export default StyleSheet.create({
  location: {
    flexDirection: 'row',
    marginTop: Utils.scaleWithPixel(10),
    alignItems: 'center',
  },
  contentInfor: {paddingTop:30,top:-20,right:30,marginBottom:-30},
  contentInforLeft: {
    flex: 1,
    paddingLeft: Utils.scaleWithPixel(10),
  },
  itemInfor: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  tabbar: {
    height: 40,
    marginTop:25
    
  },
  tab: {
    width: 95,
    paddingLeft:0
    
  },
  indicator: {
    height: 1,
    width:65,
   left:20
  },
  label: {
    fontWeight: '400',
  },
  containProfileItem: {
    paddingLeft: Utils.scaleWithPixel(20),
    paddingRight: Utils.scaleWithPixel(20),
  },
  profileItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: Utils.scaleWithPixel(20),
    paddingTop: Utils.scaleWithPixel(20),
  },
  image: {
    flex: 1,
    justifyContent: "center",
    height:365,
  },
  profileItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
  themeIcon: {
    width: 16,
    height: 16,
  },
});
