import React, { useState, useRef, useCallback, useEffect } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { BaseStyle, Images, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Image, Text, TextInput, ProfileAuthor6 } from '@components';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
// import { GiftedChat } from 'react-native-gifted-chat'
import { useDispatch, useSelector } from 'react-redux';
import { commentsGetRequest, commentsPostRequest } from '../../api/comments';


export default function UserFollowers({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();

  const [input, setInput] = useState('');
  const [Loading, setLoading] = useState();

  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const userId = UserData?.user?.id;
  const followers = UserData?.user?.followersProfiles


  const saveComment = async () => {
    console.log("commentPressed");
    if (!input) {
      alert('please enter Comment')
    }
    else {
      const postType = "feed"
      setLoading(true);
      // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
      await dispatch(commentsPostRequest(input, feedId, userId, postType));

    }

  };

  return (
    <View style={{ margin: 10,flex:3 }}>
      <Header
       title={t('Followers')}
       renderLeft={() => {
         return <Icon name="chevron-left" size={28} color={colors.text} />;
       }}
       onPressLeft={() => {
         navigation.goBack();
       }}
     />
      <ScrollView vertical={true}>
        <FlatList
          data={followers}
          style={{margin:10}}
          vertical={true}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) => (
            <ProfileAuthor6
              image={item?.backBlazeImageUrl ? item?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
              title={item?.username}
            //   description={item?.message}
            //   time={commentTime}
              style={{ top: 0 }}
            />
          )}
        />
      </ScrollView>
      {/* <View style={{ flex: 3,justifyContent:"flex-end" }}>
        <Neomorph
          inner
          style={{
            shadowRadius: 7,
            borderRadius: 20,
            backgroundColor: colors.neoThemebg,
            width: 370,
            height: 70,
           
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <TextInput
              onChangeText={text => setInput(text)}
              style={{ top: 15, left: 10 }}
              // onSubmitEditing={() => sendMessage()}

              placeholder={t('Add a new comment')}
              value={input}
            />
            <NeomorphBlur
              style={{
                shadowRadius: 3,
                borderRadius: 20,
                backgroundColor: colors.neoThemebg,
                width: 60,
                height: 56,

                justifyContent: 'center',
                alignItems: 'center',
                top: 8,
                left: -70
              }}
            >

              <TouchableOpacity onPress={() => saveComment()} style={{ borderRadius: 15, height: 54, width: 58, justifyContent: "center", backgroundColor: colors.neoThemeMainButtonBorderColor }}>
                <Icon name="message-plus" size={28} color="#DDDDDD" style={{ left: 15 }} />
              </TouchableOpacity>

            </NeomorphBlur>
          </View>
        </Neomorph>
      </View> */}
    </View>
  )
}
