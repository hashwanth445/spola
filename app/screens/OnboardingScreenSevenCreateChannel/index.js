import React, { useState } from 'react';
import { View, ScrollView, KeyboardAvoidingView, Platform, TouchableOpacity,  PermissionsAndroid,
  ActivityIndicator } from 'react-native';
import { signupRequest } from '../../api/signup'
import { BaseStyle, useTheme } from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  TextInput,
} from '@components';
import styles from './styles';
// import { UserData } from '@data';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-crop-picker';
import { uploadFilesPostRequest } from '../../api/uploadApi';
import { channelPostRequest } from '../../api/channel';


export default function OnboardingScreenSevenCreateChannel({ navigation, route }) {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const [image, setImage] = useState(
    'https://www.pngall.com/wp-content/uploads/5/Profile.png',
  );


  // const { MobileNumber, password } = route.params


  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const [channelName, setChannelName] = useState("");
  const [channelDesc, setChannelDesc] = useState("");
  const [channelImage, setChannelImage] = useState("");




  const userId = UserData?.user?.id;
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  console.log(UserData,userToken,userId,"OnboardingScreenSevenCreateChannel")
  // const [image] = useState(UserData[0].image);
  const [loading, setLoading] = useState(false);

  const confirm = async () => {
    if (!channelName) {
      alert('please enter channelName')
    }

    else {
      setLoading(true);
      await dispatch(channelPostRequest(channelName,channelDesc,channelImage,userToken,userId));

      // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
      navigation.navigate('ThankyouScreen')
      setLoading(false);

    }

  };

  const requestCameraPermission = async () => {
    console.log('requestCameraPermission');

    if (Platform.OS === 'android') {
      try {
        console.log('requestCameraPermission');

        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
        console.log(PermissionsAndroid.RESULTS.GRANTED);

      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    console.log('requestExternalWritePermission');

    if (Platform.OS === 'android') {
      try {
        console.log('requestExternalWritePermission');

        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
        console.log(PermissionsAndroid.RESULTS.GRANTED);

      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const captureImage = async type => {
    console.log('outsideee');

    // let options = {
    //   storageOptions: {
    //     skipBackup: true,
    //     path: 'images',
    //   },
    // };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    console.log('outsideee', isCameraPermitted, isStoragePermitted);

    if (isCameraPermitted && isStoragePermitted) {
      console.log('insideeee');

      ImagePicker.openPicker({
        multiple: true,
      }).then(images => {
        console.log('imgApi');
        toNextPage(images);

      });
    }
  };

 
  const toNextPage = async images => {
    setLoading(true)

    let formData = new FormData();
    setLoading(true);
    formData.append('files', {
      name: images[0].modificationDate,
      type: images[0].mime,
      uri: images[0].path,
    });

    let imgApi = await uploadFilesPostRequest(formData);
    setLoading(false)
    console.log('image api', imgApi);

    setImage(imgApi[0].url);
    console.log('url', imgApi[0].url);

  };


  return (
    <View style={{ flex: 1 }}>
      {/* <Header
        title={t('edit_profile')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
        onPressRight={() => { }}
      /> */}
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <Icon onPress={() => navigation.goBack()}
            name="chevron-left" size={28} color="white" style={{ top: Utils.scaleWithPixel(20), left: Utils.scaleWithPixel(10) }} />
          <ScrollView contentContainerStyle={styles.contain}>
            <View style={{
              marginTop: Utils.scaleWithPixel(110),
            }}>
              <Text style={{ fontSize: 22, color: "#B3B6B7", fontFamily: "ProximaNova", fontWeight: "bold", top: Utils.scaleWithPixel(-90), alignSelf:"center" }}> Create a Channel</Text>
              <Text bold style={{ fontSize: 16, top: Utils.scaleWithPixel(-80), color: "#6a6c6e", alignSelf:"center" }}>To start uploading Your videos </Text>
              <View style={{
                            flexDirection: "column",
                            alignItems: "center"
                        }}>
                            <Neomorph
                                Outer
                                style={{
                                    shadowRadius: Utils.scaleWithPixel(7),
                                    borderRadius: Utils.scaleWithPixel(20),
                                    backgroundColor: colors.neoThemebg,
                                    width: Utils.scaleWithPixel(235),
                                    height: Utils.scaleWithPixel(155),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    marginTop: Utils.scaleWithPixel(-40),
                                    marginBottom: Utils.scaleWithPixel(40)
                                }}
                            >
                                <View>
                                <TouchableOpacity onPress={() => captureImage()} style={{ flexDirection: "row" }}>

                                  
                                    <Image source={{ uri: image ? image : '' }} style={{height:175,width:260,borderRadius:20,alignSelf:"center"}} />
                                    </TouchableOpacity>

                                </View>
                            </Neomorph>
                        </View>
             
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-5), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(40), borderRadius: Utils.scaleWithPixel(10)
                  }}
                  onChangeText={text => setChannelName(text)}
                  placeholder={t('channel Name')}
                  value={channelName}
                />
              </Neomorph>
            </View>
            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-2), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(40), borderRadius: Utils.scaleWithPixel(10)
                  }}
                  onChangeText={text => setChannelDesc(text)}
                  placeholder={t('Channel Description')}
                  value={channelDesc}
                />
              </Neomorph>
            </View>
           

              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 50,
                  backgroundColor: colors.neoThemebg,
                  width: 230,
                  height: 56,
                  top: 50,
                  marginBottom: 50,
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                {!channelName && !channelDesc ? (
                <Text
                  style={{ fontFamily:"ProximaNova",fontSize:22,color:"#3a404b", backgroundColor: colors.neoThemebg, }}
                >
                  {t('confirm')}
                </Text>
                ) : (
                <TouchableOpacity onPress={() => {
                  confirm()
                }}
                >
                  <LinearGradient style={styles.button} colors={['#43D4FF', '#38ABFD', '#2974FA']} >
                    {loading ? <ActivityIndicator size="small" color="#0000ff" />: 
                    <Text semibold style={styles.text}> Save and Next</Text>}
                  </LinearGradient>
                </TouchableOpacity>
                )}
              </NeomorphBlur>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View >
  );
}
