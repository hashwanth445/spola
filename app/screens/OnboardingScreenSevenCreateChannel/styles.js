import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contentTitle: {
    alignItems: 'flex-start',
    width: '100%',
    height: Utils.scaleWithPixel(32),
    justifyContent: 'center',
  },
  contain: {
    alignItems: 'center',
    padding: Utils.scaleWithPixel(20),
  },
  textInput: {
    height: Utils.scaleWithPixel(46),
    backgroundColor: BaseColor.fieldColor,
    borderRadius: Utils.scaleWithPixel(5),
    padding: Utils.scaleWithPixel(10),
    width: '100%',
    color: BaseColor.grayColor,
  },
  thumb: {
    width: Utils.scaleWithPixel(100),
    height: Utils.scaleWithPixel(100),
    borderRadius: Utils.scaleWithPixel(50),
    marginBottom: Utils.scaleWithPixel(20),
  },
  button: {
    height: 45,
    width: 220,
    borderRadius: 12,
    justifyContent: "center"
  },
  text: {
    fontFamily:"ProximaNova",
    fontSize:18,
    color:"white",
    alignSelf:"center",
    fontWeight:'700'
  }
});
