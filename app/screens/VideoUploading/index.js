import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Alert,
  FlatList,
  Dimensions,
  StyleSheet,
} from 'react-native';
import { BaseStyle, BaseColor, useTheme, Images } from '@config';
import { YoutubePlayer, FacebookPlayer } from 'react-native-video-extension';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  BookingTime,
  TextInput,
  Image
} from '@components';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import Modal from 'react-native-modal';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import Video from 'react-native-video';
import ImagePicker from 'react-native-image-crop-picker';
import { PermissionsAndroid } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import VideoPlayer from 'react-native-rn-videoplayer';
import { videosPostRequest, feedsGetRequest } from '../../api/videos';
import { useDispatch, useSelector } from 'react-redux';
import { uploadFilesPostRequest } from '../../api/uploadApi'
import { shortsPostRequest } from '../../api/shorts';
const IS_IOS = Platform.OS === 'ios';
const entryBorderRadius = 8;

export default function VideoUploading({ route, navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  console.log("werwer", route.params);
  const [keyword, setKeyword] = useState('');
  const { result } = route.params;
  const img = result?.video;
  const [hashtags, setHashtags] = useState('')
  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState(route?.params?.images ? route?.params?.images : []);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  // const { data } = route.params;
  const userData = useSelector(state => state.accessTokenReducer.userData);
  // setImage(data)
  /**
   * call when action modal
   * @param {*} modal
   */
  //   const openModal = modal => {
  //     setModalVisible(modal);
  //   };

  /**
   * call when on change value
   * @param {*} mode
   * @param {*} value
   */


  const ViewsImage = () => {
    let options = {
      title: 'Video Picker',
      mediaType: 'image/video',
    };
    Alert.alert(
      'Select Your Video',
      'You want to select from you device?',
      [
        {
          text: 'Device',
          onPress: () =>
            launchImageLibrary(options, responce => {
              console.log('Response = ', responce.uri);
              if (responce.didCancel) {
                console.log('User cancelled image picker');
              } else if (responce.error) {
                console.log('ImagePicker Error: ', responce.errorMessage);
              } else {
                console.log(responce, "all 2 data how selected", responce.uri)
                let source = responce.uri;
                setVideoPlay(source)
                console.log(responce.uri, "uri video data")
              }
            }),
        },
        {
          text: 'Not Now',
          onPress: () => null,
        },
      ],
    );
    return true;
  };
  selectImage = () => {
    const options = {
      noData: true
    }
    ImagePicker.launchImageLibrary(options, response => {
      console.log(response.assets, "all response", response.assets.uri)
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        const source = { uri: response.uri }
        console.log(source)
        setImage(source)
      }
    })
  }
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write camera err', err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const captureImage = async (type) => {
    // let options = {
    //   mediaType: type,
    //   maxWidth: 300,
    //   maxHeight: 550,
    //   quality: 1,
    //   videoQuality: 'low',
    //   durationLimit: 30, //Video max duration in seconds
    //   saveToPhotos: true,
    // };
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {

      ImagePicker.openPicker({
        multiple: true
      }).then(images => {
        let imgd = image.concat(images)
        setImage(imgd);
      });
    }
  };
  const toNextPage = async () => {
    let formData = new FormData();
    setLoading(true);
    if (imageData.length > 0) {
      for (let i = 0; i < imageData.length; i++) {
        formData.append('files', { "name": imageData[i].modificationDate, "type": imageData[i].mime, "uri": imageData[i].path });
      };
      let imgApi = await listingsUpdateRequest({ formData });
      setLoading(false);
      navigation.navigate('BasicInformation', { picdata: imageData, picselected: selectedItems, imgApi });
    } else {
      let imgApi = [];
      setLoading(false);
      navigation.navigate('BasicInformation', { picdata: imageData, picselected: selectedItems, imgApi });
    }

  }


  const ImgSelector = ({ item, index }) => (
    <Image source={{ uri: item.path }}
      style={{ width: 300, height: 400, margin: 8, alignItems: 'center' }}
      resizeMode="contain"
    />
  )

  const renderItem = ({ item, index }) => {
    return (
      <ImgSelector item={item} />
    )
  }

  const isCarousel = React.useRef(null)
  const SLIDER_WIDTH = Dimensions.get('window').width + 80
  const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7)

  const CarouselCardItem = ({ item, index }) => {
    console.log("pathh", item)
    return (
      <View style={{
        backgroundColor: 'white',
        borderRadius: 8,
        width: ITEM_WIDTH,
        paddingBottom: 40,
        shadowColor: "#fff",
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
      }} key={index}>
        {item.mime === "video/mp4" ? (
          <YoutubePlayer
            source={{ uri: video ? video : 'video' }}
            // // poster={require('../../assets/images/avata.png')}
            // controls={true}

            // pictureInPicture={PictureInPicture.start()}
            mode="auto-fit"
            aspecRatio="landscape"
            fullscreenOrientation='landscape'
            fullscreenAutorotate={true}
            fullscreen={true}
          />) : (
          <Image
            source={{ uri: item.path }}
            style={{
              width: ITEM_WIDTH,
              height: 300,
            }}
          />)}

        {/* <Text style={styles.header}>{item.title}</Text>
      <Text style={styles.body}>{item.body}</Text> */}
      </View>
    );
  }
  const [success, setSuccess] = useState(true);
  // post method 
  const saveVideo = async () => {
    // debugger
      let formData = new FormData();

      formData.append('files', {
        name: title,
        type: 'mime/mp4',
        uri: result.video,
      });

      let videoApi = await uploadFilesPostRequest(formData);
      console.log('videoApi', videoApi);

      if (videoApi) {
        setLoading(true);
        console.log("userdata", userData)
        let response = await dispatch(
          shortsPostRequest(videoApi, description, userData),
          navigation.navigate('Home')
        );
        console.log('response', response);
        setLoading(false);
      }
  };


  return (

    <View style={{ flex: 1, marginTop: 20 }}>
       <Header
          title={t('Spola details')}
          renderLeft={() => {
            return <Icon name="chevron-left" size={28} color={colors.text} />;
          }}
          onPressLeft={() => {
            navigation.goBack();
          }}
        />
      <Neomorph
        inner
        style={{
          shadowRadius: 7,
          borderRadius: 25,
          backgroundColor: colors.neoThemebg,
          width: 385,
          height: 200,
          justifyContent: 'space-around',
          alignItems: 'center',
          marginLeft: 5,
          marginTop: 30,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <Image
            source={{ uri: img ? img : '' }}
            style={{ height: 170, width: 130, left: 80, borderRadius: 20 }}
          />
          <TextInput
            placeholder='Enter description'
            style={{ alignSelf: "flex-end", left: 100 }}
            value={description}
            onChangeText={description => setDescription(description)}
          />
        </View>
      </Neomorph>
      <View style={{ height: 2.5, width: 380, left: 10, top: 30, backgroundColor: colors.card }}></View>
     <View style={{height:100}}>
       <TextInput
       placeholder="Hashtags"
       value={hashtags}
       onChangeText={hashtags => setHashtags(hashtags)}
       style={{margin:10,marginTop:15}}
       /> 
     </View>
      <View style={{ height: 2.5, width: 380, left: 10, top: 0, backgroundColor: colors.card }}></View>
        <View style={{ marginTop: 10,alignSelf:"center"}}>
          <NeomorphBlur
            style={{
              shadowRadius: 3,
              borderRadius: 100,
              backgroundColor: colors.neoThemebg,
              width: 180,
              height: 56,
              top:20,
              marginBottom: 70,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Button status='success' onPress={() => {
              saveVideo();
            }}>
              Upload Spola
            </Button>
          </NeomorphBlur>

        </View>
    </View>
  );
}

