import React, { useState, useRef, useCallback, useEffect } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import _ from 'lodash';
import { BaseStyle, Images, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Image, Text, TextInput, ProfileAuthor1 } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
// import { GiftedChat } from 'react-native-gifted-chat'
import { useDispatch, useSelector } from 'react-redux';
import { commentsGetRequest, commentsGetRequestForFeed, commentsPostRequest } from '../../api/comments';
import { BottomSheet, Button, ListItem } from 'react-native-elements';


export default function Comments({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(commentsGetRequestForFeed(feedId));

  }, [dispatch])

  const [isVisible, setIsVisible] = useState(false);
  const list = [
    { title: 'Delete',
    containerStyle: { backgroundColor: 'transparent', alignSelf: "center" },
    titleStyle: { color: 'white', alignSelf: "center",fontFamily:"ProximaNova",fontSize:20 }, },
    { title: 'Edit',
    containerStyle: { backgroundColor: 'transparent', alignSelf: "center" },
    titleStyle: { color: 'white', alignSelf: "center",fontFamily:"ProximaNova",fontSize:20 },
  },
    {
      title: 'Cancel',
      containerStyle: { backgroundColor: 'transparent', alignSelf: "center" },
      titleStyle: { color: 'white', alignSelf: "center", fontFamily:"ProximaNova",fontSize:20 },
      onPress: () => setIsVisible(false),
    },
  ];


  const [input, setInput] = useState(false);
  const [Loading, setLoading] = useState();

  const UserData = useSelector(state => state.accessTokenReducer.userData)
  console.log("UserData", UserData);
  const userId = UserData?.user?.id;
  const commentDataFromUser = useSelector(state => state.CommentsReducer.commentsDataForFeed)
  // console.log("commentDataFromUser", commentDataFromUser);
  const { feedDataObject } = route.params

  console.log("feedDataObject", feedDataObject);

  const feedComments = feedDataObject?.comments
  const feedId = feedDataObject?.id

  const userToken = useSelector(state => state.accessTokenReducer.accessToken)

  const [commentId, setCommentId] = useState("")
  useEffect(() => {
    const commented = _.find(commentDataFromUser, function (o) { return o.commentFeed?.id === feedDataObject.id; });
    if (commented) {
      setCommentId(commented.id);
      setInput(true);
    }
  }, []);

  useEffect(() => {
    const commented = _.find(commentDataFromUser, function (o) { return o.commentFeed?.id === feedDataObject.id; });
    if (commented) {
      setCommentId(commented.id);
      setInput(true);
    }
  }, [commentDataFromUser]);
  const saveComment = async () => {
    console.log("commentPressed");
    if (!input) {
      alert('please enter Comment')
      await dispatch(commentsGetRequest())
      await dispatch(commentsGetRequestForFeed(feedId));
    }
    else {
      let objectValue = {};
      objectValue.commentFeed = feedId;
      objectValue.commentedBy = userId;
      objectValue.message = input;
      objectValue.postType = "feed";
      setLoading(true);
      // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
      await dispatch(commentsPostRequest(objectValue));

      await dispatch(commentsGetRequestForFeed(feedId))
    }

  };

  return (
    <ScrollView showsVerticalScrollIndicator={true}>
      <View style={{ margin: 10, flex: 1 }}>
        <Header
          title={t('Comments')}
          renderLeft={() => {
            return <Icon name="chevron-left" size={25} color={colors.text} />;
          }}
          onPressLeft={() => {
            navigation.goBack();
          }}
        />
        <View>
          <FlatList
            data={commentDataFromUser}
            style={{ margin: 10 }}
            vertical={true}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item, index }) => (
              <ProfileAuthor1
                image={item?.commentedBy?.backBlazeImageUrl ? item?.commentedBy?.backBlazeImageUrl : 'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}
                name={item?.commentedBy?.username}
                description={item?.message}
                time={item?.createdAt}
                style={{ top: 0 }}
                onPress={() => setIsVisible(true)}
              />
            )}
          />
        </View>
        
        <BottomSheet modalProps={{}} isVisible={isVisible}>
          {list.map((l, i) => (
            <ListItem
              key={i}
              containerStyle={l.containerStyle}
              onPress={l.onPress}
            >
              <Neomorph
                outer
                style={{
                  shadowRadius: 7,
                  borderRadius: 20,
                  backgroundColor: colors.neoThemebg,
                  width: 100,
                  height: 50,

                }}
              >
                <ListItem.Content>
                  <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
                </ListItem.Content>
              </Neomorph>
            </ListItem>
          ))}
        </BottomSheet>
        <View style={{ flex: 3, justifyContent: "flex-end" }}>
          <Neomorph
            inner
            style={{
              shadowRadius: 7,
              borderRadius: 20,
              backgroundColor: colors.neoThemebg,
              width: 370,
              height: 70,

            }}
          >
            <View style={{ flexDirection: "row" }}>
              <TextInput
                onChangeText={text => setInput(text)}
                style={{ top: 10, left: 10 }}
                // onSubmitEditing={() => sendMessage()}

                placeholder={t('Add a new comment')}
                value={input}
              />
              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 20,
                  backgroundColor: colors.neoThemebg,
                  width: 60,
                  height: 56,

                  justifyContent: 'center',
                  alignItems: 'center',
                  top: 8,
                  left: -70
                }}
              >

                <TouchableOpacity onPress={() => saveComment()} style={{ borderRadius: 15, height: 54, width: 58, justifyContent: "center", backgroundColor: colors.neoThemeMainButtonBorderColor }}>
                  <Icon name="message-plus" size={28} color="#DDDDDD" style={{ left: 15 }} />
                </TouchableOpacity>

              </NeomorphBlur>
            </View>
          </Neomorph>
        </View>
      </View>
    </ScrollView>
  )
}
