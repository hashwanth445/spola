import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: Utils.scaleWithPixel(20),
    flex: 1,
  },
  textInput: {
    height: Utils.scaleWithPixel(46),
    backgroundColor: BaseColor.backgroundColor,
    borderRadius: Utils.scaleWithPixel(5),
    marginTop: Utils.scaleWithPixel(14),
    marginLeft: Utils.scaleWithPixel(15),
    padding:Utils.scaleWithPixel(10),
    width: Utils.scaleWithPixel(370),
    height: Utils.scaleWithPixel(48)
  },
});
