import React, { useState } from 'react';
import { View, KeyboardAvoidingView, Platform } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Button, TextInput, Text } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Neomorph,NeomorphBlur } from 'react-native-neomorph-shadows';
import { BaseColor } from '../../config/theme';
import * as Utils from '@utils';

export default function ForgotPassword({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  const [MobileNumber, setMobileNumber] = useState('');
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState({
    MobileNumber: true,
    password: true,
    address: true,
  });

  /**
   * call when action signup
   *
   */
  const onSignUp = () => {
    if (MobileNumber == '') {
      setSuccess({
        ...success,
        MobileNumber: MobileNumber = '' ? true : false

      });
    } else {
      setLoading(true);
      setTimeout(() => {
        setLoading(false);
        navigation.navigate('SignIn');
      }, 500);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      {/* <Header
        title= "Forgot Password"
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <View style={styles.contain}>
            <View style={{
              marginTop: Utils.scaleWithPixel(50),
            }}>
              <Text style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova-Semibold", fontWeight: "bold", left: Utils.scaleWithPixel(100), top: Utils.scaleWithPixel(-100) }}>Forgot Password</Text>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                     marginLeft: Utils.scaleWithPixel(10),width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(60),
                  }}
                  onChangeText={text => setMobileNumber(text)}
                  onFocus={() => {
                    setSuccess({
                      ...success,
                      MobileNumber: true,
                    });
                  }}
                  placeholder={t('input_id')}
                  success={success.MobileNumber}
                  value={MobileNumber}
                  keyboardType="numeric"
                />
              </Neomorph>
            </View>

            <View style={{
              marginTop: Utils.scaleWithPixel(50),
            }}>
              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 50,
                  backgroundColor: colors.neoThemebg,
                  width: 230,
                  height: 56,

                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                  <Button
                    style={{ backgroundColor: colors.neoThemebg }}
                    full
                    loading={loading}
                    onPress={() => {
                      navigation.navigate('VerifyOTP')
                    }}>
                    Send OTP
                  </Button>
              </NeomorphBlur>
            </View>

          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
}
