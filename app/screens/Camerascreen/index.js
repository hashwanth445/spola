import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Alert,
  Image,
  FlatList,
  Dimensions,
  StyleSheet,
} from 'react-native';
import {BaseStyle, BaseColor, useTheme} from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  BookingTime,
  TextInput,
} from '@components';
import {useTranslation} from 'react-i18next';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import {PermissionsAndroid} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import VideoPlayer from 'react-native-rn-videoplayer';
import {feedsPostRequest, feedsGetRequest} from '../../api/feeds';
import {useDispatch, useSelector} from 'react-redux';

const IS_IOS = Platform.OS === 'ios';
const entryBorderRadius = 8;

export default function Camerascreen({route, navigation}) {
  const Tryvideo = () => {
    ImagePicker.openCamera({
      mediaType: 'video',
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then(image => {
      console.log(image.path);
    });
  };
  return (
    <View>
      {Tryvideo()}
     
    </View>
  );
}
