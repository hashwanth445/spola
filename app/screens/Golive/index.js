import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Alert,
  PermissionsAndroid,
} from 'react-native';
import { NodeCameraView } from 'react-native-nodemediaclient';
import get from 'lodash/get';
import { LIVE_STATUS, videoConfig, audioConfig } from '../../utils/constants';
import SocketManager from './socketManager';
import styles from './styles';
import LiveStreamActionButton from './LiveStreamActionButton';
import ChatInputGroup from '../../components/ChatInputGroup';
import MessagesList from '../../components/MessagesList/MessagesList';
import { RTMP_SERVER } from '../../config';
import Logger from '../../utils/logger';
import FloatingHearts from '../../components/FloatingHearts';

export default class Streamer extends React.Component {
  constructor(props) {
    super(props);
    const { route } = props;
    const roomName = 'yyyy'
    const userName = 'yyyy';
    this.state = {
      currentLiveStatus: LIVE_STATUS.PREPARE,
      messages: [],
      countHeart: 0,
      isVisibleMessages: true,
      userName:'yyyy'
    };
    this.roomName = roomName;
    this.userName = userName;
  }

  componentDidMount() {
    console.log("live pagev ==============================")
    this.requestCameraPermission();
    SocketManager.instance.emitPrepareLiveStream({
      userName: this.userName,
      roomName: this.roomName,
    });
    SocketManager.instance.emitJoinRoom({
      userName: this.userName,
      roomName: this.roomName,
    });
    SocketManager.instance.listenBeginLiveStream((data) => {
      const currentLiveStatus = get(data, 'liveStatus', '');
      this.setState({ currentLiveStatus });
    });
    SocketManager.instance.listenFinishLiveStream((data) => {
      const currentLiveStatus = get(data, 'liveStatus', '');
      this.setState({ currentLiveStatus });
    });
    SocketManager.instance.listenSendHeart(() => {
      this.setState((prevState) => ({ countHeart: prevState.countHeart + 1 }));
    });
    SocketManager.instance.listenSendMessage((data) => {
      const messages = get(data, 'messages', []);
      this.setState({ messages });
    });
  }

  componentWillUnmount() {
    if (this.nodeCameraViewRef) this.nodeCameraViewRef.stop();
    SocketManager.instance.emitLeaveRoom({
      userName: this.userName,
      roomName: this.roomName,
    });
  }

  onPressHeart = () => {
    SocketManager.instance.emitSendHeart({
      roomName: this.roomName,
    });
  };

  onPressSend = (message) => {
    SocketManager.instance.emitSendMessage({
      roomName: this.roomName,
      userName: this.userName,
      message,
    });
    this.setState({ isVisibleMessages: true });
  };

  onEndEditing = () => this.setState({ isVisibleMessages: true });

  onFocusChatGroup = () => {
    this.setState({ isVisibleMessages: false });
  };

  onPressClose = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  onPressLiveStreamButton = () => {
    const { navigation, route } = this.props;
    const userName=this.userName
   const { currentLiveStatus } = this.state;
    console.log("onPressLiveStreamButton----------",Number(currentLiveStatus),Number(LIVE_STATUS.PREPARE),currentLiveStatus,LIVE_STATUS.PREPARE)
    if(currentLiveStatus ===LIVE_STATUS.PREPARE )
    this.setState({currentLiveStatus:LIVE_STATUS.ON_LIVE})
    if(currentLiveStatus ===LIVE_STATUS.ON_LIVE )
    this.setState({currentLiveStatus:LIVE_STATUS.FINISH})
    // if (Number(currentLiveStatus) === Number(LIVE_STATUS.PREPARE)) {
    //   /**
    //    * Waiting live stream
    //    */
    //   SocketManager.instance.emitBeginLiveStream({ userName, roomName: userName });
    //   SocketManager.instance.emitJoinRoom({ userName, roomName: userName });
    //   // console.log("currentlivestatus",currentLiveStatus);
    //   // console.log("user",this.nodeCameraViewRef)
    //   if (this.nodeCameraViewRef) this.nodeCameraViewRef.start();
    // } else if (Number(currentLiveStatus) === Number(LIVE_STATUS.ON_LIVE)) {
    //   /**
    //    * Finish live stream
    //    */
    //   SocketManager.instance.emitFinishLiveStream({ userName, roomName: userName });
    //   if (this.nodeCameraViewRef) this.nodeCameraViewRef.stop();
    //   Alert.alert(
    //     'Alert ',
    //     'Thanks for your live stream',
    //     [
    //       {
    //         text: 'Okay',
    //         onPress: () => {
    //           navigation.goBack();
    //           SocketManager.instance.emitLeaveRoom({ userName, roomName: userName });
    //         },
    //       },
    //     ],
    //     { cancelable: false }
    //   );
    // }
  };

  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        [PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.RECORD_AUDIO],
        {
          title: 'LiveStreamExample need Camera And Microphone Permission',
          message:
            'LiveStreamExample needs access to your camera so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        }
      );
      if (
        granted['android.permission.CAMERA'] === PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.RECORD_AUDIO'] === PermissionsAndroid.RESULTS.GRANTED
      ) {
        if (this.nodeCameraViewRef) this.nodeCameraViewRef.startPreview();
      } else {
        Logger.log('Camera permission denied');
      }
    } catch (err) {
      Logger.warn(err);
    }
  };

  renderChatGroup = () => {
    return (
      <ChatInputGroup
        onPressHeart={this.onPressHeart}
        onPressSend={this.onPressSend}
        onFocus={this.onFocusChatGroup}
        onEndEditing={this.onEndEditing}
      />
    );
  };

  renderListMessages = () => {
    const { messages, isVisibleMessages } = this.state;
    if (!isVisibleMessages) return null;
    return <MessagesList messages={messages} />;
  };

  setCameraRef = (ref) => {
    this.nodeCameraViewRef = ref;
  };

  render() {
    const { route } = this.props;
    const { currentLiveStatus, countHeart } = this.state;
    const userName = get(route, 'params.userName', '');
    // const outputUrl = 'rtmp://3.111.78.117/live/stream';
    const outputUrl = "rtmp://192.168.0.103/live/nSWpTR99Y";
    return (
      <SafeAreaView style={styles.container}>
        <NodeCameraView
          style={styles.streamerView}
          ref={this.setCameraRef}
          outputUrl={outputUrl}
          camera={{ cameraId:1, cameraFrontMirror: true }}
          audio={audioConfig}
          video={videoConfig}
          smoothSkinLevel={3}
          autopreview={false}
        />
        <SafeAreaView style={styles.contentWrapper}>
          <View style={styles.header}>
            <TouchableOpacity style={styles.btnClose} onPress={this.onPressClose}>
              <Image
                style={styles.icoClose}
                source={require('../../assets/camera/close.png')}
                tintColor="white"
              />
            </TouchableOpacity>
            <LiveStreamActionButton
              currentLiveStatus={currentLiveStatus}
              onPress={this.onPressLiveStreamButton}
            />
          </View>
          <View style={styles.center} />
          <View style={styles.footer}>
            <TouchableOpacity>

            </TouchableOpacity>
            {this.renderChatGroup()}
            {this.renderListMessages()}
          </View>
        </SafeAreaView>
        <FloatingHearts count={countHeart} />
      </SafeAreaView>
    );
  }
}

Streamer.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func,
  }),
  route: PropTypes.shape({}),
};

Streamer.defaultProps = {
  navigation: {
    goBack: null,
  },
  route: null,
};
