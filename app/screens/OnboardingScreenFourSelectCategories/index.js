import React, { useState, useEffect } from 'react';
import { View, KeyboardAvoidingView,Image, Platform, TouchableOpacity, FlatList,ActivityIndicator } from 'react-native';
import { BaseStyle, useTheme, Images } from '@config';
import { Header, SafeAreaView, Icon, Button, Text, CategoryCard } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { CategoryData } from '@data';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { BaseColor } from '../../config/theme';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Dots from 'react-native-dots-pagination';
import * as Utils from '@utils';
import { useDispatch, useSelector } from 'react-redux';
import { categoriesGetRequest, creatorsGetRequest } from '../../api/categories'
// import { sendVerifyOtpRequest} from '../../api/signup';
import LinearGradient from 'react-native-linear-gradient';
import { signupRequestPut } from '../../api/signup';


export default function OnboardingScreenFourSelectCategories({ navigation, route }) {
    const dispatch = useDispatch();
    const { colors } = useTheme();
    const { t } = useTranslation();
    const accessToken = useSelector(state => state.accessTokenReducer.accessToken)
    const userdata = useSelector(state => state.signupReducer.signupMessageData);
    // const { image } = route.params;
    const [selectedCat, setSelectedCat] = useState([]);
    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });
    useEffect(() => {
        dispatch(categoriesGetRequest());

    }, [dispatch])
    useEffect(() => {
        dispatch(creatorsGetRequest(accessToken));

    }, [dispatch])
    const creatorsData = useSelector(state => state.CategoriesReducer.creatorsData);
    const categoryFromData = useSelector(state => state.CategoriesReducer.categoriesData);
    const categoriesGetCheckSuccess = useSelector(state => state.CategoriesReducer.categoriesGetCheckSuccess);
    // const [categories] = useState(CategoryData);
    console.log("category",selectedCat, userdata);

    // const [shouldShowGaming, setShouldShowGaming] = useState(true);
    // const [shouldShowFood, setShouldShowFood] = useState(true);
    
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        setSelectedCat(categoryFromData)    
      }, [categoryFromData])
    const selectCategories = async data => {
        console.log('selectedCat 767-------------',selectedCat,data);

        // let selectedCategories = selectedCat;
        // const found = selectedCategories.find(element => element === data);
        // const foundIndex = selectedCategories.findIndex(element => element === data);
        // console.log('selectedCat', selectedCategories);

        // if (found) {
        //     selectedCategories.splice(foundIndex, 1);
        //     setSelectedCat(selectedCategories);
        // } else {
        //     selectedCategories.push({ "_id": data });
        //     setSelectedCat(selectedCategories);
        // }
        setSelectedCat(
            selectedCat.map(item => {
                if (item.id == data.id) {
                    return {
                        ...item,
                        isSelected: !data.isSelected,
                    };
                }else{
                    return {
                        ...item,
                    };
                }
            }),
        );
            // selectedCat.map(item => {
                // if (item.id == data.id) {
                //     item.isSelected = !data.isSelected
                // }
            // }),
            // setSelectedCat(selectedCat)
        console.log('after selection   ',selectedCat)
    };

    const success = async () => {
        let NewArray = []
        setLoading(true);
        selectedCat.map((item) => {
            if(item.isSelected){
                NewArray.push(item)
            }
        }
           
        )
        // await dispatch(signupRequestPut(userId,userToken,selectedCat));
        navigation.navigate('OnboardingScreenFiveFollowCretors', { NewArray });
        setLoading(false);



    };

    const [shouldShowMale, setShouldShowMale] = useState(true);



    return (
        <View style={{ flex: 1 }}>
            {/* <Header
                title="Categories"
                renderLeft={() => {
                    return (
                        <Icon
                            name="arrow-left"
                            size={20}
                            color={colors.primary}
                            enableRTL={true}
                        />
                    );
                }}
                onPressLeft={() => {
                    navigation.goBack();
                }}
            /> */}
              <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
            <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>
                    <View style={styles.contain}>
                        <Text bold style={{ fontSize: 22, color: "#DDDDDD", top: Utils.scaleWithPixel(30) }}>Choose your Interests</Text>
                        <Text bold style={{ fontSize: 16, color: "#6a6c6e", top: Utils.scaleWithPixel(35) }}>Personalize your Feed</Text>
                        <Text bold style={{ fontSize: 18, color: "#008cff", top: Utils.scaleWithPixel(50), }}>Choose upto 5 Categories</Text>
                        <ScrollView vertical={true} showsVerticalScrollIndicator={false}>
                        <View style={{ flexDirection: "column", top: Utils.scaleWithPixel(0), }}>

                        <FlatList
                                    numColumns={3}
                                    contentContainerStyle={{ paddingTop: Utils.scaleWithPixel(70),  marginBottom: Utils.scaleWithPixel(40) }}
                                    data={selectedCat}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={({ item, index }) => (
                                        
                                 item.isSelected ? (
                                     <Neomorph
                                     inner
                                     style={{
                                         shadowRadius: Utils.scaleWithPixel(7),
                                         borderRadius: Utils.scaleWithPixel(10),
                                         backgroundColor: colors.neoThemebg,
                                         width: Utils.scaleWithPixel(65),
                                         height: Utils.scaleWithPixel(65),
                                         justifyContent: 'center',
                                         alignItems: 'center',
                                         margin: Utils.scaleWithPixel(20)
                                     }}
                                 >
                                     <TouchableOpacity
                                         onPress={() => { selectCategories(item) }}>

                                         <Image source={{ uri: item.backBlazeImageUrl ? item.backBlazeImageUrl : ''}}  tintColor="#00c6ff" style={{ top: Utils.scaleWithPixel(0), margin: Utils.scaleWithPixel(5), height: Utils.scaleWithPixel(38), width: Utils.scaleWithPixel(38), }} />
                                     </TouchableOpacity>
                                     <Text bold style={{ fontSize: 10, color: "#6a6c6e", top: Utils.scaleWithPixel(0) }}>{item.name}</Text>

                                 </Neomorph>
                               ) : (
                                <Neomorph
                                    outer
                                    style={{
                                        shadowRadius: Utils.scaleWithPixel(7),
                                         borderRadius: Utils.scaleWithPixel(10),
                                         backgroundColor: colors.neoThemebg,
                                         width: Utils.scaleWithPixel(65),
                                         height: Utils.scaleWithPixel(65),
                                         justifyContent: 'center',
                                         alignItems: 'center',
                                         margin: Utils.scaleWithPixel(20)
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => {
                                            selectCategories(item)
                                            // setShouldShowFemale(true)
                                            // setShouldShowOther(true)
                                            // setgender("Male")
                                        }}>
                                         <Image source={{ uri: item.backBlazeImageUrl ? item.backBlazeImageUrl : ''}}  tintColor="#00c6ff" style={{ top: Utils.scaleWithPixel(0), margin: Utils.scaleWithPixel(5), height: Utils.scaleWithPixel(38), width: Utils.scaleWithPixel(38), }} />
                                    </TouchableOpacity>
                                    <Text bold style={{ fontSize: 10, color: "#6a6c6e", top: Utils.scaleWithPixel(0) }}>{item.name}</Text>

                                </Neomorph>
                               
                              )
                                    )}
                                    />

                           


</View>
                        </ScrollView>
                        <View>
                            <NeomorphBlur
                                style={{
                                    shadowRadius: 3,
                                    borderRadius: 15,
                                    backgroundColor: colors.neoThemebg,
                                    width: 230,
                                    height: 36,
                                    top: 0,
                                    marginBottom: 0,
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => success()}
                                >
                                    <LinearGradient style={styles.button} colors={['#43D4FF', '#38ABFD', '#2974FA']} >
                                        {loading ? <ActivityIndicator size="small" color="#0000ff" /> : 
                                        <Text bold style={styles.text}>Next</Text>}
                                    </LinearGradient>
                                </TouchableOpacity>
                            </NeomorphBlur>

                        </View>
                    </View>
                    <View style={{ marginTop: Utils.scaleWithPixel(10) }}>
                        <Dots length={4} activeColor="#0081ff" active={1} />
                    </View>
                </KeyboardAvoidingView>
            
                
            </SafeAreaView>
        </View>
    );
}

