import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: Utils.scaleWithPixel(10),
    flex:1,
  },
  Icon: {
    
    left:Utils.scaleWithPixel(10),
    // color: "#FFBF00"
  },
  button: {
    height: 45,
    width: 220,
    borderRadius: 12,
    justifyContent: "center"
  },
  text: {
    fontFamily:"ProximaNova",
    fontSize:20,
    color:"white",
    alignSelf:"center",
    fontWeight:'700'
  }
});
