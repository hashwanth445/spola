import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';


export default StyleSheet.create({
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: Utils.scaleWithPixel(0),
    flex: 1,
  },
  Icon: {
    marginTop: Utils.scaleWithPixel(-20),
    color: "#FFBF00"
  },
  image: {
      height: Utils.scaleWithPixel(100),
      width: Utils.scaleWithPixel(165),
      borderRadius: Utils.scaleWithPixel(10),
      marginTop: Utils.scaleWithPixel(5)
  },
  image1: {
      height: Utils.scaleWithPixel(30),
      width: Utils.scaleWithPixel(30),
      borderRadius: Utils.scaleWithPixel(50),
      marginTop: Utils.scaleWithPixel(-15)
  },
  button: {
    height: 45,
    width: 220,
    borderRadius: 12,
    justifyContent: "center"
  },
  text: {
    fontFamily:"ProximaNova",
    fontSize:20,
    color:"white",
    alignSelf:"center",
    fontWeight:'700'
  }
});
