import React, { useState,useEffect } from 'react';
import { View, KeyboardAvoidingView, Platform, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Button, Text, ChannelsCard, ProfileAuthor6 } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { NeomorphBlur } from 'react-native-neomorph-shadows';
import { BaseColor } from '../../config/theme';
// import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Dots from 'react-native-dots-pagination';
import { ChannelsData, UserData1 } from '@data';
import * as Utils from '@utils';
import { useDispatch, useSelector } from 'react-redux';
import { channelGetRequest } from '../../api/channel'
import { signupRequestPutForChannel } from '../../api/signup';
import LinearGradient from 'react-native-linear-gradient';

export default function ThankyouScreen({ navigation,route }) {
    const dispatch = useDispatch();
    const { colors } = useTheme();
    const { t } = useTranslation();
    const userToken =  useSelector(state => state.accessTokenReducer.accessToken);
    const UserData = useSelector(state => state.accessTokenReducer.userData);
    const userId = UserData?.user?.id;
    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });


    // const [channels] = useState(ChannelsData);
    const [user1] = useState(UserData1);

    useEffect(() => {
        dispatch(channelGetRequest());

    }, [dispatch])

    const channels = useSelector(state => state.ChannelReducer.channelData);
    const channelGetCheckSuccess = useSelector(state => state.ChannelReducer.channelGetCheckSuccess);
    // const userToken = useSelector(state => state.accessTokenReducer.accessToken)
    console.log('channels',channels);
    const signupSuccess = useSelector(state => state.signupReducer.signupSuccess)
    const [selectedChannel, setSelectedChannels] = useState([]);
    const [loading, setLoading] = useState(false);
    const { selectedCat} = route.params;
    const { selectedFallowing } = route.params;
    console.log(selectedFallowing,selectedCat, "sammangalll")
    const selectChannel = async data => {
        let selectedChannels = selectedChannel;
        selectedChannels.push({ "_id": data });
        setSelectedChannels(selectedChannels);


    };
    const success = async () => {

        setLoading(true);
        navigation.navigate('Home')

        // navigation.navigate('Home')
        setLoading(false);



    };

   

    return (
        <View style={{ flex: 1, top: Utils.scaleWithPixel(30) }}>
            {/* <Header
                title="Channels"
                renderLeft={() => {
                    return (
                        <Icon
                            name="arrow-left"
                            size={20}
                            color={colors.primary}
                            enableRTL={true}
                        />
                    );
                }}
                onPressLeft={() => {
                    navigation.goBack();
                }}
            /> */}
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>

                    <View style={styles.contain}>
                        <Text bold style={{ fontSize: 20, color: "#DDDDDD", }}>Channels you might like</Text>
                        <Text bold style={{ fontSize: 15, color: "#6a6c6e", marginTop: Utils.scaleWithPixel(10) }}>Follow to stay updated with fresh content</Text>
                        <Text bold style={{ fontSize: 18, color: "#008cff", top: Utils.scaleWithPixel(15), }}>Follow upto 3 Channels</Text>
                        <ScrollView style={{ top: Utils.scaleWithPixel(30) }} vertical={true} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}>
                            <View>
                                <Text>Thank you for Registering with us.Please enjoy</Text>
                            </View>
                        </ScrollView>
                        <View style={{ marginTop: Utils.scaleWithPixel(100), top: Utils.scaleWithPixel(-50) }}>
                        <NeomorphBlur
                                style={{
                                    shadowRadius: 3,
                                    borderRadius: 15,
                                    backgroundColor: colors.neoThemebg,
                                    width: 230,
                                    height: 56,
                                    marginBottom: 0,
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                   <TouchableOpacity
                                    onPress={() => success()}
                                >
                                    <LinearGradient style={styles.button} colors={['#43D4FF', '#38ABFD', '#2974FA']} >
                                        <Text bold style={styles.text}>take me to Home</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </NeomorphBlur>
                        </View>
                    </View>
                    <View style={{ top: Utils.scaleWithPixel(-40), }}>
                        <Dots length={4} activeColor="#0081ff" active={2} />
                    </View>

                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    );
}