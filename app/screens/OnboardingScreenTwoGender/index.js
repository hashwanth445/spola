import React, { useState } from 'react';
import { View, KeyboardAvoidingView, Platform, TouchableOpacity,ActivityIndicator } from 'react-native';
import { BaseStyle, useTheme, Images } from '@config';
import { Header, SafeAreaView, Icon, Button, Text, Image } from '@components';
import styles from './styles';
import { signupRequest } from '../../api/signup';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { useDispatch, useSelector } from 'react-redux';
import { BaseColor } from '../../config/theme';
import { TextInput } from 'react-native-gesture-handler';
import * as Utils from '@utils';
import { signupRequestPut } from '../../api/signup'
import LinearGradient from 'react-native-linear-gradient';


export default function OnboardingScreenTwoGender({ navigation, route }) {
    const { colors } = useTheme();
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });

    const {  firstName, lastName, userId, userToken } = route.params;
    const [loading, setLoading] = useState(false);


    const [shouldShowMale, setShouldShowMale] = useState(true);
    const [shouldShowFemale, setShouldShowFemale] = useState(true);
    const [shouldShowOther, setShouldShowOther] = useState(true);
    const [gender, setgender] = useState('');


    // if(shouldShowOther && shouldShowFemale && !shouldShowMale){
    //     setgender("male")
    // }else if(shouldShowOther && !shouldShowFemale && shouldShowMale){
    //     setgender("female")
    // }else{
    //     setgender("other")
    // }

    const signupdone = async () => {

        if (shouldShowMale && shouldShowFemale && shouldShowOther) {
            alert('please select one');
        }

        else {
            setLoading(true);
            // await dispatch(signupRequestPut(firstName, lastName, userId, gender, userToken));
            navigation.navigate('OnboardingScreenThreeProfilePic',{firstName, lastName,gender, userId, userToken })
            setLoading(false);

        }


    }



    return (
        <View style={{ flex: 1 }}>

            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>
                    <Icon onPress={() => navigation.goBack()}
                        name="chevron-left" size={28} color="white" style={{ top: Utils.scaleWithPixel(20), left: Utils.scaleWithPixel(10) }} />
                    <View style={styles.contain}>
                        <View style={{
                            marginTop: Utils.scaleWithPixel(20),
                            flexDirection: "column",
                            alignItems: "center"
                        }}>
                            <Text bold style={{ fontSize: 20, color: "#B3B6B7", padding: Utils.scaleWithPixel(30), fontFamily: "ProximaNova",alignSelf:"center" }}> How do you identify? </Text>
                            {shouldShowMale ? (
                                <Neomorph
                                    outer
                                    style={{
                                        shadowRadius: Utils.scaleWithPixel(7),
                                        borderRadius: Utils.scaleWithPixel(20),
                                        backgroundColor: colors.neoThemebg,
                                        width: Utils.scaleWithPixel(95),
                                        height: Utils.scaleWithPixel(95),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        margin: Utils.scaleWithPixel(20)
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => {
                                            setShouldShowMale(false)
                                            setShouldShowFemale(true)
                                            setShouldShowOther(true)
                                            setgender("Male")
                                        }}>
                                        <Image source={Images.male} style={{ top: Utils.scaleWithPixel(0), margin: Utils.scaleWithPixel(5), height: Utils.scaleWithPixel(38), width: Utils.scaleWithPixel(38) }} />
                                        <Text bold style={{ fontSize: 20, color: "#6a6c6e", left: Utils.scaleWithPixel(5), top: Utils.scaleWithPixel(0) }}>Male</Text>
                                    </TouchableOpacity>
                                </Neomorph>) : (<Neomorph
                                    inner
                                    style={{
                                        shadowRadius: Utils.scaleWithPixel(7),
                                        borderRadius: Utils.scaleWithPixel(20),
                                        backgroundColor: colors.neoThemebg,
                                        width: Utils.scaleWithPixel(95),
                                        height: Utils.scaleWithPixel(95),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        margin: Utils.scaleWithPixel(20)
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => { setShouldShowMale(!shouldShowMale) }}>
                                        <Image source={Images.male} tintColor="#00c6ff" style={{ top: Utils.scaleWithPixel(0), margin: Utils.scaleWithPixel(5), height: Utils.scaleWithPixel(38), width: Utils.scaleWithPixel(38), }} />
                                        <Text bold style={{ fontSize: 20, color: "#6a6c6e", left: Utils.scaleWithPixel(5), top: Utils.scaleWithPixel(0) }}>Male</Text>
                                    </TouchableOpacity>
                                </Neomorph>)}

                            {shouldShowFemale ? (
                                <Neomorph
                                    outer
                                    style={{
                                        shadowRadius: Utils.scaleWithPixel(7),
                                        borderRadius: Utils.scaleWithPixel(20),
                                        backgroundColor: colors.neoThemebg,
                                        width: Utils.scaleWithPixel(95),
                                        height: Utils.scaleWithPixel(95),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        margin: Utils.scaleWithPixel(20)
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => {
                                            setShouldShowFemale(false)
                                            setShouldShowOther(true)
                                            setShouldShowMale(true)
                                            setgender("Female")
                                        }}>
                                        <Image source={Images.female} style={{ top: Utils.scaleWithPixel(0), margin: Utils.scaleWithPixel(5), height: Utils.scaleWithPixel(38), width: Utils.scaleWithPixel(38) }} />
                                        <Text bold style={{ fontSize: 20, color: "#6a6c6e", top: Utils.scaleWithPixel(0) }}>Female</Text>
                                    </TouchableOpacity>
                                </Neomorph>) : (<Neomorph
                                    inner
                                    style={{
                                        shadowRadius: Utils.scaleWithPixel(7),
                                        borderRadius: Utils.scaleWithPixel(20),
                                        backgroundColor: colors.neoThemebg,
                                        width: Utils.scaleWithPixel(95),
                                        height: Utils.scaleWithPixel(95),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        margin: Utils.scaleWithPixel(20)
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => { setShouldShowFemale(!shouldShowFemale) }}>
                                        <Image source={Images.female} tintColor="#00c6ff" style={{ top: Utils.scaleWithPixel(0), margin: Utils.scaleWithPixel(5), height: Utils.scaleWithPixel(38), width: Utils.scaleWithPixel(38) }} />
                                        <Text bold style={{ fontSize: 20, color: "#6a6c6e", top: Utils.scaleWithPixel(0) }}>Female</Text>
                                    </TouchableOpacity>
                                </Neomorph>)}

                            {shouldShowOther ? (
                                <Neomorph
                                    outer
                                    style={{
                                        shadowRadius: Utils.scaleWithPixel(7),
                                        borderRadius: Utils.scaleWithPixel(20),
                                        backgroundColor: colors.neoThemebg,
                                        width: Utils.scaleWithPixel(95),
                                        height: Utils.scaleWithPixel(95),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        margin: Utils.scaleWithPixel(20)
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => {
                                            setShouldShowOther(false)
                                            setShouldShowMale(true)
                                            setShouldShowFemale(true)
                                            setgender("Other")
                                        }
                                        }>
                                        <Image source={Images.trans} style={{ top: Utils.scaleWithPixel(0), margin: Utils.scaleWithPixel(5), height: Utils.scaleWithPixel(47), width: Utils.scaleWithPixel(40), }} />
                                        <Text bold style={{ fontSize: 20, color: "#6a6c6e", left: Utils.scaleWithPixel(5), top: Utils.scaleWithPixel(0) }}>Other</Text>
                                    </TouchableOpacity>
                                </Neomorph>) : (<Neomorph
                                    inner
                                    style={{
                                        shadowRadius: Utils.scaleWithPixel(7),
                                        borderRadius: Utils.scaleWithPixel(20),
                                        backgroundColor: colors.neoThemebg,
                                        width: Utils.scaleWithPixel(95),
                                        height: Utils.scaleWithPixel(95),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        margin: Utils.scaleWithPixel(20)
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => setShouldShowOther(!shouldShowOther)}>
                                        <Image source={Images.trans} tintColor="#00c6ff" style={{ top: Utils.scaleWithPixel(0), margin: Utils.scaleWithPixel(5), height: Utils.scaleWithPixel(47), width: Utils.scaleWithPixel(40) }} />
                                        <Text bold style={{ fontSize: 20, color: "#6a6c6e", left: Utils.scaleWithPixel(5), top: Utils.scaleWithPixel(0) }}>Other</Text>
                                    </TouchableOpacity>
                                </Neomorph>)}


                        </View>
                        <View style={{ marginTop: Utils.scaleWithPixel(50) }}>
                            <NeomorphBlur
                                style={{
                                    shadowRadius: 3,
                                    borderRadius: 15,
                                    backgroundColor: colors.neoThemebg,
                                    width: 230,
                                    height: 56,
                                    marginBottom: 50,
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}
                            >
                                {shouldShowMale && shouldShowFemale && shouldShowOther ? (
                                    <Text body1 bold style={{ fontSize: 22, color: "#3a404b" }}>
                                        Save and Next
                                    </Text>
                                ) : (
                                    <TouchableOpacity onPress={() => {
                                        signupdone()
                                    }}>
                                        <LinearGradient style={styles.button} colors={['#43D4FF', '#38ABFD', '#2974FA']} >
                                        {loading ? <ActivityIndicator size="small" color="#0000ff" />: 
                                            <Text bold style={styles.text}> Save and Next</Text>}
                                        </LinearGradient>
                                    </TouchableOpacity>
                                )}
                                {/*  onPress={() => navigation.navigate('OnboardingScreenThreeProfilePic')}> */}
                            </NeomorphBlur>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    );
}