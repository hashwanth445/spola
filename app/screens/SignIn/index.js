import React, { useState,useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AuthActions } from '@actions';
import messaging from '@react-native-firebase/messaging';
import {
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { BaseStyle, useTheme, Images } from '@config';
import { Header, SafeAreaView, Icon, Text, Button, TextInput, Image } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import { loginRequest } from '../../api/login';
import LinearGradient from 'react-native-linear-gradient';
import DeviceInfo from 'react-native-device-info';
import { getUniqueId, getManufacturer } from 'react-native-device-info';
import { devicePostRequest } from '../../api/device';

export default function SignIn({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  useEffect(() => {
    messaging()
      .getToken()
      .then(token => {
        setToken(token);
        console.log("token",token)
      });
  }, [])
const [token, setToken] = useState('');
console.log(token)

  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [secure, setSecure] = useState(true);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [success, setSuccess] = useState({ id: true, password: true });
  const accessToken = useSelector(state => state.accessTokenReducer.accessToken);
  const loginLoading = useSelector(state => state.loginReducer.loginLoading);

  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const deviceId = DeviceInfo.getUniqueId();
  console.log("1", deviceId);
  const deviceName = DeviceInfo.getModel();
  console.log("2", deviceName);
  const deviceOs = DeviceInfo.getSystemName();
  console.log("3", deviceOs);
  const deviceType = DeviceInfo.getDeviceType();
  console.log("4", deviceType);

  /**
   * call when action login
   *
   */

  const onLogin = async () => {
    // debugger
    if (!phone && !password) {
      setSuccess({
        ...success,
        phone: false,
        password: false,
      });
    } else {
      try {
        setLoading(true);
        await dispatch(loginRequest(phone, password));
        const objectValue = {};
        objectValue.deviceType = deviceType;
        objectValue.deviceOs = deviceOs;
        objectValue.deviceId = deviceId;
        objectValue.deviceToken = token;
        objectValue.user = UserData?.user?.id
        await dispatch(devicePostRequest(objectValue));
        if (accessToken != null) {
          dispatch(
            AuthActions.authentication(true, (response) => {
              setLoading(false);
              navigation.navigate('Home', accessToken);
            }),
          );
        }
        setLoading(false);
      } catch (error) {

        alert(error);
      }
    }
  };



  return (
    <View style={{ flex: 1 }}>
      {/* <Header
        // title={t('sign_in')}
       
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}

      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <Image source={Images.logo} style={{ height: Utils.scaleWithPixel(150), width: Utils.scaleWithPixel(150), alignSelf: "center", top: Utils.scaleWithPixel(0) }} />
          <View style={styles.contain}>
            <View style={{
              marginTop: Utils.scaleWithPixel(0), justifyContent: 'center',
              alignItems: 'center',
            }}>

              <Text style={{ fontSize: 20, color: "#B3B6B7", fontFamily: "ProximaNova", fontWeight: "bold", top: Utils.scaleWithPixel(-30) }}>Sign in</Text>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(60)
                  }}
                  onChangeText={text => setPhone(text)}
                  onFocus={() => {
                    setSuccess({
                      ...success,
                      phone: true,
                    });
                  }}
                  placeholder={t('Phone number')}
                  success={success.phone}
                  value={phone}
                  keyboardType="phone-pad"
                />
              </Neomorph>
            </View>
            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>
              <Neomorph
                inner
                style={{
                  shadowRadius: Utils.scaleWithPixel(7),
                  borderRadius: Utils.scaleWithPixel(15),
                  backgroundColor: colors.neoThemebg,
                  width: Utils.scaleWithPixel(300),
                  height: Utils.scaleWithPixel(50),
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <TextInput
                  style={{
                    marginTop: Utils.scaleWithPixel(-20), marginLeft: Utils.scaleWithPixel(10), width: Utils.scaleWithPixel(310),
                    height: Utils.scaleWithPixel(60)
                  }}
                  onChangeText={text => setPassword(text)}
                  onFocus={() => {
                    setSuccess({
                      ...success,
                      password: true,
                    });
                  }}

                  placeholder={t('password')}
                  // placeholderTextColor="red" 
                  secureTextEntry={secure}
                  success={success.password}
                  value={password}
                />
                <Icon
                  name={secure ? "eye-off-outline" : "eye-outline"}
                  onPress={() => setSecure(!secure)}
                  solid color="white" size={24} style={{ marginLeft: Utils.scaleWithPixel(250), marginTop: Utils.scaleWithPixel(-40), right: Utils.scaleWithPixel(0) }} />
              </Neomorph>
            </View>
            <TouchableOpacity
              style={{ alignSelf: "flex-end", margin: 20 }}
              onPress={() => navigation.navigate('ForgotPassword')}>
              <Text style={{ fontFamily: "ProximaNova-Semibold", color: "#727676", fontSize: 16, }}>
                {t('forgot_your_password')}
              </Text>
            </TouchableOpacity>
            <View style={{
              marginTop: Utils.scaleWithPixel(30),
            }}>

              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 15,
                  backgroundColor: colors.neoThemebg,
                  width: 230,
                  height: 56,
                  marginBottom: 30,
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                {phone.length<10 || password.length<6 ? (

                  <Button
                    style={{ backgroundColor: colors.neoThemebg }}
                    full
                    loading={loading}
                  >
                    {t('sign_in')}
                  </Button>
                ) : (

                  <TouchableOpacity onPress={() => {
                    onLogin();
                  }}>
                    <LinearGradient style={styles.button} colors={['#43D4FF', '#38ABFD', '#2974FA']} >
                      <Text semibold style={styles.text}>Sign In</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                )}

              </NeomorphBlur>

              <TouchableOpacity
                style={{ alignItems: "center" }}
                onPress={() => navigation.navigate('SignUp')}>
                <View style={{ flexDirection: "row", top: Utils.scaleWithPixel(30), }}>
                  <Text body1 style={{ fontFamily: "ProximaNova-Semibold", color: "#6a6c6e" }}>
                    First time here?
                  </Text>
                  <Text body1 style={{ fontFamily: "ProximaNova-Semibold", color: "#0094ff" }}>
                    {t('Sign Up')}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View >
  );
}
