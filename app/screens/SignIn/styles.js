import React from 'react';
import { StyleSheet } from 'react-native';
import { BaseColor } from '@config';
import * as Utils from '@utils';
import { fontWeight } from 'styled-system';

export default StyleSheet.create({
  textInput: {
    height: Utils.scaleWithPixel(46),
    backgroundColor: BaseColor.fieldColor,
    borderRadius: Utils.scaleWithPixel(5),
    marginTop: Utils.scaleWithPixel(10),
    padding: Utils.scaleWithPixel(10),
    width: '100%',
    placeholderTextColor: "red"
  },
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: Utils.scaleWithPixel(20),
    flex: 1,
  },
  button: {
    height: 45,
    width: 220,
    borderRadius: 12,
    justifyContent: "center"
  },
  text: {
    fontFamily:"ProximaNova",
    fontSize:18,
    color:"white",
    alignSelf:"center",
    fontWeight:'700'
  }
});
