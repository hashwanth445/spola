import React, { useState, useRef, useCallback, useEffect } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  Animated,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import _ from 'lodash';
import { BaseStyle, Images, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Image, Text, TextInput, Cart } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
// import { GiftedChat } from 'react-native-gifted-chat'
import { useDispatch, useSelector } from 'react-redux';
import Icon1 from "react-native-vector-icons/SimpleLineIcons";
import Modal from 'react-native-modal';
import { ordersGetRequest, ordersPostRequest } from '../../api/orders';
import { cartsGetRequest, cartsGetRequestForUser, cartsDeleteRequest } from '../../api/carts';
import RazorpayCheckout from 'react-native-razorpay';
var ID = require("nodejs-unique-numeric-id-generator")

export default function Carts({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(cartsGetRequestForUser(userId));

  }, [dispatch])
  const [modalVisible, setModalVisible] = useState(false);
  const openModal = (modal) => {
    setModalVisible(modal);
  };
  const orderId = ID.generate(new Date().toJSON());

  const renderModal = () => {
    return (
      <View>
        <Modal
          isVisible={modalVisible === 'orderPlaced'}
          onSwipeComplete={() => setModalVisible(false)}
          swipeDirection={['down']}
          style={styles.bottomModal}>
          <View style={styles.contain, { flex: 3, backgroundColor: colors.neoThemebg, height: 600, justifyContent: "center", flexDirection: "column" }}>
            {/* <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Icon name='chevron-left' size={28} color={colors.text} style={{marginLeft:20}} />
            </TouchableOpacity> */}
            <Icon1 name="check" size={50} color="#0bb332" style={{ alignSelf: "center" }} />
            <View style={{ left: 10 }}>
              <Text grayColor semibold style={{ fontSize: 20, fontFamily: "ProximaNova", alignSelf: "center", top: Utils.scaleWithPixel(10) }}>Order No: {orderId}</Text>

              <Text bold whiteColor style={{ fontSize: 25, fontFamily: "ProximaNova", alignSelf: "center", marginTop: 20 }}>Order Successfully Placed</Text>

              <Text semibold style={{ textAlign: 'center', letterSpacing: 1, fontSize: 20, color: "#e3dce1", fontFamily: "ProximaNova", alignSelf: "flex-start", marginTop: 20 }}>{t('order_content')}</Text>

            </View>
            <View style={{ flexDirection: "row", alignSelf: "center" }}>
              {carts.map((item, index) => {
                return (
                  <Image
                    key={index}
                    source={{ uri: item?.productId?.backBlazeProductUrl ? item?.productId?.backBlazeProductUrl : '' }}
                    style={[
                      styles.thumb,

                      // index != 0 ? { left: Utils.scaleWithPixel(-20), marginTop: 15 } : {},

                    ]}
                  />
                );
              })}
            </View>
          </View>
          <View style={{ marginTop: -70, marginBottom: 10 }}>
            <TouchableOpacity style={{ alignSelf: "center" }} onPress={() => navigation.navigate('Orders')}>
              <NeomorphBlur
                style={{
                  shadowRadius: 3,
                  borderRadius: 100,
                  backgroundColor: colors.neoThemebg,
                  width: 150,
                  height: 60,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Text style={{ fontSize: 24, color: "#DDDDDD", fontFamily: "ProximaNova", alignSelf: "center" }}>Go to Orders</Text>
              </NeomorphBlur>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    )
  }

  const [isVisible, setIsVisible] = useState(false);

  const [input, setInput] = useState(false);
  const [Loading, setLoading] = useState();


  const UserData = useSelector(state => state.accessTokenReducer.userData)
  console.log("UserData", UserData);
  const userId = UserData?.user?.id;
  const carts = useSelector(state => state.cartsReducer.cartsDataForUser)
  const sumValue = _.sumBy(carts, item => Number(item?.productId?.productPrice));

  const products = [];

  carts?.forEach((i) => {
    products.push({ _id: i?.productId?.id });
  })


  const orderPage = async () => {
    {
      let objectValue = {};
      objectValue.orderId = orderId;
      objectValue.products = products;
      objectValue.totalCost = sumValue;
      objectValue.OrderedBy = userId;
      setLoading(true);
      // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
      await dispatch(ordersPostRequest(objectValue));
      openModal('orderPlaced');

    }

  };

  const payment = () => {
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: 'INR',
      key: 'rzp_test_s2Vw6rrT8MlDYg',
      amount: sumValue,
      name: 'Acme Corp',
      prefill: {
        email: 'gaurav.kumar@example.com',
        contact: '9191919191',
        name: 'Gaurav Kumar'
      },
      theme: { color: '#53a20e' }
    }
    RazorpayCheckout.open(options).then((data) => {
      // handle success
      alert(`Success: ${data.razorpay_payment_id}`);

    }).catch((error) => {
      // handle failure
      alert(`Error: ${error.code} | ${error.description}`);
    });
  }

  const userToken = useSelector(state => state.accessTokenReducer.accessToken)


  return (
    <SafeAreaView>
      <View>
        <ScrollView>
          {renderModal()}
          <View style={{ margin: 10 }}>
            <Header
              title={t('Carts')}
              renderLeft={() => {
                return <Icon name="chevron-left" size={25} color={colors.text} />;
              }}
              onPressLeft={() => {
                navigation.goBack();
              }}
            />
            <View>
              <FlatList
                data={carts}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <View>
                    <Cart
                      image={item?.productId?.backBlazeProductUrl}
                      txtLeftTitle={item?.productId?.productName}
                      // txtContent={item?.message}
                      productObj={item}
                      txtRight={item?.productId?.productPrice}
                      onPress={() => success(item)}
                    />
                  </View>
                )}
              />
            </View>
          </View>
        </ScrollView>
        {carts?.length>0 ? 
        <View style={{ flex: 1, justifyContent: "space-around", flexDirection: "row" }}>
          <NeomorphBlur
            style={{
              shadowRadius: 3,
              borderRadius: 20,
              backgroundColor: colors.neoThemebg,
              width: 150,
              height: 56,

              justifyContent: 'center',
              alignItems: 'center',
              top: 8,
              left: 0
            }}
          >

            <TouchableOpacity>
              <Text style={{ fontFamily: "ProximaNova", fontSize: 16, color: "white" }}>Total Price : {sumValue}</Text>
            </TouchableOpacity>

          </NeomorphBlur>
          <NeomorphBlur
            style={{
              shadowRadius: 3,
              borderRadius: 20,
              backgroundColor: colors.neoThemebg,
              width: 100,
              height: 56,

              justifyContent: 'center',
              alignItems: 'center',
              top: 8,
              left: 0
            }}
          >

            <TouchableOpacity onPress={() => payment() && orderPage()}>
              <Text style={{ fontFamily: "ProximaNova", fontSize: 16, color: "white" }}>Checkout</Text>
            </TouchableOpacity>

          </NeomorphBlur>
        </View> : 
        <View style={{alignContent:"center",flexDirection:"column"}}>
          <Image source={require('../../assets/images2/cart.png')} style={{height:300,width:300,alignSelf:"center"}} />
          <Text bold style={{fontFamily:"ProximaNova", fontSize:24,alignSelf:"center"}}>Your cart is empty!</Text>
        </View>
        }
      </View>
    </SafeAreaView>
  )
}
