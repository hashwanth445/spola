import {StyleSheet} from 'react-native';
import { colors } from 'react-native-elements';
import * as Utils from '@utils';
import {BaseColor} from '@config';

export default StyleSheet.create({
  inputContent: {
    paddingHorizontal: Utils.scaleWithPixel(20),
    paddingVertical: Utils.scaleWithPixel(8),
    alignItems: 'center',
    flexDirection: 'row',
    width:Utils.scaleWithPixel(300),
    marginLeft:Utils.scaleWithPixel(10),
    height:Utils.scaleWithPixel(45),
    marginBottom:Utils.scaleWithPixel(20)
  },
  sendIcon: {
    width: Utils.scaleWithPixel(45),
    height: Utils.scaleWithPixel(45),
    borderRadius: Utils.scaleWithPixel(25),
    alignItems: 'center',
    justifyContent: 'center',
    
    marginLeft:Utils.scaleWithPixel(-30)    
  },
  userContent: {
    paddingVertical: Utils.scaleWithPixel(8),
    paddingHorizontal: Utils.scaleWithPixel(16),
    flexDirection: 'row',
    margin:Utils.scaleWithPixel(10)
  },
  avatar: {
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    borderWidth: Utils.scaleWithPixel(3),
    marginTop: Utils.scaleWithPixel(-14),
    left:-10
  },
  userContentMessage: {
    marginTop: Utils.scaleWithPixel(0),
    padding: Utils.scaleWithPixel(16),
    borderTopRightRadius: Utils.scaleWithPixel(16),
    borderBottomRightRadius: Utils.scaleWithPixel(16),
    borderBottomLeftRadius: Utils.scaleWithPixel(16),
    flex: 1,
    left:Utils.scaleWithPixel(-5)
  },
  userContentDate: {flex: 3, justifyContent: 'center',left:Utils.scaleWithPixel(40),top:Utils.scaleWithPixel(20)},
  meContent: {
    paddingVertical: Utils.scaleWithPixel(8),
    paddingHorizontal: Utils.scaleWithPixel(16),
    flexDirection: 'row',
    justifyContent: 'flex-end',
    margin:Utils.scaleWithPixel(10)
  },
  meContentDate: {
    flex: 3,
    left:Utils.scaleWithPixel(50),
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  meContentMessage: {
    marginTop:Utils.scaleWithPixel(3),
    padding: Utils.scaleWithPixel(15),
    borderTopLeftRadius: Utils.scaleWithPixel(16),
    borderTopRightRadius: Utils.scaleWithPixel(16),
    borderBottomLeftRadius: Utils.scaleWithPixel(16),
    left:Utils.scaleWithPixel(30),
    flex: 1,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin:0,
    
  },
  contain: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    flex: 1,
    marginTop:150,
  },
  imgBanner: {
    width: '100%',
    height: Utils.scaleWithPixel(250),
    position: 'absolute',
  },
  thumb: {
    // borderWidth: 1,
    borderColor: BaseColor.whiteColor,
    width: Utils.scaleWithPixel(80),
    margin:10,
    height: Utils.scaleWithPixel(80),
    borderRadius: Utils.scaleWithPixel(20),
    top:Utils.scaleWithPixel(25),
  },
});
